﻿using System;
using System.Threading;
using System.Net.Sockets;
using ServerData;

namespace Server
{
    class ClientData
    {
        public Socket clientSocket;
        public Thread clientThread;
        public string id;

        public ClientData()
        {
            id = Guid.NewGuid().ToString();
            clientThread = new Thread(Server.Data_IN);
            clientThread.Start(clientSocket);
            SendRegistrationPacketToClient();
        }

        public ClientData(Socket clientSocket)
        {
            this.clientSocket = clientSocket;
            id = Guid.NewGuid().ToString();

            clientThread = new Thread(Server.Data_IN);
            clientThread.Start(clientSocket);
            SendRegistrationPacketToClient();
        }

        public void SendRegistrationPacketToClient()
        {
            Packet p = new Packet(PacketType.ClientConnect, "server");
            p.data.Add(id);
            clientSocket.Send(p.ToBytes());
        }
    }
}
