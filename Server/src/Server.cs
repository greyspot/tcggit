﻿using System.Linq;
using ServerData;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using Server.src.services.Player;
using System.Collections.Generic;
using Server.src.services.Messages;
using Server.src.services.Request;
using Server.src.services.Connection;
using Server.src.services.Database;
using System;

namespace Server
{
    class Server
    {
        private static Socket listenerSocket;
        static List<ClientData> _clients;

        private static ConnectionService conSrv;
        private static ConsoleMessageService consoleMsgSrv;
        private static DatabaseService dbSrv;
        private static PlayerService playerSrv;
        private static RequestService requestSrv;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);

            conSrv = new ConnectionService();
            consoleMsgSrv = new ConsoleMessageService();
            dbSrv = new DatabaseService();
            requestSrv = new RequestService();
            playerSrv = new PlayerService();
            _clients = new List<ClientData>();

            StartServer();
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {

        }

        private static void StartServer()
        {
            listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(Packet.GetIP4Address()), conSrv.GetServerPort());
            listenerSocket.Bind(ip);

            new Thread(ListenThread).Start();
            consoleMsgSrv.WriteConsoleMessage("Started TCG Server..");
            consoleMsgSrv.WriteConsoleMessage("Server Running On: " + Packet.GetIP4Address() + ":" + conSrv.GetServerPort());
        }

        static void ListenThread()
        {
            while (true)
            {
                listenerSocket.Listen(0);
                _clients.Add(new ClientData(listenerSocket.Accept()));
            }
        }

        public static void Data_IN(object cSocket)
        {
            Socket clientSocket = (Socket)cSocket;

            byte[] Buffer;
            int readBytes;

            while (true)
            {
                try
                {
                    Buffer = new byte[clientSocket.SendBufferSize];
                    readBytes = clientSocket.Receive(Buffer);

                    if (readBytes > 0)
                        DataManager(new Packet(Buffer));
                }
                catch (SocketException ex)
                {
                    consoleMsgSrv.WriteConsoleMessage("Crash:\n" + ex.Message);
                }
            }
        }

        public static void DataManager(Packet p)
        {
            switch (p.packetType)
            {
                case PacketType.ClientCommand:
                    consoleMsgSrv.WriteConsoleMessage(p.data[0] + " used command " + p.data[1]);
                    SendMessageToClients(p,_clients);
                    break;

                case PacketType.ClientConnect:
                    consoleMsgSrv.WriteConsoleMessage(p.data[0] + " logged on the server.");
                    playerSrv.AddOnlinePlayer(p.data[0]);
                    break;

                case PacketType.IncomingRequest:
                    requestSrv.SetRequest(p.data[0]);
                    requestSrv.HandleRequest();
                    break;

                case PacketType.ClientChat:
                    consoleMsgSrv.WriteConsoleMessage(p.data[0] + ":" + p.data[1]);
                    SendMessageToClients(p, _clients);

                    if (p.data[1].Equals(" joined the lobby."))
                    {
                        playerSrv.AddPlayerToCasualLobby(p.data[0]);
                        playerSrv.AddClientToCasualLobby(GetClientByID(p));
                    }
                    else if (p.data[1].Equals(" left the lobby."))
                    {
                        playerSrv.RemovePlayerFromCasualLobby(p.data[0]);
                        playerSrv.RemoveClientFromCasualLobby(GetClientByID(p));
                    }

                    Packet packet = new Packet(PacketType.IncomingInfo, "server");
                    packet.data.Add("UPDATE_LOBBY");
                    packet.data.Add(string.Join(",", playerSrv.GetPlayersInCasualLobby()));
                    SendMessageToClients(packet, _clients);
                    break;

                case PacketType.ClientDisconnect:
                    consoleMsgSrv.WriteConsoleMessage(p.data[0] + " logged off the server.");

                    var exitClient = GetClientByID(p);

                    playerSrv.RemoveOnlinePlayer(p.data[0]);

                    if (playerSrv.GetPlayersInCasualLobby().Contains(p.data[0]))
                    {
                        playerSrv.RemovePlayerFromCasualLobby(p.data[0]);
                        playerSrv.RemoveClientFromCasualLobby(GetClientByID(p));

                        Packet rmPacket = new Packet(PacketType.ClientChat, "server");
                        rmPacket.data.Add(p.data[0]);
                        rmPacket.data.Add(" left the lobby.");
                        SendMessageToClients(rmPacket, playerSrv.GetClientsInCasualLobby());
                    }

                    CloseClientConnection(exitClient);
                    RemoveClientFromList(exitClient);
                    AbortClientThread(exitClient);
                    break;
            }
        }

        public static void SendMessageToClients(Packet p, List<ClientData> clients)
        {
            for (int i = 0; i < clients.Count; i++)
                clients[i].clientSocket.Send(p.ToBytes());
        }

        public static void SendMessageToClient(ClientData client, string message)
        {
            Packet p = new Packet(PacketType.IncomingInfo, "server");
            p.data.Add(message);
            client.clientSocket.Send(p.ToBytes());
        }

        private static ClientData GetClientByID(Packet p)
        {
            return (from client in _clients
                    where client.id == p.senderID
                    select client)
                    .FirstOrDefault();
        }

        private static void CloseClientConnection(ClientData c)
        {
            c.clientSocket.Close();
        }

        private static void RemoveClientFromList(ClientData c)
        {
            _clients.Remove(c);
        }
        private static void AbortClientThread(ClientData c)
        {
            c.clientThread.Abort();
        }
    }
}
