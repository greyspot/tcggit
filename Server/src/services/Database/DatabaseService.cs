﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using MySql.Data.MySqlClient;
using System.Data;

namespace Server.src.services.Database
{
    class DatabaseService
    {
        private static readonly MySqlConnection mySqlTCGConnection = new MySqlConnection("host=45.37.80.181;user=admin;password=V9_PfUYP2gqG!cuJTgyh;database=tcg;");
        private static readonly MySqlConnection mySqlForumConnection = new MySqlConnection("host=45.37.80.181;user=admin;password=V9_PfUYP2gqG!cuJTgyh;database=mybb;");
        private static MySqlConnection currentConnection;

        public DatabaseService()
        {
            Messages.ConsoleMessageService.WriteConsoleMessageStatic("Started Database Service..");
        }

        public static MySqlConnection GetMySqlConnection(int connection)
        {
            return GetConnectionFromInt(connection);
        }

        private static MySqlConnection GetConnectionFromInt(int connection)
        {
            if (connection == 1)
                return mySqlTCGConnection;
            else if (connection == 2)
                return mySqlForumConnection;
            return null;
        }

        public static void OpenConnectionIfNotOpen(int con)
        {
            if (currentConnection != null && currentConnection.State != ConnectionState.Closed)
                currentConnection.Close();

            currentConnection = GetConnectionFromInt(con);

            if (currentConnection.State != ConnectionState.Open)
                currentConnection.Open();
        }

        public static void OpenConnectionIfNotOpen(MySqlConnection con)
        {
            if (currentConnection != null && currentConnection.State != ConnectionState.Closed)
                currentConnection.Close();

            currentConnection = con;

            if (currentConnection.State != ConnectionState.Open)
                currentConnection.Open();
        }

        public void ExecuteCommand(string command)
        {
            using (mySqlTCGConnection)
            {
                using (MySqlCommand cmd = new MySqlCommand(command, mySqlTCGConnection))
                {
                    OpenConnectionIfNotOpen(mySqlTCGConnection);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
        }

        public static int GetNumberOfTableRows(string table)
        {
            using (MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) FROM " + table, mySqlTCGConnection))
            {
                if (mySqlTCGConnection.State != ConnectionState.Open)
                    mySqlTCGConnection.Open();

                return System.Convert.ToInt32(cmd.ExecuteScalar());
            }
        }

        public static string GetValue(string table, string valueNeeded, string locateValue, string definiteValue)
        {
            MySqlConnection connection = null;

            if (!table.StartsWith("mybb_"))
                connection = mySqlTCGConnection;
            else
                connection = mySqlForumConnection;

            if (connection.State != ConnectionState.Closed)
                connection.Close();

            OpenConnectionIfNotOpen(connection);

            string returnString = null;
            
            using (connection)
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT " + valueNeeded + " FROM " + table + " WHERE " + locateValue + " = '" + definiteValue.Replace("'", "''") + "'", connection))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    MySqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                        returnString = reader.GetString(valueNeeded);

                    reader.Dispose();
                    cmd.Dispose();
                    connection.Dispose();
                    return returnString;
                }
            }
        }
    }
}
