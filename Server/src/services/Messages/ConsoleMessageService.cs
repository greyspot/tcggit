﻿using System;

namespace Server.src.services.Messages
{
    class ConsoleMessageService
    {
        public ConsoleMessageService()
        {
            WriteConsoleMessage("Started Console Message Service..");
        }

        public void WriteConsoleMessage(string message)
        {
            Console.WriteLine("[" + DateTime.Now + "] " + message);
        }

        public static void WriteConsoleMessageStatic(string message)
        {
            Console.WriteLine("[" + DateTime.Now + "] " + message);
        }

        public void WriteConsoleMessage(string message, string player)
        {
            Console.WriteLine("[" + DateTime.Now + "] " + message.Replace("%20", player));
        }
    }
}
