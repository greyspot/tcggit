﻿using ServerData;

namespace Server.src.services.Request
{
    class Requests
    {
        private ClientData client;

        public Requests()
        {

        }

        private void SetClient(ClientData client)
        {
            this.client = client;
        }

        public void SendLoginCredentialCheck()
        {
            SendPacketToClient(LoginCredentialsIsCorrect().ToString());
        }

        public void SendUserIsBannedCheck()
        {
            SendPacketToClient(UserIsBanned().ToString());
        }

        private bool LoginCredentialsIsCorrect()
        {
            return false;
        }

        private bool UserIsBanned()
        {
            return false;
        }

        public void SendPacketToClient(string message)
        {
            Packet p = new Packet(PacketType.IncomingInfo, "server");
            p.data.Add(message);
            client.clientSocket.Send(p.ToBytes());
        }
    }
}
