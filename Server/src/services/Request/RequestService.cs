﻿namespace Server.src.services.Request
{
    class RequestService
    {
        private string request;

        Requests requests;

        public RequestService()
        {
            requests = new Requests();

            Messages.ConsoleMessageService.WriteConsoleMessageStatic("Started Request Service..");
        }

        public void HandleRequest()
        {
            if (request.Equals(clientRequests[0]))
                requests.SendLoginCredentialCheck();
            else if (request.Equals(clientRequests[1]))
                requests.SendUserIsBannedCheck();
        }

        public void SetRequest(string request)
        {
            this.request = request;
        }

        public string GetRequest()
        {
            return request;
        }

        private readonly string[] clientRequests = {
            "checkLoginCredentials",
            "checkUserBanned",
            "checkServerOffline"
        };
    }
}
