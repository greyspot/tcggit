﻿using System.IO;

namespace Server.src.services.Config
{
    class ConfigService
    {
        private readonly string NETWORK_CFG = "network.cfg";
        private readonly string FEATURES_CFG = "features.cfg";

        private static bool CONSOLE_LOGGING;
        public ConfigService()
        {
            CONSOLE_LOGGING = GetConfigValue(FEATURES_CFG, "CONSOLE_LOGGING").Equals("1");
        }

        public string GetConfigValue(string file, string cfgValue)
        {
            string value;

            using (StreamReader reader = new StreamReader(@".\cfg\" + file))
            {
                while ((value = reader.ReadLine()) != null)
                {
                    if (value.Contains(cfgValue))
                        return value.Replace(cfgValue, string.Empty);
                }
            }
            return value;
        }

        public string GetNetworkCfg()
        {
            return NETWORK_CFG;
        }

        public string GetFeaturesCfg()
        {
            return FEATURES_CFG;
        }
    }
}
