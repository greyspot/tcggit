﻿using System.Collections.Generic;

namespace Server.src.services.Player
{
    class PlayerService
    {
        private List<string> onlinePlayers;
        private List<string> playersInCasualLobby;

        private List<ClientData> clientsInCasualLobby;

        public PlayerService()
        {
            onlinePlayers = new List<string>();
            playersInCasualLobby = new List<string>();
            clientsInCasualLobby = new List<ClientData>();
            Messages.ConsoleMessageService.WriteConsoleMessageStatic("Started Player Service..");
        }

        public List<string> GetOnlinePlayers()
        {
            return onlinePlayers;
        }

        public void SetOnlinePlayers(List<string> onlinePlayers)
        {
            this.onlinePlayers = onlinePlayers;
        }

        public void AddOnlinePlayer(string playerToAdd)
        {
            onlinePlayers.Add(playerToAdd);
        }

        public void RemoveOnlinePlayer(string playerToRemove)
        {
            onlinePlayers.Remove(playerToRemove);
        }
        
        public void AddPlayerToCasualLobby(string player)
        {
            playersInCasualLobby.Add(player);
        }

        public void RemovePlayerFromCasualLobby(string player)
        {
            playersInCasualLobby.Remove(player);
        }

        public List<string> GetPlayersInCasualLobby()
        {
            return playersInCasualLobby;
        }

        public void AddClientToCasualLobby(ClientData client)
        {
            clientsInCasualLobby.Add(client);
        }

        public void RemoveClientFromCasualLobby(ClientData client)
        {
            clientsInCasualLobby.Remove(client);
        }

        public List<ClientData> GetClientsInCasualLobby()
        {
            return clientsInCasualLobby;
        }
    }
}
