﻿using Server.src.services.Config;

namespace Server.src.services.Connection
{
    class ConnectionService
    {
        private readonly int serverPort;

        private readonly string dbAddress;

        private ConfigService cfgSrv;

        public ConnectionService()
        {
            cfgSrv = new ConfigService();

            serverPort = int.Parse(cfgSrv.GetConfigValue(cfgSrv.GetNetworkCfg(), "SERVER_PORT="));
            dbAddress = cfgSrv.GetConfigValue(cfgSrv.GetNetworkCfg(), "DB_ADDRESS=");

            Messages.ConsoleMessageService.WriteConsoleMessageStatic("Started Connection Service..");
        }

        public int GetServerPort()
        {
            return serverPort;
        }

        public string GetDBAddress()
        {
            return dbAddress;
        }
    }
}
