package services.packet;

import services.player.PlayerService;

public class PacketHandler {
	
	private String type;
	private String user;
	private String args;
	
	private PlayerService playerSrv;
	
	public PacketHandler() {
		playerSrv = new PlayerService();
	}
	
	public void handlePacket(String[] decodedPacket) {
		this.type = decodedPacket[0];
		this.user = decodedPacket[1];
		
		if (decodedPacket.length > 2) {
			this.args = decodedPacket[2];
		}
		
		handle();
	}
	
	private void handle() {
		if (type.equals("ClientConnect")) {
			playerSrv.addPlayerToOnlinePlayers(user);
			System.out.println("User Connected: " + user);
		} else if (type.equals("ClientDisconnect")) {
			playerSrv.removePlayerFromOnlinePlayers(user);
			System.out.println("User Disconnected: " + user);
		}
	}
}
