package services.player;

import java.util.ArrayList;
import java.util.List;

public class PlayerService {
	private List<String> onlinePlayers;
	
	public PlayerService() {
		onlinePlayers = new ArrayList<String>();
	}
	
	public List<String> getOnlinePlayers() {
		return onlinePlayers;
	}
	
	public void addPlayerToOnlinePlayers(String player) {
		onlinePlayers.add(player);
	}
	
	public void removePlayerFromOnlinePlayers(String player) {
		onlinePlayers.remove(player);
	}
}
