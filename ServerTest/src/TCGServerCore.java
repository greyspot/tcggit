import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import services.packet.PacketHandler;

public class TCGServerCore {
	
	private final static int SERVER_PORT = 4242;
	
	private static PacketHandler packetHandler;
	
	public static void main(String[] args) throws IOException {
		System.out.println("Starting server..");
		
		packetHandler = new PacketHandler();
		
		startServer();
	}
	
	public static void startServer() throws IOException {
		ServerSocket socket = new ServerSocket(SERVER_PORT);
		System.out.println("Server started..");
	    
	    while (true)
	    {
	        Socket connectionSocket = socket.accept();
	        BufferedReader inputFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
	        System.out.println("Client Connected: " + connectionSocket.getRemoteSocketAddress());
	        DataOutputStream outputStream = new DataOutputStream(connectionSocket.getOutputStream());
	        outputStream.writeBytes("Test");
	        handleIncomingPacket(inputFromClient.readLine());
	    }
	}
	
	private static void handleIncomingPacket(String packet) {
		System.out.println("Incoming Packet: " + packet);
		
		if (packet == null)
			return;
		
		String[] decodedPacket = packet.split(",");
		packetHandler.handlePacket(decodedPacket);
	}
}
