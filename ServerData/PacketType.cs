﻿namespace ServerData
{
    public enum PacketType
    {
        ClientChat,
        ClientCommand,
        ClientConnect,
        ClientDisconnect,
        DatabaseQuery,
        IncomingRequest,
        IncomingInfo
    }
}
