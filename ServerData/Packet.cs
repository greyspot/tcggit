﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ServerData
{
    [Serializable]
    public class Packet
    {
        public List<string> data;
        public int packetInt;
        public bool packetBool;
        public string senderID;
        public PacketType packetType;

        public Packet(PacketType type, string senderID)
        {
            data = new List<string>();
            this.senderID = senderID;
            packetType = type;
        }
        public Packet(byte[] packetBytes)
        {
            Packet p = (Packet)new BinaryFormatter().Deserialize(new MemoryStream(packetBytes));
            data = p.data;
            packetInt = p.packetInt;
            packetBool = p.packetBool;
            senderID = p.senderID;
            packetType = p.packetType;
        }

        public byte[] ToBytes()
        {
            MemoryStream ms = new MemoryStream();

            new BinaryFormatter().Serialize(ms, this);
            byte[] bytes = ms.ToArray();
            ms.Close();

            return bytes;
        }

        public static string GetIP4Address()
        {
            IPAddress[] ips = Dns.GetHostAddresses(Dns.GetHostName());

            return (from ip in ips
                   where ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork
                   select ip)
                   .FirstOrDefault()
                   .ToString();
        }
    }
}
