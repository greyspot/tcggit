# README #

This README contains the steps to download, compile, and run the TCG.

### What does this repository contain? ###

* TCG Client
* TCG Server
* TCG ServerData dll

### How do I get set up? ###

## Downloads

#### Required
* [Visual Studio 2017 RC - Enterprise](https://www.visualstudio.com/vs/visual-studio-2017-rc/)
* [SourceTree](https://www.sourcetreeapp.com/)
* [Client Resources](http://www.mediafire.com/file/bf4s10d7b8ftajj/Resources.zip)

#### Recommended
* [Notepad++](https://notepad-plus-plus.org/)

## Getting the Source
* Fork the original TCG repository.
* Open SourceTree and clone the newly forked repository.
* Unzip the Client Resources into the Resources folder under the Client folder.

## Opening the Source
* Run Visual Studio 2017 RC
* Open Trading Card Game.sln

## Compiling & Running the Source
* Select the *Trading Card Game Client* project at the top.
* Choose the compile option *debug* and *Any CPU*.
* Right click the client and press build in the Solution Exporer. - (An error will appear)
* Right Click on the *ServerData* project and put the ServerData.dll into the client bin directory.
* Run the client again, and it will be successfull.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
