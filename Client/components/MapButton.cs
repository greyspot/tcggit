﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;

namespace TradingCardGame.components
{
    public partial class MapButton : UserControl
    {
        private byte buttonType;

        public MapButton(byte buttonType)
        {
            InitializeComponent();

            this.buttonType = buttonType;
            LoadButtonData();
        }

        private readonly string[] BACKGROUNDS = {
                "tutorials",
                "casual",
                "trade",
                "tournaments"
        };

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void LoadButtonData()
        {
            string[] titles = {
                "Tutorials / Scenarios",
                "Casual Games",
                "Trade",
                "Tournaments"
            };

            string[] descriptions = {
                "Learn to play and hone\nyour skills against the AI.",
                "Challenge other players to\nnon-rated games.",
                "Barter with other players\nto gather the cards you're\nlooking for.",
                "Compete in tournaments\nto increase your rating and\nearn rewards."
            };

            int[] locations = {
                24,
                161,
                299,
                437
            };

            BackgroundImage = (System.Drawing.Image)Images.map_screen.map_screen.ResourceManager.GetObject("button_" + BACKGROUNDS[buttonType]);
            title.Text = titles[buttonType];
            description.Text = descriptions[buttonType];
            Location = new System.Drawing.Point(14, locations[buttonType]);
        }

        private void MapButton_MouseEnter(object sender, EventArgs e)
        {
            BackgroundImage = (System.Drawing.Image)Images.map_screen.map_screen.ResourceManager.GetObject("button_" + BACKGROUNDS[buttonType] + "_over");
        }

        private void MapButton_MouseLeave(object sender, EventArgs e)
        {
            if (!ClientRectangle.Contains(PointToClient(MousePosition)))
                BackgroundImage = (System.Drawing.Image)Images.map_screen.map_screen.ResourceManager.GetObject("button_" + BACKGROUNDS[buttonType]);
        }

        private void MapButton_MouseDown(object sender, MouseEventArgs e)
        {
            BackgroundImage = (System.Drawing.Image)Images.map_screen.map_screen.ResourceManager.GetObject("button_" + BACKGROUNDS[buttonType] + "_down");
        }

        private void MapButton_MouseUp(object sender, MouseEventArgs e)
        {
            BackgroundImage = (System.Drawing.Image)Images.map_screen.map_screen.ResourceManager.GetObject("button_" + BACKGROUNDS[buttonType]);
        }

        private void MapButton_Click(object sender, EventArgs e)
        {
            if (buttonType == 0)
                Parent.Parent.Controls.Add(new forms.dialogs.Tutorials());
            else if (buttonType == 1)
                Parent.Parent.Controls.Add(new forms.dialogs.CasualLobbySelection());
            else if (buttonType == 2)
            {
                services.data.TemporaryData.setLobbyType(1);
                Parent.Parent.Parent.Controls.Add(new forms.Lobby());
                Parent.Parent.Dispose();
            }
            else
                MessageBox.Show("This function currently isn't implemented yet.");
            services.sound.SoundService.PlaySound("button");

        }
    }
}
