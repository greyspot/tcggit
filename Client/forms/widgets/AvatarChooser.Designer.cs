﻿namespace TradingCardGame.forms.widgets
{
    partial class AvatarChooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AvatarChooser));
            this.currentSelectionLabel = new System.Windows.Forms.Label();
            this.selectedAvatarPictureBox = new System.Windows.Forms.PictureBox();
            this.rightPanel = new System.Windows.Forms.Panel();
            this.avatarSelectionPanel = new System.Windows.Forms.Panel();
            this.avatar16PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar15PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar12PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar11PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar14PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar10PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar13PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar9PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar8PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar7PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar6PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar5PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar4PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar3PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar2PictureBox = new System.Windows.Forms.PictureBox();
            this.avatar1PictureBox = new System.Windows.Forms.PictureBox();
            this.leftArrowButton = new System.Windows.Forms.Button();
            this.rightArrowButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.selectedAvatarPictureBox)).BeginInit();
            this.rightPanel.SuspendLayout();
            this.avatarSelectionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.avatar16PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar15PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar12PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar11PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar14PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar10PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar13PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar9PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar8PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar7PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar6PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar5PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar4PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar1PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // currentSelectionLabel
            // 
            this.currentSelectionLabel.AutoSize = true;
            this.currentSelectionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentSelectionLabel.ForeColor = System.Drawing.Color.White;
            this.currentSelectionLabel.Location = new System.Drawing.Point(26, 10);
            this.currentSelectionLabel.Name = "currentSelectionLabel";
            this.currentSelectionLabel.Size = new System.Drawing.Size(154, 13);
            this.currentSelectionLabel.TabIndex = 0;
            this.currentSelectionLabel.Text = "Current Selection: Human Male";
            // 
            // selectedAvatarPictureBox
            // 
            this.selectedAvatarPictureBox.BackColor = System.Drawing.Color.Black;
            this.selectedAvatarPictureBox.Location = new System.Drawing.Point(67, 28);
            this.selectedAvatarPictureBox.Name = "selectedAvatarPictureBox";
            this.selectedAvatarPictureBox.Size = new System.Drawing.Size(70, 70);
            this.selectedAvatarPictureBox.TabIndex = 1;
            this.selectedAvatarPictureBox.TabStop = false;
            // 
            // rightPanel
            // 
            this.rightPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.rightPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rightPanel.Controls.Add(this.currentSelectionLabel);
            this.rightPanel.Controls.Add(this.selectedAvatarPictureBox);
            this.rightPanel.Location = new System.Drawing.Point(314, 9);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(207, 333);
            this.rightPanel.TabIndex = 2;
            // 
            // avatarSelectionPanel
            // 
            this.avatarSelectionPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.avatarSelectionPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.avatarSelectionPanel.Controls.Add(this.avatar16PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar15PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar12PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar11PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar14PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar10PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar13PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar9PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar8PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar7PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar6PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar5PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar4PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar3PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar2PictureBox);
            this.avatarSelectionPanel.Controls.Add(this.avatar1PictureBox);
            this.avatarSelectionPanel.Location = new System.Drawing.Point(12, 9);
            this.avatarSelectionPanel.Name = "avatarSelectionPanel";
            this.avatarSelectionPanel.Size = new System.Drawing.Size(300, 300);
            this.avatarSelectionPanel.TabIndex = 3;
            // 
            // avatar16PictureBox
            // 
            this.avatar16PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar16PictureBox.Location = new System.Drawing.Point(223, 223);
            this.avatar16PictureBox.Name = "avatar16PictureBox";
            this.avatar16PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar16PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar16PictureBox.TabIndex = 17;
            this.avatar16PictureBox.TabStop = false;
            this.avatar16PictureBox.Click += new System.EventHandler(this.avatar16PictureBox_Click);
            // 
            // avatar15PictureBox
            // 
            this.avatar15PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar15PictureBox.Location = new System.Drawing.Point(152, 223);
            this.avatar15PictureBox.Name = "avatar15PictureBox";
            this.avatar15PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar15PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar15PictureBox.TabIndex = 16;
            this.avatar15PictureBox.TabStop = false;
            this.avatar15PictureBox.Click += new System.EventHandler(this.avatar15PictureBox_Click);
            // 
            // avatar12PictureBox
            // 
            this.avatar12PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar12PictureBox.Location = new System.Drawing.Point(223, 152);
            this.avatar12PictureBox.Name = "avatar12PictureBox";
            this.avatar12PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar12PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar12PictureBox.TabIndex = 15;
            this.avatar12PictureBox.TabStop = false;
            this.avatar12PictureBox.Click += new System.EventHandler(this.avatar12PictureBox_Click);
            // 
            // avatar11PictureBox
            // 
            this.avatar11PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar11PictureBox.Location = new System.Drawing.Point(152, 152);
            this.avatar11PictureBox.Name = "avatar11PictureBox";
            this.avatar11PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar11PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar11PictureBox.TabIndex = 14;
            this.avatar11PictureBox.TabStop = false;
            this.avatar11PictureBox.Click += new System.EventHandler(this.avatar11PictureBox_Click);
            // 
            // avatar14PictureBox
            // 
            this.avatar14PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar14PictureBox.Location = new System.Drawing.Point(81, 223);
            this.avatar14PictureBox.Name = "avatar14PictureBox";
            this.avatar14PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar14PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar14PictureBox.TabIndex = 13;
            this.avatar14PictureBox.TabStop = false;
            this.avatar14PictureBox.Click += new System.EventHandler(this.avatar14PictureBox_Click);
            // 
            // avatar10PictureBox
            // 
            this.avatar10PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar10PictureBox.Location = new System.Drawing.Point(81, 152);
            this.avatar10PictureBox.Name = "avatar10PictureBox";
            this.avatar10PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar10PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar10PictureBox.TabIndex = 12;
            this.avatar10PictureBox.TabStop = false;
            this.avatar10PictureBox.Click += new System.EventHandler(this.avatar10PictureBox_Click);
            // 
            // avatar13PictureBox
            // 
            this.avatar13PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar13PictureBox.Location = new System.Drawing.Point(10, 223);
            this.avatar13PictureBox.Name = "avatar13PictureBox";
            this.avatar13PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar13PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar13PictureBox.TabIndex = 11;
            this.avatar13PictureBox.TabStop = false;
            this.avatar13PictureBox.Click += new System.EventHandler(this.avatar13PictureBox_Click);
            // 
            // avatar9PictureBox
            // 
            this.avatar9PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar9PictureBox.Location = new System.Drawing.Point(10, 152);
            this.avatar9PictureBox.Name = "avatar9PictureBox";
            this.avatar9PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar9PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar9PictureBox.TabIndex = 10;
            this.avatar9PictureBox.TabStop = false;
            this.avatar9PictureBox.Click += new System.EventHandler(this.avatar9PictureBox_Click);
            // 
            // avatar8PictureBox
            // 
            this.avatar8PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar8PictureBox.Location = new System.Drawing.Point(223, 81);
            this.avatar8PictureBox.Name = "avatar8PictureBox";
            this.avatar8PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar8PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar8PictureBox.TabIndex = 9;
            this.avatar8PictureBox.TabStop = false;
            this.avatar8PictureBox.Click += new System.EventHandler(this.avatar8PictureBox_Click);
            // 
            // avatar7PictureBox
            // 
            this.avatar7PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar7PictureBox.Location = new System.Drawing.Point(152, 81);
            this.avatar7PictureBox.Name = "avatar7PictureBox";
            this.avatar7PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar7PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar7PictureBox.TabIndex = 8;
            this.avatar7PictureBox.TabStop = false;
            this.avatar7PictureBox.Click += new System.EventHandler(this.avatar7PictureBox_Click);
            // 
            // avatar6PictureBox
            // 
            this.avatar6PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar6PictureBox.Location = new System.Drawing.Point(81, 81);
            this.avatar6PictureBox.Name = "avatar6PictureBox";
            this.avatar6PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar6PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar6PictureBox.TabIndex = 7;
            this.avatar6PictureBox.TabStop = false;
            this.avatar6PictureBox.Click += new System.EventHandler(this.avatar6PictureBox_Click);
            // 
            // avatar5PictureBox
            // 
            this.avatar5PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar5PictureBox.Location = new System.Drawing.Point(10, 81);
            this.avatar5PictureBox.Name = "avatar5PictureBox";
            this.avatar5PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar5PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar5PictureBox.TabIndex = 6;
            this.avatar5PictureBox.TabStop = false;
            this.avatar5PictureBox.Click += new System.EventHandler(this.avatar5PictureBox_Click);
            // 
            // avatar4PictureBox
            // 
            this.avatar4PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar4PictureBox.Location = new System.Drawing.Point(223, 10);
            this.avatar4PictureBox.Name = "avatar4PictureBox";
            this.avatar4PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar4PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar4PictureBox.TabIndex = 5;
            this.avatar4PictureBox.TabStop = false;
            this.avatar4PictureBox.Click += new System.EventHandler(this.avatar4PictureBox_Click);
            // 
            // avatar3PictureBox
            // 
            this.avatar3PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar3PictureBox.Location = new System.Drawing.Point(152, 10);
            this.avatar3PictureBox.Name = "avatar3PictureBox";
            this.avatar3PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar3PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar3PictureBox.TabIndex = 4;
            this.avatar3PictureBox.TabStop = false;
            this.avatar3PictureBox.Click += new System.EventHandler(this.avatar3PictureBox_Click);
            // 
            // avatar2PictureBox
            // 
            this.avatar2PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar2PictureBox.Location = new System.Drawing.Point(81, 10);
            this.avatar2PictureBox.Name = "avatar2PictureBox";
            this.avatar2PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar2PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar2PictureBox.TabIndex = 3;
            this.avatar2PictureBox.TabStop = false;
            this.avatar2PictureBox.Click += new System.EventHandler(this.avatar2PictureBox_Click);
            // 
            // avatar1PictureBox
            // 
            this.avatar1PictureBox.BackColor = System.Drawing.Color.Black;
            this.avatar1PictureBox.Location = new System.Drawing.Point(10, 10);
            this.avatar1PictureBox.Name = "avatar1PictureBox";
            this.avatar1PictureBox.Size = new System.Drawing.Size(65, 65);
            this.avatar1PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatar1PictureBox.TabIndex = 2;
            this.avatar1PictureBox.TabStop = false;
            this.avatar1PictureBox.Click += new System.EventHandler(this.avatar1PictureBox_Click);
            // 
            // leftArrowButton
            // 
            this.leftArrowButton.BackColor = System.Drawing.Color.Black;
            this.leftArrowButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("leftArrowButton.BackgroundImage")));
            this.leftArrowButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.leftArrowButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.leftArrowButton.FlatAppearance.BorderSize = 0;
            this.leftArrowButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.leftArrowButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.leftArrowButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.leftArrowButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftArrowButton.ForeColor = System.Drawing.Color.Black;
            this.leftArrowButton.Location = new System.Drawing.Point(12, 315);
            this.leftArrowButton.Name = "leftArrowButton";
            this.leftArrowButton.Size = new System.Drawing.Size(75, 23);
            this.leftArrowButton.TabIndex = 48;
            this.leftArrowButton.Text = "<<";
            this.leftArrowButton.UseVisualStyleBackColor = false;
            this.leftArrowButton.Click += new System.EventHandler(this.leftArrowButton_Click);
            this.leftArrowButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.leftArrowButton_MouseDown);
            this.leftArrowButton.MouseEnter += new System.EventHandler(this.leftArrowButton_MouseEnter);
            this.leftArrowButton.MouseLeave += new System.EventHandler(this.leftArrowButton_MouseLeave);
            this.leftArrowButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.leftArrowButton_MouseUp);
            // 
            // rightArrowButton
            // 
            this.rightArrowButton.BackColor = System.Drawing.Color.Black;
            this.rightArrowButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rightArrowButton.BackgroundImage")));
            this.rightArrowButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rightArrowButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.rightArrowButton.FlatAppearance.BorderSize = 0;
            this.rightArrowButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.rightArrowButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.rightArrowButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rightArrowButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightArrowButton.ForeColor = System.Drawing.Color.Black;
            this.rightArrowButton.Location = new System.Drawing.Point(93, 315);
            this.rightArrowButton.Name = "rightArrowButton";
            this.rightArrowButton.Size = new System.Drawing.Size(75, 23);
            this.rightArrowButton.TabIndex = 49;
            this.rightArrowButton.Text = ">>";
            this.rightArrowButton.UseVisualStyleBackColor = false;
            this.rightArrowButton.Click += new System.EventHandler(this.rightArrowButton_Click);
            this.rightArrowButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rightArrowButton_MouseDown);
            this.rightArrowButton.MouseEnter += new System.EventHandler(this.rightArrowButton_MouseEnter);
            this.rightArrowButton.MouseLeave += new System.EventHandler(this.rightArrowButton_MouseLeave);
            this.rightArrowButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rightArrowButton_MouseUp);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Black;
            this.closeButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("closeButton.BackgroundImage")));
            this.closeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.closeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.ForeColor = System.Drawing.Color.Black;
            this.closeButton.Location = new System.Drawing.Point(233, 315);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 50;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            this.closeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.closeButton_MouseDown);
            this.closeButton.MouseEnter += new System.EventHandler(this.closeButton_MouseEnter);
            this.closeButton.MouseLeave += new System.EventHandler(this.closeButton_MouseLeave);
            // 
            // AvatarChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(533, 351);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.rightArrowButton);
            this.Controls.Add(this.leftArrowButton);
            this.Controls.Add(this.avatarSelectionPanel);
            this.Controls.Add(this.rightPanel);
            this.DoubleBuffered = true;
            this.Name = "AvatarChooser";
            this.Text = "AvatarChooser";
            this.Load += new System.EventHandler(this.AvatarChooser_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AvatarChooser_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AvatarChooser_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AvatarChooser_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.selectedAvatarPictureBox)).EndInit();
            this.rightPanel.ResumeLayout(false);
            this.rightPanel.PerformLayout();
            this.avatarSelectionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.avatar16PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar15PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar12PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar11PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar14PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar10PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar13PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar9PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar8PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar7PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar6PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar5PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar4PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatar1PictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label currentSelectionLabel;
        private System.Windows.Forms.PictureBox selectedAvatarPictureBox;
        private System.Windows.Forms.Panel rightPanel;
        private System.Windows.Forms.Panel avatarSelectionPanel;
        private System.Windows.Forms.Button leftArrowButton;
        private System.Windows.Forms.Button rightArrowButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.PictureBox avatar1PictureBox;
        private System.Windows.Forms.PictureBox avatar4PictureBox;
        private System.Windows.Forms.PictureBox avatar3PictureBox;
        private System.Windows.Forms.PictureBox avatar2PictureBox;
        private System.Windows.Forms.PictureBox avatar6PictureBox;
        private System.Windows.Forms.PictureBox avatar5PictureBox;
        private System.Windows.Forms.PictureBox avatar8PictureBox;
        private System.Windows.Forms.PictureBox avatar7PictureBox;
        private System.Windows.Forms.PictureBox avatar13PictureBox;
        private System.Windows.Forms.PictureBox avatar9PictureBox;
        private System.Windows.Forms.PictureBox avatar16PictureBox;
        private System.Windows.Forms.PictureBox avatar15PictureBox;
        private System.Windows.Forms.PictureBox avatar12PictureBox;
        private System.Windows.Forms.PictureBox avatar11PictureBox;
        private System.Windows.Forms.PictureBox avatar14PictureBox;
        private System.Windows.Forms.PictureBox avatar10PictureBox;
    }
}