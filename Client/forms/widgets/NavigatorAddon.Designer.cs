﻿namespace TradingCardGame.forms.widgets
{
    partial class NavigatorAddon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuButton4 = new System.Windows.Forms.Panel();
            this.title4 = new System.Windows.Forms.Label();
            this.menuIcon4 = new System.Windows.Forms.Panel();
            this.menuButton3 = new System.Windows.Forms.Panel();
            this.title3 = new System.Windows.Forms.Label();
            this.menuIcon3 = new System.Windows.Forms.Panel();
            this.menuButton2 = new System.Windows.Forms.Panel();
            this.title2 = new System.Windows.Forms.Label();
            this.menuIcon2 = new System.Windows.Forms.Panel();
            this.menuButton1 = new System.Windows.Forms.Panel();
            this.title1 = new System.Windows.Forms.Label();
            this.menuIcon1 = new System.Windows.Forms.Panel();
            this.menuButton4.SuspendLayout();
            this.menuButton3.SuspendLayout();
            this.menuButton2.SuspendLayout();
            this.menuButton1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuButton4
            // 
            this.menuButton4.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.menu_item;
            this.menuButton4.Controls.Add(this.title4);
            this.menuButton4.Controls.Add(this.menuIcon4);
            this.menuButton4.Location = new System.Drawing.Point(3, 265);
            this.menuButton4.Name = "menuButton4";
            this.menuButton4.Size = new System.Drawing.Size(284, 88);
            this.menuButton4.TabIndex = 15;
            this.menuButton4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menuButton4_MouseClick);
            this.menuButton4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuButton4_MouseDown);
            this.menuButton4.MouseEnter += new System.EventHandler(this.menuButton4_MouseEnter);
            this.menuButton4.MouseLeave += new System.EventHandler(this.menuButton4_MouseLeave);
            // 
            // title4
            // 
            this.title4.AutoSize = true;
            this.title4.BackColor = System.Drawing.Color.Transparent;
            this.title4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title4.ForeColor = System.Drawing.Color.White;
            this.title4.Location = new System.Drawing.Point(91, 3);
            this.title4.Name = "title4";
            this.title4.Size = new System.Drawing.Size(44, 16);
            this.title4.TabIndex = 8;
            this.title4.Text = "Title 1";
            // 
            // menuIcon4
            // 
            this.menuIcon4.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.button_image_posted_trade;
            this.menuIcon4.Location = new System.Drawing.Point(7, 6);
            this.menuIcon4.Name = "menuIcon4";
            this.menuIcon4.Size = new System.Drawing.Size(76, 76);
            this.menuIcon4.TabIndex = 7;
            // 
            // menuButton3
            // 
            this.menuButton3.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.menu_item;
            this.menuButton3.Controls.Add(this.title3);
            this.menuButton3.Controls.Add(this.menuIcon3);
            this.menuButton3.Location = new System.Drawing.Point(3, 178);
            this.menuButton3.Name = "menuButton3";
            this.menuButton3.Size = new System.Drawing.Size(284, 88);
            this.menuButton3.TabIndex = 14;
            this.menuButton3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menuButton3_MouseClick);
            this.menuButton3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuButton3_MouseDown);
            this.menuButton3.MouseEnter += new System.EventHandler(this.menuButton3_MouseEnter);
            this.menuButton3.MouseLeave += new System.EventHandler(this.menuButton3_MouseLeave);
            // 
            // title3
            // 
            this.title3.AutoSize = true;
            this.title3.BackColor = System.Drawing.Color.Transparent;
            this.title3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title3.ForeColor = System.Drawing.Color.White;
            this.title3.Location = new System.Drawing.Point(91, 3);
            this.title3.Name = "title3";
            this.title3.Size = new System.Drawing.Size(44, 16);
            this.title3.TabIndex = 6;
            this.title3.Text = "Title 1";
            // 
            // menuIcon3
            // 
            this.menuIcon3.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.button_image_trade;
            this.menuIcon3.Location = new System.Drawing.Point(7, 6);
            this.menuIcon3.Name = "menuIcon3";
            this.menuIcon3.Size = new System.Drawing.Size(76, 76);
            this.menuIcon3.TabIndex = 5;
            // 
            // menuButton2
            // 
            this.menuButton2.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.menu_item;
            this.menuButton2.Controls.Add(this.title2);
            this.menuButton2.Controls.Add(this.menuIcon2);
            this.menuButton2.Location = new System.Drawing.Point(3, 90);
            this.menuButton2.Name = "menuButton2";
            this.menuButton2.Size = new System.Drawing.Size(284, 88);
            this.menuButton2.TabIndex = 13;
            this.menuButton2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menuButton2_MouseClick);
            this.menuButton2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuButton2_MouseDown);
            this.menuButton2.MouseEnter += new System.EventHandler(this.menuButton2_MouseEnter);
            this.menuButton2.MouseLeave += new System.EventHandler(this.menuButton2_MouseLeave);
            // 
            // title2
            // 
            this.title2.AutoSize = true;
            this.title2.BackColor = System.Drawing.Color.Transparent;
            this.title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title2.ForeColor = System.Drawing.Color.White;
            this.title2.Location = new System.Drawing.Point(91, 3);
            this.title2.Name = "title2";
            this.title2.Size = new System.Drawing.Size(44, 16);
            this.title2.TabIndex = 5;
            this.title2.Text = "Title 1";
            // 
            // menuIcon2
            // 
            this.menuIcon2.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.button_image_collection;
            this.menuIcon2.Location = new System.Drawing.Point(7, 6);
            this.menuIcon2.Name = "menuIcon2";
            this.menuIcon2.Size = new System.Drawing.Size(76, 76);
            this.menuIcon2.TabIndex = 4;
            // 
            // menuButton1
            // 
            this.menuButton1.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.menu_item;
            this.menuButton1.Controls.Add(this.title1);
            this.menuButton1.Controls.Add(this.menuIcon1);
            this.menuButton1.Location = new System.Drawing.Point(3, 2);
            this.menuButton1.Name = "menuButton1";
            this.menuButton1.Size = new System.Drawing.Size(284, 88);
            this.menuButton1.TabIndex = 12;
            this.menuButton1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menuButton1_MouseClick);
            this.menuButton1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuButton1_MouseDown);
            this.menuButton1.MouseEnter += new System.EventHandler(this.menuButton1_MouseEnter);
            this.menuButton1.MouseLeave += new System.EventHandler(this.menuButton1_MouseLeave);
            // 
            // title1
            // 
            this.title1.AutoSize = true;
            this.title1.BackColor = System.Drawing.Color.Transparent;
            this.title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title1.ForeColor = System.Drawing.Color.White;
            this.title1.Location = new System.Drawing.Point(89, 2);
            this.title1.Name = "title1";
            this.title1.Size = new System.Drawing.Size(44, 16);
            this.title1.TabIndex = 4;
            this.title1.Text = "Title 1";
            // 
            // menuIcon1
            // 
            this.menuIcon1.BackgroundImage = global::TradingCardGame.Images.navigation.navigation.button_image_deckbuilder;
            this.menuIcon1.Location = new System.Drawing.Point(7, 6);
            this.menuIcon1.Name = "menuIcon1";
            this.menuIcon1.Size = new System.Drawing.Size(76, 76);
            this.menuIcon1.TabIndex = 3;
            // 
            // NavigatorAddon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.menuButton4);
            this.Controls.Add(this.menuButton3);
            this.Controls.Add(this.menuButton2);
            this.Controls.Add(this.menuButton1);
            this.DoubleBuffered = true;
            this.Name = "NavigatorAddon";
            this.Size = new System.Drawing.Size(295, 355);
            this.MouseLeave += new System.EventHandler(this.NavigatorAddon_MouseLeave);
            this.menuButton4.ResumeLayout(false);
            this.menuButton4.PerformLayout();
            this.menuButton3.ResumeLayout(false);
            this.menuButton3.PerformLayout();
            this.menuButton2.ResumeLayout(false);
            this.menuButton2.PerformLayout();
            this.menuButton1.ResumeLayout(false);
            this.menuButton1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel menuButton4;
        private System.Windows.Forms.Label title4;
        private System.Windows.Forms.Panel menuIcon4;
        private System.Windows.Forms.Panel menuButton3;
        private System.Windows.Forms.Label title3;
        private System.Windows.Forms.Panel menuIcon3;
        private System.Windows.Forms.Panel menuButton2;
        private System.Windows.Forms.Label title2;
        private System.Windows.Forms.Panel menuIcon2;
        private System.Windows.Forms.Panel menuButton1;
        private System.Windows.Forms.Label title1;
        private System.Windows.Forms.Panel menuIcon1;
    }
}
