﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TradingCardGame.services.account;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.widgets
{
    public partial class AvatarChooser : UserControl
    {
        public AvatarChooser()
        {
            InitializeComponent();
        }

        Point mouseDownPoint;

        int currentIndex = 1;

        string[] avatars = { "avatar_aqualish_female", "avatar_aqualish_male", "avatar_bith_female", "avatar_bith_male",
                             "avatar_bothan_female", "avatar_bothan_male", "avatar_chiss_female", "avatar_chiss_male",
                             "avatar_devaronian_female", "avatar_devaronian_male", "avatar_duros_female", "avatar_duros_male",
                             "avatar_falleen_female", "avatar_falleen_male", "avatar_human_female", "avatar_human_male",
                             "avatar_ithorian_female", "avatar_ithorian_male", "avatar_mon_calamari_female", "avatar_mon_calamari_male",
                             "avatar_nautolan_female", "avatar_nautolan_male", "avatar_rodian_female", "avatar_rodian_male",
                             "avatar_selonian_female", "avatar_selonian_male", "avatar_sullustan_female", "avatar_sullustan_male",
                             "avatar_trandoshan_female", "avatar_trandoshan_male", "avatar_twilek_female", "avatar_twilek_male",
                             "avatar_wookiee_female", "avatar_wookiee_male", "avatar_zabrak_female", "avatar_zabrak_male" };

        private void closeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void AvatarChooser_Load(object sender, EventArgs e)
        {
            setAvatars();
            string avatarName = services.database.DatabaseService.GetValue("users", "avatar", "username", AccountService.GetUsername());
            selectedAvatarPictureBox.Image = (Image)Images.buttons.avatars.ResourceManager.GetObject(avatarName);
            currentSelectionLabel.Text = "Current Selection: " + Regex.Replace(avatarName.Replace("_", " ").Replace("avatar ", ""), @"(^\w)|(\s\w)", m => m.Value.ToUpper());
            currentSelectionLabel.Left = (rightPanel.Width - currentSelectionLabel.Width) / 2;
        }

        private void setSelectedAvatarName(PictureBox pictureBox)
        {
            currentSelectionLabel.Text = "Current Selection: " + Regex.Replace(getAvatarName(pictureBox).Replace("_", " ").Replace("avatar ", ""), @"(^\w)|(\s\w)", m => m.Value.ToUpper());
            currentSelectionLabel.Left = (rightPanel.Width - currentSelectionLabel.Width) / 2;
        }

        private void rightArrowButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (currentIndex == 3)
                return;

            currentIndex++;
            setAvatars();
        }

        private void leftArrowButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (currentIndex == 1)
                return;

            currentIndex--;
            setAvatars();
        }

        private void setAvatars()
        {
            if (currentIndex == 1)
            {
                for (byte i = 0; i < 16; i++)
                    ((PictureBox)avatarSelectionPanel.Controls["avatar" + (i + 1) + "PictureBox"]).Image = (Image)Images.buttons.avatars.ResourceManager.GetObject(avatars[i]);
            }
            else if (currentIndex == 2)
            {
                for (byte i = 16; i < 32; i++)
                    ((PictureBox)avatarSelectionPanel.Controls["avatar" + (i - 15) + "PictureBox"]).Image = (Image)Images.buttons.avatars.ResourceManager.GetObject(avatars[i]);
            }
            else if (currentIndex == 3)
            {
                for (byte i = 1; i < 5; i++)
                    ((PictureBox)avatarSelectionPanel.Controls["avatar" + i + "PictureBox"]).Image = (Image)Images.buttons.avatars.ResourceManager.GetObject(avatars[i + 31]);

                for (byte i = 5; i < 17; i++)
                    ((PictureBox)avatarSelectionPanel.Controls["avatar" + i + "PictureBox"]).Image = null;
            }
        }

        private void selectedAvatarChanged(PictureBox pictureBox)
        {
            if (pictureBox.Image == null)
                return;

            SoundService.PlaySound("button");
            selectedAvatarPictureBox.Image = pictureBox.Image;
            services.database.DatabaseService.ChangeValue("users", "avatar", getAvatarName(pictureBox), AccountService.GetUsername());
            setSelectedAvatarName(pictureBox);
        }

        private string getAvatarName(PictureBox pictureBox)
        {
            int cardPictureBox = 0;

            for (byte i = 1; i < 17; i++)
            {
                if (((PictureBox)avatarSelectionPanel.Controls["avatar" + i + "PictureBox"]) == pictureBox)
                    cardPictureBox = i - 1;
            }

            if (currentIndex == 1)
                return avatars[cardPictureBox];
            else if (currentIndex == 2)
                return avatars[cardPictureBox + 16];
            else
                return avatars[cardPictureBox + 32];
        }

        private void avatar1PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar1PictureBox);
        }

        private void avatar2PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar2PictureBox);
        }

        private void avatar3PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar3PictureBox);
        }

        private void avatar4PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar4PictureBox);
        }

        private void avatar5PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar5PictureBox);
        }

        private void avatar6PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar6PictureBox);
        }

        private void avatar7PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar7PictureBox);
        }

        private void avatar8PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar8PictureBox);
        }

        private void avatar9PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar9PictureBox);
        }

        private void avatar10PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar10PictureBox);
        }

        private void avatar11PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar11PictureBox);
        }

        private void avatar12PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar12PictureBox);
        }

        private void avatar13PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar13PictureBox);
        }

        private void avatar14PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar14PictureBox);
        }

        private void avatar15PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar15PictureBox);
        }

        private void avatar16PictureBox_Click(object sender, EventArgs e)
        {
            selectedAvatarChanged(avatar16PictureBox);
        }

        private void AvatarChooser_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDownPoint.IsEmpty)
            {
                Form f = sender as Form;
                f.Location = new Point(f.Location.X + (e.X - mouseDownPoint.X), f.Location.Y + (e.Y - mouseDownPoint.Y));
            }
        }

        private void AvatarChooser_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }

        private void AvatarChooser_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownPoint = Point.Empty;
        }

        private void leftArrowButton_MouseEnter(object sender, EventArgs e)
        {
            leftArrowButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void leftArrowButton_MouseDown(object sender, MouseEventArgs e)
        {
            leftArrowButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void leftArrowButton_MouseLeave(object sender, EventArgs e)
        {
            leftArrowButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void leftArrowButton_MouseUp(object sender, MouseEventArgs e)
        {
            leftArrowButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void rightArrowButton_MouseDown(object sender, MouseEventArgs e)
        {
            rightArrowButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void rightArrowButton_MouseEnter(object sender, EventArgs e)
        {
            rightArrowButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void rightArrowButton_MouseLeave(object sender, EventArgs e)
        {
            rightArrowButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void rightArrowButton_MouseUp(object sender, MouseEventArgs e)
        {
            rightArrowButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void closeButton_MouseDown(object sender, MouseEventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void closeButton_MouseEnter(object sender, EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void closeButton_MouseLeave(object sender, EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }
    }
}
