﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Diagnostics;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs;
using TradingCardGame.services.account;
using TradingCardGame.services.data;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.widgets
{
    public partial class NavigatorAddon : UserControl
    {
        public NavigatorAddon()
        {
            InitializeComponent();
        }

        private static int selection;

        public void setPlayOptions()
        {
            menuIcon1.BackgroundImage = Images.navigation.navigation.button_image_casual;
            menuIcon2.BackgroundImage = Images.navigation.navigation.button_image_tournaments;
            menuIcon3.BackgroundImage = Images.navigation.navigation.button_image_tutorials;
            menuButton4.BackgroundImage = null;

            title1.Text = "Casuals";
            title2.Text = "Tournaments";
            title3.Text = "Tutorials";
            title4.Text = string.Empty;

            Height = 272;
            selection = 1;
        }

        public void setCardsOptions()
        {
            menuIcon1.BackgroundImage = Images.navigation.navigation.button_image_deckbuilder;
            menuIcon2.BackgroundImage = Images.navigation.navigation.button_image_collection;
            menuIcon3.BackgroundImage = Images.navigation.navigation.button_image_trade;
            menuIcon4.BackgroundImage = Images.navigation.navigation.button_image_posted_trade;

            title1.Text = "Deck Builder";
            title2.Text = "Collections";
            title3.Text = "Trades";
            title4.Text = "Posted Trades";

            Height = 359;
            selection = 2;
        }

        public void setCommunityOptions()
        {
            menuIcon1.BackgroundImage = Images.navigation.navigation.button_image_friends;
            menuIcon2.BackgroundImage = Images.navigation.navigation.button_image_myguild;
            menuIcon3.BackgroundImage = Images.navigation.navigation.button_image_guilds;
            menuIcon4.BackgroundImage = Images.navigation.navigation.button_image_leaderboards;

            title1.Text = "Friends";
            title2.Text = "My Guild";
            title3.Text = "Guilds";
            title4.Text = "Leaderboards";

            Height = 359;
            selection = 3;
        }

        public void setMiscOptions()
        {
            menuIcon1.BackgroundImage = Images.navigation.navigation.button_image_preferences;
            menuIcon2.BackgroundImage = Images.navigation.navigation.button_image_help;
            menuIcon3.BackgroundImage = Images.navigation.navigation.button_image_eula;
            menuButton4.BackgroundImage = null;

            title1.Text = "Preferences";
            title2.Text = "Help";
            title3.Text = "EULA";
            title4.Text = string.Empty;

            Height = 272;
            selection = 4;
        }

        // button effects & animations

        private void menuButton1_MouseEnter(object sender, EventArgs e)
        {
            menuButton1.BackgroundImage = Images.navigation.navigation.menu_item_over;
        }

        private void menuButton1_MouseLeave(object sender, EventArgs e)
        {
            if (!menuButton1.ClientRectangle.Contains(menuButton1.PointToClient(MousePosition)))
                menuButton1.BackgroundImage = Images.navigation.navigation.menu_item;
        }

        private void menuButton1_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");

            if (selection == 1)
            {
                //casualLobbyPanel.Visible = true;
                ///showCasualLobbyPanel();
            }
            else if (selection == 2)
            {
                DeckBuilder deckBuilder = new DeckBuilder();
                Parent.Parent.Controls.Add(deckBuilder);
                Parent.Dispose();
            }
            else if (selection == 3)
            {
                if (TemporaryData.getDialogStatus())
                    return;

                TemporaryData.changeDialogStatus();
                Friends friends = new Friends();
                Controls.Add(friends);
                friends.BringToFront();
            }
            else if (selection == 4)
            {
                Parent.Controls.Add(new Preferences());
            }
        }

        private void menuButton1_MouseDown(object sender, MouseEventArgs e)
        {
            menuButton1.BackgroundImage = Images.navigation.navigation.menu_item_down;
        }

        private void menuButton2_MouseEnter(object sender, EventArgs e)
        {
            menuButton2.BackgroundImage = Images.navigation.navigation.menu_item_over;
        }

        private void menuButton2_MouseLeave(object sender, EventArgs e)
        {
            if (!menuButton2.ClientRectangle.Contains(menuButton2.PointToClient(MousePosition)))
                menuButton2.BackgroundImage = Images.navigation.navigation.menu_item;
        }

        private void menuButton2_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");

            if (selection == 1)
            {
                //casualLobbyPanel.Visible = true;
                //showCasualLobbyPanel();
            }
            else if (selection == 2)
            {
                // tournaments lobby
            }
            else if (selection == 3)
            {
                if (string.IsNullOrEmpty(services.database.DatabaseService.GetValue("users", "guild", "username", AccountService.GetUsername())))
                {
                    Parent.Controls.Add(new Guild());
                }
                else
                {
                    Parent.Controls.Add(new GuildInfo());
                }
            }
            else if (selection == 4)
            {
                Process.Start("http://swghuttcartel.com/tcg/help");
            }
        }

        private void menuButton2_MouseDown(object sender, MouseEventArgs e)
        {
            menuButton2.BackgroundImage = Images.navigation.navigation.menu_item_down;
        }

        private void menuButton3_MouseEnter(object sender, EventArgs e)
        {
            menuButton3.BackgroundImage = Images.navigation.navigation.menu_item_over;
        }

        private void menuButton3_MouseLeave(object sender, EventArgs e)
        {
            if (!menuButton3.ClientRectangle.Contains(menuButton3.PointToClient(MousePosition)))
                menuButton3.BackgroundImage = Images.navigation.navigation.menu_item;
        }

        private void menuButton3_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");

            if (selection == 1) { }
            //showTutorials();
            else if (selection == 2)
            {
                TemporaryData.setLobbyType(1);
                Parent.Parent.Controls.Add(new Lobby());
                Parent.Dispose();
            }
            else if (selection == 3)
            {
                Parent.Controls.Add(new Guild());
            }
            else if (selection == 4)
            {
                // eula
            }
        }

        private void menuButton3_MouseDown(object sender, MouseEventArgs e)
        {
            menuButton3.BackgroundImage = Images.navigation.navigation.menu_item_down;
        }

        private void menuButton4_MouseEnter(object sender, EventArgs e)
        {
            menuButton4.BackgroundImage = Images.navigation.navigation.menu_item_over;
        }

        private void menuButton4_MouseLeave(object sender, EventArgs e)
        {
            if (!menuButton4.ClientRectangle.Contains(menuButton4.PointToClient(MousePosition)))
                menuButton4.BackgroundImage = Images.navigation.navigation.menu_item;
        }

        private void menuButton4_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");

            if (selection == 1)
            {
                // posted trades
            }
            else if (selection == 2)
            {
                // leaderboards
            }
        }

        private void menuButton4_MouseDown(object sender, MouseEventArgs e)
        {
            menuButton4.BackgroundImage = Images.navigation.navigation.menu_item_down;
        }

        private void NavigatorAddon_MouseLeave(object sender, EventArgs e)
        {
            if (ClientRectangle.Contains(PointToClient(MousePosition)))
                return;

            Dispose();
        }
    }
}
