﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.widgets
{
    public partial class Navigator : UserControl
    {
        public Navigator()
        {
            InitializeComponent();
        }

        UserControl control;

        private bool mouseInside;
        private int navSideBarAnimation = 0;
        private string pictureName;

        private void Navigator_Load(object sender, EventArgs e)
        {
            Location = new Point(-80, (Parent.Height - Height) / 2);
            BackgroundImage = Images.navigation.navigation.nav_dock_glow_01;
            button1.Image = Images.navigation.navigation.nav_home;
            button2.Image = Images.navigation.navigation.nav_play;
            button3.Image = Images.navigation.navigation.nav_cards;
            button4.Image = Images.navigation.navigation.nav_community;
            button5.Image = Images.navigation.navigation.nav_misc;
            button6.Image = Images.navigation.navigation.nav_store_disabled;
            button7.Image = Images.navigation.navigation.nav_exit;
            BringToFront();
        }

        private void Navigator_MouseEnter(object sender, EventArgs e)
        {
            if (mouseInside)
                return;

            mouseInside = true;
            SoundService.PlaySound("sidetab_appear");
            despandTimer.Enabled = false;
            expandTimer.Enabled = true;
        }

        private void Navigator_MouseLeave(object sender, EventArgs e)
        {
            if (ClientRectangle.Contains(PointToClient(MousePosition)))
                return;

            if (control != null)
                control.Dispose();

            mouseInside = false;
            SoundService.PlaySound("sidetab_disappear");
            expandTimer.Enabled = false;
            despandTimer.Enabled = true;
        }

        private void DespandTimer_Tick(object sender, EventArgs e)
        {
            if (Left > -80)
                Left -= 4;
            else
                despandTimer.Enabled = false;
        }

        private void ExpandTimer_Tick(object sender, EventArgs e)
        {
            if (Left < 0)
                Left += 4;
            else
                expandTimer.Enabled = false;
        }

        private void DealWithButtonClick(PictureBox button)
        {
            SoundService.PlaySound("button");

            if (button == button1)
            {
                if (services.data.TemporaryData.getCurrentForm() == services.data.TemporaryData.getFormName(5))
                    return;

                Parent.Parent.Controls.Add(new Map());
                Parent.Dispose();
                return;
            }
            else if (button == button6)
            {
                MessageBox.Show("Sorry, but the store is currently unavailable.", "Store Unavailable");
                return;
            }
            else if (button == button7)
            {
                services.command.CommandService.exitGame();
                return;
            }

            if (control != null)
                control.Dispose();

            NavigatorAddon navAdd = new NavigatorAddon();
            control = navAdd;

            if (button == button2)
            {
                navAdd.setPlayOptions();
            }
            else if (button == button3)
            {
                navAdd.setCardsOptions();
            }
            else if (button == button4)
            {
                navAdd.setCommunityOptions();
            }
            else if (button == button5)
            {
                navAdd.setMiscOptions();
            }

            Parent.Controls.Add(navAdd);
            navAdd.Location = new Point(80, Location.Y + button.Location.Y);
            navAdd.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button7);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.Image = Images.navigation.navigation.nav_home_over;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.Image = Images.navigation.navigation.nav_home;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.Image = Images.navigation.navigation.nav_play_over;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.Image = Images.navigation.navigation.nav_play;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.Image = Images.navigation.navigation.nav_cards_over;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.Image = Images.navigation.navigation.nav_cards;
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            button4.Image = Images.navigation.navigation.nav_community_over;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.Image = Images.navigation.navigation.nav_community;
        }

        private void button5_MouseEnter(object sender, EventArgs e)
        {
            button5.Image = Images.navigation.navigation.nav_misc_over;
        }

        private void button5_MouseLeave(object sender, EventArgs e)
        {
            button5.Image = Images.navigation.navigation.nav_misc;
        }

        private void button6_MouseEnter(object sender, EventArgs e)
        {
            // store
        }

        private void button6_MouseLeave(object sender, EventArgs e)
        {
            // store
        }

        private void button7_MouseEnter(object sender, EventArgs e)
        {
            button7.Image = Images.navigation.navigation.nav_exit_over;
        }

        private void button7_MouseLeave(object sender, EventArgs e)
        {
            button7.Image = Images.navigation.navigation.nav_exit;
        }

        private void animationTimer_Tick(object sender, EventArgs e)
        {
            if (navSideBarAnimation > 60)
                navSideBarAnimation = 1;
            
            if (navSideBarAnimation < 10)
                pictureName = "nav_dock_glow_0";
            else
                pictureName = "nav_dock_glow_";

            BackgroundImage = (Image)Images.navigation.navigation.ResourceManager.GetObject(pictureName + navSideBarAnimation++);
        }
    }
}
