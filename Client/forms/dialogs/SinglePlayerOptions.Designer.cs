﻿namespace TradingCardGame.forms.dialogs
{
    partial class SinglePlayerOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SinglePlayerOptions));
            this.singlePlayerSkirmishButton = new System.Windows.Forms.Panel();
            this.skirmishDescriptionLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.singlePlayerScenariosButton = new System.Windows.Forms.Panel();
            this.scenariosDescriptionLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.singlePlayerTutorialsButton = new System.Windows.Forms.Panel();
            this.tutorialDescriptionLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.closeSinglePlayerWindow = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.selectButton = new System.Windows.Forms.Button();
            this.singlePlayerSkirmishButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.singlePlayerScenariosButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.singlePlayerTutorialsButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeSinglePlayerWindow)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // singlePlayerSkirmishButton
            // 
            this.singlePlayerSkirmishButton.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.singlePlayerSkirmishButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.singlePlayerSkirmishButton.Controls.Add(this.skirmishDescriptionLabel);
            this.singlePlayerSkirmishButton.Controls.Add(this.label12);
            this.singlePlayerSkirmishButton.Controls.Add(this.pictureBox6);
            this.singlePlayerSkirmishButton.Location = new System.Drawing.Point(35, 290);
            this.singlePlayerSkirmishButton.Name = "singlePlayerSkirmishButton";
            this.singlePlayerSkirmishButton.Size = new System.Drawing.Size(385, 105);
            this.singlePlayerSkirmishButton.TabIndex = 15;
            // 
            // skirmishDescriptionLabel
            // 
            this.skirmishDescriptionLabel.AutoSize = true;
            this.skirmishDescriptionLabel.Location = new System.Drawing.Point(141, 47);
            this.skirmishDescriptionLabel.Name = "skirmishDescriptionLabel";
            this.skirmishDescriptionLabel.Size = new System.Drawing.Size(212, 26);
            this.skirmishDescriptionLabel.TabIndex = 2;
            this.skirmishDescriptionLabel.Text = "Test your newest deck designs against four\r\ndifferent computer-controlled opponen" +
    "ts.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(140, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 24);
            this.label12.TabIndex = 1;
            this.label12.Text = "Skirmish";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(1, 2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(133, 100);
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // singlePlayerScenariosButton
            // 
            this.singlePlayerScenariosButton.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.singlePlayerScenariosButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.singlePlayerScenariosButton.Controls.Add(this.scenariosDescriptionLabel);
            this.singlePlayerScenariosButton.Controls.Add(this.label10);
            this.singlePlayerScenariosButton.Controls.Add(this.pictureBox5);
            this.singlePlayerScenariosButton.Location = new System.Drawing.Point(36, 177);
            this.singlePlayerScenariosButton.Name = "singlePlayerScenariosButton";
            this.singlePlayerScenariosButton.Size = new System.Drawing.Size(385, 105);
            this.singlePlayerScenariosButton.TabIndex = 14;
            // 
            // scenariosDescriptionLabel
            // 
            this.scenariosDescriptionLabel.AutoSize = true;
            this.scenariosDescriptionLabel.Location = new System.Drawing.Point(141, 47);
            this.scenariosDescriptionLabel.Name = "scenariosDescriptionLabel";
            this.scenariosDescriptionLabel.Size = new System.Drawing.Size(235, 39);
            this.scenariosDescriptionLabel.TabIndex = 2;
            this.scenariosDescriptionLabel.Text = "Uncover ancient relics for both the Rebl Alliance\r\nand the Empire as you battle t" +
    "hrough a series of\r\ncomputer-controlled opponents.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(140, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 24);
            this.label10.TabIndex = 1;
            this.label10.Text = "Scenarios";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(1, 2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(133, 100);
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // singlePlayerTutorialsButton
            // 
            this.singlePlayerTutorialsButton.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.singlePlayerTutorialsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.singlePlayerTutorialsButton.Controls.Add(this.tutorialDescriptionLabel);
            this.singlePlayerTutorialsButton.Controls.Add(this.label8);
            this.singlePlayerTutorialsButton.Controls.Add(this.pictureBox3);
            this.singlePlayerTutorialsButton.Location = new System.Drawing.Point(36, 64);
            this.singlePlayerTutorialsButton.Name = "singlePlayerTutorialsButton";
            this.singlePlayerTutorialsButton.Size = new System.Drawing.Size(385, 105);
            this.singlePlayerTutorialsButton.TabIndex = 13;
            this.singlePlayerTutorialsButton.Paint += new System.Windows.Forms.PaintEventHandler(this.singlePlayerTutorialsButton_Paint);
            // 
            // tutorialDescriptionLabel
            // 
            this.tutorialDescriptionLabel.AutoSize = true;
            this.tutorialDescriptionLabel.Location = new System.Drawing.Point(141, 47);
            this.tutorialDescriptionLabel.Name = "tutorialDescriptionLabel";
            this.tutorialDescriptionLabel.Size = new System.Drawing.Size(243, 26);
            this.tutorialDescriptionLabel.TabIndex = 2;
            this.tutorialDescriptionLabel.Text = "Learn about trading card games, the playmat, your\r\navatar, playing a card, and ho" +
    "w to win the game.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(140, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "Tutorials";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(1, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(133, 100);
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // closeSinglePlayerWindow
            // 
            this.closeSinglePlayerWindow.BackColor = System.Drawing.Color.Transparent;
            this.closeSinglePlayerWindow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("closeSinglePlayerWindow.BackgroundImage")));
            this.closeSinglePlayerWindow.Location = new System.Drawing.Point(421, 2);
            this.closeSinglePlayerWindow.Name = "closeSinglePlayerWindow";
            this.closeSinglePlayerWindow.Size = new System.Drawing.Size(42, 37);
            this.closeSinglePlayerWindow.TabIndex = 12;
            this.closeSinglePlayerWindow.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.label7);
            this.panel4.Location = new System.Drawing.Point(13, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 30);
            this.panel4.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SkyBlue;
            this.label7.Location = new System.Drawing.Point(21, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(158, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Single Player Options";
            // 
            // selectButton
            // 
            this.selectButton.BackColor = System.Drawing.Color.Transparent;
            this.selectButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("selectButton.BackgroundImage")));
            this.selectButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.selectButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.selectButton.FlatAppearance.BorderSize = 0;
            this.selectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.selectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.selectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectButton.ForeColor = System.Drawing.Color.Black;
            this.selectButton.Location = new System.Drawing.Point(331, 412);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(120, 29);
            this.selectButton.TabIndex = 51;
            this.selectButton.Text = "Select";
            this.selectButton.UseVisualStyleBackColor = false;
            // 
            // SinglePlayerOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.selectButton);
            this.Controls.Add(this.singlePlayerSkirmishButton);
            this.Controls.Add(this.singlePlayerScenariosButton);
            this.Controls.Add(this.singlePlayerTutorialsButton);
            this.Controls.Add(this.closeSinglePlayerWindow);
            this.Controls.Add(this.panel4);
            this.DoubleBuffered = true;
            this.Name = "SinglePlayerOptions";
            this.Size = new System.Drawing.Size(475, 460);
            this.Load += new System.EventHandler(this.SinglePlayerOptions_Load);
            this.singlePlayerSkirmishButton.ResumeLayout(false);
            this.singlePlayerSkirmishButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.singlePlayerScenariosButton.ResumeLayout(false);
            this.singlePlayerScenariosButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.singlePlayerTutorialsButton.ResumeLayout(false);
            this.singlePlayerTutorialsButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeSinglePlayerWindow)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel singlePlayerSkirmishButton;
        private System.Windows.Forms.Label skirmishDescriptionLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel singlePlayerScenariosButton;
        private System.Windows.Forms.Label scenariosDescriptionLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel singlePlayerTutorialsButton;
        private System.Windows.Forms.Label tutorialDescriptionLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox closeSinglePlayerWindow;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button selectButton;
    }
}
