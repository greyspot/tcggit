﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.account;
using TradingCardGame.services.command;
using TradingCardGame.services.multiplayer;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class CreateMatch : UserControl
    {
        public CreateMatch()
        {
            InitializeComponent();
        }

        MultiplayerService multSrv = new MultiplayerService();

        Point mouseDownPoint;

        bool _allowObservers = false;
        bool _friendsOnly = false;
        bool _hideStarters = false;
        bool _lightVsDark = false;

        private void CreateMatch_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);

            if (Properties.Settings.Default.showDeckSelector)
            {
                loadAvailableDecks();
                selectDeckPanel.Visible = true;
            }
            else
            {
                updateCardSetInfo();
                setDefaultCreateMatchValues();
                selectDeckPanel.Visible = false;
            }
        }

        private void changeCheckBox(PictureBox checkBox, bool checkedBool)
        {
            if (checkedBool)
                checkBox.Image = Images.buttons.buttons.checkbox_off;
            else
                checkBox.Image = Images.buttons.buttons.checkbox_on;
            SoundService.PlaySound("button");
        }

        private void allowObserversCheckBox_Click(object sender, EventArgs e)
        {
            changeCheckBox(allowObserversCheckBox, _allowObservers);
            _allowObservers = !_allowObservers;
        }

        private void friendsOnlyCheckBox_Click(object sender, EventArgs e)
        {
            changeCheckBox(friendsOnlyCheckBox, _friendsOnly);
            _friendsOnly = !_friendsOnly;
        }

        private void lightVsDarkCheckBox_Click(object sender, EventArgs e)
        {
            changeCheckBox(lightVsDarkCheckBox, _lightVsDark);
            _lightVsDark = !_lightVsDark;
        }

        private void closeCreateMatchPanelButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void closeCreateMatchPanelButton_MouseDown(object sender, MouseEventArgs e)
        {
            closeCreateMatchPanelButton.Image = Images.buttons.buttons.closebutton_down;
        }

        private void closeCreateMatchPanelButton_MouseEnter(object sender, EventArgs e)
        {
            closeCreateMatchPanelButton.Image = Images.buttons.buttons.closebutton_over;
        }

        private void closeCreateMatchPanelButton_MouseLeave(object sender, EventArgs e)
        {
            closeCreateMatchPanelButton.Image = Images.buttons.buttons.closebutton;
        }

        private void selectDeckButton_Click(object sender, EventArgs e)
        {
            loadAvailableDecks();
            SoundService.PlaySound("button");
        }

        private void loadAvailableDecks()
        {
            selectDeckPanel.Location = new Point(0, 41);
            panelTitleLabel.Text = "Select Deck";
            availableDecksListViewer.Items.Clear();

            for (byte x = 0; x < AccountService.GetAvailableCardDecks().Length; x++)
                availableDecksListViewer.Items.Add(AccountService.GetAvailableCardDecks()[x], "yes");

            for (byte i = 0; i < AccountService.GetAvailableCardDecks().Length; i++)
            {
                if (AccountService.GetAvailableCardDecks()[i].Equals(Properties.Settings.Default.cardSetSelected))
                {
                    avatarPicture.Visible = true;
                    availableDecksListViewer.Items[i].Selected = true;
                    availableDecksListViewer.Select();
                }
                else
                    avatarPicture.Visible = false;
            }

            selectDeckPanel.Visible = true;
        }

        private void createMatchButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (string.IsNullOrWhiteSpace(matchTitleTextBox.Text) || matchTypeComboBox.SelectedIndex == -1 ||
                numberOfPlayersListBox.SelectedIndex == -1 || playFormatComboBox.SelectedIndex == -1 ||
                matchStructureComboBox.SelectedIndex == -1 || string.IsNullOrEmpty(deckSelectionTextBox.Text) ||
                timeLimitComboBox.SelectedIndex == -1 || guildOnlyComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please fill out all match information.");
                return;
            }

            try
            {
                MultiplayerService.createMatch("casual", matchTitleTextBox.Text.Replace("'", "''"), AccountService.GetUsername(), matchTypeComboBox.Text, numberOfPlayersListBox.Text, playFormatComboBox.Text, matchStructureComboBox.Text, timeLimitComboBox.Text, passwordTextBox.Text, guildOnlyComboBox.Text, _allowObservers.ToString(), _friendsOnly.ToString(), _lightVsDark.ToString());

                //services.database.DatabaseService.ExecuteCommand("INSERT INTO lobbies(type) VALUES('" + services.database.DatabaseService.GetValue("matches", "id", "creator", AccountService.GetUsername()) + "')");

                PacketService.SendPacket(ServerData.PacketType.ClientCommand, CommandService.COMMAND_UPDATE_AVAILABLE_MATCHES.ToString());

                //Properties.Settings.Default.matchTitle = DatabaseService.getValue("matches", "title", "creator", AccountService.getUsername());
                Properties.Settings.Default.Save();

                new LaunchMatch().Show();
                Dispose();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("Duplicate entry 'casual' for key 'PRIMARY'"))
                    MessageBox.Show("Sorry, but there's a match that already has that name.");
                else
                    MessageBox.Show("Error:\n" + ex.Message);

            }
        }

        private void setDefaultCreateMatchValues()
        {
            matchTitleTextBox.Text = AccountService.GetUsername() + "'s Match";
            matchTypeComboBox.SelectedIndex = 0;
            numberOfPlayersListBox.SelectedIndex = 0;
            playFormatComboBox.SelectedIndex = 0;
            matchStructureComboBox.SelectedIndex = 0;
            timeLimitComboBox.SelectedIndex = 0;
            guildOnlyComboBox.SelectedIndex = 0;
        }

        private void resetMatchCreationButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            setDefaultCreateMatchValues();
        }

        private void cancelMatchCreationButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (availableDecksListViewer.SelectedItems.Count != 0)
                Properties.Settings.Default.cardSetSelected = availableDecksListViewer.SelectedItems[0].Text;
            else
                Properties.Settings.Default.cardSetSelected = string.Empty;
            Properties.Settings.Default.Save();

            SoundService.PlaySound("button");

            if (Properties.Settings.Default.showDeckSelector)
                Dispose();

            selectDeckPanel.Visible = false;

            updateCardSetInfo();
        }

        private void updateCardSetInfo()
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.cardSetSelected))
            {
                deckSelectionTextBox.Text = Properties.Settings.Default.cardSetSelected;
                validDeckPictureBox.Image = Images.icons.icons.yes;
            }
            else
                validDeckPictureBox.Image = Images.icons.icons.no;
        }

        private void setAvatarInfo()
        {
            avatarTitleLabel.Text = "Elevenumb";
            avatarSubtitleLabel.Text = "Avatar, Sullustan";
            avatarDescriptionLabel.Text = "Whenever you complete a\nquest, draw two cards.";
            avatarPicture.BackgroundImage = Images.card.card.full_sullustan_male;

            if (!avatarPicture.Visible)
                avatarPicture.Visible = true;
        }

        private void availableDecksListViewer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (availableDecksListViewer.SelectedItems.Count != 0)
                setAvatarInfo();
            else
                avatarPicture.Visible = false;
        }

        private void CreateMatch_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.showDeckSelector = false;
            Properties.Settings.Default.Save();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            selectDeckPanel.Visible = false;
            SoundService.PlaySound("button");
        }

        private void CreateMatch_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }

        private void CreateMatch_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDownPoint.IsEmpty)
                Location = new Point(Location.X + (e.X - mouseDownPoint.X), Location.Y + (e.Y - mouseDownPoint.Y));
        }

        private void CreateMatch_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownPoint = Point.Empty;
        }

        private void hideStartersCheckBox_Click(object sender, EventArgs e)
        {
            changeCheckBox(hideStartersCheckBox, _hideStarters);
            _hideStarters = !_hideStarters;
        }
    }
}
