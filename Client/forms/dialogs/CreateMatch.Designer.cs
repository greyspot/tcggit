﻿namespace TradingCardGame.forms.dialogs
{
    partial class CreateMatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateMatch));
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cancelMatchCreationButton = new System.Windows.Forms.Button();
            this.resetMatchCreationButton = new System.Windows.Forms.Button();
            this.createMatchButton = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.guildOnlyComboBox = new System.Windows.Forms.ComboBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.timeLimitComboBox = new System.Windows.Forms.ComboBox();
            this.deckSelectionTextBox = new System.Windows.Forms.TextBox();
            this.selectDeckButton = new System.Windows.Forms.Button();
            this.validDeckPictureBox = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.matchStructureComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.playFormatComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numberOfPlayersListBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.matchTypeComboBox = new System.Windows.Forms.ComboBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panelTitleLabel = new System.Windows.Forms.Label();
            this.closeCreateMatchPanelButton = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.matchTitleTextBox = new System.Windows.Forms.TextBox();
            this.allowObserversCheckBox = new System.Windows.Forms.PictureBox();
            this.friendsOnlyCheckBox = new System.Windows.Forms.PictureBox();
            this.lightVsDarkCheckBox = new System.Windows.Forms.PictureBox();
            this.selectDeckPanel = new System.Windows.Forms.Panel();
            this.deleteButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.openButton = new System.Windows.Forms.Button();
            this.availableDecksListViewer = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label11 = new System.Windows.Forms.Label();
            this.hideStartersCheckBox = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.avatarPicture = new System.Windows.Forms.Panel();
            this.avatarCoverPanel = new System.Windows.Forms.Panel();
            this.avatarDescriptionLabel = new System.Windows.Forms.Label();
            this.avatarSubtitleLabel = new System.Windows.Forms.Label();
            this.avatarTitleLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.validDeckPictureBox)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeCreateMatchPanelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowObserversCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.friendsOnlyCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightVsDarkCheckBox)).BeginInit();
            this.selectDeckPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hideStartersCheckBox)).BeginInit();
            this.avatarPicture.SuspendLayout();
            this.avatarCoverPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label17.Location = new System.Drawing.Point(413, 320);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 15);
            this.label17.TabIndex = 78;
            this.label17.Text = "Light vs. Dark";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label16.Location = new System.Drawing.Point(413, 293);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 15);
            this.label16.TabIndex = 77;
            this.label16.Text = "Friends Only";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label15.Location = new System.Drawing.Point(413, 267);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 15);
            this.label15.TabIndex = 76;
            this.label15.Text = "Allow Observers";
            // 
            // cancelMatchCreationButton
            // 
            this.cancelMatchCreationButton.BackColor = System.Drawing.Color.Black;
            this.cancelMatchCreationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cancelMatchCreationButton.BackgroundImage")));
            this.cancelMatchCreationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancelMatchCreationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelMatchCreationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelMatchCreationButton.ForeColor = System.Drawing.Color.Black;
            this.cancelMatchCreationButton.Location = new System.Drawing.Point(237, 425);
            this.cancelMatchCreationButton.Name = "cancelMatchCreationButton";
            this.cancelMatchCreationButton.Size = new System.Drawing.Size(95, 29);
            this.cancelMatchCreationButton.TabIndex = 75;
            this.cancelMatchCreationButton.Text = "Cancel";
            this.cancelMatchCreationButton.UseVisualStyleBackColor = false;
            this.cancelMatchCreationButton.Click += new System.EventHandler(this.cancelMatchCreationButton_Click);
            // 
            // resetMatchCreationButton
            // 
            this.resetMatchCreationButton.BackColor = System.Drawing.Color.Black;
            this.resetMatchCreationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("resetMatchCreationButton.BackgroundImage")));
            this.resetMatchCreationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.resetMatchCreationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetMatchCreationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetMatchCreationButton.ForeColor = System.Drawing.Color.Black;
            this.resetMatchCreationButton.Location = new System.Drawing.Point(338, 425);
            this.resetMatchCreationButton.Name = "resetMatchCreationButton";
            this.resetMatchCreationButton.Size = new System.Drawing.Size(95, 29);
            this.resetMatchCreationButton.TabIndex = 74;
            this.resetMatchCreationButton.Text = "Reset";
            this.resetMatchCreationButton.UseVisualStyleBackColor = false;
            this.resetMatchCreationButton.Click += new System.EventHandler(this.resetMatchCreationButton_Click);
            // 
            // createMatchButton
            // 
            this.createMatchButton.BackColor = System.Drawing.Color.Black;
            this.createMatchButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("createMatchButton.BackgroundImage")));
            this.createMatchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createMatchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createMatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createMatchButton.ForeColor = System.Drawing.Color.Black;
            this.createMatchButton.Location = new System.Drawing.Point(439, 425);
            this.createMatchButton.Name = "createMatchButton";
            this.createMatchButton.Size = new System.Drawing.Size(95, 29);
            this.createMatchButton.TabIndex = 73;
            this.createMatchButton.Text = "Create";
            this.createMatchButton.UseVisualStyleBackColor = false;
            this.createMatchButton.Click += new System.EventHandler(this.createMatchButton_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label14.Location = new System.Drawing.Point(57, 324);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 15);
            this.label14.TabIndex = 72;
            this.label14.Text = "Guild Only:";
            // 
            // guildOnlyComboBox
            // 
            this.guildOnlyComboBox.BackColor = System.Drawing.Color.Black;
            this.guildOnlyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guildOnlyComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildOnlyComboBox.ForeColor = System.Drawing.Color.White;
            this.guildOnlyComboBox.FormattingEnabled = true;
            this.guildOnlyComboBox.Items.AddRange(new object[] {
            "No Guild Restriction",
            "Guild Officers Only",
            "All Guild Members"});
            this.guildOnlyComboBox.Location = new System.Drawing.Point(131, 321);
            this.guildOnlyComboBox.Name = "guildOnlyComboBox";
            this.guildOnlyComboBox.Size = new System.Drawing.Size(262, 23);
            this.guildOnlyComboBox.TabIndex = 71;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.BackColor = System.Drawing.Color.Black;
            this.passwordTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.passwordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.passwordTextBox.Location = new System.Drawing.Point(131, 293);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.ReadOnly = true;
            this.passwordTextBox.Size = new System.Drawing.Size(262, 22);
            this.passwordTextBox.TabIndex = 70;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label13.Location = new System.Drawing.Point(60, 293);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 15);
            this.label13.TabIndex = 69;
            this.label13.Text = "Password:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label12.Location = new System.Drawing.Point(57, 264);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 15);
            this.label12.TabIndex = 68;
            this.label12.Text = "Time Limit:";
            // 
            // timeLimitComboBox
            // 
            this.timeLimitComboBox.BackColor = System.Drawing.Color.Black;
            this.timeLimitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timeLimitComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLimitComboBox.ForeColor = System.Drawing.Color.White;
            this.timeLimitComboBox.FormattingEnabled = true;
            this.timeLimitComboBox.Items.AddRange(new object[] {
            "No Time Limit",
            "90m Chess Clock"});
            this.timeLimitComboBox.Location = new System.Drawing.Point(131, 261);
            this.timeLimitComboBox.Name = "timeLimitComboBox";
            this.timeLimitComboBox.Size = new System.Drawing.Size(262, 23);
            this.timeLimitComboBox.TabIndex = 67;
            // 
            // deckSelectionTextBox
            // 
            this.deckSelectionTextBox.BackColor = System.Drawing.Color.Black;
            this.deckSelectionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.deckSelectionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deckSelectionTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.deckSelectionTextBox.Location = new System.Drawing.Point(13, 229);
            this.deckSelectionTextBox.Name = "deckSelectionTextBox";
            this.deckSelectionTextBox.ReadOnly = true;
            this.deckSelectionTextBox.Size = new System.Drawing.Size(354, 22);
            this.deckSelectionTextBox.TabIndex = 61;
            // 
            // selectDeckButton
            // 
            this.selectDeckButton.BackColor = System.Drawing.Color.Black;
            this.selectDeckButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("selectDeckButton.BackgroundImage")));
            this.selectDeckButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.selectDeckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectDeckButton.ForeColor = System.Drawing.Color.Black;
            this.selectDeckButton.Location = new System.Drawing.Point(442, 229);
            this.selectDeckButton.Name = "selectDeckButton";
            this.selectDeckButton.Size = new System.Drawing.Size(95, 29);
            this.selectDeckButton.TabIndex = 65;
            this.selectDeckButton.Text = "Select Deck";
            this.selectDeckButton.UseVisualStyleBackColor = false;
            this.selectDeckButton.Click += new System.EventHandler(this.selectDeckButton_Click);
            // 
            // validDeckPictureBox
            // 
            this.validDeckPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("validDeckPictureBox.BackgroundImage")));
            this.validDeckPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.validDeckPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("validDeckPictureBox.Image")));
            this.validDeckPictureBox.Location = new System.Drawing.Point(365, 226);
            this.validDeckPictureBox.Name = "validDeckPictureBox";
            this.validDeckPictureBox.Size = new System.Drawing.Size(91, 29);
            this.validDeckPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.validDeckPictureBox.TabIndex = 66;
            this.validDeckPictureBox.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label8.Location = new System.Drawing.Point(10, 211);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 15);
            this.label8.TabIndex = 60;
            this.label8.Text = "Deck Selection:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label6.Location = new System.Drawing.Point(275, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 15);
            this.label6.TabIndex = 59;
            this.label6.Text = "Match Structure:";
            // 
            // matchStructureComboBox
            // 
            this.matchStructureComboBox.BackColor = System.Drawing.Color.Black;
            this.matchStructureComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.matchStructureComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchStructureComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.matchStructureComboBox.FormattingEnabled = true;
            this.matchStructureComboBox.Items.AddRange(new object[] {
            "Single Game",
            "Best of Three Games",
            "Best of Five Games"});
            this.matchStructureComboBox.Location = new System.Drawing.Point(278, 175);
            this.matchStructureComboBox.Name = "matchStructureComboBox";
            this.matchStructureComboBox.Size = new System.Drawing.Size(259, 24);
            this.matchStructureComboBox.TabIndex = 58;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label7.Location = new System.Drawing.Point(11, 157);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 57;
            this.label7.Text = "Play Format:";
            // 
            // playFormatComboBox
            // 
            this.playFormatComboBox.BackColor = System.Drawing.Color.Black;
            this.playFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.playFormatComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playFormatComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.playFormatComboBox.FormattingEnabled = true;
            this.playFormatComboBox.Items.AddRange(new object[] {
            "Standard",
            "Raid"});
            this.playFormatComboBox.Location = new System.Drawing.Point(13, 175);
            this.playFormatComboBox.Name = "playFormatComboBox";
            this.playFormatComboBox.Size = new System.Drawing.Size(259, 24);
            this.playFormatComboBox.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label5.Location = new System.Drawing.Point(275, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 15);
            this.label5.TabIndex = 55;
            this.label5.Text = "Number of Players:";
            // 
            // numberOfPlayersListBox
            // 
            this.numberOfPlayersListBox.BackColor = System.Drawing.Color.Black;
            this.numberOfPlayersListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numberOfPlayersListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfPlayersListBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.numberOfPlayersListBox.FormattingEnabled = true;
            this.numberOfPlayersListBox.Items.AddRange(new object[] {
            "2 Players",
            "4 Players"});
            this.numberOfPlayersListBox.Location = new System.Drawing.Point(278, 125);
            this.numberOfPlayersListBox.Name = "numberOfPlayersListBox";
            this.numberOfPlayersListBox.Size = new System.Drawing.Size(259, 24);
            this.numberOfPlayersListBox.TabIndex = 54;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label4.Location = new System.Drawing.Point(11, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 53;
            this.label4.Text = "Match Type:";
            // 
            // matchTypeComboBox
            // 
            this.matchTypeComboBox.BackColor = System.Drawing.Color.Black;
            this.matchTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.matchTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchTypeComboBox.ForeColor = System.Drawing.Color.White;
            this.matchTypeComboBox.FormattingEnabled = true;
            this.matchTypeComboBox.Items.AddRange(new object[] {
            "Constructed"});
            this.matchTypeComboBox.Location = new System.Drawing.Point(13, 125);
            this.matchTypeComboBox.Name = "matchTypeComboBox";
            this.matchTypeComboBox.Size = new System.Drawing.Size(259, 24);
            this.matchTypeComboBox.TabIndex = 52;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.BackgroundImage = global::TradingCardGame.Images.dialogs.dialogs.title_base;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.panelTitleLabel);
            this.panel10.Location = new System.Drawing.Point(19, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(150, 37);
            this.panel10.TabIndex = 51;
            // 
            // panelTitleLabel
            // 
            this.panelTitleLabel.AutoSize = true;
            this.panelTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.panelTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelTitleLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.panelTitleLabel.Location = new System.Drawing.Point(21, 9);
            this.panelTitleLabel.Name = "panelTitleLabel";
            this.panelTitleLabel.Size = new System.Drawing.Size(109, 18);
            this.panelTitleLabel.TabIndex = 0;
            this.panelTitleLabel.Text = "Create Match";
            // 
            // closeCreateMatchPanelButton
            // 
            this.closeCreateMatchPanelButton.BackColor = System.Drawing.Color.Transparent;
            this.closeCreateMatchPanelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closeCreateMatchPanelButton.Image = global::TradingCardGame.Images.buttons.buttons.closebutton;
            this.closeCreateMatchPanelButton.Location = new System.Drawing.Point(500, 3);
            this.closeCreateMatchPanelButton.Name = "closeCreateMatchPanelButton";
            this.closeCreateMatchPanelButton.Size = new System.Drawing.Size(42, 37);
            this.closeCreateMatchPanelButton.TabIndex = 50;
            this.closeCreateMatchPanelButton.TabStop = false;
            this.closeCreateMatchPanelButton.Click += new System.EventHandler(this.closeCreateMatchPanelButton_Click);
            this.closeCreateMatchPanelButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.closeCreateMatchPanelButton_MouseDown);
            this.closeCreateMatchPanelButton.MouseEnter += new System.EventHandler(this.closeCreateMatchPanelButton_MouseEnter);
            this.closeCreateMatchPanelButton.MouseLeave += new System.EventHandler(this.closeCreateMatchPanelButton_MouseLeave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label2.Location = new System.Drawing.Point(11, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 49;
            this.label2.Text = "Match Title:";
            // 
            // matchTitleTextBox
            // 
            this.matchTitleTextBox.BackColor = System.Drawing.Color.Black;
            this.matchTitleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.matchTitleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchTitleTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.matchTitleTextBox.Location = new System.Drawing.Point(14, 73);
            this.matchTitleTextBox.Name = "matchTitleTextBox";
            this.matchTitleTextBox.Size = new System.Drawing.Size(523, 22);
            this.matchTitleTextBox.TabIndex = 48;
            // 
            // allowObserversCheckBox
            // 
            this.allowObserversCheckBox.Image = ((System.Drawing.Image)(resources.GetObject("allowObserversCheckBox.Image")));
            this.allowObserversCheckBox.Location = new System.Drawing.Point(511, 264);
            this.allowObserversCheckBox.Name = "allowObserversCheckBox";
            this.allowObserversCheckBox.Size = new System.Drawing.Size(23, 20);
            this.allowObserversCheckBox.TabIndex = 62;
            this.allowObserversCheckBox.TabStop = false;
            this.allowObserversCheckBox.Click += new System.EventHandler(this.allowObserversCheckBox_Click);
            // 
            // friendsOnlyCheckBox
            // 
            this.friendsOnlyCheckBox.Image = ((System.Drawing.Image)(resources.GetObject("friendsOnlyCheckBox.Image")));
            this.friendsOnlyCheckBox.Location = new System.Drawing.Point(511, 290);
            this.friendsOnlyCheckBox.Name = "friendsOnlyCheckBox";
            this.friendsOnlyCheckBox.Size = new System.Drawing.Size(23, 20);
            this.friendsOnlyCheckBox.TabIndex = 63;
            this.friendsOnlyCheckBox.TabStop = false;
            this.friendsOnlyCheckBox.Click += new System.EventHandler(this.friendsOnlyCheckBox_Click);
            // 
            // lightVsDarkCheckBox
            // 
            this.lightVsDarkCheckBox.Image = ((System.Drawing.Image)(resources.GetObject("lightVsDarkCheckBox.Image")));
            this.lightVsDarkCheckBox.Location = new System.Drawing.Point(511, 315);
            this.lightVsDarkCheckBox.Name = "lightVsDarkCheckBox";
            this.lightVsDarkCheckBox.Size = new System.Drawing.Size(23, 20);
            this.lightVsDarkCheckBox.TabIndex = 64;
            this.lightVsDarkCheckBox.TabStop = false;
            this.lightVsDarkCheckBox.Click += new System.EventHandler(this.lightVsDarkCheckBox_Click);
            // 
            // selectDeckPanel
            // 
            this.selectDeckPanel.BackColor = System.Drawing.Color.Transparent;
            this.selectDeckPanel.Controls.Add(this.deleteButton);
            this.selectDeckPanel.Controls.Add(this.cancelButton);
            this.selectDeckPanel.Controls.Add(this.openButton);
            this.selectDeckPanel.Controls.Add(this.availableDecksListViewer);
            this.selectDeckPanel.Controls.Add(this.label11);
            this.selectDeckPanel.Controls.Add(this.hideStartersCheckBox);
            this.selectDeckPanel.Controls.Add(this.label10);
            this.selectDeckPanel.Controls.Add(this.label9);
            this.selectDeckPanel.Controls.Add(this.label3);
            this.selectDeckPanel.Controls.Add(this.avatarPicture);
            this.selectDeckPanel.Location = new System.Drawing.Point(0, 49);
            this.selectDeckPanel.Name = "selectDeckPanel";
            this.selectDeckPanel.Size = new System.Drawing.Size(553, 450);
            this.selectDeckPanel.TabIndex = 79;
            this.selectDeckPanel.Visible = false;
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.Black;
            this.deleteButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("deleteButton.BackgroundImage")));
            this.deleteButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.deleteButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.deleteButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.Black;
            this.deleteButton.Location = new System.Drawing.Point(241, 394);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(95, 29);
            this.deleteButton.TabIndex = 47;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = false;
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Black;
            this.cancelButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cancelButton.BackgroundImage")));
            this.cancelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancelButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(342, 394);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(95, 29);
            this.cancelButton.TabIndex = 46;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // openButton
            // 
            this.openButton.BackColor = System.Drawing.Color.Black;
            this.openButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("openButton.BackgroundImage")));
            this.openButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.openButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.openButton.FlatAppearance.BorderSize = 0;
            this.openButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.openButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.openButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openButton.ForeColor = System.Drawing.Color.Black;
            this.openButton.Location = new System.Drawing.Point(443, 394);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(95, 29);
            this.openButton.TabIndex = 45;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = false;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // availableDecksListViewer
            // 
            this.availableDecksListViewer.BackColor = System.Drawing.Color.Black;
            this.availableDecksListViewer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.availableDecksListViewer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableDecksListViewer.ForeColor = System.Drawing.Color.White;
            this.availableDecksListViewer.FullRowSelect = true;
            this.availableDecksListViewer.Location = new System.Drawing.Point(257, 64);
            this.availableDecksListViewer.MultiSelect = false;
            this.availableDecksListViewer.Name = "availableDecksListViewer";
            this.availableDecksListViewer.Size = new System.Drawing.Size(281, 300);
            this.availableDecksListViewer.TabIndex = 3;
            this.availableDecksListViewer.UseCompatibleStateImageBehavior = false;
            this.availableDecksListViewer.View = System.Windows.Forms.View.Details;
            this.availableDecksListViewer.SelectedIndexChanged += new System.EventHandler(this.availableDecksListViewer_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Deck Name";
            this.columnHeader2.Width = 280;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label11.Location = new System.Drawing.Point(282, 374);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Hide Starters";
            // 
            // hideStartersCheckBox
            // 
            this.hideStartersCheckBox.Image = ((System.Drawing.Image)(resources.GetObject("hideStartersCheckBox.Image")));
            this.hideStartersCheckBox.Location = new System.Drawing.Point(253, 370);
            this.hideStartersCheckBox.Name = "hideStartersCheckBox";
            this.hideStartersCheckBox.Size = new System.Drawing.Size(23, 20);
            this.hideStartersCheckBox.TabIndex = 8;
            this.hideStartersCheckBox.TabStop = false;
            this.hideStartersCheckBox.Click += new System.EventHandler(this.hideStartersCheckBox_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label10.Location = new System.Drawing.Point(20, 325);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Total Cards In Deck: 50";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label9.Location = new System.Drawing.Point(252, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 15);
            this.label9.TabIndex = 6;
            this.label9.Text = "Online Decks:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label3.Location = new System.Drawing.Point(252, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Select a deck to open";
            // 
            // avatarPicture
            // 
            this.avatarPicture.BackgroundImage = global::TradingCardGame.Images.card.card._100007402;
            this.avatarPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.avatarPicture.Controls.Add(this.avatarCoverPanel);
            this.avatarPicture.Location = new System.Drawing.Point(21, 9);
            this.avatarPicture.Name = "avatarPicture";
            this.avatarPicture.Size = new System.Drawing.Size(225, 300);
            this.avatarPicture.TabIndex = 4;
            this.avatarPicture.Visible = false;
            // 
            // avatarCoverPanel
            // 
            this.avatarCoverPanel.BackColor = System.Drawing.Color.Transparent;
            this.avatarCoverPanel.BackgroundImage = global::TradingCardGame.Images.card.card.template_generic;
            this.avatarCoverPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.avatarCoverPanel.Controls.Add(this.avatarDescriptionLabel);
            this.avatarCoverPanel.Controls.Add(this.avatarSubtitleLabel);
            this.avatarCoverPanel.Controls.Add(this.avatarTitleLabel);
            this.avatarCoverPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.avatarCoverPanel.Location = new System.Drawing.Point(0, 0);
            this.avatarCoverPanel.Name = "avatarCoverPanel";
            this.avatarCoverPanel.Size = new System.Drawing.Size(225, 300);
            this.avatarCoverPanel.TabIndex = 0;
            // 
            // avatarDescriptionLabel
            // 
            this.avatarDescriptionLabel.AutoSize = true;
            this.avatarDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avatarDescriptionLabel.Location = new System.Drawing.Point(15, 221);
            this.avatarDescriptionLabel.Name = "avatarDescriptionLabel";
            this.avatarDescriptionLabel.Size = new System.Drawing.Size(118, 16);
            this.avatarDescriptionLabel.TabIndex = 2;
            this.avatarDescriptionLabel.Text = "Avatar Description";
            // 
            // avatarSubtitleLabel
            // 
            this.avatarSubtitleLabel.AutoSize = true;
            this.avatarSubtitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avatarSubtitleLabel.Location = new System.Drawing.Point(49, 19);
            this.avatarSubtitleLabel.Name = "avatarSubtitleLabel";
            this.avatarSubtitleLabel.Size = new System.Drawing.Size(76, 13);
            this.avatarSubtitleLabel.TabIndex = 1;
            this.avatarSubtitleLabel.Text = "Avatar Subtitle";
            // 
            // avatarTitleLabel
            // 
            this.avatarTitleLabel.AutoSize = true;
            this.avatarTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avatarTitleLabel.Location = new System.Drawing.Point(49, 5);
            this.avatarTitleLabel.Name = "avatarTitleLabel";
            this.avatarTitleLabel.Size = new System.Drawing.Size(73, 13);
            this.avatarTitleLabel.TabIndex = 0;
            this.avatarTitleLabel.Text = "Avatar Title";
            // 
            // CreateMatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(553, 487);
            this.Controls.Add(this.selectDeckPanel);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cancelMatchCreationButton);
            this.Controls.Add(this.resetMatchCreationButton);
            this.Controls.Add(this.createMatchButton);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.guildOnlyComboBox);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.timeLimitComboBox);
            this.Controls.Add(this.deckSelectionTextBox);
            this.Controls.Add(this.selectDeckButton);
            this.Controls.Add(this.validDeckPictureBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.matchStructureComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.playFormatComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numberOfPlayersListBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.matchTypeComboBox);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.closeCreateMatchPanelButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.matchTitleTextBox);
            this.Controls.Add(this.allowObserversCheckBox);
            this.Controls.Add(this.friendsOnlyCheckBox);
            this.Controls.Add(this.lightVsDarkCheckBox);
            this.DoubleBuffered = true;
            this.Name = "CreateMatch";
            this.Text = "CreateMatch";
            this.Load += new System.EventHandler(this.CreateMatch_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CreateMatch_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.CreateMatch_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CreateMatch_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.validDeckPictureBox)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeCreateMatchPanelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowObserversCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.friendsOnlyCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightVsDarkCheckBox)).EndInit();
            this.selectDeckPanel.ResumeLayout(false);
            this.selectDeckPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hideStartersCheckBox)).EndInit();
            this.avatarPicture.ResumeLayout(false);
            this.avatarCoverPanel.ResumeLayout(false);
            this.avatarCoverPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button cancelMatchCreationButton;
        private System.Windows.Forms.Button resetMatchCreationButton;
        private System.Windows.Forms.Button createMatchButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox guildOnlyComboBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox timeLimitComboBox;
        private System.Windows.Forms.TextBox deckSelectionTextBox;
        private System.Windows.Forms.Button selectDeckButton;
        private System.Windows.Forms.PictureBox validDeckPictureBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox matchStructureComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox playFormatComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox numberOfPlayersListBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox matchTypeComboBox;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label panelTitleLabel;
        private System.Windows.Forms.PictureBox closeCreateMatchPanelButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox matchTitleTextBox;
        private System.Windows.Forms.PictureBox allowObserversCheckBox;
        private System.Windows.Forms.PictureBox friendsOnlyCheckBox;
        private System.Windows.Forms.PictureBox lightVsDarkCheckBox;
        private System.Windows.Forms.Panel selectDeckPanel;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.ListView availableDecksListViewer;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox hideStartersCheckBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel avatarPicture;
        private System.Windows.Forms.Panel avatarCoverPanel;
        private System.Windows.Forms.Label avatarDescriptionLabel;
        private System.Windows.Forms.Label avatarSubtitleLabel;
        private System.Windows.Forms.Label avatarTitleLabel;
    }
}