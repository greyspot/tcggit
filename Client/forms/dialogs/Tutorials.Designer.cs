﻿namespace TradingCardGame.forms.dialogs
{
    partial class Tutorials
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tutorials));
            this.firstPanel = new System.Windows.Forms.Panel();
            this.tutorialAboutTextLabel = new System.Windows.Forms.Label();
            this.tutorialTitlePictureBox = new System.Windows.Forms.PictureBox();
            this.tutorial11Button = new System.Windows.Forms.PictureBox();
            this.tutorial10Button = new System.Windows.Forms.PictureBox();
            this.tutorial9Button = new System.Windows.Forms.PictureBox();
            this.tutorial8Button = new System.Windows.Forms.PictureBox();
            this.tutorial7Button = new System.Windows.Forms.PictureBox();
            this.tutorial6Button = new System.Windows.Forms.PictureBox();
            this.tutorial5Button = new System.Windows.Forms.PictureBox();
            this.tutorial4Button = new System.Windows.Forms.PictureBox();
            this.tutorial3Button = new System.Windows.Forms.PictureBox();
            this.tutorial2Button = new System.Windows.Forms.PictureBox();
            this.tutorial1Button = new System.Windows.Forms.PictureBox();
            this.helpButton = new System.Windows.Forms.PictureBox();
            this.closeButton = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.centeringPanel = new System.Windows.Forms.Panel();
            this.firstPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tutorialTitlePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial11Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial10Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial9Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial8Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial7Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial6Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial5Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial4Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial3Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial2Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial1Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).BeginInit();
            this.panel6.SuspendLayout();
            this.centeringPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // firstPanel
            // 
            this.firstPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("firstPanel.BackgroundImage")));
            this.firstPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.firstPanel.Controls.Add(this.centeringPanel);
            this.firstPanel.Controls.Add(this.tutorialTitlePictureBox);
            this.firstPanel.Controls.Add(this.tutorial11Button);
            this.firstPanel.Controls.Add(this.tutorial10Button);
            this.firstPanel.Controls.Add(this.tutorial9Button);
            this.firstPanel.Controls.Add(this.tutorial8Button);
            this.firstPanel.Controls.Add(this.tutorial7Button);
            this.firstPanel.Controls.Add(this.tutorial6Button);
            this.firstPanel.Controls.Add(this.tutorial5Button);
            this.firstPanel.Controls.Add(this.tutorial4Button);
            this.firstPanel.Controls.Add(this.tutorial3Button);
            this.firstPanel.Controls.Add(this.tutorial2Button);
            this.firstPanel.Controls.Add(this.tutorial1Button);
            this.firstPanel.Location = new System.Drawing.Point(23, 68);
            this.firstPanel.Name = "firstPanel";
            this.firstPanel.Size = new System.Drawing.Size(905, 660);
            this.firstPanel.TabIndex = 1;
            // 
            // tutorialAboutTextLabel
            // 
            this.tutorialAboutTextLabel.AutoSize = true;
            this.tutorialAboutTextLabel.BackColor = System.Drawing.Color.Transparent;
            this.tutorialAboutTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tutorialAboutTextLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.tutorialAboutTextLabel.Location = new System.Drawing.Point(51, 0);
            this.tutorialAboutTextLabel.Name = "tutorialAboutTextLabel";
            this.tutorialAboutTextLabel.Size = new System.Drawing.Size(416, 176);
            this.tutorialAboutTextLabel.TabIndex = 14;
            this.tutorialAboutTextLabel.Text = resources.GetString("tutorialAboutTextLabel.Text");
            this.tutorialAboutTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tutorialTitlePictureBox
            // 
            this.tutorialTitlePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.tutorialTitlePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorialTitlePictureBox.Image = ((System.Drawing.Image)(resources.GetObject("tutorialTitlePictureBox.Image")));
            this.tutorialTitlePictureBox.Location = new System.Drawing.Point(348, 50);
            this.tutorialTitlePictureBox.Name = "tutorialTitlePictureBox";
            this.tutorialTitlePictureBox.Size = new System.Drawing.Size(518, 33);
            this.tutorialTitlePictureBox.TabIndex = 13;
            this.tutorialTitlePictureBox.TabStop = false;
            // 
            // tutorial11Button
            // 
            this.tutorial11Button.BackColor = System.Drawing.Color.Black;
            this.tutorial11Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial11Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial11Button.Image")));
            this.tutorial11Button.Location = new System.Drawing.Point(789, 521);
            this.tutorial11Button.Name = "tutorial11Button";
            this.tutorial11Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial11Button.TabIndex = 10;
            this.tutorial11Button.TabStop = false;
            this.tutorial11Button.Click += new System.EventHandler(this.tutorial11Button_Click);
            this.tutorial11Button.MouseEnter += new System.EventHandler(this.tutorial11Button_MouseEnter);
            // 
            // tutorial10Button
            // 
            this.tutorial10Button.BackColor = System.Drawing.Color.Black;
            this.tutorial10Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial10Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial10Button.Image")));
            this.tutorial10Button.Location = new System.Drawing.Point(712, 561);
            this.tutorial10Button.Name = "tutorial10Button";
            this.tutorial10Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial10Button.TabIndex = 9;
            this.tutorial10Button.TabStop = false;
            this.tutorial10Button.Click += new System.EventHandler(this.tutorial10Button_Click);
            this.tutorial10Button.MouseEnter += new System.EventHandler(this.tutorial10Button_MouseEnter);
            // 
            // tutorial9Button
            // 
            this.tutorial9Button.BackColor = System.Drawing.Color.Black;
            this.tutorial9Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial9Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial9Button.Image")));
            this.tutorial9Button.Location = new System.Drawing.Point(625, 554);
            this.tutorial9Button.Name = "tutorial9Button";
            this.tutorial9Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial9Button.TabIndex = 8;
            this.tutorial9Button.TabStop = false;
            this.tutorial9Button.Click += new System.EventHandler(this.tutorial9Button_Click);
            this.tutorial9Button.MouseEnter += new System.EventHandler(this.tutorial9Button_MouseEnter);
            // 
            // tutorial8Button
            // 
            this.tutorial8Button.BackColor = System.Drawing.Color.Black;
            this.tutorial8Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial8Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial8Button.Image")));
            this.tutorial8Button.Location = new System.Drawing.Point(541, 534);
            this.tutorial8Button.Name = "tutorial8Button";
            this.tutorial8Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial8Button.TabIndex = 7;
            this.tutorial8Button.TabStop = false;
            this.tutorial8Button.Click += new System.EventHandler(this.tutorial8Button_Click);
            this.tutorial8Button.MouseEnter += new System.EventHandler(this.tutorial8Button_MouseEnter);
            // 
            // tutorial7Button
            // 
            this.tutorial7Button.BackColor = System.Drawing.Color.Black;
            this.tutorial7Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial7Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial7Button.Image")));
            this.tutorial7Button.Location = new System.Drawing.Point(454, 500);
            this.tutorial7Button.Name = "tutorial7Button";
            this.tutorial7Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial7Button.TabIndex = 6;
            this.tutorial7Button.TabStop = false;
            this.tutorial7Button.Click += new System.EventHandler(this.tutorial7Button_Click);
            this.tutorial7Button.MouseEnter += new System.EventHandler(this.tutorial7Button_MouseEnter);
            // 
            // tutorial6Button
            // 
            this.tutorial6Button.BackColor = System.Drawing.Color.Black;
            this.tutorial6Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial6Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial6Button.Image")));
            this.tutorial6Button.Location = new System.Drawing.Point(371, 438);
            this.tutorial6Button.Name = "tutorial6Button";
            this.tutorial6Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial6Button.TabIndex = 5;
            this.tutorial6Button.TabStop = false;
            this.tutorial6Button.Click += new System.EventHandler(this.tutorial6Button_Click);
            this.tutorial6Button.MouseEnter += new System.EventHandler(this.tutorial6Button_MouseEnter);
            // 
            // tutorial5Button
            // 
            this.tutorial5Button.BackColor = System.Drawing.Color.Black;
            this.tutorial5Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial5Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial5Button.Image")));
            this.tutorial5Button.Location = new System.Drawing.Point(290, 374);
            this.tutorial5Button.Name = "tutorial5Button";
            this.tutorial5Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial5Button.TabIndex = 4;
            this.tutorial5Button.TabStop = false;
            this.tutorial5Button.Click += new System.EventHandler(this.tutorial5Button_Click);
            this.tutorial5Button.MouseEnter += new System.EventHandler(this.tutorial5Button_MouseEnter);
            // 
            // tutorial4Button
            // 
            this.tutorial4Button.BackColor = System.Drawing.Color.Black;
            this.tutorial4Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial4Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial4Button.Image")));
            this.tutorial4Button.Location = new System.Drawing.Point(216, 297);
            this.tutorial4Button.Name = "tutorial4Button";
            this.tutorial4Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial4Button.TabIndex = 3;
            this.tutorial4Button.TabStop = false;
            this.tutorial4Button.Click += new System.EventHandler(this.tutorial4Button_Click);
            this.tutorial4Button.MouseEnter += new System.EventHandler(this.tutorial4Button_MouseEnter);
            // 
            // tutorial3Button
            // 
            this.tutorial3Button.BackColor = System.Drawing.Color.Black;
            this.tutorial3Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial3Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial3Button.Image")));
            this.tutorial3Button.Location = new System.Drawing.Point(153, 217);
            this.tutorial3Button.Name = "tutorial3Button";
            this.tutorial3Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial3Button.TabIndex = 2;
            this.tutorial3Button.TabStop = false;
            this.tutorial3Button.Click += new System.EventHandler(this.tutorial3Button_Click);
            this.tutorial3Button.MouseEnter += new System.EventHandler(this.tutorial3Button_MouseEnter);
            // 
            // tutorial2Button
            // 
            this.tutorial2Button.BackColor = System.Drawing.Color.Black;
            this.tutorial2Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial2Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial2Button.Image")));
            this.tutorial2Button.Location = new System.Drawing.Point(102, 128);
            this.tutorial2Button.Name = "tutorial2Button";
            this.tutorial2Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial2Button.TabIndex = 1;
            this.tutorial2Button.TabStop = false;
            this.tutorial2Button.Click += new System.EventHandler(this.tutorial2Button_Click);
            this.tutorial2Button.MouseEnter += new System.EventHandler(this.tutorial2Button_MouseEnter);
            // 
            // tutorial1Button
            // 
            this.tutorial1Button.BackColor = System.Drawing.Color.Black;
            this.tutorial1Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorial1Button.Image = ((System.Drawing.Image)(resources.GetObject("tutorial1Button.Image")));
            this.tutorial1Button.Location = new System.Drawing.Point(102, 35);
            this.tutorial1Button.Name = "tutorial1Button";
            this.tutorial1Button.Size = new System.Drawing.Size(70, 70);
            this.tutorial1Button.TabIndex = 0;
            this.tutorial1Button.TabStop = false;
            this.tutorial1Button.Click += new System.EventHandler(this.tutorial1Button_Click);
            this.tutorial1Button.MouseEnter += new System.EventHandler(this.tutorial1Button_MouseEnter);
            // 
            // helpButton
            // 
            this.helpButton.BackColor = System.Drawing.Color.Transparent;
            this.helpButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("helpButton.BackgroundImage")));
            this.helpButton.Location = new System.Drawing.Point(832, 13);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(42, 37);
            this.helpButton.TabIndex = 9;
            this.helpButton.TabStop = false;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Transparent;
            this.closeButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("closeButton.BackgroundImage")));
            this.closeButton.Location = new System.Drawing.Point(880, 13);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(42, 37);
            this.closeButton.TabIndex = 8;
            this.closeButton.TabStop = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            this.closeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.closeButton_MouseDown);
            this.closeButton.MouseEnter += new System.EventHandler(this.closeButton_MouseEnter);
            this.closeButton.MouseLeave += new System.EventHandler(this.closeButton_MouseLeave);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel6.BackgroundImage")));
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Controls.Add(this.label9);
            this.panel6.Location = new System.Drawing.Point(48, 14);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(130, 37);
            this.panel6.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.SkyBlue;
            this.label9.Location = new System.Drawing.Point(26, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tutorials";
            // 
            // centeringPanel
            // 
            this.centeringPanel.Controls.Add(this.tutorialAboutTextLabel);
            this.centeringPanel.Location = new System.Drawing.Point(348, 89);
            this.centeringPanel.Name = "centeringPanel";
            this.centeringPanel.Size = new System.Drawing.Size(518, 293);
            this.centeringPanel.TabIndex = 15;
            // 
            // Tutorials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.firstPanel);
            this.DoubleBuffered = true;
            this.Name = "Tutorials";
            this.Size = new System.Drawing.Size(950, 742);
            this.Load += new System.EventHandler(this.Tutorials_Load);
            this.firstPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tutorialTitlePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial11Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial10Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial9Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial8Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial7Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial6Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial5Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial4Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial3Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial2Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tutorial1Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.centeringPanel.ResumeLayout(false);
            this.centeringPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel firstPanel;
        private System.Windows.Forms.Label tutorialAboutTextLabel;
        private System.Windows.Forms.PictureBox tutorialTitlePictureBox;
        private System.Windows.Forms.PictureBox tutorial11Button;
        private System.Windows.Forms.PictureBox tutorial10Button;
        private System.Windows.Forms.PictureBox tutorial9Button;
        private System.Windows.Forms.PictureBox tutorial8Button;
        private System.Windows.Forms.PictureBox tutorial7Button;
        private System.Windows.Forms.PictureBox tutorial6Button;
        private System.Windows.Forms.PictureBox tutorial5Button;
        private System.Windows.Forms.PictureBox tutorial4Button;
        private System.Windows.Forms.PictureBox tutorial3Button;
        private System.Windows.Forms.PictureBox tutorial2Button;
        private System.Windows.Forms.PictureBox tutorial1Button;
        private System.Windows.Forms.PictureBox helpButton;
        private System.Windows.Forms.PictureBox closeButton;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel centeringPanel;
    }
}
