﻿namespace TradingCardGame.forms.dialogs
{
    partial class Friends
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Friends));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.removeFriendButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.friendsListBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.ignoreListBox = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.removeIgnoredButton = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.exitButton = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 35);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(83, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Friends List";
            // 
            // removeFriendButton
            // 
            this.removeFriendButton.BackColor = System.Drawing.Color.Black;
            this.removeFriendButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("removeFriendButton.BackgroundImage")));
            this.removeFriendButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.removeFriendButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.removeFriendButton.FlatAppearance.BorderSize = 0;
            this.removeFriendButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.removeFriendButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.removeFriendButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeFriendButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeFriendButton.ForeColor = System.Drawing.Color.Black;
            this.removeFriendButton.Location = new System.Drawing.Point(78, 235);
            this.removeFriendButton.Name = "removeFriendButton";
            this.removeFriendButton.Size = new System.Drawing.Size(100, 29);
            this.removeFriendButton.TabIndex = 57;
            this.removeFriendButton.Text = "Remove";
            this.removeFriendButton.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.friendsListBox);
            this.panel2.Controls.Add(this.removeFriendButton);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Location = new System.Drawing.Point(19, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(256, 277);
            this.panel2.TabIndex = 58;
            // 
            // friendsListBox
            // 
            this.friendsListBox.BackColor = System.Drawing.Color.Black;
            this.friendsListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.friendsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.friendsListBox.ForeColor = System.Drawing.Color.White;
            this.friendsListBox.FullRowSelect = true;
            this.friendsListBox.Location = new System.Drawing.Point(0, 41);
            this.friendsListBox.MultiSelect = false;
            this.friendsListBox.Name = "friendsListBox";
            this.friendsListBox.Size = new System.Drawing.Size(256, 188);
            this.friendsListBox.TabIndex = 59;
            this.friendsListBox.UseCompatibleStateImageBehavior = false;
            this.friendsListBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Usernames";
            this.columnHeader1.Width = 252;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ignoreListBox);
            this.panel3.Controls.Add(this.removeIgnoredButton);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(281, 47);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(256, 277);
            this.panel3.TabIndex = 59;
            // 
            // ignoreListBox
            // 
            this.ignoreListBox.BackColor = System.Drawing.Color.Black;
            this.ignoreListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.ignoreListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ignoreListBox.ForeColor = System.Drawing.Color.White;
            this.ignoreListBox.FullRowSelect = true;
            this.ignoreListBox.Location = new System.Drawing.Point(0, 41);
            this.ignoreListBox.MultiSelect = false;
            this.ignoreListBox.Name = "ignoreListBox";
            this.ignoreListBox.Size = new System.Drawing.Size(256, 188);
            this.ignoreListBox.TabIndex = 59;
            this.ignoreListBox.UseCompatibleStateImageBehavior = false;
            this.ignoreListBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Usernames";
            this.columnHeader2.Width = 252;
            // 
            // removeIgnoredButton
            // 
            this.removeIgnoredButton.BackColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("removeIgnoredButton.BackgroundImage")));
            this.removeIgnoredButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.removeIgnoredButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.FlatAppearance.BorderSize = 0;
            this.removeIgnoredButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeIgnoredButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeIgnoredButton.ForeColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.Location = new System.Drawing.Point(78, 235);
            this.removeIgnoredButton.Name = "removeIgnoredButton";
            this.removeIgnoredButton.Size = new System.Drawing.Size(100, 29);
            this.removeIgnoredButton.TabIndex = 57;
            this.removeIgnoredButton.Text = "Remove";
            this.removeIgnoredButton.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(256, 35);
            this.panel4.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(83, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ignore List";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(424, 330);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 29);
            this.button2.TabIndex = 60;
            this.button2.Text = "Ignore User";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Black;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(281, 333);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(137, 22);
            this.textBox1.TabIndex = 61;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Black;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.exitButton.Image = global::TradingCardGame.Images.buttons.buttons.closebutton;
            this.exitButton.Location = new System.Drawing.Point(495, 9);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(42, 37);
            this.exitButton.TabIndex = 62;
            this.exitButton.TabStop = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // Friends
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(556, 375);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.Name = "Friends";
            this.Text = "Friends";
            this.Load += new System.EventHandler(this.Friends_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Friends_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Friends_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Friends_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button removeFriendButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView friendsListBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView ignoreListBox;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button removeIgnoredButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox exitButton;
    }
}