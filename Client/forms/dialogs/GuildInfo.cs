﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.account;
using TradingCardGame.services.guild;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class GuildInfo : UserControl
    {
        public GuildInfo()
        {
            InitializeComponent();
        }

        GuildService guildSrv = new GuildService();
        Point mouseDownPoint;

        private void exitButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void overviewButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            overviewPanel.Visible = true;
        }

        private void membersButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void GuildInfo_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDownPoint.IsEmpty)
                Location = new Point(Location.X + (e.X - mouseDownPoint.X), Location.Y + (e.Y - mouseDownPoint.Y));
        }

        private void GuildInfo_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }

        private void GuildInfo_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownPoint = Point.Empty;
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton_over;
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton;
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton_down;
        }

        private void GuildInfo_Load(object sender, EventArgs e)
        {
            BringToFront();
            Location = new Point((Parent.Width / Width) / 2, (Parent.Width - Width) / 2);
            string guildAbbrev = GuildService.getUsersGuild(AccountService.GetUsername());
            guildTitleLabel.Text = "Guild: " + GuildService.getValue(guildAbbrev, "guildname");
            guildTitleLabel.Location = new Point((titlePanel.Width - guildTitleLabel.Width) / 2, guildTitleLabel.Location.Y);
            leaderLabel.Text = GuildService.getValue(guildAbbrev, "guildleader");
            numberOfMembersLabel.Text = GuildService.getTotalGuildMembers(guildAbbrev).ToString();
            string[] guildOfficers = GuildService.getGuildOfficers(guildAbbrev);

            for (byte i = 0; i < guildOfficers.Length; i++)
                guildOfficersListBox.Items.Add(guildOfficers[i], "bronze");
        }
    }
}
