﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class PlayerTips : UserControl
    {
        public PlayerTips()
        {
            InitializeComponent();
        }

        bool hideTipsChecked;

        private void PlayerTips_Load(object sender, EventArgs e)
        {
            BackColor = Color.Transparent;
            BackgroundImage = Images.borders.borders.navframe_header;
            helpTextLabel.Text = services.account.AccountService.GetUsername() + Properties.Resources.helpText;
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void newbieTipCheckBox_Click(object sender, EventArgs e)
        {
            if (hideTipsChecked)
                newbieTipCheckBox.Image = Images.buttons.buttons.checkbox_off;
            else
                newbieTipCheckBox.Image = Images.buttons.buttons.checkbox_on;
            hideTipsChecked = !hideTipsChecked;
            SoundService.PlaySound("button");
        }

        private void newbieMessageExitButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void newbieMessageHelpButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void newbieMessageExitButton_MouseEnter(object sender, EventArgs e)
        {
            newbieMessageExitButton.BackgroundImage = Images.buttons.buttons.closebutton_over;
        }

        private void newbieMessageExitButton_MouseDown(object sender, MouseEventArgs e)
        {
            newbieMessageExitButton.BackgroundImage = Images.buttons.buttons.closebutton_down;
        }

        private void newbieMessageExitButton_MouseLeave(object sender, EventArgs e)
        {
            newbieMessageExitButton.BackgroundImage = Images.buttons.buttons.closebutton;
        }
        private void newbieMessageHelpButton_MouseEnter(object sender, EventArgs e)
        {
            newbieMessageHelpButton.BackgroundImage = Images.buttons.buttons.helpbutton_over;
        }

        private void newbieMessageHelpButton_MouseLeave(object sender, EventArgs e)
        {
            newbieMessageHelpButton.BackgroundImage = Images.buttons.buttons.helpbutton;
        }

        private void newbieMessageHelpButton_MouseDown(object sender, MouseEventArgs e)
        {
            newbieMessageHelpButton.BackgroundImage = Images.buttons.buttons.helpbutton_down;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void closeButton_MouseEnter(object sender, EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void closeButton_MouseLeave(object sender, EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void closeButton_MouseDown(object sender, MouseEventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void newbieMessageHelpButton_MouseUp(object sender, MouseEventArgs e)
        {
            newbieMessageExitButton.BackgroundImage = Images.buttons.buttons.helpbutton;
        }
    }
}
