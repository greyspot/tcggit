﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using TradingCardGame.forms.widgets;
using TradingCardGame.services.account;
using TradingCardGame.services.data;
using TradingCardGame.services.database;
using TradingCardGame.services.guild;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms
{
    public partial class UserInfo : UserControl
    {
        public UserInfo()
        {
            InitializeComponent();
        }

        private Point p;

        private void ProfilePage_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            loadProfile(TemporaryData.getViewingProfile());
        }

        private void loadProfile(string username)
        {
            bronzeMedallionPictureBox.Image = Images.icons.medallions.bronze.bm_1;
            silverMedallionPictureBox.Image = Images.icons.medallions.silver.sm_1;
            goldMedallionPictureBox.Image = Images.icons.medallions.gold.gm_1;

            winsLabel.Text = "Wins: " + DatabaseService.GetValue("users", "wins", "username", username);
            lossesLabel.Text = "Losses: " + DatabaseService.GetValue("users", "losses", "username", username);

            avatarPictureBox.BackgroundImage = getPlayersAvatar(username);
            usernameLabel.Text = AccountService.GetUsernameWithTitle(username);
            usernameLabel.Left = (userInformationPanel.Width - usernameLabel.Width) / 2;


            if (string.IsNullOrEmpty(GuildService.getUsersGuild(username)))
                guildLabel.Text = string.Empty;
            else
                guildLabel.Text = "<" + GuildService.getUsersGuild(username) + ">";
            guildLabel.Left = (userInformationPanel.Width - guildLabel.Width) / 2;

            long unixJoinTimeStamp = Convert.ToInt64(DatabaseService.GetValue("mybb_users", "regdate", "username", username));
            joinDateLabel.Text = "Joined: " + unixToDateTime(unixJoinTimeStamp).Date.ToString("MMMM yyyy");

            string birthDate = DatabaseService.GetValue("mybb_users", "birthday", "username", username).Replace("-", "/");

            if (!string.IsNullOrEmpty(birthDate))
            {
                DateTime dt = DateTime.ParseExact(birthDate, "d/M/yyyy", CultureInfo.InvariantCulture);
                DateOfBirthLabel.Text = "DOB: " + dt.ToString("MM/dd/yyyy") + " (" + Math.Round(((DateTime.Now - dt).TotalDays / 365), 0).ToString() + " years old)";
            }
            else
                DateOfBirthLabel.Text = "DOB: Unknown";

            try
            {
                playerBiographyTextBox.Text = DatabaseService.GetValue("users", "biography", "username", username);
            }
            catch
            {
                playerBiographyTextBox.Text = "This user currently does not have a message.";
            }

            bool profileIsUsers = usernameLabel.Text.Equals(AccountService.GetUsernameWithTitle(AccountService.GetUsername()));
            changeAvatarButton.Visible = profileIsUsers;
            editBiographyButton.Visible = profileIsUsers;
        }

        public DateTime unixToDateTime(long unixTime)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dtDateTime.AddSeconds(unixTime).ToLocalTime();
        }

        private Image getPlayersAvatar(string playerName)
        {
            string avatar = DatabaseService.GetValue("users", "avatar", "username", playerName);

            return (Image)Images.buttons.avatars.ResourceManager.GetObject(avatar);
        }

        private void editBiographyButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (playerBiographyTextBox.ReadOnly)
            {
                editBiographyButton.BackColor = Color.Red;
                playerBiographyTextBox.ReadOnly = false;
            }
            else
            {
                editBiographyButton.BackColor = Color.Transparent;
                playerBiographyTextBox.ReadOnly = true;
                AccountService.WriteNewValue(playerBiographyTextBox.Text.Replace("'", "''"), "biography");
            }
        }

        private void changeAvatarButton_Click(object sender, EventArgs e)
        {
            AvatarChooser avatarChooser = new AvatarChooser();
            avatarChooser.Show();
            SoundService.PlaySound("button");
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            changeAvatarButton.BackColor = Color.Transparent;
        }

        private void playerBiographyTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!playerBiographyTextBox.ReadOnly)
                SoundService.KeyPressSound(e.KeyCode);
            else
                e.SuppressKeyPress = true;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            TemporaryData.changeDialogStatus();
            Dispose();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            TemporaryData.changeDialogStatus();
            Dispose();
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton_over;
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton_down;
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton;
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (userExists())
                loadProfile(userProfileTextBox.Text);
            else
                MessageBox.Show("Sorry, but the user '" + userProfileTextBox.Text + "' doesn't exist.");
            userProfileTextBox.Text = string.Empty;
        }

        private bool userExists()
        {
            using (MySqlCommand com = new MySqlCommand("SELECT * FROM users WHERE username = '" + userProfileTextBox.Text + "'", DatabaseService.GetMySqlConnection(1)))
            {
                if (DatabaseService.GetMySqlConnection(1).State != ConnectionState.Open)
                    DatabaseService.OpenConnectionIfNotOpen(1);

                MySqlDataReader reader = com.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Dispose();
                    return true;
                }
                reader.Dispose();
                return false;

            }
        }

        private void UserInfo_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void UserInfo_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }
    }
}
