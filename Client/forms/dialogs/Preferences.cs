﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs.preferences;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class Preferences : UserControl
    {
        public Preferences()
        {
            InitializeComponent();
        }

        private Point p;
        private int currentPage;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void Preferences_Load(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton;
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();

            loadNewPage(new AccountInfo(), 1);
        }

        private void loadNewPage(UserControl control, int page)
        {
            if (page == currentPage)
                return;

            currentPage = page;
            preferencesFillPanel.Controls.Add(control);
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton_down;
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton_over;
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void cancelButton_MouseDown(object sender, MouseEventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void cancelButton_MouseEnter(object sender, EventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void cancelButton_MouseLeave(object sender, EventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void Preferences_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void Preferences_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }

        private void accountInfoButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            loadNewPage(new AccountInfo(), 1);
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            if (currentPage == 1)
                AccountInfo.saveBiography();

            SoundService.PlaySound("button");
            Dispose();
        }

        private void soundButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            loadNewPage(new DisplaySound(), 3);
        }
    }
}
