﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.data;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class CasualLobbySelection : UserControl
    {
        public CasualLobbySelection()
        {
            InitializeComponent();
        }

        private Panel lastClickedCasual;

        private void CasualLobbySelection_Load(object sender, System.EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.closebutton;

            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void closeButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void helpButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void joinButton_Click(object sender, System.EventArgs e)
        {
            if (lastClickedCasual == null)
                return;

            if (lastClickedCasual == casualGamesButton)
            {
                SoundService.PlaySound("button");
                TemporaryData.setLobbyType(0);
                Parent.Parent.Controls.Add(new Lobby());
                Parent.Dispose();
            }
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void casualGamesButton_Click(object sender, System.EventArgs e)
        {
            lobbyClicked(casualGamesButton);
        }

        private void lobbyClicked(Panel lobbyPanel)
        {
            if (lastClickedCasual != null)
                lastClickedCasual.BackgroundImage = Images.map_screen.map_screen.button_casual;

            SoundService.PlaySound("button");
            casualGamesButton.BackgroundImage = Images.map_screen.map_screen.button_casual_over;
            lastClickedCasual = lobbyPanel;
        }

        private void closeButton_MouseEnter(object sender, System.EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.closebutton_over;
        }

        private void closeButton_MouseDown(object sender, MouseEventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.closebutton_down;
        }

        private void closeButton_MouseLeave(object sender, System.EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.closebutton;
        }

        private void joinButton_MouseEnter(object sender, System.EventArgs e)
        {
            joinButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void joinButton_MouseDown(object sender, MouseEventArgs e)
        {
            joinButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void joinButton_MouseLeave(object sender, System.EventArgs e)
        {
            joinButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void cancelButton_MouseEnter(object sender, System.EventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void cancelButton_MouseDown(object sender, MouseEventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void cancelButton_MouseLeave(object sender, System.EventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void helpButton_MouseEnter(object sender, System.EventArgs e)
        {
            helpButton.BackgroundImage = Images.buttons.buttons.helpbutton_over;
        }

        private void helpButton_MouseDown(object sender, MouseEventArgs e)
        {
            helpButton.BackgroundImage = Images.buttons.buttons.helpbutton_down;
        }

        private void helpButton_MouseLeave(object sender, System.EventArgs e)
        {
            helpButton.BackgroundImage = Images.buttons.buttons.helpbutton;
        }

        private void helpButton_MouseUp(object sender, MouseEventArgs e)
        {
            helpButton.BackgroundImage = Images.buttons.buttons.helpbutton;
        }
    }
}
