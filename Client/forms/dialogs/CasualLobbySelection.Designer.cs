﻿namespace TradingCardGame.forms.dialogs
{
    partial class CasualLobbySelection
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CasualLobbySelection));
            this.cancelButton = new System.Windows.Forms.Button();
            this.joinButton = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.PictureBox();
            this.closeButton = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.mosEisleyButton = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.toscheStationButton = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.mosEspaButton = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.casualGamesButton = new System.Windows.Forms.Panel();
            this.casualGamesInfoLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.helpButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).BeginInit();
            this.panel10.SuspendLayout();
            this.mosEisleyButton.SuspendLayout();
            this.toscheStationButton.SuspendLayout();
            this.mosEspaButton.SuspendLayout();
            this.casualGamesButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Transparent;
            this.cancelButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cancelButton.BackgroundImage")));
            this.cancelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(470, 549);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(111, 29);
            this.cancelButton.TabIndex = 56;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            this.cancelButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cancelButton_MouseDown);
            this.cancelButton.MouseEnter += new System.EventHandler(this.cancelButton_MouseEnter);
            this.cancelButton.MouseLeave += new System.EventHandler(this.cancelButton_MouseLeave);
            // 
            // joinButton
            // 
            this.joinButton.BackColor = System.Drawing.Color.Transparent;
            this.joinButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("joinButton.BackgroundImage")));
            this.joinButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.joinButton.FlatAppearance.BorderSize = 0;
            this.joinButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.joinButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.joinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.joinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinButton.ForeColor = System.Drawing.Color.Black;
            this.joinButton.Location = new System.Drawing.Point(587, 549);
            this.joinButton.Name = "joinButton";
            this.joinButton.Size = new System.Drawing.Size(111, 29);
            this.joinButton.TabIndex = 55;
            this.joinButton.Text = "Join";
            this.joinButton.UseVisualStyleBackColor = false;
            this.joinButton.Click += new System.EventHandler(this.joinButton_Click);
            this.joinButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.joinButton_MouseDown);
            this.joinButton.MouseEnter += new System.EventHandler(this.joinButton_MouseEnter);
            this.joinButton.MouseLeave += new System.EventHandler(this.joinButton_MouseLeave);
            // 
            // helpButton
            // 
            this.helpButton.BackColor = System.Drawing.Color.Transparent;
            this.helpButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("helpButton.BackgroundImage")));
            this.helpButton.Location = new System.Drawing.Point(628, 8);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(42, 37);
            this.helpButton.TabIndex = 54;
            this.helpButton.TabStop = false;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            this.helpButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.helpButton_MouseDown);
            this.helpButton.MouseEnter += new System.EventHandler(this.helpButton_MouseEnter);
            this.helpButton.MouseLeave += new System.EventHandler(this.helpButton_MouseLeave);
            this.helpButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.helpButton_MouseUp);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Transparent;
            this.closeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closeButton.Location = new System.Drawing.Point(667, 8);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(42, 37);
            this.closeButton.TabIndex = 53;
            this.closeButton.TabStop = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            this.closeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.closeButton_MouseDown);
            this.closeButton.MouseEnter += new System.EventHandler(this.closeButton_MouseEnter);
            this.closeButton.MouseLeave += new System.EventHandler(this.closeButton_MouseLeave);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.BackgroundImage = global::TradingCardGame.Images.dialogs.dialogs.title_base;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.label2);
            this.panel10.Location = new System.Drawing.Point(27, 8);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(150, 37);
            this.panel10.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(26, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Select a Lobby";
            // 
            // mosEisleyButton
            // 
            this.mosEisleyButton.BackColor = System.Drawing.Color.Black;
            this.mosEisleyButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mosEisleyButton.BackgroundImage")));
            this.mosEisleyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mosEisleyButton.Controls.Add(this.label14);
            this.mosEisleyButton.Controls.Add(this.label15);
            this.mosEisleyButton.Location = new System.Drawing.Point(366, 158);
            this.mosEisleyButton.Name = "mosEisleyButton";
            this.mosEisleyButton.Size = new System.Drawing.Size(332, 150);
            this.mosEisleyButton.TabIndex = 51;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.SkyBlue;
            this.label14.Location = new System.Drawing.Point(151, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 48);
            this.label14.TabIndex = 1;
            this.label14.Text = "Users: 0 / 8\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.SkyBlue;
            this.label15.Location = new System.Drawing.Point(6, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mos Eisley";
            // 
            // toscheStationButton
            // 
            this.toscheStationButton.BackColor = System.Drawing.Color.Black;
            this.toscheStationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toscheStationButton.BackgroundImage")));
            this.toscheStationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toscheStationButton.Controls.Add(this.label11);
            this.toscheStationButton.Controls.Add(this.label13);
            this.toscheStationButton.Location = new System.Drawing.Point(366, 315);
            this.toscheStationButton.Name = "toscheStationButton";
            this.toscheStationButton.Size = new System.Drawing.Size(332, 150);
            this.toscheStationButton.TabIndex = 50;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SkyBlue;
            this.label11.Location = new System.Drawing.Point(151, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 48);
            this.label11.TabIndex = 1;
            this.label11.Text = "Users: 0 / 8\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.SkyBlue;
            this.label13.Location = new System.Drawing.Point(6, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(130, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Tosche Station";
            // 
            // mosEspaButton
            // 
            this.mosEspaButton.BackColor = System.Drawing.Color.Black;
            this.mosEspaButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mosEspaButton.BackgroundImage")));
            this.mosEspaButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mosEspaButton.Controls.Add(this.label16);
            this.mosEspaButton.Controls.Add(this.label17);
            this.mosEspaButton.Location = new System.Drawing.Point(28, 315);
            this.mosEspaButton.Name = "mosEspaButton";
            this.mosEspaButton.Size = new System.Drawing.Size(332, 150);
            this.mosEspaButton.TabIndex = 49;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.SkyBlue;
            this.label16.Location = new System.Drawing.Point(151, 41);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 48);
            this.label16.TabIndex = 1;
            this.label16.Text = "Users: 0 / 8\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.SkyBlue;
            this.label17.Location = new System.Drawing.Point(6, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Mos Espa";
            // 
            // casualGamesButton
            // 
            this.casualGamesButton.BackColor = System.Drawing.Color.Black;
            this.casualGamesButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("casualGamesButton.BackgroundImage")));
            this.casualGamesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.casualGamesButton.Controls.Add(this.casualGamesInfoLabel);
            this.casualGamesButton.Controls.Add(this.label1);
            this.casualGamesButton.Location = new System.Drawing.Point(28, 158);
            this.casualGamesButton.Name = "casualGamesButton";
            this.casualGamesButton.Size = new System.Drawing.Size(332, 150);
            this.casualGamesButton.TabIndex = 48;
            this.casualGamesButton.Click += new System.EventHandler(this.casualGamesButton_Click);
            // 
            // casualGamesInfoLabel
            // 
            this.casualGamesInfoLabel.AutoSize = true;
            this.casualGamesInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.casualGamesInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.casualGamesInfoLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.casualGamesInfoLabel.Location = new System.Drawing.Point(151, 41);
            this.casualGamesInfoLabel.Name = "casualGamesInfoLabel";
            this.casualGamesInfoLabel.Size = new System.Drawing.Size(106, 48);
            this.casualGamesInfoLabel.TabIndex = 1;
            this.casualGamesInfoLabel.Text = "Users: 0 / 6\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(6, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Casual Games";
            // 
            // CasualLobbySelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.mosEisleyButton);
            this.Controls.Add(this.toscheStationButton);
            this.Controls.Add(this.mosEspaButton);
            this.Controls.Add(this.casualGamesButton);
            this.DoubleBuffered = true;
            this.Name = "CasualLobbySelection";
            this.Size = new System.Drawing.Size(727, 599);
            this.Load += new System.EventHandler(this.CasualLobbySelection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.helpButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.mosEisleyButton.ResumeLayout(false);
            this.mosEisleyButton.PerformLayout();
            this.toscheStationButton.ResumeLayout(false);
            this.toscheStationButton.PerformLayout();
            this.mosEspaButton.ResumeLayout(false);
            this.mosEspaButton.PerformLayout();
            this.casualGamesButton.ResumeLayout(false);
            this.casualGamesButton.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button joinButton;
        private System.Windows.Forms.PictureBox helpButton;
        private System.Windows.Forms.PictureBox closeButton;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel mosEisleyButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel toscheStationButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel mosEspaButton;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel casualGamesButton;
        private System.Windows.Forms.Label casualGamesInfoLabel;
        private System.Windows.Forms.Label label1;
    }
}
