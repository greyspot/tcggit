﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.data;
using TradingCardGame.services.friends;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class Friends : UserControl
    {
        public Friends()
        {
            InitializeComponent();
        }

        FriendsListService friendsListSrv = new FriendsListService();
        Point mouseDownPoint;

        private void exitButton_Click(object sender, EventArgs e)
        {
            TemporaryData.changeDialogStatus();
            SoundService.PlaySound("button");
            Dispose();
        }

        private void Friends_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }

        private void Friends_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDownPoint.IsEmpty)
                Location = new Point(Location.X + (e.X - mouseDownPoint.X), Location.Y + (e.Y - mouseDownPoint.Y));
        }

        private void Friends_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownPoint = Point.Empty;
        }

        private void Friends_Load(object sender, EventArgs e)
        {
            friendsListBox.Items.Clear();

            for (byte i = 0; i < FriendsListService.getFriendsListValue().Split(',').Length; i ++)
                friendsListBox.Items.Add(FriendsListService.getFriendsListValue().Split(',')[i]);
        }
    }
}
