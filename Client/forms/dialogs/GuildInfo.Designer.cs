﻿namespace TradingCardGame.forms.dialogs
{
    partial class GuildInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GuildInfo));
            this.exitButton = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.settingsLabel = new System.Windows.Forms.Label();
            this.membersLabel = new System.Windows.Forms.Label();
            this.overviewLabel = new System.Windows.Forms.Label();
            this.settingsButton = new System.Windows.Forms.PictureBox();
            this.membersButton = new System.Windows.Forms.PictureBox();
            this.overviewButton = new System.Windows.Forms.PictureBox();
            this.titlePanel = new System.Windows.Forms.Panel();
            this.guildTitleLabel = new System.Windows.Forms.Label();
            this.overviewPanel = new System.Windows.Forms.Panel();
            this.averageRatingsListBox = new System.Windows.Forms.ListBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.totalRatingsListBox = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.guildOfficersListBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.numberOfMembersLabel = new System.Windows.Forms.Label();
            this.leaderLabel = new System.Windows.Forms.Label();
            this.rankLabel = new System.Windows.Forms.Label();
            this.formedLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.membersButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.overviewButton)).BeginInit();
            this.titlePanel.SuspendLayout();
            this.overviewPanel.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.exitButton.Image = global::TradingCardGame.Images.buttons.buttons.closebutton;
            this.exitButton.Location = new System.Drawing.Point(640, 6);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(42, 37);
            this.exitButton.TabIndex = 63;
            this.exitButton.TabStop = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            this.exitButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.exitButton_MouseDown);
            this.exitButton.MouseEnter += new System.EventHandler(this.exitButton_MouseEnter);
            this.exitButton.MouseLeave += new System.EventHandler(this.exitButton_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.settingsLabel);
            this.panel1.Controls.Add(this.membersLabel);
            this.panel1.Controls.Add(this.overviewLabel);
            this.panel1.Controls.Add(this.settingsButton);
            this.panel1.Controls.Add(this.membersButton);
            this.panel1.Controls.Add(this.overviewButton);
            this.panel1.Location = new System.Drawing.Point(23, 54);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 475);
            this.panel1.TabIndex = 64;
            // 
            // settingsLabel
            // 
            this.settingsLabel.AutoSize = true;
            this.settingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingsLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.settingsLabel.Location = new System.Drawing.Point(46, 272);
            this.settingsLabel.Name = "settingsLabel";
            this.settingsLabel.Size = new System.Drawing.Size(56, 16);
            this.settingsLabel.TabIndex = 5;
            this.settingsLabel.Text = "Settings";
            // 
            // membersLabel
            // 
            this.membersLabel.AutoSize = true;
            this.membersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.membersLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.membersLabel.Location = new System.Drawing.Point(42, 176);
            this.membersLabel.Name = "membersLabel";
            this.membersLabel.Size = new System.Drawing.Size(65, 16);
            this.membersLabel.TabIndex = 4;
            this.membersLabel.Text = "Members";
            // 
            // overviewLabel
            // 
            this.overviewLabel.AutoSize = true;
            this.overviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overviewLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.overviewLabel.Location = new System.Drawing.Point(42, 80);
            this.overviewLabel.Name = "overviewLabel";
            this.overviewLabel.Size = new System.Drawing.Size(64, 16);
            this.overviewLabel.TabIndex = 3;
            this.overviewLabel.Text = "Overview";
            // 
            // settingsButton
            // 
            this.settingsButton.Image = global::TradingCardGame.Images.guild.guild.guild_settings;
            this.settingsButton.Location = new System.Drawing.Point(31, 195);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(87, 74);
            this.settingsButton.TabIndex = 2;
            this.settingsButton.TabStop = false;
            this.settingsButton.Click += new System.EventHandler(this.settingsButton_Click);
            // 
            // membersButton
            // 
            this.membersButton.Image = global::TradingCardGame.Images.guild.guild.guild_members;
            this.membersButton.Location = new System.Drawing.Point(31, 99);
            this.membersButton.Name = "membersButton";
            this.membersButton.Size = new System.Drawing.Size(87, 74);
            this.membersButton.TabIndex = 1;
            this.membersButton.TabStop = false;
            this.membersButton.Click += new System.EventHandler(this.membersButton_Click);
            // 
            // overviewButton
            // 
            this.overviewButton.Image = global::TradingCardGame.Images.guild.guild.guild_overview;
            this.overviewButton.Location = new System.Drawing.Point(31, 3);
            this.overviewButton.Name = "overviewButton";
            this.overviewButton.Size = new System.Drawing.Size(87, 74);
            this.overviewButton.TabIndex = 0;
            this.overviewButton.TabStop = false;
            this.overviewButton.Click += new System.EventHandler(this.overviewButton_Click);
            // 
            // titlePanel
            // 
            this.titlePanel.BackColor = System.Drawing.Color.Transparent;
            this.titlePanel.BackgroundImage = global::TradingCardGame.Images.dialogs.dialogs.title_base;
            this.titlePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.titlePanel.Controls.Add(this.guildTitleLabel);
            this.titlePanel.Location = new System.Drawing.Point(51, 6);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(225, 35);
            this.titlePanel.TabIndex = 65;
            // 
            // guildTitleLabel
            // 
            this.guildTitleLabel.AutoSize = true;
            this.guildTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.guildTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildTitleLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.guildTitleLabel.Location = new System.Drawing.Point(40, 8);
            this.guildTitleLabel.Name = "guildTitleLabel";
            this.guildTitleLabel.Size = new System.Drawing.Size(145, 18);
            this.guildTitleLabel.TabIndex = 0;
            this.guildTitleLabel.Text = "Guild: Guild Name";
            // 
            // overviewPanel
            // 
            this.overviewPanel.Controls.Add(this.averageRatingsListBox);
            this.overviewPanel.Controls.Add(this.panel5);
            this.overviewPanel.Controls.Add(this.totalRatingsListBox);
            this.overviewPanel.Controls.Add(this.panel4);
            this.overviewPanel.Controls.Add(this.guildOfficersListBox);
            this.overviewPanel.Controls.Add(this.panel3);
            this.overviewPanel.Controls.Add(this.numberOfMembersLabel);
            this.overviewPanel.Controls.Add(this.leaderLabel);
            this.overviewPanel.Controls.Add(this.rankLabel);
            this.overviewPanel.Controls.Add(this.formedLabel);
            this.overviewPanel.Controls.Add(this.label8);
            this.overviewPanel.Controls.Add(this.label7);
            this.overviewPanel.Controls.Add(this.label6);
            this.overviewPanel.Controls.Add(this.label5);
            this.overviewPanel.Location = new System.Drawing.Point(179, 54);
            this.overviewPanel.Name = "overviewPanel";
            this.overviewPanel.Size = new System.Drawing.Size(503, 475);
            this.overviewPanel.TabIndex = 70;
            // 
            // averageRatingsListBox
            // 
            this.averageRatingsListBox.BackColor = System.Drawing.Color.Black;
            this.averageRatingsListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.averageRatingsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageRatingsListBox.ForeColor = System.Drawing.Color.White;
            this.averageRatingsListBox.FormattingEnabled = true;
            this.averageRatingsListBox.ItemHeight = 16;
            this.averageRatingsListBox.Items.AddRange(new object[] {
            "Overall\t\t1500",
            "Limited\t\t1500",
            "Constructed\t1500"});
            this.averageRatingsListBox.Location = new System.Drawing.Point(8, 365);
            this.averageRatingsListBox.Name = "averageRatingsListBox";
            this.averageRatingsListBox.Size = new System.Drawing.Size(492, 48);
            this.averageRatingsListBox.TabIndex = 83;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(8, 324);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(492, 35);
            this.panel5.TabIndex = 82;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.SkyBlue;
            this.label12.Location = new System.Drawing.Point(6, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 20);
            this.label12.TabIndex = 74;
            this.label12.Text = "Average Ratings";
            // 
            // totalRatingsListBox
            // 
            this.totalRatingsListBox.BackColor = System.Drawing.Color.Black;
            this.totalRatingsListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.totalRatingsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalRatingsListBox.ForeColor = System.Drawing.Color.White;
            this.totalRatingsListBox.FormattingEnabled = true;
            this.totalRatingsListBox.ItemHeight = 16;
            this.totalRatingsListBox.Items.AddRange(new object[] {
            "Overall\t\t1500",
            "Limited\t\t1500",
            "Constructed\t1500"});
            this.totalRatingsListBox.Location = new System.Drawing.Point(8, 270);
            this.totalRatingsListBox.Name = "totalRatingsListBox";
            this.totalRatingsListBox.Size = new System.Drawing.Size(492, 48);
            this.totalRatingsListBox.TabIndex = 81;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label11);
            this.panel4.Location = new System.Drawing.Point(8, 229);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(492, 35);
            this.panel4.TabIndex = 80;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SkyBlue;
            this.label11.Location = new System.Drawing.Point(6, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 20);
            this.label11.TabIndex = 74;
            this.label11.Text = "Total Ratings";
            // 
            // guildOfficersListBox
            // 
            this.guildOfficersListBox.BackColor = System.Drawing.Color.Black;
            this.guildOfficersListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.guildOfficersListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildOfficersListBox.ForeColor = System.Drawing.Color.White;
            this.guildOfficersListBox.FullRowSelect = true;
            this.guildOfficersListBox.Location = new System.Drawing.Point(8, 123);
            this.guildOfficersListBox.MultiSelect = false;
            this.guildOfficersListBox.Name = "guildOfficersListBox";
            this.guildOfficersListBox.Size = new System.Drawing.Size(492, 100);
            this.guildOfficersListBox.TabIndex = 79;
            this.guildOfficersListBox.UseCompatibleStateImageBehavior = false;
            this.guildOfficersListBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Username";
            this.columnHeader1.Width = 288;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label10);
            this.panel3.Location = new System.Drawing.Point(8, 82);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(492, 35);
            this.panel3.TabIndex = 78;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.SkyBlue;
            this.label10.Location = new System.Drawing.Point(6, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 20);
            this.label10.TabIndex = 74;
            this.label10.Text = "Officers";
            // 
            // numberOfMembersLabel
            // 
            this.numberOfMembersLabel.AutoSize = true;
            this.numberOfMembersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfMembersLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.numberOfMembersLabel.Location = new System.Drawing.Point(153, 52);
            this.numberOfMembersLabel.Name = "numberOfMembersLabel";
            this.numberOfMembersLabel.Size = new System.Drawing.Size(15, 16);
            this.numberOfMembersLabel.TabIndex = 77;
            this.numberOfMembersLabel.Text = "1";
            // 
            // leaderLabel
            // 
            this.leaderLabel.AutoSize = true;
            this.leaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaderLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.leaderLabel.Location = new System.Drawing.Point(59, 36);
            this.leaderLabel.Name = "leaderLabel";
            this.leaderLabel.Size = new System.Drawing.Size(54, 16);
            this.leaderLabel.TabIndex = 76;
            this.leaderLabel.Text = "Shocho";
            // 
            // rankLabel
            // 
            this.rankLabel.AutoSize = true;
            this.rankLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rankLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.rankLabel.Location = new System.Drawing.Point(51, 20);
            this.rankLabel.Name = "rankLabel";
            this.rankLabel.Size = new System.Drawing.Size(15, 16);
            this.rankLabel.TabIndex = 75;
            this.rankLabel.Text = "4";
            // 
            // formedLabel
            // 
            this.formedLabel.AutoSize = true;
            this.formedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formedLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.formedLabel.Location = new System.Drawing.Point(67, 4);
            this.formedLabel.Name = "formedLabel";
            this.formedLabel.Size = new System.Drawing.Size(51, 16);
            this.formedLabel.TabIndex = 74;
            this.formedLabel.Text = "7/25/08";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.SkyBlue;
            this.label8.Location = new System.Drawing.Point(5, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(151, 16);
            this.label8.TabIndex = 73;
            this.label8.Text = "Number of Members:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SkyBlue;
            this.label7.Location = new System.Drawing.Point(5, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 72;
            this.label7.Text = "Leader:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.SkyBlue;
            this.label6.Location = new System.Drawing.Point(5, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 16);
            this.label6.TabIndex = 71;
            this.label6.Text = "Rank:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.SkyBlue;
            this.label5.Location = new System.Drawing.Point(5, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 70;
            this.label5.Text = "Formed:";
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "bronze");
            this.imageList.Images.SetKeyName(1, "yes");
            // 
            // GuildInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.overviewPanel);
            this.Controls.Add(this.titlePanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.exitButton);
            this.DoubleBuffered = true;
            this.Name = "GuildInfo";
            this.Size = new System.Drawing.Size(700, 550);
            this.Load += new System.EventHandler(this.GuildInfo_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GuildInfo_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GuildInfo_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GuildInfo_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.membersButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.overviewButton)).EndInit();
            this.titlePanel.ResumeLayout(false);
            this.titlePanel.PerformLayout();
            this.overviewPanel.ResumeLayout(false);
            this.overviewPanel.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox exitButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox overviewButton;
        private System.Windows.Forms.PictureBox membersButton;
        private System.Windows.Forms.PictureBox settingsButton;
        private System.Windows.Forms.Label overviewLabel;
        private System.Windows.Forms.Label membersLabel;
        private System.Windows.Forms.Label settingsLabel;
        private System.Windows.Forms.Panel titlePanel;
        private System.Windows.Forms.Label guildTitleLabel;
        private System.Windows.Forms.Panel overviewPanel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label formedLabel;
        private System.Windows.Forms.Label rankLabel;
        private System.Windows.Forms.Label leaderLabel;
        private System.Windows.Forms.Label numberOfMembersLabel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ListView guildOfficersListBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListBox totalRatingsListBox;
        private System.Windows.Forms.ListBox averageRatingsListBox;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ImageList imageList;
    }
}