﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs;
using TradingCardGame.services.account;
using TradingCardGame.services.data;
using TradingCardGame.services.database;
using TradingCardGame.services.guild;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms
{
    public partial class Guild : UserControl
    {
        public Guild()
        {
            InitializeComponent();
        }

        private void GuildPage_Load(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton;
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg;

            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);

            BringToFront();

            if (GuildService.userIsInAGuild(AccountService.GetUsername()))
            {
                createGuildButton.BackgroundImage = Images.buttons.buttons.button_lg_disabled;
                leaveGuildButton.BackgroundImage = Images.buttons.buttons.button_lg;
            } 
            else
            {
                createGuildButton.BackgroundImage = Images.buttons.buttons.button_lg;
                leaveGuildButton.BackgroundImage = Images.buttons.buttons.button_lg_disabled;
            }

            creatingAGuildDescriptionLabel.Text = Properties.Resources.creatingAGuildDescriptionText;
            joiningAGuildDescriptionLabel.Text = Properties.Resources.joiningAGuildDescriptionText;

            if (GuildService.getNumberOfGuilds() == 0)
                return;

            string query = "SELECT * FROM guilds";
            MySqlCommand cmd = new MySqlCommand(query, DatabaseService.GetMySqlConnection(1));

                if (DatabaseService.GetMySqlConnection(1).State == ConnectionState.Closed)
                    DatabaseService.OpenConnectionIfNotOpen(1);

            MySqlDataReader dataReader = cmd.ExecuteReader();
            string guildName = null;
            while (dataReader.Read())
                guildName = dataReader["guildname"].ToString();
            dataReader.Close();

            string guildAbbrev = DatabaseService.GetValue("guilds", "guildabbrev", "guildname", guildName);
            string guildLeader = DatabaseService.GetValue("guilds", "guildleader", "guildabbrev", guildAbbrev);
            if (DatabaseService.GetMySqlConnection(1).State == ConnectionState.Closed)
                DatabaseService.OpenConnectionIfNotOpen(1);

            MySqlDataReader dataReader1 = cmd.ExecuteReader();

            while (dataReader1.Read())
            {
                ListViewItem guild = new ListViewItem("1");
                guild.SubItems.Add(guildName);
                guild.SubItems.Add(GuildService.getTotalGuildMembers(guildAbbrev).ToString());
                guild.SubItems.Add(guildLeader);
                guildsListViewer.Items.Add(guild);
            }
            DatabaseService.GetMySqlConnection(1).Close();

            numberOfGuildsLabel.Text = guildsListViewer.Items.Count + " guilds.";
            numberOfFriendsGuildsLabel.Text = "0 guilds with friends.";
        }

        private void GuildPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            Map Map = new Map();
            Map.Show();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void createGuildButton_Click(object sender, EventArgs e)
        {
            if (GuildService.userIsInAGuild(AccountService.GetUsername()) || TemporaryData.getDialogStatus())
                return;

            SoundService.PlaySound("button");

            TemporaryData.changeDialogStatus();
            Parent.Controls.Add(new CreateGuild());
        }

        private void leaveGuildButton_Click(object sender, EventArgs e)
        {
            if (!GuildService.userIsInAGuild(AccountService.GetUsername()))
                return;

            SoundService.PlaySound("button");

            DialogResult dr = MessageBox.Show("Are you sure you want to leave the guild '" + GuildService.getUsersGuild(AccountService.GetUsername()) + "'?", "", MessageBoxButtons.YesNoCancel);

            if (dr == DialogResult.Yes)
                GuildService.removeUserFromGuild(AccountService.GetUsername());
        }

        private void createGuildButton_MouseEnter(object sender, EventArgs e)
        {
            createGuildButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void createGuildButton_MouseDown(object sender, MouseEventArgs e)
        {
            createGuildButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void createGuildButton_MouseLeave(object sender, EventArgs e)
        {
            createGuildButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void leaveGuildButton_MouseEnter(object sender, EventArgs e)
        {
            if (GuildService.userIsInAGuild(AccountService.GetUsername()))
                leaveGuildButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void leaveGuildButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (GuildService.userIsInAGuild(AccountService.GetUsername()))
                leaveGuildButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void leaveGuildButton_MouseLeave(object sender, EventArgs e)
        {
            if (GuildService.userIsInAGuild(AccountService.GetUsername()))
                leaveGuildButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void closeButton_MouseEnter(object sender, EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void closeButton_MouseDown(object sender, MouseEventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void closeButton_MouseLeave(object sender, EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton_over;
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton_down;
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = Images.buttons.buttons.closebutton;
        }

        private void createGuildButton_MouseUp(object sender, MouseEventArgs e)
        {
            createGuildButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void leaveGuildButton_MouseUp(object sender, MouseEventArgs e)
        {
            createGuildButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }
    }
}
