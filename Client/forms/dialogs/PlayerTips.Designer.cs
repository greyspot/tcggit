﻿namespace TradingCardGame.forms.dialogs
{
    partial class PlayerTips
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerTips));
            this.label4 = new System.Windows.Forms.Label();
            this.newbieTipCheckBox = new System.Windows.Forms.PictureBox();
            this.newbieMessageHelpButton = new System.Windows.Forms.PictureBox();
            this.newbieMessageExitButton = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.helpTextLabel = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.newbieTipCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newbieMessageHelpButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newbieMessageExitButton)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label4.Location = new System.Drawing.Point(58, 556);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "Don\'t show this tip.";
            // 
            // newbieTipCheckBox
            // 
            this.newbieTipCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.newbieTipCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.newbieTipCheckBox.Image = ((System.Drawing.Image)(resources.GetObject("newbieTipCheckBox.Image")));
            this.newbieTipCheckBox.Location = new System.Drawing.Point(29, 553);
            this.newbieTipCheckBox.Name = "newbieTipCheckBox";
            this.newbieTipCheckBox.Size = new System.Drawing.Size(23, 20);
            this.newbieTipCheckBox.TabIndex = 12;
            this.newbieTipCheckBox.TabStop = false;
            this.newbieTipCheckBox.Click += new System.EventHandler(this.newbieTipCheckBox_Click);
            // 
            // newbieMessageHelpButton
            // 
            this.newbieMessageHelpButton.BackColor = System.Drawing.Color.Transparent;
            this.newbieMessageHelpButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("newbieMessageHelpButton.BackgroundImage")));
            this.newbieMessageHelpButton.Location = new System.Drawing.Point(679, 8);
            this.newbieMessageHelpButton.Name = "newbieMessageHelpButton";
            this.newbieMessageHelpButton.Size = new System.Drawing.Size(42, 37);
            this.newbieMessageHelpButton.TabIndex = 11;
            this.newbieMessageHelpButton.TabStop = false;
            this.newbieMessageHelpButton.Click += new System.EventHandler(this.newbieMessageHelpButton_Click);
            this.newbieMessageHelpButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.newbieMessageHelpButton_MouseDown);
            this.newbieMessageHelpButton.MouseEnter += new System.EventHandler(this.newbieMessageHelpButton_MouseEnter);
            this.newbieMessageHelpButton.MouseLeave += new System.EventHandler(this.newbieMessageHelpButton_MouseLeave);
            this.newbieMessageHelpButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.newbieMessageHelpButton_MouseUp);
            // 
            // newbieMessageExitButton
            // 
            this.newbieMessageExitButton.BackColor = System.Drawing.Color.Transparent;
            this.newbieMessageExitButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("newbieMessageExitButton.BackgroundImage")));
            this.newbieMessageExitButton.Location = new System.Drawing.Point(727, 8);
            this.newbieMessageExitButton.Name = "newbieMessageExitButton";
            this.newbieMessageExitButton.Size = new System.Drawing.Size(42, 37);
            this.newbieMessageExitButton.TabIndex = 10;
            this.newbieMessageExitButton.TabStop = false;
            this.newbieMessageExitButton.Click += new System.EventHandler(this.newbieMessageExitButton_Click);
            this.newbieMessageExitButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.newbieMessageExitButton_MouseDown);
            this.newbieMessageExitButton.MouseEnter += new System.EventHandler(this.newbieMessageExitButton_MouseEnter);
            this.newbieMessageExitButton.MouseLeave += new System.EventHandler(this.newbieMessageExitButton_MouseLeave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(61, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(150, 37);
            this.panel2.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SkyBlue;
            this.label3.Location = new System.Drawing.Point(27, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Player Tips";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(658, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(107, 152);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // helpTextLabel
            // 
            this.helpTextLabel.AutoSize = true;
            this.helpTextLabel.BackColor = System.Drawing.Color.Transparent;
            this.helpTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpTextLabel.ForeColor = System.Drawing.Color.White;
            this.helpTextLabel.Location = new System.Drawing.Point(30, 66);
            this.helpTextLabel.Name = "helpTextLabel";
            this.helpTextLabel.Size = new System.Drawing.Size(80, 16);
            this.helpTextLabel.TabIndex = 15;
            this.helpTextLabel.Text = "placeholder";
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Transparent;
            this.closeButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("closeButton.BackgroundImage")));
            this.closeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.closeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.ForeColor = System.Drawing.Color.Black;
            this.closeButton.Location = new System.Drawing.Point(644, 553);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(125, 30);
            this.closeButton.TabIndex = 48;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            this.closeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.closeButton_MouseDown);
            this.closeButton.MouseEnter += new System.EventHandler(this.closeButton_MouseEnter);
            this.closeButton.MouseLeave += new System.EventHandler(this.closeButton_MouseLeave);
            // 
            // PlayerTips
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.helpTextLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.newbieTipCheckBox);
            this.Controls.Add(this.newbieMessageHelpButton);
            this.Controls.Add(this.newbieMessageExitButton);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.Name = "PlayerTips";
            this.Size = new System.Drawing.Size(790, 600);
            this.Load += new System.EventHandler(this.PlayerTips_Load);
            ((System.ComponentModel.ISupportInitialize)(this.newbieTipCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newbieMessageHelpButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newbieMessageExitButton)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox newbieTipCheckBox;
        private System.Windows.Forms.PictureBox newbieMessageHelpButton;
        private System.Windows.Forms.PictureBox newbieMessageExitButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label helpTextLabel;
        private System.Windows.Forms.Button closeButton;
    }
}
