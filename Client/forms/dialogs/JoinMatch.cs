﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class JoinMatch : Form
    {
        public JoinMatch()
        {
            InitializeComponent();
        }

        Point mouseDownPoint;

        private void joinButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(services.database.DatabaseService.GetValue("matches", "password", "title", matchTitleLabel.Text.Replace("'", "''"))))
            {
                if (!services.database.DatabaseService.GetValue("matches", "password", "title", matchTitleLabel.Text.Replace("'", "''")).Equals(joinMatchPasswordTextBox.Text))
                {
                    MessageBox.Show("Sorry, but the password you entered is incorrect.");
                    return;
                }
            }

            if (string.IsNullOrEmpty(deckSelectionTextBox.Text))
            {
                MessageBox.Show("Please select a deck to use in battle.");
                return;
            }

            // TODO: Get if selected match is full
            if (openSlotAvailable() == -1)
            {
                MessageBox.Show("Sorry, but this match is full.");
                return;
            }

            //MultiplayerService.removePlayerFromLobby();

            LaunchMatch launchMatch = new LaunchMatch();
            launchMatch.Show();
        }

        private int openSlotAvailable()
        {
            /*
            string maxPlayers = DatabaseService.getValue("matches", "numberofplayers", "title", Properties.Settings.Default.matchTitle);
            int maxPlayersNumber = 0;

            if (maxPlayers.Equals("Two Players"))
                maxPlayersNumber = 2;

            string matchId = DatabaseService.getValue("matches", "id", "title", Properties.Settings.Default.matchTitle);

            for (int i = 0; i < maxPlayersNumber; i++)
            {
                if (string.IsNullOrEmpty(DatabaseService.getValue("lobbies", "position" + i, "type", matchId)))
                    return i;
            }
            */
            return -1;
        }

        private void JoinMatch_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.cardSetSelected))
            {
                deckStatusPictureBox.Image = Images.icons.icons.yes;
                deckSelectionTextBox.Text = Properties.Settings.Default.cardSetSelected;
            }
                

            //matchTitleLabel.Text = Properties.Settings.Default.matchTitle;
            joinMatchFormatLabel.Text = "Format: " + services.database.DatabaseService.GetValue("matches", "matchtype", "title", matchTitleLabel.Text.Replace("'", "''"));

            if (string.IsNullOrEmpty(services.database.DatabaseService.GetValue("matches", "password", "title", matchTitleLabel.Text.Replace("'", "''"))))
            {
                passwordLabel.Visible = false;
                joinMatchPasswordTextBox.Visible = false;
            }
        }

        private void exitJoinMatchButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void selectDeckButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.showDeckSelector = true;
            Properties.Settings.Default.Save();

            CreateMatch createMatch = new CreateMatch();
            createMatch.Show();
            SoundService.PlaySound("button");
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton_over;
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton_down;
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            exitButton.Image = Images.buttons.buttons.closebutton;
        }

        private void JoinMatch_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDownPoint.IsEmpty)
            {
                Form f = sender as Form;
                f.Location = new Point(f.Location.X + (e.X - mouseDownPoint.X), f.Location.Y + (e.Y - mouseDownPoint.Y));
            }
        }

        private void JoinMatch_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }

        private void JoinMatch_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownPoint = Point.Empty;
        }
    }
}
