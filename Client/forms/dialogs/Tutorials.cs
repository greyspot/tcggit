﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms.dialogs
{
    public partial class Tutorials : UserControl
    {
        public Tutorials()
        {
            InitializeComponent();
        }

        private PictureBox lastClicked;

        private void closeButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void Tutorials_Load(object sender, System.EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void closeButton_MouseDown(object sender, MouseEventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.closebutton_down;
        }

        private void closeButton_MouseEnter(object sender, System.EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.closebutton_over;
        }

        private void closeButton_MouseLeave(object sender, System.EventArgs e)
        {
            closeButton.BackgroundImage = Images.buttons.buttons.closebutton;
        }

        private void helpButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void dealWithHover(PictureBox picturebox)
        {
            if (lastClicked == null)
                lastClicked = tutorial1Button;

            for (byte i = 1; i < 12; i++)
            {
                if (lastClicked == ((PictureBox)firstPanel.Controls["tutorial" + i + "Button"]))
                    lastClicked.Image = (Image)Images.tutorial_nav.tutorial_nav.ResourceManager.GetObject("tutorial_" + i);

                if (picturebox == ((PictureBox)firstPanel.Controls["tutorial" + i + "Button"]))
                {
                    tutorialTitlePictureBox.Image = (Image)Images.tutorial_nav.tutorial_nav.ResourceManager.GetObject("_" + i + getImageTitle(i) + "_text");
                    picturebox.Image = (Image)Images.tutorial_nav.tutorial_nav.ResourceManager.GetObject("tutorial_" + i + "_hover");
                    tutorialAboutTextLabel.Text = Properties.Resources.ResourceManager.GetString("tutorial" + i + "DescriptionText");
                    tutorialAboutTextLabel.Location = new Point((centeringPanel.Width - tutorialAboutTextLabel.Width) / 2, 0);
                }  
            }

            lastClicked = picturebox;
        }

        private string getImageTitle(int i)
        {
            if (i == 1)
                return "getting_started";
            else if (i == 2)
                return "avatar";
            else if (i == 3)
                return "quests";
            else if (i == 4)
                return "units";
            else if (i == 5)
                return "abilities";
            else if (i == 6)
                return "items";
            else if (i == 7)
                return "tactics_combat";
            else if (i == 8)
                return "unit_combat";
            else if (i == 9)
                return "avatar_combat";
            else if (i == 10)
                return "summary";
            else if (i == 11)
                return "final_trial";
            return null;
        }

        private void tutorial1Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial2Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial3Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial4Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial5Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial6Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial7Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial8Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial9Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial10Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial11Button_Click(object sender, System.EventArgs e)
        {

        }

        private void tutorial1Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial1Button);
        }

        private void tutorial2Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial2Button);
        }

        private void tutorial3Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial3Button);
        }

        private void tutorial4Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial4Button);
        }

        private void tutorial5Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial5Button);
        }

        private void tutorial6Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial6Button);
        }

        private void tutorial7Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial7Button);
        }

        private void tutorial8Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial8Button);
        }

        private void tutorial9Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial9Button);
        }

        private void tutorial10Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial10Button);
        }

        private void tutorial11Button_MouseEnter(object sender, System.EventArgs e)
        {
            dealWithHover(tutorial11Button);
        }
    }
}
