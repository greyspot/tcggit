﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.guild;
using TradingCardGame.services.sound;
using TradingCardGame.services.account;
using TradingCardGame.services.data;

namespace TradingCardGame.forms.dialogs
{
    public partial class CreateGuild : UserControl
    {
        public CreateGuild()
        {
            InitializeComponent();
        }

        private Point p;

        private void CreateGuild_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (GuildService.userIsInAGuild(AccountService.GetUsername()))
            {
                MessageBox.Show("You cannot create a guild if you are already in one.");
                return;
            }

            if (string.IsNullOrWhiteSpace(guildNameTextBox.Text) || string.IsNullOrWhiteSpace(guildAbbreviationTextBox.Text))
            {
                MessageBox.Show("Please fill in both the Guild Name and Guild Abbreviation.");
                return;
            }

            TemporaryData.changeDialogStatus();
            GuildService.createGuild(guildNameTextBox.Text, guildAbbreviationTextBox.Text);
            MessageBox.Show("The guild '" + guildNameTextBox.Text + "' has successfully been created.");
            Parent.Controls.Add(new GuildInfo());
            Dispose();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            TemporaryData.changeDialogStatus();
            SoundService.PlaySound("button");
            Dispose();
        }

        private void createButton_MouseEnter(object sender, EventArgs e)
        {
            createButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void createButton_MouseDown(object sender, MouseEventArgs e)
        {
            createButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void createButton_MouseLeave(object sender, EventArgs e)
        {
            createButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void cancelButton_MouseEnter(object sender, EventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void cancelButton_MouseLeave(object sender, EventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void cancelButton_MouseDown(object sender, MouseEventArgs e)
        {
            cancelButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void CreateGuild_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void CreateGuild_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }
    }
}
