﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.forms.widgets;
using TradingCardGame.services.data;

namespace TradingCardGame
{
    public partial class DeckBuilder : UserControl
    {
        public DeckBuilder()
        {
            InitializeComponent();
        }

        Point p = new Point();

        private void DeckBuilder_FormClosing(object sender, FormClosingEventArgs e)
        {
            Map Map = new Map();
            Map.Show();
        }

        private void scrollbar1_MouseDown(object sender, MouseEventArgs e)
        {
            p.Y = e.Y;
        }

        private void scrollbar1_MouseMove(object sender, MouseEventArgs e)
        {
            // TODO: work on scrolling ranges
            if (e.Button == MouseButtons.Left)
                scrollbar1.Location = new Point(scrollbar1.Location.X, e.Y - p.Y + scrollbar1.Location.Y);
        }

        private void scrollbar1_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void topScrollButton1_Click(object sender, System.EventArgs e)
        {
            scrollbar1.Location = new Point(scrollbar1.Location.X, scrollbar1.Location.Y - 5);
        }

        private void bottomScrollButton1_Click(object sender, System.EventArgs e)
        {
            scrollbar1.Location = new Point(scrollbar1.Location.X, scrollbar1.Location.Y + 5);
        }

        private void DeckBuilder_Load(object sender, System.EventArgs e)
        {
            filterButton.BackgroundImage = Images.buttons.buttons.button_lg;
            searchButton.BackgroundImage = Images.buttons.buttons.button_lg;
            showValidQuestsButton.BackgroundImage = Images.buttons.buttons.button_lg;
            removeAllFiltersButton.BackgroundImage = Images.buttons.buttons.button_lg;
            standardButton.BackgroundImage = Images.buttons.buttons.button_lg;
            validDeckPictureBox.BackgroundImage = Images.buttons.buttons.button_validate;

            TemporaryData.setCurrentForm(2);
            Navigator navBar = new Navigator();
            Controls.Add(navBar);
            navBar.BringToFront();
        }
    }
}
