﻿namespace TradingCardGame.forms
{
    partial class LaunchMatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LaunchMatch));
            this.panel10 = new System.Windows.Forms.Panel();
            this.panelTitleLabel = new System.Windows.Forms.Label();
            this.userPanel1 = new System.Windows.Forms.Panel();
            this.readyToGoCheckBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.usernameLabel1 = new System.Windows.Forms.Label();
            this.avatarPictureBox1 = new System.Windows.Forms.PictureBox();
            this.userPanel2 = new System.Windows.Forms.Panel();
            this.readyToGoCheckBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.usernameLabel2 = new System.Windows.Forms.Label();
            this.avatarPictureBox2 = new System.Windows.Forms.PictureBox();
            this.exitButton = new System.Windows.Forms.PictureBox();
            this.playersNameListBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.inputChatBox = new System.Windows.Forms.TextBox();
            this.chatBox = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.matchTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numberOfPlayersListBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.playFormatComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.matchStructureComboBox = new System.Windows.Forms.ComboBox();
            this.timeLimitComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.guildOnlyComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.allowObserversCheckBox = new System.Windows.Forms.PictureBox();
            this.friendsOnlyCheckBox = new System.Windows.Forms.PictureBox();
            this.lightVsDarkCheckBox = new System.Windows.Forms.PictureBox();
            this.panel10.SuspendLayout();
            this.userPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readyToGoCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox1)).BeginInit();
            this.userPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readyToGoCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowObserversCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.friendsOnlyCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightVsDarkCheckBox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.BackgroundImage = global::TradingCardGame.Images.dialogs.dialogs.title_base;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.panelTitleLabel);
            this.panel10.Location = new System.Drawing.Point(21, 5);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(150, 37);
            this.panel10.TabIndex = 52;
            // 
            // panelTitleLabel
            // 
            this.panelTitleLabel.AutoSize = true;
            this.panelTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.panelTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelTitleLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.panelTitleLabel.Location = new System.Drawing.Point(23, 9);
            this.panelTitleLabel.Name = "panelTitleLabel";
            this.panelTitleLabel.Size = new System.Drawing.Size(104, 18);
            this.panelTitleLabel.TabIndex = 0;
            this.panelTitleLabel.Text = "Match #2654";
            // 
            // userPanel1
            // 
            this.userPanel1.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.userPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.userPanel1.Controls.Add(this.readyToGoCheckBox1);
            this.userPanel1.Controls.Add(this.label2);
            this.userPanel1.Controls.Add(this.usernameLabel1);
            this.userPanel1.Controls.Add(this.avatarPictureBox1);
            this.userPanel1.Location = new System.Drawing.Point(31, 55);
            this.userPanel1.Name = "userPanel1";
            this.userPanel1.Size = new System.Drawing.Size(250, 95);
            this.userPanel1.TabIndex = 53;
            // 
            // readyToGoCheckBox1
            // 
            this.readyToGoCheckBox1.Location = new System.Drawing.Point(170, 41);
            this.readyToGoCheckBox1.Name = "readyToGoCheckBox1";
            this.readyToGoCheckBox1.Size = new System.Drawing.Size(23, 20);
            this.readyToGoCheckBox1.TabIndex = 9;
            this.readyToGoCheckBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ready to go:";
            // 
            // usernameLabel1
            // 
            this.usernameLabel1.AutoSize = true;
            this.usernameLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel1.Location = new System.Drawing.Point(101, 18);
            this.usernameLabel1.Name = "usernameLabel1";
            this.usernameLabel1.Size = new System.Drawing.Size(85, 18);
            this.usernameLabel1.TabIndex = 1;
            this.usernameLabel1.Text = "Username1";
            // 
            // avatarPictureBox1
            // 
            this.avatarPictureBox1.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.avatarPictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.avatarPictureBox1.Image = global::TradingCardGame.Images.draft.draft.draft_avatar_seat;
            this.avatarPictureBox1.Location = new System.Drawing.Point(10, 3);
            this.avatarPictureBox1.Name = "avatarPictureBox1";
            this.avatarPictureBox1.Size = new System.Drawing.Size(85, 85);
            this.avatarPictureBox1.TabIndex = 0;
            this.avatarPictureBox1.TabStop = false;
            // 
            // userPanel2
            // 
            this.userPanel2.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.userPanel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.userPanel2.Controls.Add(this.readyToGoCheckBox2);
            this.userPanel2.Controls.Add(this.label1);
            this.userPanel2.Controls.Add(this.usernameLabel2);
            this.userPanel2.Controls.Add(this.avatarPictureBox2);
            this.userPanel2.Location = new System.Drawing.Point(31, 156);
            this.userPanel2.Name = "userPanel2";
            this.userPanel2.Size = new System.Drawing.Size(250, 95);
            this.userPanel2.TabIndex = 54;
            this.userPanel2.Visible = false;
            // 
            // readyToGoCheckBox2
            // 
            this.readyToGoCheckBox2.Location = new System.Drawing.Point(170, 41);
            this.readyToGoCheckBox2.Name = "readyToGoCheckBox2";
            this.readyToGoCheckBox2.Size = new System.Drawing.Size(23, 20);
            this.readyToGoCheckBox2.TabIndex = 9;
            this.readyToGoCheckBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ready to go:";
            // 
            // usernameLabel2
            // 
            this.usernameLabel2.AutoSize = true;
            this.usernameLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel2.Location = new System.Drawing.Point(101, 18);
            this.usernameLabel2.Name = "usernameLabel2";
            this.usernameLabel2.Size = new System.Drawing.Size(85, 18);
            this.usernameLabel2.TabIndex = 1;
            this.usernameLabel2.Text = "Username1";
            // 
            // avatarPictureBox2
            // 
            this.avatarPictureBox2.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.avatarPictureBox2.Image = global::TradingCardGame.Images.draft.draft.draft_avatar_seat;
            this.avatarPictureBox2.Location = new System.Drawing.Point(10, 3);
            this.avatarPictureBox2.Name = "avatarPictureBox2";
            this.avatarPictureBox2.Size = new System.Drawing.Size(85, 85);
            this.avatarPictureBox2.TabIndex = 0;
            this.avatarPictureBox2.TabStop = false;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.exitButton.Image = global::TradingCardGame.Images.buttons.buttons.closebutton;
            this.exitButton.Location = new System.Drawing.Point(542, 8);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(35, 35);
            this.exitButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitButton.TabIndex = 55;
            this.exitButton.TabStop = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // playersNameListBox
            // 
            this.playersNameListBox.BackColor = System.Drawing.Color.Black;
            this.playersNameListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.playersNameListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playersNameListBox.ForeColor = System.Drawing.Color.White;
            this.playersNameListBox.FullRowSelect = true;
            this.playersNameListBox.Location = new System.Drawing.Point(388, 419);
            this.playersNameListBox.MultiSelect = false;
            this.playersNameListBox.Name = "playersNameListBox";
            this.playersNameListBox.Size = new System.Drawing.Size(200, 122);
            this.playersNameListBox.SmallImageList = this.imageList;
            this.playersNameListBox.TabIndex = 56;
            this.playersNameListBox.UseCompatibleStateImageBehavior = false;
            this.playersNameListBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Username";
            this.columnHeader1.Width = 196;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "bronze");
            // 
            // inputChatBox
            // 
            this.inputChatBox.BackColor = System.Drawing.Color.Black;
            this.inputChatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputChatBox.ForeColor = System.Drawing.Color.White;
            this.inputChatBox.Location = new System.Drawing.Point(21, 519);
            this.inputChatBox.Name = "inputChatBox";
            this.inputChatBox.Size = new System.Drawing.Size(361, 22);
            this.inputChatBox.TabIndex = 57;
            this.inputChatBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputChatBox_KeyDown);
            // 
            // chatBox
            // 
            this.chatBox.BackColor = System.Drawing.Color.Black;
            this.chatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chatBox.ForeColor = System.Drawing.Color.White;
            this.chatBox.FormattingEnabled = true;
            this.chatBox.HorizontalScrollbar = true;
            this.chatBox.ItemHeight = 15;
            this.chatBox.Location = new System.Drawing.Point(21, 419);
            this.chatBox.Name = "chatBox";
            this.chatBox.Size = new System.Drawing.Size(361, 94);
            this.chatBox.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label4.Location = new System.Drawing.Point(314, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 60;
            this.label4.Text = "Match Type:";
            // 
            // matchTypeComboBox
            // 
            this.matchTypeComboBox.BackColor = System.Drawing.Color.Black;
            this.matchTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.matchTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchTypeComboBox.ForeColor = System.Drawing.Color.White;
            this.matchTypeComboBox.FormattingEnabled = true;
            this.matchTypeComboBox.Items.AddRange(new object[] {
            "Constructed"});
            this.matchTypeComboBox.Location = new System.Drawing.Point(317, 75);
            this.matchTypeComboBox.Name = "matchTypeComboBox";
            this.matchTypeComboBox.Size = new System.Drawing.Size(259, 24);
            this.matchTypeComboBox.TabIndex = 59;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label5.Location = new System.Drawing.Point(314, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 15);
            this.label5.TabIndex = 62;
            this.label5.Text = "Number of Players:";
            // 
            // numberOfPlayersListBox
            // 
            this.numberOfPlayersListBox.BackColor = System.Drawing.Color.Black;
            this.numberOfPlayersListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numberOfPlayersListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfPlayersListBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.numberOfPlayersListBox.FormattingEnabled = true;
            this.numberOfPlayersListBox.Items.AddRange(new object[] {
            "2 Players",
            "4 Players"});
            this.numberOfPlayersListBox.Location = new System.Drawing.Point(318, 126);
            this.numberOfPlayersListBox.Name = "numberOfPlayersListBox";
            this.numberOfPlayersListBox.Size = new System.Drawing.Size(259, 24);
            this.numberOfPlayersListBox.TabIndex = 61;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label7.Location = new System.Drawing.Point(314, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 64;
            this.label7.Text = "Play Format:";
            // 
            // playFormatComboBox
            // 
            this.playFormatComboBox.BackColor = System.Drawing.Color.Black;
            this.playFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.playFormatComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playFormatComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.playFormatComboBox.FormattingEnabled = true;
            this.playFormatComboBox.Items.AddRange(new object[] {
            "Standard",
            "Raid"});
            this.playFormatComboBox.Location = new System.Drawing.Point(317, 177);
            this.playFormatComboBox.Name = "playFormatComboBox";
            this.playFormatComboBox.Size = new System.Drawing.Size(259, 24);
            this.playFormatComboBox.TabIndex = 63;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label6.Location = new System.Drawing.Point(314, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 15);
            this.label6.TabIndex = 66;
            this.label6.Text = "Match Structure:";
            // 
            // matchStructureComboBox
            // 
            this.matchStructureComboBox.BackColor = System.Drawing.Color.Black;
            this.matchStructureComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.matchStructureComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchStructureComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.matchStructureComboBox.FormattingEnabled = true;
            this.matchStructureComboBox.Items.AddRange(new object[] {
            "Single Game",
            "Best of Three Games",
            "Best of Five Games"});
            this.matchStructureComboBox.Location = new System.Drawing.Point(317, 228);
            this.matchStructureComboBox.Name = "matchStructureComboBox";
            this.matchStructureComboBox.Size = new System.Drawing.Size(259, 24);
            this.matchStructureComboBox.TabIndex = 65;
            // 
            // timeLimitComboBox
            // 
            this.timeLimitComboBox.BackColor = System.Drawing.Color.Black;
            this.timeLimitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timeLimitComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLimitComboBox.ForeColor = System.Drawing.Color.White;
            this.timeLimitComboBox.FormattingEnabled = true;
            this.timeLimitComboBox.Items.AddRange(new object[] {
            "No Time Limit",
            "90m Chess Clock"});
            this.timeLimitComboBox.Location = new System.Drawing.Point(318, 279);
            this.timeLimitComboBox.Name = "timeLimitComboBox";
            this.timeLimitComboBox.Size = new System.Drawing.Size(259, 23);
            this.timeLimitComboBox.TabIndex = 68;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label3.Location = new System.Drawing.Point(314, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 69;
            this.label3.Text = "Time Limit:";
            // 
            // guildOnlyComboBox
            // 
            this.guildOnlyComboBox.BackColor = System.Drawing.Color.Black;
            this.guildOnlyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guildOnlyComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildOnlyComboBox.ForeColor = System.Drawing.Color.White;
            this.guildOnlyComboBox.FormattingEnabled = true;
            this.guildOnlyComboBox.Items.AddRange(new object[] {
            "No Guild Restriction",
            "Guild Officers Only",
            "All Guild Members"});
            this.guildOnlyComboBox.Location = new System.Drawing.Point(318, 329);
            this.guildOnlyComboBox.Name = "guildOnlyComboBox";
            this.guildOnlyComboBox.Size = new System.Drawing.Size(259, 23);
            this.guildOnlyComboBox.TabIndex = 72;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label8.Location = new System.Drawing.Point(314, 311);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 15);
            this.label8.TabIndex = 73;
            this.label8.Text = "Guild Only:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label17.Location = new System.Drawing.Point(314, 395);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 15);
            this.label17.TabIndex = 84;
            this.label17.Text = "Light vs. Dark";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label16.Location = new System.Drawing.Point(314, 375);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 15);
            this.label16.TabIndex = 83;
            this.label16.Text = "Friends Only:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label15.Location = new System.Drawing.Point(314, 355);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 15);
            this.label15.TabIndex = 82;
            this.label15.Text = "Allow Observers:";
            // 
            // allowObserversCheckBox
            // 
            this.allowObserversCheckBox.Location = new System.Drawing.Point(417, 355);
            this.allowObserversCheckBox.Name = "allowObserversCheckBox";
            this.allowObserversCheckBox.Size = new System.Drawing.Size(20, 17);
            this.allowObserversCheckBox.TabIndex = 79;
            this.allowObserversCheckBox.TabStop = false;
            // 
            // friendsOnlyCheckBox
            // 
            this.friendsOnlyCheckBox.Location = new System.Drawing.Point(417, 375);
            this.friendsOnlyCheckBox.Name = "friendsOnlyCheckBox";
            this.friendsOnlyCheckBox.Size = new System.Drawing.Size(20, 17);
            this.friendsOnlyCheckBox.TabIndex = 80;
            this.friendsOnlyCheckBox.TabStop = false;
            // 
            // lightVsDarkCheckBox
            // 
            this.lightVsDarkCheckBox.Location = new System.Drawing.Point(417, 395);
            this.lightVsDarkCheckBox.Name = "lightVsDarkCheckBox";
            this.lightVsDarkCheckBox.Size = new System.Drawing.Size(20, 17);
            this.lightVsDarkCheckBox.TabIndex = 81;
            this.lightVsDarkCheckBox.TabStop = false;
            // 
            // LaunchMatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 553);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.allowObserversCheckBox);
            this.Controls.Add(this.friendsOnlyCheckBox);
            this.Controls.Add(this.lightVsDarkCheckBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.guildOnlyComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.timeLimitComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.matchStructureComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.playFormatComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numberOfPlayersListBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.matchTypeComboBox);
            this.Controls.Add(this.chatBox);
            this.Controls.Add(this.inputChatBox);
            this.Controls.Add(this.playersNameListBox);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.userPanel2);
            this.Controls.Add(this.userPanel1);
            this.Controls.Add(this.panel10);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "LaunchMatch";
            this.Text = "s";
            this.Load += new System.EventHandler(this.LaunchMatch_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LaunchMatch_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LaunchMatch_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.LaunchMatch_MouseUp);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.userPanel1.ResumeLayout(false);
            this.userPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readyToGoCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox1)).EndInit();
            this.userPanel2.ResumeLayout(false);
            this.userPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readyToGoCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowObserversCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.friendsOnlyCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightVsDarkCheckBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label panelTitleLabel;
        private System.Windows.Forms.Panel userPanel1;
        private System.Windows.Forms.PictureBox avatarPictureBox1;
        private System.Windows.Forms.Label usernameLabel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox readyToGoCheckBox1;
        private System.Windows.Forms.Panel userPanel2;
        private System.Windows.Forms.PictureBox readyToGoCheckBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label usernameLabel2;
        private System.Windows.Forms.PictureBox avatarPictureBox2;
        private System.Windows.Forms.PictureBox exitButton;
        private System.Windows.Forms.ListView playersNameListBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.TextBox inputChatBox;
        private System.Windows.Forms.ListBox chatBox;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox matchTypeComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox numberOfPlayersListBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox playFormatComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox matchStructureComboBox;
        private System.Windows.Forms.ComboBox timeLimitComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox guildOnlyComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox allowObserversCheckBox;
        private System.Windows.Forms.PictureBox friendsOnlyCheckBox;
        private System.Windows.Forms.PictureBox lightVsDarkCheckBox;
    }
}