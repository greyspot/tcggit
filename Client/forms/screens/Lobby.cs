﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs;
using TradingCardGame.forms.widgets;
using TradingCardGame.services.account;
using TradingCardGame.services.command;
using TradingCardGame.services.data;
using TradingCardGame.services.database;
using TradingCardGame.services.multiplayer;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms
{
    public partial class Lobby : UserControl
    {
        public Lobby()
        {
            InitializeComponent();
        }

        UserControl currentControl;

        private void InputChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;

                    if (MultiplayerService.getCurrentLobbyPosition().Equals("-1"))
                    {
                        MessageBox.Show("You have desynced: removing you from casual lobby.");
                        Dispose();
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(inputChatBox.Text))
                        inputChatBox.Text = string.Empty;
                    else if (inputChatBox.Text[0].Equals('/'))
                    {
                        string command = inputChatBox.Text.Substring(1);

                        if (command.Contains(" "))
                            command = command.Substring(0, command.IndexOf(" ")).ToLower() + command.Substring(command.IndexOf(" "), command.Length - command.IndexOf(" "));

                        CommandService.command(command, chatBox);
                        chatBox.TopIndex = chatBox.Items.Count - 1;
                    }
                    else
                        PacketService.SendPacket(ServerData.PacketType.ClientChat, inputChatBox.Text);
                    inputChatBox.Text = string.Empty;
                }
                SoundService.KeyPressSound(e.KeyCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:\n" + ex.Message);
                Application.ExitThread();
            }
        }

        private delegate void MatchPanelDelegate();

        public static void LoadAvailableGames()
        {
            if (MultiplayerService.getNumberOfMatches() == 0)
            {
                if (matchBackgroundPanel1.InvokeRequired)
                    matchBackgroundPanel1.Invoke(new MatchPanelDelegate(LoadAvailableGames));
                else
                    matchBackgroundPanel1.Visible = false;
                return;
            }

            for (byte i = 0; i < MultiplayerService.getNumberOfMatches(); i++)
            {
                string[] matchTitles = MultiplayerService.getAllMatchTitles();
                //string creatorName = DatabaseService.getValue("matches", "creator", "title", matchTitles[i]);

                Panel backgroundPanel = ((Panel)activeGamesPanel.Controls["matchBackgroundPanel" + (i + 1)]);
                Panel currentPanel = ((Panel)backgroundPanel.Controls["matchPanel" + (i + 1)]);
                Label currentMatchNameLabel = ((Label)currentPanel.Controls["matchNameLabel" + (i + 1)]);
                Label currentMatchIdLabel = ((Label)currentPanel.Controls["idMatchLabel" + (i + 1)]);
                PictureBox currentPasswordPictureBox = ((PictureBox)currentPanel.Controls["passwordRequiredPictureBox" + (i + 1)]);
                PictureBox currentTimedGamePicturebox = ((PictureBox)currentPanel.Controls["timedGamePictureBox" + (i + 1)]);
                PictureBox currentFriendsOnlyPicturebox = ((PictureBox)currentPanel.Controls["friendsOnlyPictureBox" + (i + 1)]);

                if (backgroundPanel.InvokeRequired)
                    backgroundPanel.Invoke(new MatchPanelDelegate(LoadAvailableGames));
                else
                {
                    currentMatchNameLabel.Text = matchTitles[i];
                    currentMatchIdLabel.Text = "#" + DatabaseService.GetValue("matches", "id", "title", matchTitles[i]);
                    //backgroundPanel.BackgroundImage = (Image)Res.avatars.ResourceManager.GetObject(DatabaseService.getValue("users", "avatar", "username", creatorName));
                    backgroundPanel.Visible = true;

                    if (string.IsNullOrEmpty(DatabaseService.GetValue("matches", "password", "title", matchTitles[i])))
                        currentPasswordPictureBox.Visible = false;
                    else
                        currentPasswordPictureBox.Visible = true;

                    if (DatabaseService.GetValue("matches", "timelimit", "title", matchTitles[i]).Equals("No Time Limit"))
                        currentTimedGamePicturebox.Visible = false;
                    else
                        currentTimedGamePicturebox.Visible = true;

                    if (DatabaseService.GetValue("matches", "friendsonly", "title", matchTitles[i]).Equals("False"))
                        currentFriendsOnlyPicturebox.Visible = false;
                    else
                        currentFriendsOnlyPicturebox.Visible = true;
                }

                if (DatabaseService.GetValue("matches", "numberofplayers", "title", matchTitles[i]).Equals("2 Players"))
                {
                    firstPlayerSlot1.Image = null;
                    secondPlayerSlot1.Image = null;
                }
            }
        }

        private void CasualLobby_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            BringToFront();

            createButton.BackgroundImage = Images.buttons.buttons.button_lg;
            quickJoinButton.BackgroundImage = Images.buttons.buttons.button_lg;

            TemporaryData.setCurrentForm(3);

            Controls.Add(new Navigator());

            LoadAvailableGames();

            PacketService.setComponents(usersLabel, chatBox, playersNameListBox);
            PacketService.SendPacket(ServerData.PacketType.ClientChat, " joined the lobby.");

            HandleDestroyed += Lobby_HandleDestroyed;
        }

        private void Lobby_HandleDestroyed(object sender, EventArgs e)
        {
            if (PacketService.GetIsConnected())
                PacketService.SendPacket(ServerData.PacketType.ClientChat, " left the lobby.");
        }

        private void PlayNowButton_MouseEnter(object sender, EventArgs e)
        {
            playNowButton.Image = Images.dialogs.dialogs.practice_button_over;
        }

        private void PlayNowButton_MouseDown(object sender, MouseEventArgs e)
        {
            playNowButton.Image = Images.dialogs.dialogs.practice_button_down;
        }

        private void PlayNowButton_MouseLeave(object sender, EventArgs e)
        {
            playNowButton.Image = Images.dialogs.dialogs.practice_button;
        }

        private void PlayNowButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void ChangeCheckBox(PictureBox checkBox, bool checkedBool)
        {
            if (checkedBool)
                checkBox.Image = Images.buttons.buttons.checkbox_off;
            else
                checkBox.Image = Images.buttons.buttons.checkbox_on;
            SoundService.PlaySound("button");
        }

        private void CreateMatchButton_Click(object sender, EventArgs e)
        {

        }

        private void JoinMatchButton1_MouseEnter(object sender, EventArgs e)
        {
            joinMatchButton1.Image = Images.graphicsitems.match.button_join_over;
        }

        private void JoinMatchButton1_MouseLeave(object sender, EventArgs e)
        {
            joinMatchButton1.Image = Images.graphicsitems.match.button_join;
        }

        private void JoinMatchButton1_MouseDown(object sender, MouseEventArgs e)
        {
            joinMatchButton1.Image = Images.graphicsitems.match.button_join_down;
        }

        private void JoinMatchButton1_Click(object sender, EventArgs e)
        {
            //Properties.Settings.Default.matchTitle = matchNameLabel1.Text;
            Properties.Settings.Default.Save();

            new JoinMatch().Show();
            SoundService.PlaySound("button");
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (DatabaseService.GetNumberOfTableRows("matches") == int.Parse(DatabaseService.GetValue("config", "maxCasualMatches", "server", "tcg")))
            {
                MessageBox.Show("Sorry, but this lobby has reached it's maximum limit for the amount of matches that can occur at once.");
                return;
            }

            Properties.Settings.Default.showDeckSelector = false;
            Properties.Settings.Default.Save();

            CreateMatch createMatch = new CreateMatch();
            Controls.Add(createMatch);
            createMatch.BringToFront();
        }

        private void PlayersNameListBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && !TemporaryData.getDialogStatus())
            {
                if (playersNameListBox.SelectedItems.Count == 0)
                    return;

                TemporaryData.changeDialogStatus();
                TemporaryData.setViewingProfile(AccountService.GetRemovedStaffTitle(playersNameListBox.SelectedItems[0].Text));
                UserInfo userInfo = new UserInfo();
                currentControl = userInfo;
                Controls.Add(userInfo);
                userInfo.BringToFront();
            }
        }
    }
}
