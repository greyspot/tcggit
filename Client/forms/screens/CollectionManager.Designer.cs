﻿namespace TradingCardGame
{
    partial class CollectionManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CollectionManager));
            this.firstBackgroundPanel = new System.Windows.Forms.Panel();
            this.filterButton = new System.Windows.Forms.Button();
            this.numberOfCardsLabel = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.previewCard = new System.Windows.Forms.Panel();
            this.slot7 = new System.Windows.Forms.PictureBox();
            this.slot8 = new System.Windows.Forms.PictureBox();
            this.slot9 = new System.Windows.Forms.PictureBox();
            this.slot6 = new System.Windows.Forms.PictureBox();
            this.slot5 = new System.Windows.Forms.PictureBox();
            this.slot4 = new System.Windows.Forms.PictureBox();
            this.slot3 = new System.Windows.Forms.PictureBox();
            this.slot2 = new System.Windows.Forms.PictureBox();
            this.slot1 = new System.Windows.Forms.PictureBox();
            this.slot0 = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.quantity9Label = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.quantity8Label = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.quantity7Label = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.quantity6Label = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.forTrade6Label = new System.Windows.Forms.Label();
            this.want6Label = new System.Windows.Forms.Label();
            this.quantity5Label = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.nextButton = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.quantity4Label = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.quantity3Label = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.quantity2Label = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.quantity1Label = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.previousPageButton = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.forTrade1Label = new System.Windows.Forms.Label();
            this.want1Label = new System.Windows.Forms.Label();
            this.quantity0Label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.firstBackgroundPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slot7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot0)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nextButton)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previousPageButton)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // firstBackgroundPanel
            // 
            this.firstBackgroundPanel.BackColor = System.Drawing.Color.Black;
            this.firstBackgroundPanel.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.firstBackgroundPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.firstBackgroundPanel.Controls.Add(this.filterButton);
            this.firstBackgroundPanel.Controls.Add(this.numberOfCardsLabel);
            this.firstBackgroundPanel.Controls.Add(this.label61);
            this.firstBackgroundPanel.Controls.Add(this.pictureBox20);
            this.firstBackgroundPanel.Controls.Add(this.pictureBox19);
            this.firstBackgroundPanel.Controls.Add(this.panel1);
            this.firstBackgroundPanel.Location = new System.Drawing.Point(7, 9);
            this.firstBackgroundPanel.Name = "firstBackgroundPanel";
            this.firstBackgroundPanel.Size = new System.Drawing.Size(1010, 750);
            this.firstBackgroundPanel.TabIndex = 13;
            // 
            // filterButton
            // 
            this.filterButton.BackColor = System.Drawing.Color.Transparent;
            this.filterButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("filterButton.BackgroundImage")));
            this.filterButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.filterButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.filterButton.FlatAppearance.BorderSize = 0;
            this.filterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filterButton.ForeColor = System.Drawing.Color.Black;
            this.filterButton.Location = new System.Drawing.Point(34, 19);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(90, 29);
            this.filterButton.TabIndex = 52;
            this.filterButton.Text = "Filter";
            this.filterButton.UseVisualStyleBackColor = false;
            // 
            // numberOfCardsLabel
            // 
            this.numberOfCardsLabel.AutoSize = true;
            this.numberOfCardsLabel.BackColor = System.Drawing.Color.Transparent;
            this.numberOfCardsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfCardsLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.numberOfCardsLabel.Location = new System.Drawing.Point(697, 23);
            this.numberOfCardsLabel.Name = "numberOfCardsLabel";
            this.numberOfCardsLabel.Size = new System.Drawing.Size(73, 20);
            this.numberOfCardsLabel.TabIndex = 18;
            this.numberOfCardsLabel.Text = "0 cards.";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.ForeColor = System.Drawing.Color.SkyBlue;
            this.label61.Location = new System.Drawing.Point(821, 11);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(30, 13);
            this.label61.TabIndex = 17;
            this.label61.Text = "View";
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.tableview_table;
            this.pictureBox20.Location = new System.Drawing.Point(813, 30);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(24, 24);
            this.pictureBox20.TabIndex = 16;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.tableview_graphic_down;
            this.pictureBox19.Location = new System.Drawing.Point(838, 30);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(24, 24);
            this.pictureBox19.TabIndex = 15;
            this.pictureBox19.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.collection_manager_background;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.previewCard);
            this.panel1.Controls.Add(this.slot7);
            this.panel1.Controls.Add(this.slot8);
            this.panel1.Controls.Add(this.slot9);
            this.panel1.Controls.Add(this.slot6);
            this.panel1.Controls.Add(this.slot5);
            this.panel1.Controls.Add(this.slot4);
            this.panel1.Controls.Add(this.slot3);
            this.panel1.Controls.Add(this.slot2);
            this.panel1.Controls.Add(this.slot1);
            this.panel1.Controls.Add(this.slot0);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.nextButton);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.previousPageButton);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pictureBox18);
            this.panel1.Controls.Add(this.pictureBox17);
            this.panel1.Controls.Add(this.pictureBox16);
            this.panel1.Controls.Add(this.pictureBox15);
            this.panel1.Controls.Add(this.pictureBox14);
            this.panel1.Controls.Add(this.pictureBox13);
            this.panel1.Controls.Add(this.pictureBox12);
            this.panel1.Controls.Add(this.pictureBox11);
            this.panel1.Controls.Add(this.pictureBox10);
            this.panel1.Controls.Add(this.pictureBox8);
            this.panel1.Controls.Add(this.pictureBox7);
            this.panel1.Controls.Add(this.pictureBox9);
            this.panel1.Controls.Add(this.pictureBox6);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(22, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(970, 670);
            this.panel1.TabIndex = 13;
            // 
            // previewCard
            // 
            this.previewCard.Location = new System.Drawing.Point(580, 83);
            this.previewCard.Name = "previewCard";
            this.previewCard.Size = new System.Drawing.Size(350, 489);
            this.previewCard.TabIndex = 65;
            this.previewCard.Visible = false;
            // 
            // slot7
            // 
            this.slot7.BackColor = System.Drawing.Color.Black;
            this.slot7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot7.Location = new System.Drawing.Point(402, 338);
            this.slot7.Name = "slot7";
            this.slot7.Size = new System.Drawing.Size(169, 211);
            this.slot7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot7.TabIndex = 64;
            this.slot7.TabStop = false;
            this.slot7.Click += new System.EventHandler(this.slot7_Click);
            // 
            // slot8
            // 
            this.slot8.BackColor = System.Drawing.Color.Black;
            this.slot8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot8.Location = new System.Drawing.Point(592, 338);
            this.slot8.Name = "slot8";
            this.slot8.Size = new System.Drawing.Size(169, 211);
            this.slot8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot8.TabIndex = 64;
            this.slot8.TabStop = false;
            this.slot8.Click += new System.EventHandler(this.slot8_Click);
            // 
            // slot9
            // 
            this.slot9.BackColor = System.Drawing.Color.Black;
            this.slot9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot9.Location = new System.Drawing.Point(774, 338);
            this.slot9.Name = "slot9";
            this.slot9.Size = new System.Drawing.Size(169, 211);
            this.slot9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot9.TabIndex = 64;
            this.slot9.TabStop = false;
            this.slot9.Click += new System.EventHandler(this.slot9_Click);
            // 
            // slot6
            // 
            this.slot6.BackColor = System.Drawing.Color.Black;
            this.slot6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot6.Location = new System.Drawing.Point(212, 338);
            this.slot6.Name = "slot6";
            this.slot6.Size = new System.Drawing.Size(169, 211);
            this.slot6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot6.TabIndex = 63;
            this.slot6.TabStop = false;
            this.slot6.Click += new System.EventHandler(this.slot6_Click);
            // 
            // slot5
            // 
            this.slot5.BackColor = System.Drawing.Color.Black;
            this.slot5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot5.Location = new System.Drawing.Point(28, 338);
            this.slot5.Name = "slot5";
            this.slot5.Size = new System.Drawing.Size(169, 211);
            this.slot5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot5.TabIndex = 62;
            this.slot5.TabStop = false;
            this.slot5.Click += new System.EventHandler(this.slot5_Click);
            // 
            // slot4
            // 
            this.slot4.BackColor = System.Drawing.Color.Black;
            this.slot4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot4.Location = new System.Drawing.Point(774, 53);
            this.slot4.Name = "slot4";
            this.slot4.Size = new System.Drawing.Size(169, 211);
            this.slot4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot4.TabIndex = 61;
            this.slot4.TabStop = false;
            this.slot4.Click += new System.EventHandler(this.slot4_Click);
            // 
            // slot3
            // 
            this.slot3.BackColor = System.Drawing.Color.Black;
            this.slot3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot3.Location = new System.Drawing.Point(592, 53);
            this.slot3.Name = "slot3";
            this.slot3.Size = new System.Drawing.Size(169, 211);
            this.slot3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot3.TabIndex = 60;
            this.slot3.TabStop = false;
            this.slot3.Click += new System.EventHandler(this.slot3_Click);
            // 
            // slot2
            // 
            this.slot2.BackColor = System.Drawing.Color.Black;
            this.slot2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot2.Location = new System.Drawing.Point(402, 53);
            this.slot2.Name = "slot2";
            this.slot2.Size = new System.Drawing.Size(169, 211);
            this.slot2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot2.TabIndex = 59;
            this.slot2.TabStop = false;
            this.slot2.Click += new System.EventHandler(this.slot2_Click);
            // 
            // slot1
            // 
            this.slot1.BackColor = System.Drawing.Color.Black;
            this.slot1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot1.Location = new System.Drawing.Point(212, 53);
            this.slot1.Name = "slot1";
            this.slot1.Size = new System.Drawing.Size(169, 211);
            this.slot1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot1.TabIndex = 58;
            this.slot1.TabStop = false;
            this.slot1.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot0
            // 
            this.slot0.BackColor = System.Drawing.Color.Black;
            this.slot0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot0.Location = new System.Drawing.Point(28, 53);
            this.slot0.Name = "slot0";
            this.slot0.Size = new System.Drawing.Size(169, 211);
            this.slot0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.slot0.TabIndex = 57;
            this.slot0.TabStop = false;
            this.slot0.Click += new System.EventHandler(this.slot0_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.label55);
            this.panel11.Controls.Add(this.label56);
            this.panel11.Controls.Add(this.quantity9Label);
            this.panel11.Controls.Add(this.label58);
            this.panel11.Controls.Add(this.label59);
            this.panel11.Controls.Add(this.label60);
            this.panel11.Location = new System.Drawing.Point(774, 555);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(169, 62);
            this.panel11.TabIndex = 56;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.White;
            this.label55.Location = new System.Drawing.Point(120, 42);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(14, 15);
            this.label55.TabIndex = 5;
            this.label55.Text = "0";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(120, 24);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(14, 15);
            this.label56.TabIndex = 4;
            this.label56.Text = "0";
            // 
            // quantity9Label
            // 
            this.quantity9Label.AutoSize = true;
            this.quantity9Label.BackColor = System.Drawing.Color.Transparent;
            this.quantity9Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity9Label.ForeColor = System.Drawing.Color.White;
            this.quantity9Label.Location = new System.Drawing.Point(120, 6);
            this.quantity9Label.Name = "quantity9Label";
            this.quantity9Label.Size = new System.Drawing.Size(14, 15);
            this.quantity9Label.TabIndex = 3;
            this.quantity9Label.Text = "0";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(2, 41);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(63, 15);
            this.label58.TabIndex = 2;
            this.label58.Text = "For Trade:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.White;
            this.label59.Location = new System.Drawing.Point(2, 23);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(38, 15);
            this.label59.TabIndex = 1;
            this.label59.Text = "Want:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.White;
            this.label60.Location = new System.Drawing.Point(2, 5);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(54, 15);
            this.label60.TabIndex = 0;
            this.label60.Text = "Quantity:";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.label49);
            this.panel10.Controls.Add(this.label50);
            this.panel10.Controls.Add(this.quantity8Label);
            this.panel10.Controls.Add(this.label52);
            this.panel10.Controls.Add(this.label53);
            this.panel10.Controls.Add(this.label54);
            this.panel10.Location = new System.Drawing.Point(592, 555);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(169, 62);
            this.panel10.TabIndex = 55;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(120, 42);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(14, 15);
            this.label49.TabIndex = 5;
            this.label49.Text = "0";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(120, 24);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(14, 15);
            this.label50.TabIndex = 4;
            this.label50.Text = "0";
            // 
            // quantity8Label
            // 
            this.quantity8Label.AutoSize = true;
            this.quantity8Label.BackColor = System.Drawing.Color.Transparent;
            this.quantity8Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity8Label.ForeColor = System.Drawing.Color.White;
            this.quantity8Label.Location = new System.Drawing.Point(120, 6);
            this.quantity8Label.Name = "quantity8Label";
            this.quantity8Label.Size = new System.Drawing.Size(14, 15);
            this.quantity8Label.TabIndex = 3;
            this.quantity8Label.Text = "0";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(2, 41);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(63, 15);
            this.label52.TabIndex = 2;
            this.label52.Text = "For Trade:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(2, 23);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(38, 15);
            this.label53.TabIndex = 1;
            this.label53.Text = "Want:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(2, 5);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(54, 15);
            this.label54.TabIndex = 0;
            this.label54.Text = "Quantity:";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Transparent;
            this.panel9.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel9.Controls.Add(this.label28);
            this.panel9.Controls.Add(this.label29);
            this.panel9.Controls.Add(this.quantity7Label);
            this.panel9.Controls.Add(this.label46);
            this.panel9.Controls.Add(this.label47);
            this.panel9.Controls.Add(this.label48);
            this.panel9.Location = new System.Drawing.Point(402, 555);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(169, 62);
            this.panel9.TabIndex = 54;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(120, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(14, 15);
            this.label28.TabIndex = 5;
            this.label28.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(120, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(14, 15);
            this.label29.TabIndex = 4;
            this.label29.Text = "0";
            // 
            // quantity7Label
            // 
            this.quantity7Label.AutoSize = true;
            this.quantity7Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity7Label.ForeColor = System.Drawing.Color.White;
            this.quantity7Label.Location = new System.Drawing.Point(120, 6);
            this.quantity7Label.Name = "quantity7Label";
            this.quantity7Label.Size = new System.Drawing.Size(14, 15);
            this.quantity7Label.TabIndex = 3;
            this.quantity7Label.Text = "0";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(2, 41);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(63, 15);
            this.label46.TabIndex = 2;
            this.label46.Text = "For Trade:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(2, 23);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(38, 15);
            this.label47.TabIndex = 1;
            this.label47.Text = "Want:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(2, 5);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(54, 15);
            this.label48.TabIndex = 0;
            this.label48.Text = "Quantity:";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this.label23);
            this.panel8.Controls.Add(this.quantity6Label);
            this.panel8.Controls.Add(this.label25);
            this.panel8.Controls.Add(this.label26);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Location = new System.Drawing.Point(212, 555);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(169, 62);
            this.panel8.TabIndex = 53;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(120, 42);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 15);
            this.label22.TabIndex = 5;
            this.label22.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(120, 24);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 15);
            this.label23.TabIndex = 4;
            this.label23.Text = "0";
            // 
            // quantity6Label
            // 
            this.quantity6Label.AutoSize = true;
            this.quantity6Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity6Label.ForeColor = System.Drawing.Color.White;
            this.quantity6Label.Location = new System.Drawing.Point(120, 6);
            this.quantity6Label.Name = "quantity6Label";
            this.quantity6Label.Size = new System.Drawing.Size(14, 15);
            this.quantity6Label.TabIndex = 3;
            this.quantity6Label.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(2, 41);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 15);
            this.label25.TabIndex = 2;
            this.label25.Text = "For Trade:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(2, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 15);
            this.label26.TabIndex = 1;
            this.label26.Text = "Want:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(2, 5);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 15);
            this.label27.TabIndex = 0;
            this.label27.Text = "Quantity:";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Controls.Add(this.forTrade6Label);
            this.panel7.Controls.Add(this.want6Label);
            this.panel7.Controls.Add(this.quantity5Label);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.label20);
            this.panel7.Controls.Add(this.label21);
            this.panel7.Location = new System.Drawing.Point(28, 555);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(169, 62);
            this.panel7.TabIndex = 52;
            // 
            // forTrade6Label
            // 
            this.forTrade6Label.AutoSize = true;
            this.forTrade6Label.BackColor = System.Drawing.Color.Transparent;
            this.forTrade6Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forTrade6Label.ForeColor = System.Drawing.Color.White;
            this.forTrade6Label.Location = new System.Drawing.Point(120, 42);
            this.forTrade6Label.Name = "forTrade6Label";
            this.forTrade6Label.Size = new System.Drawing.Size(14, 15);
            this.forTrade6Label.TabIndex = 5;
            this.forTrade6Label.Text = "0";
            // 
            // want6Label
            // 
            this.want6Label.AutoSize = true;
            this.want6Label.BackColor = System.Drawing.Color.Transparent;
            this.want6Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.want6Label.ForeColor = System.Drawing.Color.White;
            this.want6Label.Location = new System.Drawing.Point(120, 24);
            this.want6Label.Name = "want6Label";
            this.want6Label.Size = new System.Drawing.Size(14, 15);
            this.want6Label.TabIndex = 4;
            this.want6Label.Text = "0";
            // 
            // quantity5Label
            // 
            this.quantity5Label.AutoSize = true;
            this.quantity5Label.BackColor = System.Drawing.Color.Transparent;
            this.quantity5Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity5Label.ForeColor = System.Drawing.Color.White;
            this.quantity5Label.Location = new System.Drawing.Point(120, 6);
            this.quantity5Label.Name = "quantity5Label";
            this.quantity5Label.Size = new System.Drawing.Size(14, 15);
            this.quantity5Label.TabIndex = 3;
            this.quantity5Label.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(2, 41);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 15);
            this.label19.TabIndex = 2;
            this.label19.Text = "For Trade:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(2, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 15);
            this.label20.TabIndex = 1;
            this.label20.Text = "Want:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(2, 5);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 15);
            this.label21.TabIndex = 0;
            this.label21.Text = "Quantity:";
            // 
            // nextButton
            // 
            this.nextButton.BackColor = System.Drawing.Color.Transparent;
            this.nextButton.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_nextpage;
            this.nextButton.Location = new System.Drawing.Point(942, 310);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(25, 51);
            this.nextButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.nextButton.TabIndex = 16;
            this.nextButton.TabStop = false;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            this.nextButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.nextButton_MouseDown);
            this.nextButton.MouseEnter += new System.EventHandler(this.nextButton_MouseEnter);
            this.nextButton.MouseLeave += new System.EventHandler(this.nextButton_MouseLeave);
            this.nextButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.nextButton_MouseUp);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.quantity4Label);
            this.panel6.Controls.Add(this.label43);
            this.panel6.Controls.Add(this.label44);
            this.panel6.Controls.Add(this.label45);
            this.panel6.Location = new System.Drawing.Point(774, 270);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(169, 62);
            this.panel6.TabIndex = 51;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(120, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 15);
            this.label13.TabIndex = 5;
            this.label13.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(120, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 15);
            this.label14.TabIndex = 4;
            this.label14.Text = "0";
            // 
            // quantity4Label
            // 
            this.quantity4Label.AutoSize = true;
            this.quantity4Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity4Label.ForeColor = System.Drawing.Color.White;
            this.quantity4Label.Location = new System.Drawing.Point(120, 6);
            this.quantity4Label.Name = "quantity4Label";
            this.quantity4Label.Size = new System.Drawing.Size(14, 15);
            this.quantity4Label.TabIndex = 3;
            this.quantity4Label.Text = "0";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(2, 41);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(63, 15);
            this.label43.TabIndex = 2;
            this.label43.Text = "For Trade:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(2, 23);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(38, 15);
            this.label44.TabIndex = 1;
            this.label44.Text = "Want:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(2, 5);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 15);
            this.label45.TabIndex = 0;
            this.label45.Text = "Quantity:";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.quantity3Label);
            this.panel5.Controls.Add(this.label40);
            this.panel5.Controls.Add(this.label41);
            this.panel5.Controls.Add(this.label42);
            this.panel5.Location = new System.Drawing.Point(592, 270);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(169, 62);
            this.panel5.TabIndex = 50;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(120, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 15);
            this.label10.TabIndex = 5;
            this.label10.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(120, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 15);
            this.label11.TabIndex = 4;
            this.label11.Text = "0";
            // 
            // quantity3Label
            // 
            this.quantity3Label.AutoSize = true;
            this.quantity3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity3Label.ForeColor = System.Drawing.Color.White;
            this.quantity3Label.Location = new System.Drawing.Point(120, 6);
            this.quantity3Label.Name = "quantity3Label";
            this.quantity3Label.Size = new System.Drawing.Size(14, 15);
            this.quantity3Label.TabIndex = 3;
            this.quantity3Label.Text = "0";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(2, 41);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(63, 15);
            this.label40.TabIndex = 2;
            this.label40.Text = "For Trade:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(2, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(38, 15);
            this.label41.TabIndex = 1;
            this.label41.Text = "Want:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(2, 5);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(54, 15);
            this.label42.TabIndex = 0;
            this.label42.Text = "Quantity:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.quantity2Label);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Location = new System.Drawing.Point(402, 270);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(169, 62);
            this.panel4.TabIndex = 49;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(120, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(120, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 15);
            this.label8.TabIndex = 4;
            this.label8.Text = "0";
            // 
            // quantity2Label
            // 
            this.quantity2Label.AutoSize = true;
            this.quantity2Label.BackColor = System.Drawing.Color.Transparent;
            this.quantity2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity2Label.ForeColor = System.Drawing.Color.White;
            this.quantity2Label.Location = new System.Drawing.Point(120, 6);
            this.quantity2Label.Name = "quantity2Label";
            this.quantity2Label.Size = new System.Drawing.Size(14, 15);
            this.quantity2Label.TabIndex = 3;
            this.quantity2Label.Text = "0";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(2, 41);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(63, 15);
            this.label37.TabIndex = 2;
            this.label37.Text = "For Trade:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(2, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(38, 15);
            this.label38.TabIndex = 1;
            this.label38.Text = "Want:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(2, 5);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 15);
            this.label39.TabIndex = 0;
            this.label39.Text = "Quantity:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.quantity1Label);
            this.panel3.Controls.Add(this.label34);
            this.panel3.Controls.Add(this.label35);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Location = new System.Drawing.Point(212, 270);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(169, 62);
            this.panel3.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(120, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(120, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "0";
            // 
            // quantity1Label
            // 
            this.quantity1Label.AutoSize = true;
            this.quantity1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity1Label.ForeColor = System.Drawing.Color.White;
            this.quantity1Label.Location = new System.Drawing.Point(120, 6);
            this.quantity1Label.Name = "quantity1Label";
            this.quantity1Label.Size = new System.Drawing.Size(14, 15);
            this.quantity1Label.TabIndex = 3;
            this.quantity1Label.Text = "0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(2, 41);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(63, 15);
            this.label34.TabIndex = 2;
            this.label34.Text = "For Trade:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(2, 23);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(38, 15);
            this.label35.TabIndex = 1;
            this.label35.Text = "Want:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(2, 5);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(54, 15);
            this.label36.TabIndex = 0;
            this.label36.Text = "Quantity:";
            // 
            // previousPageButton
            // 
            this.previousPageButton.BackColor = System.Drawing.Color.Transparent;
            this.previousPageButton.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_lastpage;
            this.previousPageButton.Location = new System.Drawing.Point(4, 310);
            this.previousPageButton.Name = "previousPageButton";
            this.previousPageButton.Size = new System.Drawing.Size(25, 51);
            this.previousPageButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.previousPageButton.TabIndex = 17;
            this.previousPageButton.TabStop = false;
            this.previousPageButton.Visible = false;
            this.previousPageButton.Click += new System.EventHandler(this.previousPageButton_Click);
            this.previousPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.previousPageButton_MouseDown);
            this.previousPageButton.MouseEnter += new System.EventHandler(this.previousPageButton_MouseEnter);
            this.previousPageButton.MouseLeave += new System.EventHandler(this.previousPageButton_MouseLeave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.textbox;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.forTrade1Label);
            this.panel2.Controls.Add(this.want1Label);
            this.panel2.Controls.Add(this.quantity0Label);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(28, 270);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(169, 62);
            this.panel2.TabIndex = 27;
            // 
            // forTrade1Label
            // 
            this.forTrade1Label.AutoSize = true;
            this.forTrade1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forTrade1Label.ForeColor = System.Drawing.Color.White;
            this.forTrade1Label.Location = new System.Drawing.Point(120, 42);
            this.forTrade1Label.Name = "forTrade1Label";
            this.forTrade1Label.Size = new System.Drawing.Size(14, 15);
            this.forTrade1Label.TabIndex = 5;
            this.forTrade1Label.Text = "0";
            // 
            // want1Label
            // 
            this.want1Label.AutoSize = true;
            this.want1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.want1Label.ForeColor = System.Drawing.Color.White;
            this.want1Label.Location = new System.Drawing.Point(120, 24);
            this.want1Label.Name = "want1Label";
            this.want1Label.Size = new System.Drawing.Size(14, 15);
            this.want1Label.TabIndex = 4;
            this.want1Label.Text = "0";
            // 
            // quantity0Label
            // 
            this.quantity0Label.AutoSize = true;
            this.quantity0Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity0Label.ForeColor = System.Drawing.Color.White;
            this.quantity0Label.Location = new System.Drawing.Point(120, 6);
            this.quantity0Label.Name = "quantity0Label";
            this.quantity0Label.Size = new System.Drawing.Size(14, 15);
            this.quantity0Label.TabIndex = 3;
            this.quantity0Label.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(2, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "For Trade:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(2, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Want:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(2, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Quantity:";
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox18.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_bottom;
            this.pictureBox18.Location = new System.Drawing.Point(547, 625);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(230, 49);
            this.pictureBox18.TabIndex = 23;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox17.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_bottom;
            this.pictureBox17.Location = new System.Drawing.Point(76, 625);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(230, 49);
            this.pictureBox17.TabIndex = 22;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox16.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_left;
            this.pictureBox16.Location = new System.Drawing.Point(0, 247);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(69, 349);
            this.pictureBox16.TabIndex = 21;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox15.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_right;
            this.pictureBox15.Location = new System.Drawing.Point(905, 246);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(69, 349);
            this.pictureBox15.TabIndex = 20;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_top;
            this.pictureBox14.Location = new System.Drawing.Point(174, 0);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(230, 49);
            this.pictureBox14.TabIndex = 19;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox13.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_top;
            this.pictureBox13.Location = new System.Drawing.Point(547, 1);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(230, 49);
            this.pictureBox13.TabIndex = 18;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox12.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_bottom;
            this.pictureBox12.Location = new System.Drawing.Point(174, 625);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(230, 49);
            this.pictureBox12.TabIndex = 12;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_bottom;
            this.pictureBox11.Location = new System.Drawing.Point(666, 625);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(230, 49);
            this.pictureBox11.TabIndex = 11;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_centercap_b;
            this.pictureBox10.Location = new System.Drawing.Point(404, 588);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(143, 86);
            this.pictureBox10.TabIndex = 10;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_left;
            this.pictureBox8.Location = new System.Drawing.Point(0, 76);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(69, 349);
            this.pictureBox8.TabIndex = 9;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_right;
            this.pictureBox7.Location = new System.Drawing.Point(905, 76);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(69, 349);
            this.pictureBox7.TabIndex = 8;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_top;
            this.pictureBox9.Location = new System.Drawing.Point(76, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(230, 49);
            this.pictureBox9.TabIndex = 7;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_top;
            this.pictureBox6.Location = new System.Drawing.Point(666, 1);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(230, 49);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_centercap_t;
            this.pictureBox5.Location = new System.Drawing.Point(404, 1);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(143, 86);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_ll;
            this.pictureBox4.Location = new System.Drawing.Point(0, 595);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(76, 76);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_lr;
            this.pictureBox3.Location = new System.Drawing.Point(896, 595);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(76, 76);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_ur;
            this.pictureBox2.Location = new System.Drawing.Point(896, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 76);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::TradingCardGame.Images.graphicsitems.binder.binder_ul;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 76);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // CollectionManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::TradingCardGame.Images.backgrounds.backgrounds.dialog_bg;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.firstBackgroundPanel);
            this.DoubleBuffered = true;
            this.Name = "CollectionManager";
            this.Text = "[Star Wars Galaxies TCG] Collections";
            this.Load += new System.EventHandler(this.GameProfile_Load);
            this.firstBackgroundPanel.ResumeLayout(false);
            this.firstBackgroundPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slot7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot0)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nextButton)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previousPageButton)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel firstBackgroundPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox previousPageButton;
        private System.Windows.Forms.PictureBox nextButton;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label quantity0Label;
        private System.Windows.Forms.Label want1Label;
        private System.Windows.Forms.Label forTrade1Label;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label quantity1Label;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label quantity2Label;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label quantity3Label;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label quantity4Label;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label forTrade6Label;
        private System.Windows.Forms.Label want6Label;
        private System.Windows.Forms.Label quantity5Label;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label quantity6Label;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label quantity7Label;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label quantity8Label;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label quantity9Label;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label numberOfCardsLabel;
        private System.Windows.Forms.PictureBox slot0;
        private System.Windows.Forms.PictureBox slot1;
        private System.Windows.Forms.PictureBox slot2;
        private System.Windows.Forms.PictureBox slot3;
        private System.Windows.Forms.PictureBox slot4;
        private System.Windows.Forms.PictureBox slot5;
        private System.Windows.Forms.PictureBox slot6;
        private System.Windows.Forms.PictureBox slot7;
        private System.Windows.Forms.PictureBox slot9;
        private System.Windows.Forms.PictureBox slot8;
        private System.Windows.Forms.Panel previewCard;
        private System.Windows.Forms.Button filterButton;
    }
}