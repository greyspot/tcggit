﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using TradingCardGame.forms.widgets;
using TradingCardGame.services.account;
using TradingCardGame.services.combat;
using TradingCardGame.services.data;
using TradingCardGame.services.inventory;
using TradingCardGame.services.modifers;
using TradingCardGame.services.multiplayer;
using TradingCardGame.services.opacity;
using TradingCardGame.services.reward;
using TradingCardGame.services.sound;

namespace TradingCardGame
{
    public partial class CombatPage : UserControl
    {
        public CombatPage()
        {
            InitializeComponent();

            try
            {
                playersAvatarPictureBox.BackgroundImage = Image.FromFile(services.database.DatabaseService.GetValue("users", "avatar", "username", AccountService.GetUsername()));
            }
            catch
            {
                playersAvatarPictureBox.BackgroundImage = Images.avatars.pictures.blank;
            }
            setStats();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        void gmh_TheMouseMoved()
        {
            //Point cur_pos = Cursor.Position;
            //MessageBox.Show(cur_pos.ToString());
            //if (firstPlaymatCard.Visible)
            //    firstPlaymatCard.Location = Cursor.Position;
        }

        Panel currentCard;
        Panel leavingCard;
        Panel selectedCard;

        bool retreat = false;
        int seconds;
        int totalTimeSpent;
        int numberOfTurns = 5;
        int effectInteger = 0;

        CombatService comSrv = new CombatService();
        InventoryService invSrv = new InventoryService();
        ModifierService modSrv = new ModifierService();
        MultiplayerService multSrv = new MultiplayerService();
        OpacityService opacitySrv = new OpacityService();
        RewardService rewSrv = new RewardService();

        private void AttackButton_Click(object sender, EventArgs e)
        {
            attack();
        }

        private void attack()
        {

        }

        private void heal()
        {

        }

        private void gameLost()
        {
            timeLeftTimer.Enabled = false;
            outcomePictureBox.Image = Images.dialogs.dialogs.defeat_game;

            if (retreat)
            {
                int getLosses = int.Parse((services.database.DatabaseService.GetValue("users", "losses", "username", usernameLabel.Text)));
                AccountService.WriteNewValue((getLosses + 1).ToString(), "losses");
                creditsRewardLabel.Text = "+0 Credits";
                expRewardLabel.Text = "+0 Experience";
            }
            else
            {
                creditsRewardLabel.Text = "+" + rewSrv.rewardCredits(false).ToString() + " Credits";
                rewSrv.getRewards(false);
            }
            outcomePanel.Visible = true;
        }

        private void gameWon()
        {
            timeLeftTimer.Enabled = false;
            creditsRewardLabel.Text = "+" + rewSrv.rewardCredits(true).ToString() + " Credits";
            outcomePanel.Visible = true;
            rewSrv.getRewards(true);
        }

        private void setStats()
        {
            usernameLabel.Text = AccountService.GetUsername();
        }

        private void addLineToChatBox(string line)
        {
            chatListBox.Items.Add(line);
            chatListBox.TopIndex = chatListBox.Items.Count - 1;
        }

        private void addLinesToChatBox(string[] lines)
        {
            chatListBox.Items.AddRange(lines);
            chatListBox.TopIndex = chatListBox.Items.Count - 1;
        }

        private void setAIStats()
        {

        }

        private void setCards()
        {
            // Set Titles
            card1Title.Text = chatListBox.Items[1].ToString();
            card2Title.Text = chatListBox.Items[2].ToString();
            card3Title.Text = chatListBox.Items[3].ToString();
            card4Title.Text = chatListBox.Items[4].ToString();
            card5Title.Text = chatListBox.Items[5].ToString();
            card6Title.Text = chatListBox.Items[6].ToString().Substring(4);
        }

        private void closeAllCards()
        {
            if (currentCard != null)
            {
                leavingCard = currentCard;
                currentCard = null;
                cardLeaveAnimation(leavingCard);
            }
        }

        private void CombatPage_Load(object sender, EventArgs e)
        {
            TemporaryData.setCurrentForm(1);
            Navigator navBar = new Navigator();
            Controls.Add(navBar);
            navBar.BringToFront();

            if (string.IsNullOrEmpty(CombatService.getEnemyName()))
            {
                enemyNameLabel.Text = services.ai.AIService.generateName();
                enemiesAvatarPictureBox.Image = services.ai.AIService.generateAvatar();
                addLinesToChatBox(comSrv.cardsDrawn());
                setCards();
                addLineToChatBox("* " + enemyNameLabel.Text + " draws 6 cards.");
                setAIStats();
            }
            else
            {
                enemyNameLabel.Text = CombatService.getEnemyName();
                try
                {
                    enemiesAvatarPictureBox.ImageLocation = services.database.DatabaseService.GetValue("users", "avatar", "username", enemyNameLabel.Text);
                }
                catch
                {
                    enemiesAvatarPictureBox.Image = Images.avatars.pictures.blank;
                }
            }
        }

        private void setPercentage(ProgressBar bar, Label percentageLabel, string actionOrHealth)
        {
            double percent = Math.Round(((double)bar.Value / modSrv.getTotalActionOrHealth(actionOrHealth)), 2);
            percent *= 100;
            percentageLabel.Text = percent.ToString() + "%";
        }

        private void endTurnButton_Click(object sender, EventArgs e)
        {

        }

        private void endTurn()
        {

        }

        private void moveCombatMessages()
        {

        }

        private void enemyTimer_Tick(object sender, EventArgs e)
        {
            Random randomSeconds = new Random();
            int secondsNeeded = randomSeconds.Next(0, 4);
            seconds++;
            if (seconds >= secondsNeeded)
            {
                enemyTimer.Stop();
                seconds = 0;
                enemyMainbasePanel.Visible = false;
                addLineToChatBox("* " + enemyNameLabel.Text + " chooses not the shuffle their hand back into their draw deck.");
                
                // Draw phase
                mainbaseLabel.Text = "Draw Phase";
                Thread.Sleep(1000);
                phaseButton.Visible = false;
                secondPhaseButton.Visible = false;
                mainbasePanel.Visible = true;
                addLineToChatBox("* " + "You skip your quest phase (no ready abilities that have a level).");

                // Quest phase
                mainbaseLabel.Text = "Quest Phase";
                Thread.Sleep(1000);

                // Ready phase
                mainbaseLabel.Text = "Ready Phase";
                Thread.Sleep(1000);

                // Main phase
                mainbaseLabel.Text = "Main Phase: Play cards and take\nactions.";
                // set phase button picture to "Okay"
                phaseButton.Visible = true;
                // add green blinking animation for unit cards
                // allow user to chose a unit card to play on the playmat
            }
        }

        private void healButton_Click(object sender, EventArgs e)
        {
            heal();
        }

        private void timeLeftTimer_Tick(object sender, EventArgs e)
        {
            totalTimeSpent++;
            if (totalTimeSpent >= 61)
                endTurn();
        }

        private void abilityCardUsed(PictureBox abilityBox, int cardNumber)
        {
            if (abilityBox.Image == null)
                return;

            if (numberOfTurns < 5)
            {
                MessageBox.Show("Sorry, but this ability card is currently on cooldown.");
                return;
            }
        }

        private void retreatButton_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to retreat?", "", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                retreat = true;
                gameLost();
            }
        }

        private void CombatPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to exit in the middle of a battle?", "", MessageBoxButtons.YesNo);

            if (dr == DialogResult.No)
            {
                retreat = false;
                e.Cancel = true;
                return;
            }
            //Map menu = new Map();
            //menu.Show();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {

        }

        private void effectTimer_Tick(object sender, EventArgs e)
        {
            if (effectInteger < 10)
            {
                phaseButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("phase_button_hint_0" + effectInteger++);

                if (secondPhaseButton.Visible)
                    secondPhaseButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("phase_button_hint_0" + effectInteger);
            }
                
            else if (effectInteger < 29)
            {
                phaseButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("phase_button_hint_" + effectInteger++);

                if (secondPhaseButton.Visible)
                    secondPhaseButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("phase_button_hint_" + effectInteger);
            }
                
            else
            {
                effectInteger = 0;
                phaseButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("phase_button_hint_0" + effectInteger++);

                if (secondPhaseButton.Visible)
                    secondPhaseButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("phase_button_hint_0" + effectInteger);
            }
                

            /*if (effectInteger < 10)
                effectPictureBox.Image = (Image)Properties.Resources.ResourceManager.GetObject("card_attack_heal_blast_00" + effectInteger++);
            else if (effectInteger < 39)
                effectPictureBox.Image = (Image)Properties.Resources.ResourceManager.GetObject("card_attack_heal_blast_0" + effectInteger++);
            else if (effectInteger == 39)
            {
                effectTimer.Enabled = false;
                effectInteger = 0;
            }*/
        }

        private void animateCombatEvent(string user, string combatEvent, int amount)
        {

        }

        private void cardEnterAnimation(Panel card)
        {
            currentCard = card;
            Cursor = new Cursor(new MemoryStream(Properties.Resources.cursor_ability));

            if (currentCard == leavingCard)
                cardDespandAnimationTimer.Enabled = false;

            cardExpandAnimationTimer.Enabled = true;
        }

        private void cardLeaveAnimation(Panel card)
        {
            leavingCard = card;
            currentCard = null;
            Cursor = Cursors.Default;

            //if (leavingCard == card)
            //    return;

            cardExpandAnimationTimer.Enabled = false;
            cardDespandAnimationTimer.Enabled = true;
        }

        private void cardExpandAnimationTimer_Tick(object sender, EventArgs e)
        {
            if (currentCard == null)
                return;

            if (currentCard.Location.Y <= ClientSize.Height - currentCard.Height)
            {
                cardExpandAnimationTimer.Enabled = false;
                return;
            }

            currentCard.Location = new Point(currentCard.Location.X, currentCard.Location.Y - 5);

            if (currentCard.Location.Y <= ClientSize.Height - currentCard.Height)
                cardExpandAnimationTimer.Enabled = false;
        }

        private void cardDespandAnimationTimer_Tick(object sender, EventArgs e)
        {
            leavingCard.Location = new Point(leavingCard.Location.X, leavingCard.Location.Y + 5);

            if (leavingCard.Location.Y >= 730)
            {
                cardDespandAnimationTimer.Enabled = false;
                leavingCard = null;
            }
        }

        private void phaseButton_MouseEnter(object sender, EventArgs e)
        {
            phaseButton.BackgroundImage = Images.graphicsitems.gamescreen.mainbase_button_over;
        }

        private void phaseButton_MouseDown(object sender, MouseEventArgs e)
        {
            phaseButton.BackgroundImage = Images.graphicsitems.gamescreen.mainbase_button_down;
        }

        private void phaseButton_MouseLeave(object sender, EventArgs e)
        {
            phaseButton.BackgroundImage = Images.graphicsitems.gamescreen.mainbase_button;
        }

        private void phaseButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            mainbasePanel.Visible = false;
        }

        private void secondPhaseButton_MouseEnter(object sender, EventArgs e)
        {
            secondPhaseButton.BackgroundImage = Images.graphicsitems.gamescreen.mainbase_button_over;
        }

        private void secondPhaseButton_MouseLeave(object sender, EventArgs e)
        {
            secondPhaseButton.BackgroundImage = Images.graphicsitems.gamescreen.mainbase_button;
        }

        private void secondPhaseButton_MouseDown(object sender, MouseEventArgs e)
        {
            secondPhaseButton.BackgroundImage = Images.graphicsitems.gamescreen.mainbase_button_down;
        }

        private void secondPhaseButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            mainbasePanel.Visible = false;
            effectTimer.Enabled = false;
            addLineToChatBox("* You choose not to shuffle your hand back into your draw deck.");
            enemyMainbaseLabel.Text = "Please wait while " + enemyNameLabel.Text + " completes the action.";
            enemyMainbaseLabel.Left = (enemyMainbasePanel.Width - enemyMainbaseLabel.Width) / 2;
            enemyMainbasePanel.Visible = true;
            enemyTimer.Enabled = true;
        }

        private void placeCardOnPlaymat()
        {
            //firstPlaymatCard.Location = new Point(Cursor.Position.X - Left, Cursor.Position.Y - Top);
            //firstPlaymatCard.Visible = true;
        }

        private void card5_MouseDown(object sender, MouseEventArgs e)
        {
            //cardFromHand = card5;
            //placeCardOnPlaymat();
        }

        private void CombatPage_MouseMove(object sender, MouseEventArgs e)
        {
            //firstPlaymatCard.Location = new Point(e.X, e.Y);
            //MessageBox.Show("debug");
        }

        private void card5_MouseUp(object sender, MouseEventArgs e)
        {
            //cardFromHand.Visible = true;
            //firstPlaymatCard.Visible = false;
        }

        private void CombatPage_MouseDown(object sender, MouseEventArgs e)
        {
            //firstPlaymatCard.Location = new Point(e.X, e.Y);
            //MessageBox.Show("debug");
        }

        private void card5_MouseMove(object sender, MouseEventArgs e)
        {
            //firstPlaymatCard.Location = new Point(e.X, e.Y);
        }

        private void card5_Click(object sender, EventArgs e)
        {
            setSelectedCard(card5);
            //card5.Visible = false;
            //firstPlaymatCard.Visible = true;
        }

        private void setSelectedCard(Panel card)
        {
            SoundService.PlaySound("button");
            card.Location = new Point(card.Location.X, ClientSize.Height - card.Height);
            card.BackgroundImage = opacitySrv.SetImageOpacity(card.BackgroundImage, 0.6f);
            selectedCard = card;
        }

        private void card1Cover_MouseEnter(object sender, EventArgs e)
        {
            cardEnterAnimation(card1);
        }

        private void card1Level_MouseEnter(object sender, EventArgs e)
        {
            cardEnterAnimation(card1);
        }

        private void card1Title_MouseEnter(object sender, EventArgs e)
        {
            cardEnterAnimation(card1);
        }

        private void card1Description_MouseEnter(object sender, EventArgs e)
        {
            cardEnterAnimation(card1);
        }

        private void card1Subtitle_MouseEnter(object sender, EventArgs e)
        {
            cardEnterAnimation(card1);
        }

        private void itemsToolbarPanel_MouseEnter(object sender, EventArgs e)
        {
            closeAllCards();
        }

        private void itemsPanel1_MouseEnter(object sender, EventArgs e)
        {
            closeAllCards();
        }

        private void itemsPanel2_MouseEnter(object sender, EventArgs e)
        {
            closeAllCards();
        }
    }
}
