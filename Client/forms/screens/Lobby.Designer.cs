﻿namespace TradingCardGame.forms
{
    partial class Lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lobby));
            this.panel1 = new System.Windows.Forms.Panel();
            this.quickJoinButton = new System.Windows.Forms.Button();
            this.createButton = new System.Windows.Forms.Button();
            activeGamesPanel = new System.Windows.Forms.Panel();
            matchBackgroundPanel1 = new System.Windows.Forms.Panel();
            this.matchPanel1 = new System.Windows.Forms.Panel();
            this.idMatchLabel1 = new System.Windows.Forms.Label();
            this.friendsOnlyPictureBox1 = new System.Windows.Forms.PictureBox();
            this.timedGamePictureBox1 = new System.Windows.Forms.PictureBox();
            this.passwordRequiredPictureBox1 = new System.Windows.Forms.PictureBox();
            firstPlayerSlot1 = new System.Windows.Forms.PictureBox();
            secondPlayerSlot1 = new System.Windows.Forms.PictureBox();
            this.numberOfPlayersLabel1 = new System.Windows.Forms.Label();
            this.formatLabel1 = new System.Windows.Forms.Label();
            this.thirdPlayerSlot1 = new System.Windows.Forms.PictureBox();
            this.fourthPlayerSlot1 = new System.Windows.Forms.PictureBox();
            this.matchNameLabel1 = new System.Windows.Forms.Label();
            this.matchInfoButton1 = new System.Windows.Forms.PictureBox();
            this.observeMatchButton1 = new System.Windows.Forms.PictureBox();
            this.lvldButton = new System.Windows.Forms.PictureBox();
            this.joinMatchButton1 = new System.Windows.Forms.PictureBox();
            this.inputChatBox = new System.Windows.Forms.TextBox();
            this.chatBox = new System.Windows.Forms.ListBox();
            this.matchesLabel = new System.Windows.Forms.Label();
            this.usersLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.playNowButton = new System.Windows.Forms.PictureBox();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.playersNameListBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            activeGamesPanel.SuspendLayout();
            matchBackgroundPanel1.SuspendLayout();
            this.matchPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.friendsOnlyPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timedGamePictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRequiredPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(firstPlayerSlot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(secondPlayerSlot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thirdPlayerSlot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fourthPlayerSlot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchInfoButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.observeMatchButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvldButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinMatchButton1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playNowButton)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.quickJoinButton);
            this.panel1.Controls.Add(this.createButton);
            this.panel1.Controls.Add(activeGamesPanel);
            this.panel1.Controls.Add(this.inputChatBox);
            this.panel1.Controls.Add(this.chatBox);
            this.panel1.Controls.Add(this.matchesLabel);
            this.panel1.Controls.Add(this.usersLabel);
            this.panel1.Location = new System.Drawing.Point(13, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(698, 753);
            this.panel1.TabIndex = 0;
            // 
            // quickJoinButton
            // 
            this.quickJoinButton.BackColor = System.Drawing.Color.Transparent;
            this.quickJoinButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.quickJoinButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.quickJoinButton.FlatAppearance.BorderSize = 0;
            this.quickJoinButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.quickJoinButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.quickJoinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quickJoinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quickJoinButton.ForeColor = System.Drawing.Color.Black;
            this.quickJoinButton.Location = new System.Drawing.Point(127, 16);
            this.quickJoinButton.Name = "quickJoinButton";
            this.quickJoinButton.Size = new System.Drawing.Size(120, 29);
            this.quickJoinButton.TabIndex = 49;
            this.quickJoinButton.Text = "Quick Join";
            this.quickJoinButton.UseVisualStyleBackColor = false;
            // 
            // createButton
            // 
            this.createButton.BackColor = System.Drawing.Color.Transparent;
            this.createButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.createButton.FlatAppearance.BorderSize = 0;
            this.createButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.createButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.createButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createButton.ForeColor = System.Drawing.Color.Black;
            this.createButton.Location = new System.Drawing.Point(22, 16);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(100, 29);
            this.createButton.TabIndex = 48;
            this.createButton.Text = "Create";
            this.createButton.UseVisualStyleBackColor = false;
            this.createButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // activeGamesPanel
            // 
            activeGamesPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            activeGamesPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            activeGamesPanel.Controls.Add(matchBackgroundPanel1);
            activeGamesPanel.Location = new System.Drawing.Point(22, 65);
            activeGamesPanel.Name = "activeGamesPanel";
            activeGamesPanel.Size = new System.Drawing.Size(654, 420);
            activeGamesPanel.TabIndex = 7;
            // 
            // matchBackgroundPanel1
            // 
            matchBackgroundPanel1.BackColor = System.Drawing.Color.Transparent;
            matchBackgroundPanel1.BackgroundImage = global::TradingCardGame.Images.avatars.pictures.avatar_189;
            matchBackgroundPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            matchBackgroundPanel1.Controls.Add(this.matchPanel1);
            matchBackgroundPanel1.Location = new System.Drawing.Point(15, 3);
            matchBackgroundPanel1.Name = "matchBackgroundPanel1";
            matchBackgroundPanel1.Size = new System.Drawing.Size(243, 160);
            matchBackgroundPanel1.TabIndex = 7;
            matchBackgroundPanel1.Visible = false;
            // 
            // matchPanel1
            // 
            this.matchPanel1.BackColor = System.Drawing.Color.Transparent;
            this.matchPanel1.BackgroundImage = global::TradingCardGame.Images.graphicsitems.match.matchbase_open;
            this.matchPanel1.Controls.Add(this.idMatchLabel1);
            this.matchPanel1.Controls.Add(this.friendsOnlyPictureBox1);
            this.matchPanel1.Controls.Add(this.timedGamePictureBox1);
            this.matchPanel1.Controls.Add(this.passwordRequiredPictureBox1);
            this.matchPanel1.Controls.Add(firstPlayerSlot1);
            this.matchPanel1.Controls.Add(secondPlayerSlot1);
            this.matchPanel1.Controls.Add(this.numberOfPlayersLabel1);
            this.matchPanel1.Controls.Add(this.formatLabel1);
            this.matchPanel1.Controls.Add(this.thirdPlayerSlot1);
            this.matchPanel1.Controls.Add(this.fourthPlayerSlot1);
            this.matchPanel1.Controls.Add(this.matchNameLabel1);
            this.matchPanel1.Controls.Add(this.matchInfoButton1);
            this.matchPanel1.Controls.Add(this.observeMatchButton1);
            this.matchPanel1.Controls.Add(this.lvldButton);
            this.matchPanel1.Controls.Add(this.joinMatchButton1);
            this.matchPanel1.Location = new System.Drawing.Point(-7, -8);
            this.matchPanel1.Name = "matchPanel1";
            this.matchPanel1.Size = new System.Drawing.Size(243, 160);
            this.matchPanel1.TabIndex = 0;
            // 
            // idMatchLabel1
            // 
            this.idMatchLabel1.AutoSize = true;
            this.idMatchLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idMatchLabel1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.idMatchLabel1.Location = new System.Drawing.Point(97, 5);
            this.idMatchLabel1.Name = "idMatchLabel1";
            this.idMatchLabel1.Size = new System.Drawing.Size(42, 15);
            this.idMatchLabel1.TabIndex = 15;
            this.idMatchLabel1.Text = "#2654";
            // 
            // friendsOnlyPictureBox1
            // 
            this.friendsOnlyPictureBox1.Location = new System.Drawing.Point(156, 70);
            this.friendsOnlyPictureBox1.Name = "friendsOnlyPictureBox1";
            this.friendsOnlyPictureBox1.Size = new System.Drawing.Size(19, 19);
            this.friendsOnlyPictureBox1.TabIndex = 14;
            this.friendsOnlyPictureBox1.TabStop = false;
            // 
            // timedGamePictureBox1
            // 
            this.timedGamePictureBox1.Location = new System.Drawing.Point(130, 70);
            this.timedGamePictureBox1.Name = "timedGamePictureBox1";
            this.timedGamePictureBox1.Size = new System.Drawing.Size(19, 19);
            this.timedGamePictureBox1.TabIndex = 13;
            this.timedGamePictureBox1.TabStop = false;
            // 
            // passwordRequiredPictureBox1
            // 
            this.passwordRequiredPictureBox1.Location = new System.Drawing.Point(104, 70);
            this.passwordRequiredPictureBox1.Name = "passwordRequiredPictureBox1";
            this.passwordRequiredPictureBox1.Size = new System.Drawing.Size(19, 19);
            this.passwordRequiredPictureBox1.TabIndex = 8;
            this.passwordRequiredPictureBox1.TabStop = false;
            // 
            // firstPlayerSlot1
            // 
            firstPlayerSlot1.Image = global::TradingCardGame.Images.graphicsitems.match.playerslot_none;
            firstPlayerSlot1.Location = new System.Drawing.Point(102, 39);
            firstPlayerSlot1.Name = "firstPlayerSlot1";
            firstPlayerSlot1.Size = new System.Drawing.Size(27, 27);
            firstPlayerSlot1.TabIndex = 12;
            firstPlayerSlot1.TabStop = false;
            // 
            // secondPlayerSlot1
            // 
            secondPlayerSlot1.Image = global::TradingCardGame.Images.graphicsitems.match.playerslot_none;
            secondPlayerSlot1.Location = new System.Drawing.Point(134, 39);
            secondPlayerSlot1.Name = "secondPlayerSlot1";
            secondPlayerSlot1.Size = new System.Drawing.Size(27, 27);
            secondPlayerSlot1.TabIndex = 11;
            secondPlayerSlot1.TabStop = false;
            // 
            // numberOfPlayersLabel1
            // 
            this.numberOfPlayersLabel1.AutoSize = true;
            this.numberOfPlayersLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfPlayersLabel1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.numberOfPlayersLabel1.Location = new System.Drawing.Point(188, 69);
            this.numberOfPlayersLabel1.Name = "numberOfPlayersLabel1";
            this.numberOfPlayersLabel1.Size = new System.Drawing.Size(32, 33);
            this.numberOfPlayersLabel1.TabIndex = 9;
            this.numberOfPlayersLabel1.Text = "1";
            // 
            // formatLabel1
            // 
            this.formatLabel1.AutoSize = true;
            this.formatLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formatLabel1.ForeColor = System.Drawing.Color.Black;
            this.formatLabel1.Location = new System.Drawing.Point(99, 16);
            this.formatLabel1.Name = "formatLabel1";
            this.formatLabel1.Size = new System.Drawing.Size(93, 24);
            this.formatLabel1.TabIndex = 8;
            this.formatLabel1.Text = "Standard";
            // 
            // thirdPlayerSlot1
            // 
            this.thirdPlayerSlot1.Image = global::TradingCardGame.Images.graphicsitems.match.playerslot_none;
            this.thirdPlayerSlot1.Location = new System.Drawing.Point(167, 39);
            this.thirdPlayerSlot1.Name = "thirdPlayerSlot1";
            this.thirdPlayerSlot1.Size = new System.Drawing.Size(27, 27);
            this.thirdPlayerSlot1.TabIndex = 7;
            this.thirdPlayerSlot1.TabStop = false;
            // 
            // fourthPlayerSlot1
            // 
            this.fourthPlayerSlot1.Image = global::TradingCardGame.Images.graphicsitems.match.playerslot_none;
            this.fourthPlayerSlot1.Location = new System.Drawing.Point(199, 39);
            this.fourthPlayerSlot1.Name = "fourthPlayerSlot1";
            this.fourthPlayerSlot1.Size = new System.Drawing.Size(27, 27);
            this.fourthPlayerSlot1.TabIndex = 6;
            this.fourthPlayerSlot1.TabStop = false;
            // 
            // matchNameLabel1
            // 
            this.matchNameLabel1.AutoSize = true;
            this.matchNameLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchNameLabel1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.matchNameLabel1.Location = new System.Drawing.Point(77, 138);
            this.matchNameLabel1.Name = "matchNameLabel1";
            this.matchNameLabel1.Size = new System.Drawing.Size(127, 15);
            this.matchNameLabel1.TabIndex = 5;
            this.matchNameLabel1.Text = "Iosnowore\'s Match";
            // 
            // matchInfoButton1
            // 
            this.matchInfoButton1.Image = global::TradingCardGame.Images.graphicsitems.match.button_info;
            this.matchInfoButton1.Location = new System.Drawing.Point(140, 92);
            this.matchInfoButton1.Name = "matchInfoButton1";
            this.matchInfoButton1.Size = new System.Drawing.Size(32, 32);
            this.matchInfoButton1.TabIndex = 4;
            this.matchInfoButton1.TabStop = false;
            // 
            // observeMatchButton1
            // 
            this.observeMatchButton1.Image = global::TradingCardGame.Images.graphicsitems.match.button_observe;
            this.observeMatchButton1.Location = new System.Drawing.Point(107, 92);
            this.observeMatchButton1.Name = "observeMatchButton1";
            this.observeMatchButton1.Size = new System.Drawing.Size(32, 32);
            this.observeMatchButton1.TabIndex = 3;
            this.observeMatchButton1.TabStop = false;
            // 
            // lvldButton
            // 
            this.lvldButton.Image = global::TradingCardGame.Images.graphicsitems.match.button_lvd;
            this.lvldButton.Location = new System.Drawing.Point(73, 92);
            this.lvldButton.Name = "lvldButton";
            this.lvldButton.Size = new System.Drawing.Size(32, 32);
            this.lvldButton.TabIndex = 2;
            this.lvldButton.TabStop = false;
            // 
            // joinMatchButton1
            // 
            this.joinMatchButton1.Image = global::TradingCardGame.Images.graphicsitems.match.button_join;
            this.joinMatchButton1.Location = new System.Drawing.Point(13, 105);
            this.joinMatchButton1.Name = "joinMatchButton1";
            this.joinMatchButton1.Size = new System.Drawing.Size(44, 41);
            this.joinMatchButton1.TabIndex = 1;
            this.joinMatchButton1.TabStop = false;
            this.joinMatchButton1.Click += new System.EventHandler(this.JoinMatchButton1_Click);
            this.joinMatchButton1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.JoinMatchButton1_MouseDown);
            this.joinMatchButton1.MouseEnter += new System.EventHandler(this.JoinMatchButton1_MouseEnter);
            this.joinMatchButton1.MouseLeave += new System.EventHandler(this.JoinMatchButton1_MouseLeave);
            // 
            // inputChatBox
            // 
            this.inputChatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.inputChatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputChatBox.ForeColor = System.Drawing.Color.White;
            this.inputChatBox.Location = new System.Drawing.Point(22, 709);
            this.inputChatBox.Name = "inputChatBox";
            this.inputChatBox.Size = new System.Drawing.Size(654, 22);
            this.inputChatBox.TabIndex = 5;
            this.inputChatBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InputChatBox_KeyDown);
            // 
            // chatBox
            // 
            this.chatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.chatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chatBox.ForeColor = System.Drawing.Color.White;
            this.chatBox.FormattingEnabled = true;
            this.chatBox.HorizontalScrollbar = true;
            this.chatBox.ItemHeight = 16;
            this.chatBox.Location = new System.Drawing.Point(22, 491);
            this.chatBox.Name = "chatBox";
            this.chatBox.Size = new System.Drawing.Size(654, 212);
            this.chatBox.TabIndex = 4;
            // 
            // matchesLabel
            // 
            this.matchesLabel.AutoSize = true;
            this.matchesLabel.BackColor = System.Drawing.Color.Transparent;
            this.matchesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchesLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.matchesLabel.Location = new System.Drawing.Point(259, 31);
            this.matchesLabel.Name = "matchesLabel";
            this.matchesLabel.Size = new System.Drawing.Size(72, 16);
            this.matchesLabel.TabIndex = 3;
            this.matchesLabel.Text = "Matches: 0";
            // 
            // usersLabel
            // 
            this.usersLabel.AutoSize = true;
            this.usersLabel.BackColor = System.Drawing.Color.Transparent;
            this.usersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usersLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.usersLabel.Location = new System.Drawing.Point(259, 15);
            this.usersLabel.Name = "usersLabel";
            this.usersLabel.Size = new System.Drawing.Size(74, 16);
            this.usersLabel.TabIndex = 2;
            this.usersLabel.Text = "Users: 0 / 6";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.playNowButton);
            this.panel2.Location = new System.Drawing.Point(722, 11);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(294, 519);
            this.panel2.TabIndex = 1;
            // 
            // playNowButton
            // 
            this.playNowButton.BackColor = System.Drawing.Color.Black;
            this.playNowButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.playNowButton.Image = global::TradingCardGame.Images.dialogs.dialogs.practice_button;
            this.playNowButton.Location = new System.Drawing.Point(23, 12);
            this.playNowButton.Name = "playNowButton";
            this.playNowButton.Size = new System.Drawing.Size(248, 178);
            this.playNowButton.TabIndex = 0;
            this.playNowButton.TabStop = false;
            this.playNowButton.Click += new System.EventHandler(this.PlayNowButton_Click);
            this.playNowButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PlayNowButton_MouseDown);
            this.playNowButton.MouseEnter += new System.EventHandler(this.PlayNowButton_MouseEnter);
            this.playNowButton.MouseLeave += new System.EventHandler(this.PlayNowButton_MouseLeave);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "bronze");
            this.imageList.Images.SetKeyName(1, "yes");
            // 
            // playersNameListBox
            // 
            this.playersNameListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.playersNameListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.playersNameListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playersNameListBox.ForeColor = System.Drawing.Color.White;
            this.playersNameListBox.FullRowSelect = true;
            this.playersNameListBox.Location = new System.Drawing.Point(722, 536);
            this.playersNameListBox.MultiSelect = false;
            this.playersNameListBox.Name = "playersNameListBox";
            this.playersNameListBox.Size = new System.Drawing.Size(294, 225);
            this.playersNameListBox.SmallImageList = this.imageList;
            this.playersNameListBox.TabIndex = 2;
            this.playersNameListBox.UseCompatibleStateImageBehavior = false;
            this.playersNameListBox.View = System.Windows.Forms.View.Details;
            this.playersNameListBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PlayersNameListBox_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Username";
            this.columnHeader1.Width = 288;
            // 
            // Lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::TradingCardGame.Images.backgrounds.backgrounds.game_bg_01;
            this.Controls.Add(this.playersNameListBox);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "Lobby";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.CasualLobby_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            activeGamesPanel.ResumeLayout(false);
            matchBackgroundPanel1.ResumeLayout(false);
            this.matchPanel1.ResumeLayout(false);
            this.matchPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.friendsOnlyPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timedGamePictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRequiredPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(firstPlayerSlot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(secondPlayerSlot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thirdPlayerSlot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fourthPlayerSlot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchInfoButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.observeMatchButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvldButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinMatchButton1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playNowButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label matchesLabel;
        private System.Windows.Forms.TextBox inputChatBox;
        private System.Windows.Forms.PictureBox playNowButton;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Panel matchPanel1;
        private System.Windows.Forms.Label numberOfPlayersLabel1;
        private System.Windows.Forms.Label formatLabel1;
        private System.Windows.Forms.PictureBox thirdPlayerSlot1;
        private System.Windows.Forms.PictureBox fourthPlayerSlot1;
        private System.Windows.Forms.Label matchNameLabel1;
        private System.Windows.Forms.PictureBox matchInfoButton1;
        private System.Windows.Forms.PictureBox observeMatchButton1;
        private System.Windows.Forms.PictureBox lvldButton;
        private System.Windows.Forms.PictureBox joinMatchButton1;
        private System.Windows.Forms.PictureBox passwordRequiredPictureBox1;
        private System.Windows.Forms.PictureBox timedGamePictureBox1;
        private System.Windows.Forms.PictureBox friendsOnlyPictureBox1;
        private System.Windows.Forms.Label idMatchLabel1;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Button quickJoinButton;
        private System.Windows.Forms.Label usersLabel;
        public System.Windows.Forms.ListBox chatBox;
        private System.Windows.Forms.ListView playersNameListBox;
        private static System.Windows.Forms.Panel activeGamesPanel;
        private static System.Windows.Forms.Panel matchBackgroundPanel1;
        private static System.Windows.Forms.PictureBox secondPlayerSlot1;
        private static System.Windows.Forms.PictureBox firstPlayerSlot1;
    }
}