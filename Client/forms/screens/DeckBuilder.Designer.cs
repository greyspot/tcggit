﻿namespace TradingCardGame
{
    partial class DeckBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainPanel = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.removeAllFiltersButton = new System.Windows.Forms.Button();
            this.showValidQuestsButton = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.validDeckPictureBox = new System.Windows.Forms.PictureBox();
            this.standardButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.scrollBar2 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.scrollbar1 = new System.Windows.Forms.PictureBox();
            this.bottomScrollButton1 = new System.Windows.Forms.PictureBox();
            this.topScrollButton1 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label61 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.exitButton = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.filterButton = new System.Windows.Forms.Button();
            this.mainPanel.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.validDeckPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scrollBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scrollbar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomScrollButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topScrollButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.Black;
            this.mainPanel.BackgroundImage = global::TradingCardGame.Images.borders.borders.navframe_header;
            this.mainPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mainPanel.Controls.Add(this.panel8);
            this.mainPanel.Controls.Add(this.panel1);
            this.mainPanel.Controls.Add(this.label61);
            this.mainPanel.Controls.Add(this.pictureBox20);
            this.mainPanel.Controls.Add(this.pictureBox19);
            this.mainPanel.Controls.Add(this.searchButton);
            this.mainPanel.Controls.Add(this.textBox1);
            this.mainPanel.Controls.Add(this.exitButton);
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Controls.Add(this.filterButton);
            this.mainPanel.Location = new System.Drawing.Point(34, 12);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(975, 745);
            this.mainPanel.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.panel11);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.pictureBox1);
            this.panel8.Controls.Add(this.pictureBox2);
            this.panel8.Controls.Add(this.button5);
            this.panel8.Controls.Add(this.button4);
            this.panel8.Controls.Add(this.button3);
            this.panel8.Controls.Add(this.button2);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(27, 287);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(925, 441);
            this.panel8.TabIndex = 60;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel18);
            this.panel11.Controls.Add(this.panel17);
            this.panel11.Controls.Add(this.panel16);
            this.panel11.Controls.Add(this.panel15);
            this.panel11.Controls.Add(this.panel14);
            this.panel11.Controls.Add(this.panel13);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.removeAllFiltersButton);
            this.panel11.Controls.Add(this.showValidQuestsButton);
            this.panel11.Location = new System.Drawing.Point(3, 69);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(200, 350);
            this.panel11.TabIndex = 64;
            // 
            // panel18
            // 
            this.panel18.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.button_generic;
            this.panel18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel18.Location = new System.Drawing.Point(43, 236);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(147, 31);
            this.panel18.TabIndex = 70;
            // 
            // panel17
            // 
            this.panel17.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.button_generic;
            this.panel17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel17.Location = new System.Drawing.Point(43, 197);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(147, 31);
            this.panel17.TabIndex = 69;
            // 
            // panel16
            // 
            this.panel16.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.button_generic;
            this.panel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel16.Location = new System.Drawing.Point(43, 160);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(147, 31);
            this.panel16.TabIndex = 68;
            // 
            // panel15
            // 
            this.panel15.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.button_generic;
            this.panel15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel15.Location = new System.Drawing.Point(43, 123);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(147, 31);
            this.panel15.TabIndex = 67;
            // 
            // panel14
            // 
            this.panel14.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.button_drawdeck;
            this.panel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel14.Location = new System.Drawing.Point(11, 83);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(179, 34);
            this.panel14.TabIndex = 66;
            // 
            // panel13
            // 
            this.panel13.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.button_quest;
            this.panel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel13.Location = new System.Drawing.Point(11, 43);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(179, 34);
            this.panel13.TabIndex = 65;
            // 
            // panel12
            // 
            this.panel12.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.button_avatar;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel12.Location = new System.Drawing.Point(11, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(179, 34);
            this.panel12.TabIndex = 64;
            // 
            // removeAllFiltersButton
            // 
            this.removeAllFiltersButton.BackColor = System.Drawing.Color.Black;
            this.removeAllFiltersButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.removeAllFiltersButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.removeAllFiltersButton.FlatAppearance.BorderSize = 0;
            this.removeAllFiltersButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.removeAllFiltersButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.removeAllFiltersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeAllFiltersButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeAllFiltersButton.ForeColor = System.Drawing.Color.Black;
            this.removeAllFiltersButton.Location = new System.Drawing.Point(13, 308);
            this.removeAllFiltersButton.Name = "removeAllFiltersButton";
            this.removeAllFiltersButton.Size = new System.Drawing.Size(175, 29);
            this.removeAllFiltersButton.TabIndex = 63;
            this.removeAllFiltersButton.Text = "Remove All Filters";
            this.removeAllFiltersButton.UseVisualStyleBackColor = false;
            // 
            // showValidQuestsButton
            // 
            this.showValidQuestsButton.BackColor = System.Drawing.Color.Black;
            this.showValidQuestsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.showValidQuestsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.showValidQuestsButton.FlatAppearance.BorderSize = 0;
            this.showValidQuestsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.showValidQuestsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.showValidQuestsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showValidQuestsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showValidQuestsButton.ForeColor = System.Drawing.Color.Black;
            this.showValidQuestsButton.Location = new System.Drawing.Point(13, 273);
            this.showValidQuestsButton.Name = "showValidQuestsButton";
            this.showValidQuestsButton.Size = new System.Drawing.Size(175, 29);
            this.showValidQuestsButton.TabIndex = 62;
            this.showValidQuestsButton.Text = "Show Valid Quests";
            this.showValidQuestsButton.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.validDeckPictureBox);
            this.panel10.Controls.Add(this.standardButton);
            this.panel10.Location = new System.Drawing.Point(716, 69);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(200, 350);
            this.panel10.TabIndex = 63;
            // 
            // validDeckPictureBox
            // 
            this.validDeckPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.validDeckPictureBox.Location = new System.Drawing.Point(55, 314);
            this.validDeckPictureBox.Name = "validDeckPictureBox";
            this.validDeckPictureBox.Size = new System.Drawing.Size(91, 29);
            this.validDeckPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.validDeckPictureBox.TabIndex = 67;
            this.validDeckPictureBox.TabStop = false;
            // 
            // standardButton
            // 
            this.standardButton.BackColor = System.Drawing.Color.Black;
            this.standardButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.standardButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.standardButton.FlatAppearance.BorderSize = 0;
            this.standardButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.standardButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.standardButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.standardButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.standardButton.ForeColor = System.Drawing.Color.Black;
            this.standardButton.Location = new System.Drawing.Point(38, 279);
            this.standardButton.Name = "standardButton";
            this.standardButton.Size = new System.Drawing.Size(125, 29);
            this.standardButton.TabIndex = 62;
            this.standardButton.Text = "Standard";
            this.standardButton.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(861, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "View";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.tableview_table;
            this.pictureBox1.Location = new System.Drawing.Point(853, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.TabIndex = 60;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Black;
            this.pictureBox2.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.tableview_graphic_down;
            this.pictureBox2.Location = new System.Drawing.Point(878, 22);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.TabIndex = 59;
            this.pictureBox2.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Black;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(351, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(90, 29);
            this.button5.TabIndex = 55;
            this.button5.Text = "Tools";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Black;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(235, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 29);
            this.button4.TabIndex = 54;
            this.button4.Text = "Save Deck";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(119, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 29);
            this.button3.TabIndex = 53;
            this.button3.Text = "Open Deck";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(3, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 29);
            this.button2.TabIndex = 52;
            this.button2.Text = "New Deck";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.collection_manager_background;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.panel20);
            this.panel9.Location = new System.Drawing.Point(210, 69);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(500, 350);
            this.panel9.TabIndex = 0;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(114)))), ((int)(((byte)(130)))));
            this.panel20.Controls.Add(this.scrollBar2);
            this.panel20.Controls.Add(this.pictureBox7);
            this.panel20.Controls.Add(this.pictureBox8);
            this.panel20.Location = new System.Drawing.Point(480, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(15, 350);
            this.panel20.TabIndex = 7;
            // 
            // scrollBar2
            // 
            this.scrollBar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.scrollBar2.Image = global::TradingCardGame.Images.scrollbar.scrollbar.scroll_bar;
            this.scrollBar2.Location = new System.Drawing.Point(0, 14);
            this.scrollBar2.Name = "scrollBar2";
            this.scrollBar2.Size = new System.Drawing.Size(15, 37);
            this.scrollBar2.TabIndex = 2;
            this.scrollBar2.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::TradingCardGame.Images.scrollbar.scrollbar.scroll_down;
            this.pictureBox7.Location = new System.Drawing.Point(0, 332);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(15, 15);
            this.pictureBox7.TabIndex = 1;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::TradingCardGame.Images.scrollbar.scrollbar.scroll_up;
            this.pictureBox8.Location = new System.Drawing.Point(0, 0);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(15, 15);
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::TradingCardGame.Images.graphicsitems.binder.collection_manager_background;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel19);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(27, 81);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(925, 200);
            this.panel1.TabIndex = 59;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(114)))), ((int)(((byte)(130)))));
            this.panel19.Controls.Add(this.scrollbar1);
            this.panel19.Controls.Add(this.bottomScrollButton1);
            this.panel19.Controls.Add(this.topScrollButton1);
            this.panel19.Location = new System.Drawing.Point(905, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(15, 198);
            this.panel19.TabIndex = 6;
            // 
            // scrollbar1
            // 
            this.scrollbar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.scrollbar1.Image = global::TradingCardGame.Images.scrollbar.scrollbar.scroll_bar;
            this.scrollbar1.Location = new System.Drawing.Point(0, 14);
            this.scrollbar1.Name = "scrollbar1";
            this.scrollbar1.Size = new System.Drawing.Size(15, 37);
            this.scrollbar1.TabIndex = 2;
            this.scrollbar1.TabStop = false;
            this.scrollbar1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.scrollbar1_MouseDown);
            this.scrollbar1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.scrollbar1_MouseMove);
            this.scrollbar1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.scrollbar1_MouseUp);
            // 
            // bottomScrollButton1
            // 
            this.bottomScrollButton1.Image = global::TradingCardGame.Images.scrollbar.scrollbar.scroll_down;
            this.bottomScrollButton1.Location = new System.Drawing.Point(0, 181);
            this.bottomScrollButton1.Name = "bottomScrollButton1";
            this.bottomScrollButton1.Size = new System.Drawing.Size(15, 15);
            this.bottomScrollButton1.TabIndex = 1;
            this.bottomScrollButton1.TabStop = false;
            this.bottomScrollButton1.Click += new System.EventHandler(this.bottomScrollButton1_Click);
            // 
            // topScrollButton1
            // 
            this.topScrollButton1.Image = global::TradingCardGame.Images.scrollbar.scrollbar.scroll_up;
            this.topScrollButton1.Location = new System.Drawing.Point(0, 0);
            this.topScrollButton1.Name = "topScrollButton1";
            this.topScrollButton1.Size = new System.Drawing.Size(15, 15);
            this.topScrollButton1.TabIndex = 0;
            this.topScrollButton1.TabStop = false;
            this.topScrollButton1.Click += new System.EventHandler(this.topScrollButton1_Click);
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(755, 8);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(140, 180);
            this.panel7.TabIndex = 5;
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(609, 8);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(140, 180);
            this.panel6.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(463, 8);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(140, 180);
            this.panel5.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(317, 8);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(140, 180);
            this.panel4.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(171, 8);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(140, 180);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(25, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(140, 180);
            this.panel2.TabIndex = 0;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.ForeColor = System.Drawing.Color.SkyBlue;
            this.label61.Location = new System.Drawing.Point(594, 12);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(30, 13);
            this.label61.TabIndex = 58;
            this.label61.Text = "View";
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox20.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.tableview_table;
            this.pictureBox20.Location = new System.Drawing.Point(586, 31);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(24, 24);
            this.pictureBox20.TabIndex = 57;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox19.BackgroundImage = global::TradingCardGame.Images.buttons.buttons.tableview_graphic_down;
            this.pictureBox19.Location = new System.Drawing.Point(611, 31);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(24, 24);
            this.pictureBox19.TabIndex = 56;
            this.pictureBox19.TabStop = false;
            // 
            // searchButton
            // 
            this.searchButton.BackColor = System.Drawing.Color.Transparent;
            this.searchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.searchButton.FlatAppearance.BorderSize = 0;
            this.searchButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.searchButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.searchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchButton.ForeColor = System.Drawing.Color.Black;
            this.searchButton.Location = new System.Drawing.Point(648, 18);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(100, 29);
            this.searchButton.TabIndex = 55;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = false;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Black;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(754, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 22);
            this.textBox1.TabIndex = 54;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.exitButton.Image = global::TradingCardGame.Images.buttons.buttons.helpbutton;
            this.exitButton.Location = new System.Drawing.Point(908, 14);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(42, 37);
            this.exitButton.TabIndex = 53;
            this.exitButton.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(121, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 52;
            this.label1.Text = "3717 cards.";
            // 
            // filterButton
            // 
            this.filterButton.BackColor = System.Drawing.Color.Transparent;
            this.filterButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.filterButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.filterButton.FlatAppearance.BorderSize = 0;
            this.filterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filterButton.ForeColor = System.Drawing.Color.Black;
            this.filterButton.Location = new System.Drawing.Point(27, 19);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(90, 29);
            this.filterButton.TabIndex = 51;
            this.filterButton.Text = "Filter";
            this.filterButton.UseVisualStyleBackColor = false;
            // 
            // DeckBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TradingCardGame.Images.backgrounds.backgrounds.dialog_bg;
            this.Controls.Add(this.mainPanel);
            this.DoubleBuffered = true;
            this.Name = "DeckBuilder";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.DeckBuilder_Load);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.validDeckPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scrollBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scrollbar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomScrollButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topScrollButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button filterButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox exitButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button standardButton;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button showValidQuestsButton;
        private System.Windows.Forms.Button removeAllFiltersButton;
        private System.Windows.Forms.PictureBox validDeckPictureBox;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.PictureBox topScrollButton1;
        private System.Windows.Forms.PictureBox bottomScrollButton1;
        private System.Windows.Forms.PictureBox scrollbar1;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.PictureBox scrollBar2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
    }
}