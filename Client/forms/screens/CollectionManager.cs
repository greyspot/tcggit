﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;
using System.Drawing;
using TradingCardGame.services.sound;
using TradingCardGame.services.inventory;
using TradingCardGame.forms.widgets;
using TradingCardGame.services.data;
using TradingCardGame.services.collection;

namespace TradingCardGame
{
    public partial class CollectionManager : UserControl
    {
        public CollectionManager()
        {
            InitializeComponent();
        }

        Point p = new Point();
        InventoryService invSrv = new InventoryService();
        Point mouseDownPoint = Point.Empty;
        private byte currentPage = 1;
        private readonly byte MAX_PAGE = 3;

        private void loadCardInfo()
        {
            string[] lootCards = CollectionService.getLootCardsForPage(currentPage);

            for (byte i = 0; i < 10; i++)
            {
                Panel currentPanel = (Panel)panel1.Controls["panel" + (2 + i)];
                if (!string.IsNullOrEmpty(lootCards[i]))
                {
                    ((Label)currentPanel.Controls["quantity" + i + "Label"]).Text = invSrv.getQuantityOfSpecificCard(lootCards[i]).ToString();
                    ((PictureBox)panel1.Controls["slot" + i]).BackgroundImage = (Image)Images.loot_cards.COTF.ResourceManager.GetObject(lootCards[i]);
                }
                else
                {
                    ((Label)currentPanel.Controls["quantity" + i + "Label"]).Text = "0";
                    ((PictureBox)panel1.Controls["slot" + i]).BackgroundImage = null;
                }
            }
        }

        private void GameProfile_Load(object sender, EventArgs e)
        {
            filterButton.BackgroundImage = Images.buttons.buttons.button_lg;
            TemporaryData.setCurrentForm(0);
            Controls.Add(new Navigator());
            numberOfCardsLabel.Text = invSrv.getNumberOfCardsInInventory() + " cards.";
            loadCardInfo();
        }

        private void GameProfile_FormClosing(object sender, FormClosingEventArgs e)
        {
            Map map = new Map();
            map.Show();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void profileSettingsButton_Click(object sender, EventArgs e)
        {

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {

        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            
        }

        private void changeAvatarButton_Click(object sender, EventArgs e)
        {
            
            /*OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "New Avatar Picture";
            ofd.Filter = "Image Files (.jpg, .png, .bmp)|*.jpg;*.png;*.bmp";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                avatarPictureBox.ImageLocation = ofd.FileName;
                //AccountService.changeAvatar(ofd.FileName);
            }*/
        }

        private void changeNameButton_Click(object sender, EventArgs e)
        {
            
        }

        private void removeAvatarButton_Click(object sender, EventArgs e)
        {

        }

        private void profileSettingsPanel_MouseMove(object sender, MouseEventArgs e)
        {
            
        }

        private void profileSettingsPanel_MouseDown(object sender, MouseEventArgs e)
        {
            p.X = e.X;
            p.Y = e.Y;
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("page_flip");
            currentPage++;
            loadCardInfo();

            if (currentPage == MAX_PAGE)
            {
                nextButton.Image = Images.graphicsitems.binder.binder_nextpage_disabled;
                nextButton.Visible = false;
            }
            previousPageButton.Visible = true;
        }

        private void previousPageButton_Click(object sender, EventArgs e)
        {
            currentPage--;
            loadCardInfo();

            if (currentPage == 1)
                previousPageButton.Visible = false;
            nextButton.Image = Images.graphicsitems.binder.binder_nextpage;
            nextButton.Visible = true;
            SoundService.PlaySound("page_flip");
        }

        private void nextButton_MouseEnter(object sender, EventArgs e)
        {
            if (currentPage != MAX_PAGE)
                nextButton.Image = Images.graphicsitems.binder.binder_nextpage_over;
        }

        private void nextButton_MouseLeave(object sender, EventArgs e)
        {
            if (currentPage != MAX_PAGE)
                nextButton.Image = Images.graphicsitems.binder.binder_nextpage;
        }

        private void nextButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (currentPage != MAX_PAGE)
                nextButton.Image = Images.graphicsitems.binder.binder_nextpage_down;
        }

        private void nextButton_MouseUp(object sender, MouseEventArgs e)
        {
            nextButton.Image = Images.graphicsitems.binder.binder_nextpage;
        }

        private void previousPageButton_MouseEnter(object sender, EventArgs e)
        {
            previousPageButton.Image = Images.graphicsitems.binder.binder_lastpage_over;
        }

        private void previousPageButton_MouseLeave(object sender, EventArgs e)
        {
            previousPageButton.Image = Images.graphicsitems.binder.binder_lastpage;
        }

        private void previousPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            previousPageButton.Image = Images.graphicsitems.binder.binder_lastpage_down;
        }

        private void navHomeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void setPreviewCard(PictureBox cardHovering)
        {
            if (cardHovering.BackgroundImage == null)
                return;

            SoundService.PlaySound("button");

            if ((previewCard.BackgroundImage == cardHovering.BackgroundImage) && previewCard.Visible)
            {
                previewCard.Visible = false;
                return;
            }

            if (cardHovering == slot3 || cardHovering == slot4 || cardHovering == slot8 || cardHovering == slot9)
                previewCard.Location = new Point(cardHovering.Location.X - (previewCard.Width + 10), slot0.Location.Y);
            else
                previewCard.Location = new Point(cardHovering.Location.X + cardHovering.Width + 10, slot0.Location.Y);

            previewCard.BackgroundImage = cardHovering.BackgroundImage;
            previewCard.Visible = true;
        }

        private void slot0_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot0);
        }

        private void slot1_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot1);
        }

        private void slot2_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot2);
        }

        private void slot3_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot3);
        }

        private void slot4_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot4);
        }

        private void slot5_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot5);
        }

        private void slot6_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot6);
        }

        private void slot7_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot7);
        }

        private void slot8_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot8);
        }

        private void slot9_Click(object sender, EventArgs e)
        {
            setPreviewCard(slot9);
        }
    }
}
