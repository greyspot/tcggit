﻿namespace TradingCardGame
{
    partial class Map
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Map));
            this.bottomButtonsPanel = new System.Windows.Forms.Panel();
            this.exitButton = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.deckBuilderButton = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.collectionsButton = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.preferencesButton = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.mainButtonsPanel = new System.Windows.Forms.Panel();
            this.mainInfoPanel = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.viewMoreButton = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.leaderboardsListViewer = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel7 = new System.Windows.Forms.Panel();
            this.motdTextBox = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.mainLeftPanel = new System.Windows.Forms.Panel();
            this.gettingStartedLabel = new System.Windows.Forms.Label();
            this.tcgLogo = new System.Windows.Forms.PictureBox();
            this.bottomButtonsPanel.SuspendLayout();
            this.exitButton.SuspendLayout();
            this.deckBuilderButton.SuspendLayout();
            this.collectionsButton.SuspendLayout();
            this.preferencesButton.SuspendLayout();
            this.mainInfoPanel.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.mainLeftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcgLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomButtonsPanel
            // 
            this.bottomButtonsPanel.BackColor = System.Drawing.Color.Transparent;
            this.bottomButtonsPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bottomButtonsPanel.BackgroundImage")));
            this.bottomButtonsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bottomButtonsPanel.Controls.Add(this.exitButton);
            this.bottomButtonsPanel.Controls.Add(this.deckBuilderButton);
            this.bottomButtonsPanel.Controls.Add(this.collectionsButton);
            this.bottomButtonsPanel.Controls.Add(this.preferencesButton);
            this.bottomButtonsPanel.Location = new System.Drawing.Point(12, 637);
            this.bottomButtonsPanel.Name = "bottomButtonsPanel";
            this.bottomButtonsPanel.Size = new System.Drawing.Size(1000, 119);
            this.bottomButtonsPanel.TabIndex = 22;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Black;
            this.exitButton.Controls.Add(this.label13);
            this.exitButton.Location = new System.Drawing.Point(782, 15);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(192, 88);
            this.exitButton.TabIndex = 45;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            this.exitButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.exitButton_MouseDown);
            this.exitButton.MouseEnter += new System.EventHandler(this.exitButton_MouseEnter);
            this.exitButton.MouseLeave += new System.EventHandler(this.exitButton_MouseLeave);
            this.exitButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.exitButton_MouseUp);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label13.Location = new System.Drawing.Point(90, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 19);
            this.label13.TabIndex = 9;
            this.label13.Text = "Exit";
            // 
            // deckBuilderButton
            // 
            this.deckBuilderButton.BackColor = System.Drawing.Color.Black;
            this.deckBuilderButton.Controls.Add(this.label15);
            this.deckBuilderButton.Controls.Add(this.label11);
            this.deckBuilderButton.Location = new System.Drawing.Point(535, 15);
            this.deckBuilderButton.Name = "deckBuilderButton";
            this.deckBuilderButton.Size = new System.Drawing.Size(259, 88);
            this.deckBuilderButton.TabIndex = 43;
            this.deckBuilderButton.Click += new System.EventHandler(this.deckBuilderButton_Click);
            this.deckBuilderButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.deckBuilderButton_MouseDown);
            this.deckBuilderButton.MouseEnter += new System.EventHandler(this.deckBuilderButton_MouseEnter);
            this.deckBuilderButton.MouseLeave += new System.EventHandler(this.deckBuilderButton_MouseLeave);
            this.deckBuilderButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.deckBuilderButton_MouseUp);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label15.Location = new System.Drawing.Point(86, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(160, 45);
            this.label15.TabIndex = 11;
            this.label15.Text = "Create new decks and\r\nimprove your old ones using\r\nthe cards you\'ve collected.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label11.Location = new System.Drawing.Point(86, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 19);
            this.label11.TabIndex = 7;
            this.label11.Text = "Deck Builder";
            // 
            // collectionsButton
            // 
            this.collectionsButton.BackColor = System.Drawing.Color.Black;
            this.collectionsButton.Controls.Add(this.label14);
            this.collectionsButton.Controls.Add(this.label10);
            this.collectionsButton.Location = new System.Drawing.Point(286, 15);
            this.collectionsButton.Name = "collectionsButton";
            this.collectionsButton.Size = new System.Drawing.Size(259, 88);
            this.collectionsButton.TabIndex = 42;
            this.collectionsButton.Click += new System.EventHandler(this.collectionsButton_Click);
            this.collectionsButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.collectionsButton_MouseDown);
            this.collectionsButton.MouseEnter += new System.EventHandler(this.collectionsButton_MouseEnter);
            this.collectionsButton.MouseLeave += new System.EventHandler(this.collectionsButton_MouseLeave);
            this.collectionsButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.collectionsButton_MouseUp);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label14.Location = new System.Drawing.Point(86, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(168, 45);
            this.label14.TabIndex = 10;
            this.label14.Text = "Open packs you\'ve purchased\r\nand view or sort all the cards\r\nyou own.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label10.Location = new System.Drawing.Point(86, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 19);
            this.label10.TabIndex = 6;
            this.label10.Text = "Collection";
            // 
            // preferencesButton
            // 
            this.preferencesButton.BackColor = System.Drawing.Color.Black;
            this.preferencesButton.Controls.Add(this.label9);
            this.preferencesButton.Location = new System.Drawing.Point(52, 15);
            this.preferencesButton.Name = "preferencesButton";
            this.preferencesButton.Size = new System.Drawing.Size(259, 88);
            this.preferencesButton.TabIndex = 44;
            this.preferencesButton.Click += new System.EventHandler(this.preferencesButton_Click);
            this.preferencesButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.preferencesButton_MouseDown);
            this.preferencesButton.MouseEnter += new System.EventHandler(this.preferencesButton_MouseEnter);
            this.preferencesButton.MouseLeave += new System.EventHandler(this.preferencesButton_MouseLeave);
            this.preferencesButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.preferencesButton_MouseUp);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label9.Location = new System.Drawing.Point(86, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 19);
            this.label9.TabIndex = 5;
            this.label9.Text = "Preferences";
            // 
            // mainButtonsPanel
            // 
            this.mainButtonsPanel.BackColor = System.Drawing.Color.Transparent;
            this.mainButtonsPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mainButtonsPanel.BackgroundImage")));
            this.mainButtonsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mainButtonsPanel.Location = new System.Drawing.Point(652, 6);
            this.mainButtonsPanel.Name = "mainButtonsPanel";
            this.mainButtonsPanel.Size = new System.Drawing.Size(360, 610);
            this.mainButtonsPanel.TabIndex = 50;
            // 
            // mainInfoPanel
            // 
            this.mainInfoPanel.BackColor = System.Drawing.Color.Transparent;
            this.mainInfoPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mainInfoPanel.BackgroundImage")));
            this.mainInfoPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mainInfoPanel.Controls.Add(this.panel8);
            this.mainInfoPanel.Controls.Add(this.panel7);
            this.mainInfoPanel.Controls.Add(this.mainLeftPanel);
            this.mainInfoPanel.Location = new System.Drawing.Point(12, 6);
            this.mainInfoPanel.Name = "mainInfoPanel";
            this.mainInfoPanel.Size = new System.Drawing.Size(634, 610);
            this.mainInfoPanel.TabIndex = 51;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.viewMoreButton);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.leaderboardsListViewer);
            this.panel8.Location = new System.Drawing.Point(320, 308);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(300, 280);
            this.panel8.TabIndex = 2;
            // 
            // viewMoreButton
            // 
            this.viewMoreButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.viewMoreButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("viewMoreButton.BackgroundImage")));
            this.viewMoreButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.viewMoreButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.viewMoreButton.FlatAppearance.BorderSize = 0;
            this.viewMoreButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.viewMoreButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.viewMoreButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.viewMoreButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewMoreButton.ForeColor = System.Drawing.Color.Black;
            this.viewMoreButton.Location = new System.Drawing.Point(167, 241);
            this.viewMoreButton.Name = "viewMoreButton";
            this.viewMoreButton.Size = new System.Drawing.Size(120, 29);
            this.viewMoreButton.TabIndex = 50;
            this.viewMoreButton.Text = "View More...";
            this.viewMoreButton.UseVisualStyleBackColor = false;
            this.viewMoreButton.Click += new System.EventHandler(this.viewMoreButton_Click);
            this.viewMoreButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.viewMoreButton_MouseDown);
            this.viewMoreButton.MouseEnter += new System.EventHandler(this.viewMoreButton_MouseEnter);
            this.viewMoreButton.MouseLeave += new System.EventHandler(this.viewMoreButton_MouseLeave);
            this.viewMoreButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.viewMoreButton_MouseUp);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(8, 4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 20);
            this.label19.TabIndex = 4;
            this.label19.Text = "Leaderboards";
            // 
            // leaderboardsListViewer
            // 
            this.leaderboardsListViewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.leaderboardsListViewer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.leaderboardsListViewer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaderboardsListViewer.ForeColor = System.Drawing.Color.White;
            this.leaderboardsListViewer.FullRowSelect = true;
            this.leaderboardsListViewer.Location = new System.Drawing.Point(12, 25);
            this.leaderboardsListViewer.MultiSelect = false;
            this.leaderboardsListViewer.Name = "leaderboardsListViewer";
            this.leaderboardsListViewer.Size = new System.Drawing.Size(275, 210);
            this.leaderboardsListViewer.TabIndex = 3;
            this.leaderboardsListViewer.UseCompatibleStateImageBehavior = false;
            this.leaderboardsListViewer.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Rank";
            this.columnHeader1.Width = 45;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Rating";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 126;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.motdTextBox);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Location = new System.Drawing.Point(320, 23);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(300, 279);
            this.panel7.TabIndex = 1;
            // 
            // motdTextBox
            // 
            this.motdTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.motdTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.motdTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motdTextBox.ForeColor = System.Drawing.Color.White;
            this.motdTextBox.Location = new System.Drawing.Point(12, 34);
            this.motdTextBox.Name = "motdTextBox";
            this.motdTextBox.ReadOnly = true;
            this.motdTextBox.Size = new System.Drawing.Size(275, 232);
            this.motdTextBox.TabIndex = 2;
            this.motdTextBox.Text = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(13, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(169, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "Message of the Day";
            // 
            // mainLeftPanel
            // 
            this.mainLeftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.mainLeftPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainLeftPanel.Controls.Add(this.gettingStartedLabel);
            this.mainLeftPanel.Controls.Add(this.tcgLogo);
            this.mainLeftPanel.Location = new System.Drawing.Point(14, 23);
            this.mainLeftPanel.Name = "mainLeftPanel";
            this.mainLeftPanel.Size = new System.Drawing.Size(300, 565);
            this.mainLeftPanel.TabIndex = 0;
            // 
            // gettingStartedLabel
            // 
            this.gettingStartedLabel.AutoSize = true;
            this.gettingStartedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gettingStartedLabel.ForeColor = System.Drawing.Color.White;
            this.gettingStartedLabel.Location = new System.Drawing.Point(3, 135);
            this.gettingStartedLabel.Name = "gettingStartedLabel";
            this.gettingStartedLabel.Size = new System.Drawing.Size(39, 15);
            this.gettingStartedLabel.TabIndex = 1;
            this.gettingStartedLabel.Text = "Text...";
            // 
            // tcgLogo
            // 
            this.tcgLogo.Location = new System.Drawing.Point(3, 4);
            this.tcgLogo.Name = "tcgLogo";
            this.tcgLogo.Size = new System.Drawing.Size(292, 128);
            this.tcgLogo.TabIndex = 0;
            this.tcgLogo.TabStop = false;
            // 
            // Map
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.mainInfoPanel);
            this.Controls.Add(this.bottomButtonsPanel);
            this.Controls.Add(this.mainButtonsPanel);
            this.DoubleBuffered = true;
            this.Name = "Map";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.Map_Load);
            this.bottomButtonsPanel.ResumeLayout(false);
            this.exitButton.ResumeLayout(false);
            this.exitButton.PerformLayout();
            this.deckBuilderButton.ResumeLayout(false);
            this.deckBuilderButton.PerformLayout();
            this.collectionsButton.ResumeLayout(false);
            this.collectionsButton.PerformLayout();
            this.preferencesButton.ResumeLayout(false);
            this.preferencesButton.PerformLayout();
            this.mainInfoPanel.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.mainLeftPanel.ResumeLayout(false);
            this.mainLeftPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcgLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel bottomButtonsPanel;
        private System.Windows.Forms.Panel mainButtonsPanel;
        private System.Windows.Forms.Panel mainInfoPanel;
        private System.Windows.Forms.Panel collectionsButton;
        private System.Windows.Forms.Panel deckBuilderButton;
        private System.Windows.Forms.Panel preferencesButton;
        private System.Windows.Forms.Panel exitButton;
        private System.Windows.Forms.Panel mainLeftPanel;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox motdTextBox;
        private System.Windows.Forms.PictureBox tcgLogo;
        private System.Windows.Forms.ListView leaderboardsListViewer;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button viewMoreButton;
        private System.Windows.Forms.Label gettingStartedLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
    }
}