﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.account;
using TradingCardGame.services.database;
using TradingCardGame.services.sound;

namespace TradingCardGame.forms
{
    public partial class LaunchMatch : UserControl
    {
        public LaunchMatch()
        {
            InitializeComponent();
        }

        Point mouseDownPoint;

        private void LaunchMatch_Load(object sender, EventArgs e)
        {
            readyToGoCheckBox1.BackgroundImage = Images.buttons.buttons.checkbox_off;
            readyToGoCheckBox2.BackgroundImage = Images.buttons.buttons.checkbox_off;
            allowObserversCheckBox.BackgroundImage = Images.buttons.buttons.checkbox_off;
            friendsOnlyCheckBox.BackgroundImage = Images.buttons.buttons.checkbox_off;
            lightVsDarkCheckBox.BackgroundImage = Images.buttons.buttons.checkbox_off;

            //panelTitleLabel.Text = "Match #" + DatabaseService.getValue("matches", "id", "title", Properties.Settings.Default.matchTitle);

            if (avatarPictureBox1.BackgroundImage == null)
            {
                avatarPictureBox1.BackgroundImage = (Image)Images.buttons.avatars.ResourceManager.GetObject(DatabaseService.GetValue("users", "avatar", "username", AccountService.GetUsername()));
                usernameLabel1.Text = AccountService.GetUsernameWithTitle(AccountService.GetUsername());
                userPanel1.BackColor = Color.SkyBlue;

                //DatabaseService.ExecuteCommand("UPDATE lobbies SET position0 = '" + AccountService.GetUsername() + "' WHERE type = '" + panelTitleLabel.Text.Replace("Match #", "") + "'");
            }
            else if (avatarPictureBox2.BackgroundImage == null)
            {
                avatarPictureBox2.BackgroundImage = (Image)Images.buttons.avatars.ResourceManager.GetObject(DatabaseService.GetValue("users", "avatar", "username", AccountService.GetUsername()));
                usernameLabel2.Text = AccountService.GetUsername();
                userPanel2.BackColor = Color.SkyBlue;
                userPanel2.Visible = true;

                //DatabaseService.ExecuteCommand("UPDATE lobbies SET position1 = '" + AccountService.GetUsername() + "' WHERE type = '" + panelTitleLabel.Text.Replace(" Match #", "") + "'");
            }
            Properties.Settings.Default.joiningGame = false;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void LaunchMatch_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Properties.Settings.Default.joiningGame)
                return;

            // TODO: remove player from the private lobby, if owner delete private lobby and match from DB


            if (usernameLabel1.Text.Equals(AccountService.GetUsernameWithTitle(AccountService.GetUsername())))
            {
                DatabaseService.ExecuteCommand("DELETE FROM matches WHERE id = '" + panelTitleLabel.Text.Replace("Match #", "") + "'");
                //DatabaseService.ExecuteCommand("DELETE FROM lobbies WHERE type = '" + panelTitleLabel.Text.Replace("Match #", "") + "'");

                //LobbyPage lobbyPage = new LobbyPage();
                //lobbyPage.sendPacket(cmdSrv.COMMAND_UPDATE_AVAILABLE_MATCHES.ToString());
            }
            else
            {
                //DatabaseService.ExecuteCommand("UPDATE lobbies SET position1 = '' WHERE type = '" + panelTitleLabel.Text.Replace("Match #", "") + "'");
            }
        }

        private void inputChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    // Add packet sending and handling here
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error:\n" + ex.Message);
                    Application.ExitThread();
                }

                return;
            }
            SoundService.KeyPressSound(e.KeyCode);
        }

        private void LaunchMatch_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }

        private void LaunchMatch_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDownPoint.IsEmpty)
            {
                Form f = sender as Form;
                f.Location = new Point(f.Location.X + (e.X - mouseDownPoint.X), f.Location.Y + (e.Y - mouseDownPoint.Y));
            }
        }

        private void LaunchMatch_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownPoint = Point.Empty;
        }
    }
}
