﻿namespace TradingCardGame
{
    partial class CombatPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CombatPage));
            this.enemyNameLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.enemyTimer = new System.Windows.Forms.Timer(this.components);
            this.timeLeftTimer = new System.Windows.Forms.Timer(this.components);
            this.outcomePanel = new System.Windows.Forms.Panel();
            this.nextButton = new System.Windows.Forms.Button();
            this.expRewardLabel = new System.Windows.Forms.Label();
            this.creditsRewardLabel = new System.Windows.Forms.Label();
            this.outcomePictureBox = new System.Windows.Forms.PictureBox();
            this.effectTimer = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.enemyDiscardPileLabel = new System.Windows.Forms.Label();
            this.enemyDeckLabel = new System.Windows.Forms.Label();
            this.enemyHandLabel = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.enemiesAvatarPictureBox = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.playersAvatarPictureBox = new System.Windows.Forms.Panel();
            this.avatarCoverPanel = new System.Windows.Forms.Panel();
            this.itemsToolbarPanel = new System.Windows.Forms.Panel();
            this.cardsInDicardPileLabel = new System.Windows.Forms.Label();
            this.cardsInDeckLabel = new System.Windows.Forms.Label();
            this.cardsInHandLabel = new System.Windows.Forms.Label();
            this.itemsPanel2 = new System.Windows.Forms.Panel();
            this.itemsPanel1 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.cardExpandAnimationTimer = new System.Windows.Forms.Timer(this.components);
            this.cardDespandAnimationTimer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mainbasePanel = new System.Windows.Forms.Panel();
            this.secondPhaseButton = new System.Windows.Forms.PictureBox();
            this.phaseButton = new System.Windows.Forms.PictureBox();
            this.mainbaseLabel = new System.Windows.Forms.Label();
            this.chatPanel = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.chatInputTextBox = new System.Windows.Forms.TextBox();
            this.chatListBox = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.enemyMainbasePanel = new System.Windows.Forms.Panel();
            this.enemyMainbaseLabel = new System.Windows.Forms.Label();
            this.card1 = new System.Windows.Forms.Panel();
            this.card1Cover = new System.Windows.Forms.Panel();
            this.card1Description = new System.Windows.Forms.Label();
            this.card1Subtitle = new System.Windows.Forms.Label();
            this.card1Title = new System.Windows.Forms.Label();
            this.card1Level = new System.Windows.Forms.Label();
            this.card2 = new System.Windows.Forms.Panel();
            this.card2Cover = new System.Windows.Forms.Panel();
            this.card2Description = new System.Windows.Forms.Label();
            this.card2Subtitle = new System.Windows.Forms.Label();
            this.card2Title = new System.Windows.Forms.Label();
            this.card2Level = new System.Windows.Forms.Label();
            this.card3 = new System.Windows.Forms.Panel();
            this.card3Cover = new System.Windows.Forms.Panel();
            this.card3Description = new System.Windows.Forms.Label();
            this.card3Subtitle = new System.Windows.Forms.Label();
            this.card3Title = new System.Windows.Forms.Label();
            this.card3Level = new System.Windows.Forms.Label();
            this.card4 = new System.Windows.Forms.Panel();
            this.card4Cover = new System.Windows.Forms.Panel();
            this.card4Description = new System.Windows.Forms.Label();
            this.card4Subtitle = new System.Windows.Forms.Label();
            this.card4Title = new System.Windows.Forms.Label();
            this.card4Level = new System.Windows.Forms.Label();
            this.card5 = new System.Windows.Forms.Panel();
            this.card5Cover = new System.Windows.Forms.Panel();
            this.card5Description = new System.Windows.Forms.Label();
            this.card5Subtitle = new System.Windows.Forms.Label();
            this.card5Title = new System.Windows.Forms.Label();
            this.card5Level = new System.Windows.Forms.Label();
            this.card6 = new System.Windows.Forms.Panel();
            this.card6Cover = new System.Windows.Forms.Panel();
            this.card6Description = new System.Windows.Forms.Label();
            this.card6Subtitle = new System.Windows.Forms.Label();
            this.card6Title = new System.Windows.Forms.Label();
            this.card6Level = new System.Windows.Forms.Label();
            this.playmatCard1 = new System.Windows.Forms.Panel();
            this.playmatCard1Cover = new System.Windows.Forms.Panel();
            this.playmatCard1Title = new System.Windows.Forms.Label();
            this.secondLeftPlaymatCard = new System.Windows.Forms.Panel();
            this.playmatCard2Cover = new System.Windows.Forms.Panel();
            this.playmatCard2Title = new System.Windows.Forms.Label();
            this.outcomePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.outcomePictureBox)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enemiesAvatarPictureBox)).BeginInit();
            this.panel8.SuspendLayout();
            this.playersAvatarPictureBox.SuspendLayout();
            this.itemsToolbarPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.mainbasePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondPhaseButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseButton)).BeginInit();
            this.chatPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel1.SuspendLayout();
            this.enemyMainbasePanel.SuspendLayout();
            this.card1.SuspendLayout();
            this.card1Cover.SuspendLayout();
            this.card2.SuspendLayout();
            this.card2Cover.SuspendLayout();
            this.card3.SuspendLayout();
            this.card3Cover.SuspendLayout();
            this.card4.SuspendLayout();
            this.card4Cover.SuspendLayout();
            this.card5.SuspendLayout();
            this.card5Cover.SuspendLayout();
            this.card6.SuspendLayout();
            this.card6Cover.SuspendLayout();
            this.playmatCard1.SuspendLayout();
            this.playmatCard1Cover.SuspendLayout();
            this.secondLeftPlaymatCard.SuspendLayout();
            this.playmatCard2Cover.SuspendLayout();
            this.SuspendLayout();
            // 
            // enemyNameLabel
            // 
            this.enemyNameLabel.AutoSize = true;
            this.enemyNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.enemyNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyNameLabel.ForeColor = System.Drawing.Color.White;
            this.enemyNameLabel.Location = new System.Drawing.Point(5, 118);
            this.enemyNameLabel.Name = "enemyNameLabel";
            this.enemyNameLabel.Size = new System.Drawing.Size(70, 24);
            this.enemyNameLabel.TabIndex = 6;
            this.enemyNameLabel.Text = "Enemy";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.BackColor = System.Drawing.Color.Transparent;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(4, 70);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(105, 24);
            this.usernameLabel.TabIndex = 7;
            this.usernameLabel.Text = "Username";
            // 
            // enemyTimer
            // 
            this.enemyTimer.Interval = 1000;
            this.enemyTimer.Tick += new System.EventHandler(this.enemyTimer_Tick);
            // 
            // timeLeftTimer
            // 
            this.timeLeftTimer.Interval = 1000;
            this.timeLeftTimer.Tick += new System.EventHandler(this.timeLeftTimer_Tick);
            // 
            // outcomePanel
            // 
            this.outcomePanel.BackColor = System.Drawing.Color.Transparent;
            this.outcomePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.outcomePanel.Controls.Add(this.nextButton);
            this.outcomePanel.Controls.Add(this.expRewardLabel);
            this.outcomePanel.Controls.Add(this.creditsRewardLabel);
            this.outcomePanel.Controls.Add(this.outcomePictureBox);
            this.outcomePanel.Location = new System.Drawing.Point(1020, 735);
            this.outcomePanel.Name = "outcomePanel";
            this.outcomePanel.Size = new System.Drawing.Size(600, 390);
            this.outcomePanel.TabIndex = 44;
            this.outcomePanel.Visible = false;
            // 
            // nextButton
            // 
            this.nextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextButton.Location = new System.Drawing.Point(108, 277);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(80, 30);
            this.nextButton.TabIndex = 45;
            this.nextButton.Text = "Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // expRewardLabel
            // 
            this.expRewardLabel.AutoSize = true;
            this.expRewardLabel.BackColor = System.Drawing.Color.Black;
            this.expRewardLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expRewardLabel.ForeColor = System.Drawing.Color.White;
            this.expRewardLabel.Location = new System.Drawing.Point(66, 195);
            this.expRewardLabel.Name = "expRewardLabel";
            this.expRewardLabel.Size = new System.Drawing.Size(194, 29);
            this.expRewardLabel.TabIndex = 2;
            this.expRewardLabel.Text = "+300 Experience";
            // 
            // creditsRewardLabel
            // 
            this.creditsRewardLabel.AutoSize = true;
            this.creditsRewardLabel.BackColor = System.Drawing.Color.Black;
            this.creditsRewardLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditsRewardLabel.ForeColor = System.Drawing.Color.White;
            this.creditsRewardLabel.Location = new System.Drawing.Point(66, 166);
            this.creditsRewardLabel.Name = "creditsRewardLabel";
            this.creditsRewardLabel.Size = new System.Drawing.Size(149, 29);
            this.creditsRewardLabel.TabIndex = 1;
            this.creditsRewardLabel.Text = "+300 Credits";
            // 
            // outcomePictureBox
            // 
            this.outcomePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.outcomePictureBox.Image = global::TradingCardGame.Images.dialogs.dialogs.victory_game;
            this.outcomePictureBox.Location = new System.Drawing.Point(146, 75);
            this.outcomePictureBox.Name = "outcomePictureBox";
            this.outcomePictureBox.Size = new System.Drawing.Size(600, 390);
            this.outcomePictureBox.TabIndex = 0;
            this.outcomePictureBox.TabStop = false;
            // 
            // effectTimer
            // 
            this.effectTimer.Enabled = true;
            this.effectTimer.Interval = 40;
            this.effectTimer.Tick += new System.EventHandler(this.effectTimer_Tick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_jedi_ul;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.enemyDiscardPileLabel);
            this.panel2.Controls.Add(this.enemyDeckLabel);
            this.panel2.Controls.Add(this.enemyHandLabel);
            this.panel2.Controls.Add(this.panel19);
            this.panel2.Controls.Add(this.panel20);
            this.panel2.Controls.Add(this.enemyNameLabel);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(380, 179);
            this.panel2.TabIndex = 48;
            // 
            // enemyDiscardPileLabel
            // 
            this.enemyDiscardPileLabel.AutoSize = true;
            this.enemyDiscardPileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyDiscardPileLabel.ForeColor = System.Drawing.Color.White;
            this.enemyDiscardPileLabel.Location = new System.Drawing.Point(55, 85);
            this.enemyDiscardPileLabel.Name = "enemyDiscardPileLabel";
            this.enemyDiscardPileLabel.Size = new System.Drawing.Size(21, 24);
            this.enemyDiscardPileLabel.TabIndex = 11;
            this.enemyDiscardPileLabel.Text = "5";
            // 
            // enemyDeckLabel
            // 
            this.enemyDeckLabel.AutoSize = true;
            this.enemyDeckLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyDeckLabel.ForeColor = System.Drawing.Color.White;
            this.enemyDeckLabel.Location = new System.Drawing.Point(51, 48);
            this.enemyDeckLabel.Name = "enemyDeckLabel";
            this.enemyDeckLabel.Size = new System.Drawing.Size(32, 24);
            this.enemyDeckLabel.TabIndex = 10;
            this.enemyDeckLabel.Text = "34";
            // 
            // enemyHandLabel
            // 
            this.enemyHandLabel.AutoSize = true;
            this.enemyHandLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyHandLabel.ForeColor = System.Drawing.Color.White;
            this.enemyHandLabel.Location = new System.Drawing.Point(57, 10);
            this.enemyHandLabel.Name = "enemyHandLabel";
            this.enemyHandLabel.Size = new System.Drawing.Size(21, 24);
            this.enemyHandLabel.TabIndex = 9;
            this.enemyHandLabel.Text = "5";
            // 
            // panel19
            // 
            this.panel19.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_top_items_right;
            this.panel19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel19.Location = new System.Drawing.Point(228, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(200, 115);
            this.panel19.TabIndex = 7;
            // 
            // panel20
            // 
            this.panel20.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_top_items_middle;
            this.panel20.Location = new System.Drawing.Point(101, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(130, 115);
            this.panel20.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_jedi_tile_top;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1024, 150);
            this.panel3.TabIndex = 49;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_top_ability1;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(639, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(120, 150);
            this.panel4.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_top_ability2;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(765, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(120, 150);
            this.panel6.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_top_ability3;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Location = new System.Drawing.Point(891, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(120, 150);
            this.panel5.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_jedi_top;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Controls.Add(this.enemiesAvatarPictureBox);
            this.panel7.Controls.Add(this.panel14);
            this.panel7.Location = new System.Drawing.Point(375, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(272, 194);
            this.panel7.TabIndex = 50;
            // 
            // enemiesAvatarPictureBox
            // 
            this.enemiesAvatarPictureBox.Image = global::TradingCardGame.Images.avatars.pictures.blank;
            this.enemiesAvatarPictureBox.Location = new System.Drawing.Point(117, 15);
            this.enemiesAvatarPictureBox.Name = "enemiesAvatarPictureBox";
            this.enemiesAvatarPictureBox.Size = new System.Drawing.Size(110, 110);
            this.enemiesAvatarPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.enemiesAvatarPictureBox.TabIndex = 52;
            this.enemiesAvatarPictureBox.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.button_conditions;
            this.panel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel14.Location = new System.Drawing.Point(218, 25);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(30, 100);
            this.panel14.TabIndex = 51;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_generic_tile_bottom;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel8.Controls.Add(this.panel12);
            this.panel8.Controls.Add(this.playersAvatarPictureBox);
            this.panel8.Controls.Add(this.itemsToolbarPanel);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Controls.Add(this.panel11);
            this.panel8.Location = new System.Drawing.Point(0, 590);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1024, 150);
            this.panel8.TabIndex = 50;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_bottom_ability1;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Location = new System.Drawing.Point(639, 20);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(120, 110);
            this.panel12.TabIndex = 0;
            // 
            // playersAvatarPictureBox
            // 
            this.playersAvatarPictureBox.BackgroundImage = global::TradingCardGame.Images.avatars.pictures.blank;
            this.playersAvatarPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.playersAvatarPictureBox.Controls.Add(this.avatarCoverPanel);
            this.playersAvatarPictureBox.Location = new System.Drawing.Point(422, 6);
            this.playersAvatarPictureBox.Name = "playersAvatarPictureBox";
            this.playersAvatarPictureBox.Size = new System.Drawing.Size(265, 124);
            this.playersAvatarPictureBox.TabIndex = 52;
            // 
            // avatarCoverPanel
            // 
            this.avatarCoverPanel.BackColor = System.Drawing.Color.Transparent;
            this.avatarCoverPanel.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_generic_top;
            this.avatarCoverPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.avatarCoverPanel.Location = new System.Drawing.Point(-12, 1);
            this.avatarCoverPanel.Name = "avatarCoverPanel";
            this.avatarCoverPanel.Size = new System.Drawing.Size(240, 166);
            this.avatarCoverPanel.TabIndex = 51;
            // 
            // itemsToolbarPanel
            // 
            this.itemsToolbarPanel.BackColor = System.Drawing.Color.Transparent;
            this.itemsToolbarPanel.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_generic_ll;
            this.itemsToolbarPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.itemsToolbarPanel.Controls.Add(this.cardsInDicardPileLabel);
            this.itemsToolbarPanel.Controls.Add(this.cardsInDeckLabel);
            this.itemsToolbarPanel.Controls.Add(this.cardsInHandLabel);
            this.itemsToolbarPanel.Controls.Add(this.itemsPanel2);
            this.itemsToolbarPanel.Controls.Add(this.usernameLabel);
            this.itemsToolbarPanel.Controls.Add(this.itemsPanel1);
            this.itemsToolbarPanel.Location = new System.Drawing.Point(0, -67);
            this.itemsToolbarPanel.Name = "itemsToolbarPanel";
            this.itemsToolbarPanel.Size = new System.Drawing.Size(403, 211);
            this.itemsToolbarPanel.TabIndex = 51;
            this.itemsToolbarPanel.MouseEnter += new System.EventHandler(this.itemsToolbarPanel_MouseEnter);
            // 
            // cardsInDicardPileLabel
            // 
            this.cardsInDicardPileLabel.AutoSize = true;
            this.cardsInDicardPileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardsInDicardPileLabel.ForeColor = System.Drawing.Color.White;
            this.cardsInDicardPileLabel.Location = new System.Drawing.Point(64, 178);
            this.cardsInDicardPileLabel.Name = "cardsInDicardPileLabel";
            this.cardsInDicardPileLabel.Size = new System.Drawing.Size(21, 24);
            this.cardsInDicardPileLabel.TabIndex = 14;
            this.cardsInDicardPileLabel.Text = "5";
            // 
            // cardsInDeckLabel
            // 
            this.cardsInDeckLabel.AutoSize = true;
            this.cardsInDeckLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardsInDeckLabel.ForeColor = System.Drawing.Color.White;
            this.cardsInDeckLabel.Location = new System.Drawing.Point(57, 141);
            this.cardsInDeckLabel.Name = "cardsInDeckLabel";
            this.cardsInDeckLabel.Size = new System.Drawing.Size(32, 24);
            this.cardsInDeckLabel.TabIndex = 13;
            this.cardsInDeckLabel.Text = "34";
            // 
            // cardsInHandLabel
            // 
            this.cardsInHandLabel.AutoSize = true;
            this.cardsInHandLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardsInHandLabel.ForeColor = System.Drawing.Color.White;
            this.cardsInHandLabel.Location = new System.Drawing.Point(64, 103);
            this.cardsInHandLabel.Name = "cardsInHandLabel";
            this.cardsInHandLabel.Size = new System.Drawing.Size(21, 24);
            this.cardsInHandLabel.TabIndex = 12;
            this.cardsInHandLabel.Text = "5";
            // 
            // itemsPanel2
            // 
            this.itemsPanel2.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_bottom_items_right;
            this.itemsPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.itemsPanel2.Location = new System.Drawing.Point(259, 100);
            this.itemsPanel2.Name = "itemsPanel2";
            this.itemsPanel2.Size = new System.Drawing.Size(140, 97);
            this.itemsPanel2.TabIndex = 9;
            this.itemsPanel2.MouseEnter += new System.EventHandler(this.itemsPanel2_MouseEnter);
            // 
            // itemsPanel1
            // 
            this.itemsPanel1.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_bottom_items_middle;
            this.itemsPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.itemsPanel1.Location = new System.Drawing.Point(106, 100);
            this.itemsPanel1.Name = "itemsPanel1";
            this.itemsPanel1.Size = new System.Drawing.Size(155, 97);
            this.itemsPanel1.TabIndex = 10;
            this.itemsPanel1.MouseEnter += new System.EventHandler(this.itemsPanel1_MouseEnter);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_bottom_ability2;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Location = new System.Drawing.Point(765, 20);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(120, 110);
            this.panel10.TabIndex = 2;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_cavity_bottom_ability3;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Location = new System.Drawing.Point(891, 20);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(120, 110);
            this.panel11.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.BackgroundImage = global::TradingCardGame.Images.card.card._100007693;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::TradingCardGame.Images.card.card.playmatcard_quest_generic;
            this.pictureBox1.Location = new System.Drawing.Point(899, 322);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 125);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 57;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Black;
            this.pictureBox2.BackgroundImage = global::TradingCardGame.Images.card.card._100007693;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = global::TradingCardGame.Images.card.card.playmatcard_quest_generic;
            this.pictureBox2.Location = new System.Drawing.Point(0, 322);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(125, 125);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 58;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::TradingCardGame.Images.graphicsitems.gamescreen.button_raid_disabled;
            this.pictureBox3.Location = new System.Drawing.Point(851, 359);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 59;
            this.pictureBox3.TabStop = false;
            // 
            // cardExpandAnimationTimer
            // 
            this.cardExpandAnimationTimer.Interval = 8;
            this.cardExpandAnimationTimer.Tick += new System.EventHandler(this.cardExpandAnimationTimer_Tick);
            // 
            // cardDespandAnimationTimer
            // 
            this.cardDespandAnimationTimer.Interval = 8;
            this.cardDespandAnimationTimer.Tick += new System.EventHandler(this.cardDespandAnimationTimer_Tick);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::TradingCardGame.Images.graphicsitems.gamescreen.button_raid_disabled;
            this.pictureBox4.Location = new System.Drawing.Point(122, 359);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 66;
            this.pictureBox4.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(909, 422);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 15);
            this.label1.TabIndex = 67;
            this.label1.Text = "4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(11, 421);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 15);
            this.label2.TabIndex = 68;
            this.label2.Text = "4";
            // 
            // mainbasePanel
            // 
            this.mainbasePanel.BackColor = System.Drawing.Color.Transparent;
            this.mainbasePanel.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.phase_dialogue_bottom_small;
            this.mainbasePanel.Controls.Add(this.secondPhaseButton);
            this.mainbasePanel.Controls.Add(this.phaseButton);
            this.mainbasePanel.Controls.Add(this.mainbaseLabel);
            this.mainbasePanel.Location = new System.Drawing.Point(315, 505);
            this.mainbasePanel.Name = "mainbasePanel";
            this.mainbasePanel.Size = new System.Drawing.Size(394, 105);
            this.mainbasePanel.TabIndex = 69;
            // 
            // secondPhaseButton
            // 
            this.secondPhaseButton.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.mainbase_button;
            this.secondPhaseButton.Image = global::TradingCardGame.Images.effects.effects.phase_button_hint_00;
            this.secondPhaseButton.Location = new System.Drawing.Point(282, 49);
            this.secondPhaseButton.Name = "secondPhaseButton";
            this.secondPhaseButton.Size = new System.Drawing.Size(78, 20);
            this.secondPhaseButton.TabIndex = 2;
            this.secondPhaseButton.TabStop = false;
            this.secondPhaseButton.Click += new System.EventHandler(this.secondPhaseButton_Click);
            this.secondPhaseButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.secondPhaseButton_MouseDown);
            this.secondPhaseButton.MouseEnter += new System.EventHandler(this.secondPhaseButton_MouseEnter);
            this.secondPhaseButton.MouseLeave += new System.EventHandler(this.secondPhaseButton_MouseLeave);
            // 
            // phaseButton
            // 
            this.phaseButton.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.mainbase_button;
            this.phaseButton.Image = global::TradingCardGame.Images.effects.effects.phase_button_hint_00;
            this.phaseButton.Location = new System.Drawing.Point(282, 27);
            this.phaseButton.Name = "phaseButton";
            this.phaseButton.Size = new System.Drawing.Size(78, 20);
            this.phaseButton.TabIndex = 0;
            this.phaseButton.TabStop = false;
            this.phaseButton.Click += new System.EventHandler(this.phaseButton_Click);
            this.phaseButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.phaseButton_MouseDown);
            this.phaseButton.MouseEnter += new System.EventHandler(this.phaseButton_MouseEnter);
            this.phaseButton.MouseLeave += new System.EventHandler(this.phaseButton_MouseLeave);
            // 
            // mainbaseLabel
            // 
            this.mainbaseLabel.AutoSize = true;
            this.mainbaseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainbaseLabel.ForeColor = System.Drawing.Color.SandyBrown;
            this.mainbaseLabel.Location = new System.Drawing.Point(25, 27);
            this.mainbaseLabel.Name = "mainbaseLabel";
            this.mainbaseLabel.Size = new System.Drawing.Size(252, 32);
            this.mainbaseLabel.TabIndex = 1;
            this.mainbaseLabel.Text = "Would you like to deal 1 damage to\r\nyour avatar to redraw your hand?";
            // 
            // chatPanel
            // 
            this.chatPanel.BackColor = System.Drawing.Color.Transparent;
            this.chatPanel.Controls.Add(this.pictureBox6);
            this.chatPanel.Controls.Add(this.pictureBox5);
            this.chatPanel.Controls.Add(this.chatInputTextBox);
            this.chatPanel.Controls.Add(this.chatListBox);
            this.chatPanel.Location = new System.Drawing.Point(753, 451);
            this.chatPanel.Name = "chatPanel";
            this.chatPanel.Size = new System.Drawing.Size(275, 125);
            this.chatPanel.TabIndex = 70;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::TradingCardGame.Images.graphicsitems.gamescreen.texttoggle_playerlist;
            this.pictureBox6.Location = new System.Drawing.Point(242, 95);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(26, 26);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::TradingCardGame.Images.graphicsitems.gamescreen.texttoggle_playerlist;
            this.pictureBox5.Location = new System.Drawing.Point(212, 95);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(26, 26);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // chatInputTextBox
            // 
            this.chatInputTextBox.BackColor = System.Drawing.Color.Black;
            this.chatInputTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chatInputTextBox.ForeColor = System.Drawing.Color.White;
            this.chatInputTextBox.Location = new System.Drawing.Point(0, 95);
            this.chatInputTextBox.Name = "chatInputTextBox";
            this.chatInputTextBox.Size = new System.Drawing.Size(210, 26);
            this.chatInputTextBox.TabIndex = 3;
            // 
            // chatListBox
            // 
            this.chatListBox.BackColor = System.Drawing.Color.Black;
            this.chatListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chatListBox.ForeColor = System.Drawing.Color.White;
            this.chatListBox.FormattingEnabled = true;
            this.chatListBox.HorizontalScrollbar = true;
            this.chatListBox.ItemHeight = 15;
            this.chatListBox.Location = new System.Drawing.Point(0, 0);
            this.chatListBox.Name = "chatListBox";
            this.chatListBox.Size = new System.Drawing.Size(267, 94);
            this.chatListBox.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.playerbase_shelf;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(0, 522);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(252, 68);
            this.panel1.TabIndex = 71;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(63, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Turn: 1 Match 0 Game: 0";
            // 
            // enemyMainbasePanel
            // 
            this.enemyMainbasePanel.BackColor = System.Drawing.Color.Transparent;
            this.enemyMainbasePanel.BackgroundImage = global::TradingCardGame.Images.graphicsitems.gamescreen.phase_dialogue_top;
            this.enemyMainbasePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.enemyMainbasePanel.Controls.Add(this.enemyMainbaseLabel);
            this.enemyMainbasePanel.Location = new System.Drawing.Point(381, 150);
            this.enemyMainbasePanel.Name = "enemyMainbasePanel";
            this.enemyMainbasePanel.Size = new System.Drawing.Size(263, 79);
            this.enemyMainbasePanel.TabIndex = 72;
            this.enemyMainbasePanel.Visible = false;
            // 
            // enemyMainbaseLabel
            // 
            this.enemyMainbaseLabel.AutoSize = true;
            this.enemyMainbaseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyMainbaseLabel.ForeColor = System.Drawing.Color.SandyBrown;
            this.enemyMainbaseLabel.Location = new System.Drawing.Point(22, 23);
            this.enemyMainbaseLabel.MaximumSize = new System.Drawing.Size(220, 0);
            this.enemyMainbaseLabel.Name = "enemyMainbaseLabel";
            this.enemyMainbaseLabel.Size = new System.Drawing.Size(219, 32);
            this.enemyMainbaseLabel.TabIndex = 2;
            this.enemyMainbaseLabel.Text = "Please wait while enemy name\r\ncompletes the action.";
            // 
            // card1
            // 
            this.card1.BackColor = System.Drawing.Color.Transparent;
            this.card1.BackgroundImage = global::TradingCardGame.Images.card.card._100007634;
            this.card1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.card1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card1.Controls.Add(this.card1Cover);
            this.card1.Location = new System.Drawing.Point(10, 730);
            this.card1.Name = "card1";
            this.card1.Size = new System.Drawing.Size(258, 347);
            this.card1.TabIndex = 76;
            // 
            // card1Cover
            // 
            this.card1Cover.BackColor = System.Drawing.Color.Transparent;
            this.card1Cover.BackgroundImage = global::TradingCardGame.Images.card.card.template_generic;
            this.card1Cover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.card1Cover.Controls.Add(this.card1Description);
            this.card1Cover.Controls.Add(this.card1Subtitle);
            this.card1Cover.Controls.Add(this.card1Title);
            this.card1Cover.Controls.Add(this.card1Level);
            this.card1Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.card1Cover.Location = new System.Drawing.Point(0, 0);
            this.card1Cover.Name = "card1Cover";
            this.card1Cover.Size = new System.Drawing.Size(256, 345);
            this.card1Cover.TabIndex = 74;
            this.card1Cover.MouseEnter += new System.EventHandler(this.card1Cover_MouseEnter);
            // 
            // card1Description
            // 
            this.card1Description.AutoSize = true;
            this.card1Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card1Description.Location = new System.Drawing.Point(24, 256);
            this.card1Description.Name = "card1Description";
            this.card1Description.Size = new System.Drawing.Size(118, 16);
            this.card1Description.TabIndex = 3;
            this.card1Description.Text = "Card 1 Description";
            this.card1Description.MouseEnter += new System.EventHandler(this.card1Description_MouseEnter);
            // 
            // card1Subtitle
            // 
            this.card1Subtitle.AutoSize = true;
            this.card1Subtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card1Subtitle.Location = new System.Drawing.Point(54, 21);
            this.card1Subtitle.Name = "card1Subtitle";
            this.card1Subtitle.Size = new System.Drawing.Size(87, 15);
            this.card1Subtitle.TabIndex = 2;
            this.card1Subtitle.Text = "Card 1 Subtitle";
            this.card1Subtitle.MouseEnter += new System.EventHandler(this.card1Subtitle_MouseEnter);
            // 
            // card1Title
            // 
            this.card1Title.AutoSize = true;
            this.card1Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card1Title.Location = new System.Drawing.Point(54, 6);
            this.card1Title.Name = "card1Title";
            this.card1Title.Size = new System.Drawing.Size(88, 16);
            this.card1Title.TabIndex = 1;
            this.card1Title.Text = "Card 1 Title";
            this.card1Title.MouseEnter += new System.EventHandler(this.card1Title_MouseEnter);
            // 
            // card1Level
            // 
            this.card1Level.AutoSize = true;
            this.card1Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card1Level.Location = new System.Drawing.Point(8, 6);
            this.card1Level.Name = "card1Level";
            this.card1Level.Size = new System.Drawing.Size(30, 31);
            this.card1Level.TabIndex = 0;
            this.card1Level.Text = "1";
            this.card1Level.MouseEnter += new System.EventHandler(this.card1Level_MouseEnter);
            // 
            // card2
            // 
            this.card2.BackColor = System.Drawing.Color.Transparent;
            this.card2.BackgroundImage = global::TradingCardGame.Images.card.card._100007634;
            this.card2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.card2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card2.Controls.Add(this.card2Cover);
            this.card2.Location = new System.Drawing.Point(160, 730);
            this.card2.Name = "card2";
            this.card2.Size = new System.Drawing.Size(258, 347);
            this.card2.TabIndex = 77;
            // 
            // card2Cover
            // 
            this.card2Cover.BackColor = System.Drawing.Color.Transparent;
            this.card2Cover.BackgroundImage = global::TradingCardGame.Images.card.card.template_generic;
            this.card2Cover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.card2Cover.Controls.Add(this.card2Description);
            this.card2Cover.Controls.Add(this.card2Subtitle);
            this.card2Cover.Controls.Add(this.card2Title);
            this.card2Cover.Controls.Add(this.card2Level);
            this.card2Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.card2Cover.Location = new System.Drawing.Point(0, 0);
            this.card2Cover.Name = "card2Cover";
            this.card2Cover.Size = new System.Drawing.Size(256, 345);
            this.card2Cover.TabIndex = 74;
            // 
            // card2Description
            // 
            this.card2Description.AutoSize = true;
            this.card2Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card2Description.Location = new System.Drawing.Point(24, 256);
            this.card2Description.Name = "card2Description";
            this.card2Description.Size = new System.Drawing.Size(118, 16);
            this.card2Description.TabIndex = 3;
            this.card2Description.Text = "Card 2 Description";
            // 
            // card2Subtitle
            // 
            this.card2Subtitle.AutoSize = true;
            this.card2Subtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card2Subtitle.Location = new System.Drawing.Point(54, 21);
            this.card2Subtitle.Name = "card2Subtitle";
            this.card2Subtitle.Size = new System.Drawing.Size(87, 15);
            this.card2Subtitle.TabIndex = 2;
            this.card2Subtitle.Text = "Card 2 Subtitle";
            // 
            // card2Title
            // 
            this.card2Title.AutoSize = true;
            this.card2Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card2Title.Location = new System.Drawing.Point(54, 6);
            this.card2Title.Name = "card2Title";
            this.card2Title.Size = new System.Drawing.Size(88, 16);
            this.card2Title.TabIndex = 1;
            this.card2Title.Text = "Card 2 Title";
            // 
            // card2Level
            // 
            this.card2Level.AutoSize = true;
            this.card2Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card2Level.Location = new System.Drawing.Point(8, 6);
            this.card2Level.Name = "card2Level";
            this.card2Level.Size = new System.Drawing.Size(30, 31);
            this.card2Level.TabIndex = 0;
            this.card2Level.Text = "1";
            // 
            // card3
            // 
            this.card3.BackColor = System.Drawing.Color.Transparent;
            this.card3.BackgroundImage = global::TradingCardGame.Images.card.card._100007634;
            this.card3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.card3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card3.Controls.Add(this.card3Cover);
            this.card3.Location = new System.Drawing.Point(310, 730);
            this.card3.Name = "card3";
            this.card3.Size = new System.Drawing.Size(258, 347);
            this.card3.TabIndex = 78;
            // 
            // card3Cover
            // 
            this.card3Cover.BackColor = System.Drawing.Color.Transparent;
            this.card3Cover.BackgroundImage = global::TradingCardGame.Images.card.card.template_generic;
            this.card3Cover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.card3Cover.Controls.Add(this.card3Description);
            this.card3Cover.Controls.Add(this.card3Subtitle);
            this.card3Cover.Controls.Add(this.card3Title);
            this.card3Cover.Controls.Add(this.card3Level);
            this.card3Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.card3Cover.Location = new System.Drawing.Point(0, 0);
            this.card3Cover.Name = "card3Cover";
            this.card3Cover.Size = new System.Drawing.Size(256, 345);
            this.card3Cover.TabIndex = 74;
            // 
            // card3Description
            // 
            this.card3Description.AutoSize = true;
            this.card3Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card3Description.Location = new System.Drawing.Point(24, 256);
            this.card3Description.Name = "card3Description";
            this.card3Description.Size = new System.Drawing.Size(118, 16);
            this.card3Description.TabIndex = 3;
            this.card3Description.Text = "Card 3 Description";
            // 
            // card3Subtitle
            // 
            this.card3Subtitle.AutoSize = true;
            this.card3Subtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card3Subtitle.Location = new System.Drawing.Point(54, 21);
            this.card3Subtitle.Name = "card3Subtitle";
            this.card3Subtitle.Size = new System.Drawing.Size(87, 15);
            this.card3Subtitle.TabIndex = 2;
            this.card3Subtitle.Text = "Card 3 Subtitle";
            // 
            // card3Title
            // 
            this.card3Title.AutoSize = true;
            this.card3Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card3Title.Location = new System.Drawing.Point(54, 6);
            this.card3Title.Name = "card3Title";
            this.card3Title.Size = new System.Drawing.Size(88, 16);
            this.card3Title.TabIndex = 1;
            this.card3Title.Text = "Card 3 Title";
            // 
            // card3Level
            // 
            this.card3Level.AutoSize = true;
            this.card3Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card3Level.Location = new System.Drawing.Point(8, 6);
            this.card3Level.Name = "card3Level";
            this.card3Level.Size = new System.Drawing.Size(30, 31);
            this.card3Level.TabIndex = 0;
            this.card3Level.Text = "1";
            // 
            // card4
            // 
            this.card4.BackColor = System.Drawing.Color.Transparent;
            this.card4.BackgroundImage = global::TradingCardGame.Images.card.card._100007634;
            this.card4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.card4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card4.Controls.Add(this.card4Cover);
            this.card4.Location = new System.Drawing.Point(460, 730);
            this.card4.Name = "card4";
            this.card4.Size = new System.Drawing.Size(258, 347);
            this.card4.TabIndex = 79;
            // 
            // card4Cover
            // 
            this.card4Cover.BackColor = System.Drawing.Color.Transparent;
            this.card4Cover.BackgroundImage = global::TradingCardGame.Images.card.card.template_generic;
            this.card4Cover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.card4Cover.Controls.Add(this.card4Description);
            this.card4Cover.Controls.Add(this.card4Subtitle);
            this.card4Cover.Controls.Add(this.card4Title);
            this.card4Cover.Controls.Add(this.card4Level);
            this.card4Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.card4Cover.Location = new System.Drawing.Point(0, 0);
            this.card4Cover.Name = "card4Cover";
            this.card4Cover.Size = new System.Drawing.Size(256, 345);
            this.card4Cover.TabIndex = 74;
            // 
            // card4Description
            // 
            this.card4Description.AutoSize = true;
            this.card4Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card4Description.Location = new System.Drawing.Point(24, 256);
            this.card4Description.Name = "card4Description";
            this.card4Description.Size = new System.Drawing.Size(118, 16);
            this.card4Description.TabIndex = 3;
            this.card4Description.Text = "Card 4 Description";
            // 
            // card4Subtitle
            // 
            this.card4Subtitle.AutoSize = true;
            this.card4Subtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card4Subtitle.Location = new System.Drawing.Point(54, 21);
            this.card4Subtitle.Name = "card4Subtitle";
            this.card4Subtitle.Size = new System.Drawing.Size(87, 15);
            this.card4Subtitle.TabIndex = 2;
            this.card4Subtitle.Text = "Card 4 Subtitle";
            // 
            // card4Title
            // 
            this.card4Title.AutoSize = true;
            this.card4Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card4Title.Location = new System.Drawing.Point(54, 6);
            this.card4Title.Name = "card4Title";
            this.card4Title.Size = new System.Drawing.Size(88, 16);
            this.card4Title.TabIndex = 1;
            this.card4Title.Text = "Card 4 Title";
            // 
            // card4Level
            // 
            this.card4Level.AutoSize = true;
            this.card4Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card4Level.Location = new System.Drawing.Point(8, 6);
            this.card4Level.Name = "card4Level";
            this.card4Level.Size = new System.Drawing.Size(30, 31);
            this.card4Level.TabIndex = 0;
            this.card4Level.Text = "1";
            // 
            // card5
            // 
            this.card5.BackColor = System.Drawing.Color.Transparent;
            this.card5.BackgroundImage = global::TradingCardGame.Images.card.card._100007634;
            this.card5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.card5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card5.Controls.Add(this.card5Cover);
            this.card5.Location = new System.Drawing.Point(610, 730);
            this.card5.Name = "card5";
            this.card5.Size = new System.Drawing.Size(258, 347);
            this.card5.TabIndex = 80;
            // 
            // card5Cover
            // 
            this.card5Cover.BackColor = System.Drawing.Color.Transparent;
            this.card5Cover.BackgroundImage = global::TradingCardGame.Images.card.card.template_generic;
            this.card5Cover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.card5Cover.Controls.Add(this.card5Description);
            this.card5Cover.Controls.Add(this.card5Subtitle);
            this.card5Cover.Controls.Add(this.card5Title);
            this.card5Cover.Controls.Add(this.card5Level);
            this.card5Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.card5Cover.Location = new System.Drawing.Point(0, 0);
            this.card5Cover.Name = "card5Cover";
            this.card5Cover.Size = new System.Drawing.Size(256, 345);
            this.card5Cover.TabIndex = 74;
            // 
            // card5Description
            // 
            this.card5Description.AutoSize = true;
            this.card5Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card5Description.Location = new System.Drawing.Point(24, 256);
            this.card5Description.Name = "card5Description";
            this.card5Description.Size = new System.Drawing.Size(118, 16);
            this.card5Description.TabIndex = 3;
            this.card5Description.Text = "Card 5 Description";
            // 
            // card5Subtitle
            // 
            this.card5Subtitle.AutoSize = true;
            this.card5Subtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card5Subtitle.Location = new System.Drawing.Point(54, 21);
            this.card5Subtitle.Name = "card5Subtitle";
            this.card5Subtitle.Size = new System.Drawing.Size(87, 15);
            this.card5Subtitle.TabIndex = 2;
            this.card5Subtitle.Text = "Card 5 Subtitle";
            // 
            // card5Title
            // 
            this.card5Title.AutoSize = true;
            this.card5Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card5Title.Location = new System.Drawing.Point(54, 6);
            this.card5Title.Name = "card5Title";
            this.card5Title.Size = new System.Drawing.Size(88, 16);
            this.card5Title.TabIndex = 1;
            this.card5Title.Text = "Card 5 Title";
            // 
            // card5Level
            // 
            this.card5Level.AutoSize = true;
            this.card5Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card5Level.Location = new System.Drawing.Point(9, 5);
            this.card5Level.Name = "card5Level";
            this.card5Level.Size = new System.Drawing.Size(30, 31);
            this.card5Level.TabIndex = 0;
            this.card5Level.Text = "1";
            // 
            // card6
            // 
            this.card6.BackColor = System.Drawing.Color.Transparent;
            this.card6.BackgroundImage = global::TradingCardGame.Images.card.card._100007634;
            this.card6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.card6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card6.Controls.Add(this.card6Cover);
            this.card6.Location = new System.Drawing.Point(760, 730);
            this.card6.Name = "card6";
            this.card6.Size = new System.Drawing.Size(258, 347);
            this.card6.TabIndex = 81;
            // 
            // card6Cover
            // 
            this.card6Cover.BackColor = System.Drawing.Color.Transparent;
            this.card6Cover.BackgroundImage = global::TradingCardGame.Images.card.card.template_generic;
            this.card6Cover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.card6Cover.Controls.Add(this.card6Description);
            this.card6Cover.Controls.Add(this.card6Subtitle);
            this.card6Cover.Controls.Add(this.card6Title);
            this.card6Cover.Controls.Add(this.card6Level);
            this.card6Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.card6Cover.Location = new System.Drawing.Point(0, 0);
            this.card6Cover.Name = "card6Cover";
            this.card6Cover.Size = new System.Drawing.Size(256, 345);
            this.card6Cover.TabIndex = 74;
            // 
            // card6Description
            // 
            this.card6Description.AutoSize = true;
            this.card6Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card6Description.Location = new System.Drawing.Point(24, 256);
            this.card6Description.Name = "card6Description";
            this.card6Description.Size = new System.Drawing.Size(118, 16);
            this.card6Description.TabIndex = 3;
            this.card6Description.Text = "Card 6 Description";
            // 
            // card6Subtitle
            // 
            this.card6Subtitle.AutoSize = true;
            this.card6Subtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card6Subtitle.Location = new System.Drawing.Point(54, 21);
            this.card6Subtitle.Name = "card6Subtitle";
            this.card6Subtitle.Size = new System.Drawing.Size(87, 15);
            this.card6Subtitle.TabIndex = 2;
            this.card6Subtitle.Text = "Card 6 Subtitle";
            // 
            // card6Title
            // 
            this.card6Title.AutoSize = true;
            this.card6Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card6Title.Location = new System.Drawing.Point(54, 6);
            this.card6Title.Name = "card6Title";
            this.card6Title.Size = new System.Drawing.Size(88, 16);
            this.card6Title.TabIndex = 1;
            this.card6Title.Text = "Card 6 Title";
            // 
            // card6Level
            // 
            this.card6Level.AutoSize = true;
            this.card6Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card6Level.Location = new System.Drawing.Point(9, 5);
            this.card6Level.Name = "card6Level";
            this.card6Level.Size = new System.Drawing.Size(30, 31);
            this.card6Level.TabIndex = 0;
            this.card6Level.Text = "1";
            // 
            // playmatCard1
            // 
            this.playmatCard1.BackColor = System.Drawing.Color.White;
            this.playmatCard1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.playmatCard1.Controls.Add(this.playmatCard1Cover);
            this.playmatCard1.Location = new System.Drawing.Point(175, 394);
            this.playmatCard1.Name = "playmatCard1";
            this.playmatCard1.Size = new System.Drawing.Size(125, 125);
            this.playmatCard1.TabIndex = 82;
            this.playmatCard1.Visible = false;
            // 
            // playmatCard1Cover
            // 
            this.playmatCard1Cover.BackColor = System.Drawing.Color.Transparent;
            this.playmatCard1Cover.BackgroundImage = global::TradingCardGame.Images.card.card.playmatcard_short_both_sith;
            this.playmatCard1Cover.Controls.Add(this.playmatCard1Title);
            this.playmatCard1Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playmatCard1Cover.Location = new System.Drawing.Point(0, 0);
            this.playmatCard1Cover.Name = "playmatCard1Cover";
            this.playmatCard1Cover.Size = new System.Drawing.Size(125, 125);
            this.playmatCard1Cover.TabIndex = 0;
            // 
            // playmatCard1Title
            // 
            this.playmatCard1Title.AutoSize = true;
            this.playmatCard1Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playmatCard1Title.ForeColor = System.Drawing.Color.White;
            this.playmatCard1Title.Location = new System.Drawing.Point(21, 5);
            this.playmatCard1Title.Name = "playmatCard1Title";
            this.playmatCard1Title.Size = new System.Drawing.Size(104, 15);
            this.playmatCard1Title.TabIndex = 0;
            this.playmatCard1Title.Text = "Playmat Card 1";
            this.playmatCard1Title.Visible = false;
            // 
            // secondLeftPlaymatCard
            // 
            this.secondLeftPlaymatCard.BackColor = System.Drawing.Color.White;
            this.secondLeftPlaymatCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.secondLeftPlaymatCard.Controls.Add(this.playmatCard2Cover);
            this.secondLeftPlaymatCard.Location = new System.Drawing.Point(306, 394);
            this.secondLeftPlaymatCard.Name = "secondLeftPlaymatCard";
            this.secondLeftPlaymatCard.Size = new System.Drawing.Size(125, 125);
            this.secondLeftPlaymatCard.TabIndex = 83;
            this.secondLeftPlaymatCard.Visible = false;
            // 
            // playmatCard2Cover
            // 
            this.playmatCard2Cover.BackColor = System.Drawing.Color.Transparent;
            this.playmatCard2Cover.BackgroundImage = global::TradingCardGame.Images.card.card.playmatcard_short_both_sith;
            this.playmatCard2Cover.Controls.Add(this.playmatCard2Title);
            this.playmatCard2Cover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playmatCard2Cover.Location = new System.Drawing.Point(0, 0);
            this.playmatCard2Cover.Name = "playmatCard2Cover";
            this.playmatCard2Cover.Size = new System.Drawing.Size(125, 125);
            this.playmatCard2Cover.TabIndex = 1;
            // 
            // playmatCard2Title
            // 
            this.playmatCard2Title.AutoSize = true;
            this.playmatCard2Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playmatCard2Title.ForeColor = System.Drawing.Color.White;
            this.playmatCard2Title.Location = new System.Drawing.Point(21, 5);
            this.playmatCard2Title.Name = "playmatCard2Title";
            this.playmatCard2Title.Size = new System.Drawing.Size(104, 15);
            this.playmatCard2Title.TabIndex = 0;
            this.playmatCard2Title.Text = "Playmat Card 2";
            this.playmatCard2Title.Visible = false;
            // 
            // CombatPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TradingCardGame.Images.backgrounds.backgrounds.game_bg_01;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.playmatCard1);
            this.Controls.Add(this.card6);
            this.Controls.Add(this.card5);
            this.Controls.Add(this.card4);
            this.Controls.Add(this.card3);
            this.Controls.Add(this.card2);
            this.Controls.Add(this.card1);
            this.Controls.Add(this.enemyMainbasePanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.outcomePanel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.mainbasePanel);
            this.Controls.Add(this.chatPanel);
            this.Controls.Add(this.secondLeftPlaymatCard);
            this.DoubleBuffered = true;
            this.Name = "CombatPage";
            this.Text = "[Star Wars Galaxies TCG] Casual";
            this.Load += new System.EventHandler(this.CombatPage_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CombatPage_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.CombatPage_MouseMove);
            this.outcomePanel.ResumeLayout(false);
            this.outcomePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.outcomePictureBox)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.enemiesAvatarPictureBox)).EndInit();
            this.panel8.ResumeLayout(false);
            this.playersAvatarPictureBox.ResumeLayout(false);
            this.itemsToolbarPanel.ResumeLayout(false);
            this.itemsToolbarPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.mainbasePanel.ResumeLayout(false);
            this.mainbasePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondPhaseButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseButton)).EndInit();
            this.chatPanel.ResumeLayout(false);
            this.chatPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.enemyMainbasePanel.ResumeLayout(false);
            this.enemyMainbasePanel.PerformLayout();
            this.card1.ResumeLayout(false);
            this.card1Cover.ResumeLayout(false);
            this.card1Cover.PerformLayout();
            this.card2.ResumeLayout(false);
            this.card2Cover.ResumeLayout(false);
            this.card2Cover.PerformLayout();
            this.card3.ResumeLayout(false);
            this.card3Cover.ResumeLayout(false);
            this.card3Cover.PerformLayout();
            this.card4.ResumeLayout(false);
            this.card4Cover.ResumeLayout(false);
            this.card4Cover.PerformLayout();
            this.card5.ResumeLayout(false);
            this.card5Cover.ResumeLayout(false);
            this.card5Cover.PerformLayout();
            this.card6.ResumeLayout(false);
            this.card6Cover.ResumeLayout(false);
            this.card6Cover.PerformLayout();
            this.playmatCard1.ResumeLayout(false);
            this.playmatCard1Cover.ResumeLayout(false);
            this.playmatCard1Cover.PerformLayout();
            this.secondLeftPlaymatCard.ResumeLayout(false);
            this.playmatCard2Cover.ResumeLayout(false);
            this.playmatCard2Cover.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label enemyNameLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Timer enemyTimer;
        private System.Windows.Forms.Timer timeLeftTimer;
        private System.Windows.Forms.Panel outcomePanel;
        private System.Windows.Forms.PictureBox outcomePictureBox;
        private System.Windows.Forms.Label creditsRewardLabel;
        private System.Windows.Forms.Label expRewardLabel;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Timer effectTimer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel itemsToolbarPanel;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.PictureBox enemiesAvatarPictureBox;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel itemsPanel2;
        private System.Windows.Forms.Panel itemsPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer cardExpandAnimationTimer;
        private System.Windows.Forms.Timer cardDespandAnimationTimer;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label enemyHandLabel;
        private System.Windows.Forms.Label enemyDeckLabel;
        private System.Windows.Forms.Label enemyDiscardPileLabel;
        private System.Windows.Forms.Label cardsInDicardPileLabel;
        private System.Windows.Forms.Label cardsInDeckLabel;
        private System.Windows.Forms.Label cardsInHandLabel;
        private System.Windows.Forms.Panel mainbasePanel;
        private System.Windows.Forms.PictureBox phaseButton;
        private System.Windows.Forms.Label mainbaseLabel;
        private System.Windows.Forms.PictureBox secondPhaseButton;
        private System.Windows.Forms.Panel chatPanel;
        private System.Windows.Forms.TextBox chatInputTextBox;
        private System.Windows.Forms.ListBox chatListBox;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel enemyMainbasePanel;
        private System.Windows.Forms.Label enemyMainbaseLabel;
        private System.Windows.Forms.Panel card1;
        private System.Windows.Forms.Panel card1Cover;
        private System.Windows.Forms.Label card1Description;
        private System.Windows.Forms.Label card1Subtitle;
        public System.Windows.Forms.Label card1Title;
        private System.Windows.Forms.Label card1Level;
        private System.Windows.Forms.Panel playersAvatarPictureBox;
        private System.Windows.Forms.Panel avatarCoverPanel;
        private System.Windows.Forms.Panel card2;
        private System.Windows.Forms.Panel card2Cover;
        private System.Windows.Forms.Label card2Description;
        private System.Windows.Forms.Label card2Subtitle;
        private System.Windows.Forms.Label card2Title;
        private System.Windows.Forms.Label card2Level;
        private System.Windows.Forms.Panel card3;
        private System.Windows.Forms.Panel card3Cover;
        private System.Windows.Forms.Label card3Description;
        private System.Windows.Forms.Label card3Subtitle;
        private System.Windows.Forms.Label card3Title;
        private System.Windows.Forms.Label card3Level;
        private System.Windows.Forms.Panel card4;
        private System.Windows.Forms.Panel card4Cover;
        private System.Windows.Forms.Label card4Description;
        private System.Windows.Forms.Label card4Subtitle;
        private System.Windows.Forms.Label card4Title;
        private System.Windows.Forms.Label card4Level;
        private System.Windows.Forms.Panel card5;
        private System.Windows.Forms.Panel card5Cover;
        private System.Windows.Forms.Label card5Description;
        private System.Windows.Forms.Label card5Subtitle;
        private System.Windows.Forms.Label card5Title;
        private System.Windows.Forms.Label card5Level;
        private System.Windows.Forms.Panel card6;
        private System.Windows.Forms.Panel card6Cover;
        private System.Windows.Forms.Label card6Description;
        private System.Windows.Forms.Label card6Subtitle;
        private System.Windows.Forms.Label card6Title;
        private System.Windows.Forms.Label card6Level;
        private System.Windows.Forms.Panel playmatCard1;
        private System.Windows.Forms.Panel secondLeftPlaymatCard;
        private System.Windows.Forms.Panel playmatCard1Cover;
        private System.Windows.Forms.Label playmatCard1Title;
        private System.Windows.Forms.Panel playmatCard2Cover;
        private System.Windows.Forms.Label playmatCard2Title;
    }
}

