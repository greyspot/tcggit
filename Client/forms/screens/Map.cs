﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;
using TradingCardGame.components;
using TradingCardGame.services.sound;

namespace TradingCardGame
{
    public partial class Map : UserControl
    {
        public Map()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void Map_Load(object sender, EventArgs e)
        {
            for (byte i = 0; i < 4; i++)
                mainButtonsPanel.Controls.Add(new MapButton(i));

            preferencesButton.BackgroundImage = Images.map_screen.map_screen.button_preferences;
            collectionsButton.BackgroundImage = Images.map_screen.map_screen.button_collection;
            deckBuilderButton.BackgroundImage = Images.map_screen.map_screen.button_deckbuilder;
            exitButton.BackgroundImage = Images.map_screen.map_screen.button_exit;

            tcgLogo.Image = Images.map_screen.map_screen.logo;

            services.data.TemporaryData.setCurrentForm(5);

            if (!Properties.Settings.Default.hidePlayerTips)
                Controls.Add(new forms.dialogs.PlayerTips());

            Controls.Add(new forms.widgets.Navigator());
            motdTextBox.Text = services.database.DatabaseService.GetValue("config", "motd", "server", "tcg");
            gettingStartedLabel.Text = Properties.Resources.gettingStartedText;

            string[] row = { "1", "1000", "Bib Fortuna" };
            leaderboardsListViewer.Items.Add(new ListViewItem(row));
            //leaderboardsListViewer.Items[0].BackColor = Color.DarkGray;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            services.command.CommandService.exitGame();
        }

        private void deckBuilderButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Controls.Add(new DeckBuilder());
            Dispose();
        }

        private void collectionsButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Controls.Add(new CollectionManager());
            Dispose();
        }

        private void collectionsButton_MouseDown(object sender, MouseEventArgs e)
        {
            collectionsButton.BackgroundImage = Images.map_screen.map_screen.button_collection_down;
        }

        private void collectionsButton_MouseEnter(object sender, EventArgs e)
        {
            collectionsButton.BackgroundImage = Images.map_screen.map_screen.button_collection_over;
            preferencesButton.BackgroundImage = Images.map_screen.map_screen.button_preferences;
        }

        private void collectionsButton_MouseLeave(object sender, EventArgs e)
        {
            if (!collectionsButton.ClientRectangle.Contains(collectionsButton.PointToClient(MousePosition)))
                collectionsButton.BackgroundImage = Images.map_screen.map_screen.button_collection;
        }

        private void collectionsButton_MouseUp(object sender, MouseEventArgs e)
        {
            collectionsButton.BackgroundImage = Images.map_screen.map_screen.button_collection;
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.BackgroundImage = Images.map_screen.map_screen.button_exit_down;
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = Images.map_screen.map_screen.button_exit_over;
            deckBuilderButton.BackgroundImage = Images.map_screen.map_screen.button_deckbuilder;
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            if (!exitButton.ClientRectangle.Contains(exitButton.PointToClient(MousePosition)))
                exitButton.BackgroundImage = Images.map_screen.map_screen.button_exit;
        }

        private void exitButton_MouseUp(object sender, MouseEventArgs e)
        {
            exitButton.BackgroundImage = Images.map_screen.map_screen.button_exit;
        }

        private void deckBuilderButton_MouseDown(object sender, MouseEventArgs e)
        {
            deckBuilderButton.BackgroundImage = Images.map_screen.map_screen.button_deckbuilder_down;
        }

        private void deckBuilderButton_MouseEnter(object sender, EventArgs e)
        {
            deckBuilderButton.BackgroundImage = Images.map_screen.map_screen.button_deckbuilder_over;
            collectionsButton.BackgroundImage = Images.map_screen.map_screen.button_collection;
        }

        private void deckBuilderButton_MouseLeave(object sender, EventArgs e)
        {
            if (!deckBuilderButton.ClientRectangle.Contains(deckBuilderButton.PointToClient(MousePosition)))
                deckBuilderButton.BackgroundImage = Images.map_screen.map_screen.button_deckbuilder;
        }

        private void deckBuilderButton_MouseUp(object sender, MouseEventArgs e)
        {
            deckBuilderButton.BackgroundImage = Images.map_screen.map_screen.button_deckbuilder;
        }

        private void preferencesButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Controls.Add(new forms.dialogs.Preferences());
        }

        private void preferencesButton_MouseEnter(object sender, EventArgs e)
        {
            preferencesButton.BackgroundImage = Images.map_screen.map_screen.button_preferences_over;
        }

        private void preferencesButton_MouseLeave(object sender, EventArgs e)
        {
            if (!preferencesButton.ClientRectangle.Contains(preferencesButton.PointToClient(MousePosition)))
                preferencesButton.BackgroundImage = Images.map_screen.map_screen.button_preferences;
        }

        private void preferencesButton_MouseDown(object sender, MouseEventArgs e)
        {
            preferencesButton.BackgroundImage = Images.map_screen.map_screen.button_preferences_down;
        }

        private void preferencesButton_MouseUp(object sender, MouseEventArgs e)
        {
            preferencesButton.BackgroundImage = Images.map_screen.map_screen.button_preferences;
        }

        private void viewMoreButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void viewMoreButton_MouseDown(object sender, MouseEventArgs e)
        {
            viewMoreButton.BackgroundImage = Images.buttons.buttons.button_lg_down;
        }

        private void viewMoreButton_MouseEnter(object sender, EventArgs e)
        {
            viewMoreButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }

        private void viewMoreButton_MouseLeave(object sender, EventArgs e)
        {
            viewMoreButton.BackgroundImage = Images.buttons.buttons.button_lg;
        }

        private void viewMoreButton_MouseUp(object sender, MouseEventArgs e)
        {
            viewMoreButton.BackgroundImage = Images.buttons.buttons.button_lg_over;
        }
    }
}
