﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.login;
using TradingCardGame.services.sound;

namespace TradingCardGame
{
    public partial class Login : UserControl
    {
        public Login()
        {
            InitializeComponent();
            BackgroundImage = Properties.Resources.tcglogo;
        }

        private Point p = new Point();

        private int seconds = 0;

        private void LoginFunction()
        {
            try
            {
                if (!LoginService.LoginInfoIsValidAndCorrect(usernameTextBox.Text, passwordTextBox.Text))
                {
                    MessageBox.Show("Invalid username or password.");
                    return;
                }

                // check if server is offline
                if (LoginService.getServerConfigValue("status").Equals("0"))
                {
                    MessageBox.Show("Sorry, but the SWG TCG server is currently offline.", "SWG HC TCG Server Is Offline");
                    return;
                }

                // check if user is banned
                if (services.account.AccountService.UserIsBanned(usernameTextBox.Text))
                {
                    MessageBox.Show("You are currently banned from the SWG HC TCG.");
                    return;
                }

                // check if user's account is registered

                if (services.database.DatabaseService.GetValue("mybb_users", "usergroup", "username", usernameTextBox.Text).Equals("5"))
                {
                    MessageBox.Show("Sorry, but your account is not registered. Please visit the SWG Hutt Cartel website.");
                    return;
                }

                // set username and use get value to use the exact uppercase/lowercase of username
                services.account.AccountService.SetUsername(services.database.DatabaseService.GetValue("mybb_users", "username", "username", usernameTextBox.Text));

                // Connect to server
                services.packet.PacketService.connectToServer();

                Parent.Controls.Add(new Map());
                Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Login Error Message:\n" + ex.Message);
            }
        }

        private void PasswordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
             if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape || e.KeyCode == Keys.CapsLock)
                e.SuppressKeyPress = true;
            SoundService.KeyPressSound(e.KeyCode);

            if (e.KeyCode == Keys.Enter)
                LoginFunction();
        }

        private void LoginPage_Load(object sender, EventArgs e)
        {
            MusicService.playMusic();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            LoginFunction();
        }

        private void UsernameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.CapsLock)
                e.SuppressKeyPress = true;
            else if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape)
                e.Handled = e.SuppressKeyPress = true;

            SoundService.KeyPressSound(e.KeyCode);
        }

        private void SetLoginScreen()
        {
            switchLogoTimer.Enabled = false;
            versionLabel.Text = "v" + Application.ProductVersion;
            mainPanel.Location = new Point((Width - mainPanel.Width) / 2, (Height - mainPanel.Height) / 2);
            BackgroundImage = Images.backgrounds.backgrounds.lobby_background_casual;
            mainPanel.Visible = true;
            versionLabel.Visible = true;
        }

        private void SwitchLogoTimer_Tick(object sender, EventArgs e)
        {
            seconds++;
            if (seconds == 3)
                BackgroundImage = Images.backgrounds.backgrounds.menubackground_03;
            else if (seconds == 7)
                SetLoginScreen();
        }

        private void LoginPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.ExitThread();
        }

        private void MainPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                mainPanel.Location = new Point(e.X - p.X + mainPanel.Location.X, e.Y - p.Y + mainPanel.Location.Y);
        }

        private void MainPanel_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void MainPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (mainPanel.Location.X > Width || mainPanel.Location.X < (0 - mainPanel.Width) || mainPanel.Location.Y > Height || mainPanel.Location.Y < (0 - mainPanel.Height))
                mainPanel.Location = new Point((Width - mainPanel.Width) / 2, (Height - mainPanel.Height) / 2);
        }
    }
}
