﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TradingCardGame.services.database;

namespace TradingCardGame.services.account
{
    class AccountService
    {
        private static string username;

        public static string GetUsername()
        {
            return username;
        }

        public static void SetUsername(string newUsername)
        {
            username = newUsername;
        }

        public static void CreateAccountFiles()
        {
            CreateUserProfileColumn();
            CreateInventoryColumn();
        }

        public static bool UserIsBanned(string userName)
        {
            return DatabaseService.GetValue("mybb_users", "usergroup", "username", userName).Equals("1");
        }

        public static string[] GetAvailableCardDecks()
        {
            string[] cardDecks = { "Default Deck" };
            return cardDecks;
        }

        public static bool UsernameExists(string username)
        {
            using (MySql.Data.MySqlClient.MySqlCommand com = new MySql.Data.MySqlClient.MySqlCommand("SELECT COUNT(*) FROM users WHERE username = '" + username + "'", DatabaseService.GetMySqlConnection(1)))
            {
                DatabaseService.OpenConnectionIfNotOpen(1);
                return int.Parse(com.ExecuteScalar().ToString()) > 0;
            }
        }

        private static readonly string[] staffTitles = { " [HC-Support]", " [HC-Dev]", " [HC-Admin]" };

        public static string GetRemovedStaffTitle(string completeName)
        {
            for (byte i = 0; i < staffTitles.Length; i++)
            {
                if (completeName.Contains(staffTitles[i]))
                    return completeName.Replace(staffTitles[i], string.Empty);
            }
            return completeName;
        }

        public static string GetUsernameWithTitle(string user)
        {
            string accountNumber = DatabaseService.GetValue("mybb_users", "displaygroup", "username", username);

            if (accountNumber.Equals("3"))
                user += staffTitles[0];
            else if (accountNumber.Equals("6"))
                user += staffTitles[1];
            else if (accountNumber.Equals("4"))
                user += staffTitles[2];
            return user;
        }

        public static void CreateUserProfileColumn()
        {
            string commandText = "INSERT INTO users(username) VALUES('" + GetUsername() + "')";
            DatabaseService.ExecuteCommand(commandText);
        }

        public static void CreateInventoryColumn()
        {
            string commandText = "INSERT INTO inventory(username) VALUE('" + GetUsername() + "')";
            DatabaseService.ExecuteCommand(commandText);
        }

        public static void WriteNewValue(string newValue, string column)
        {
            string commandText = "UPDATE users SET " + column + " = '" + newValue + "' WHERE username = '" + GetUsername() + "'";
            DatabaseService.ExecuteCommand(commandText);
        }

        public static string GetRealForumPassword()
        {
            return DatabaseService.GetValue("mybb_users", "password", "username", username);
        }
    }
}
