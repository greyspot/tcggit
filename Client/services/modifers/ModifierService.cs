﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TradingCardGame.services.account;
using TradingCardGame.services.inventory;

namespace TradingCardGame.services.modifers
{
    class ModifierService
    {
        InventoryService invSrv = new InventoryService();

        public int dmgPower()
        {
            return 1;
        }

        public int healPower()
        {
            return 1;
        }

        public int critChance()
        {
            return 5;
        }

        public int critPowerAI()
        {
            return 2;
        }

        public int critPower()
        {
            int totalCritPwr = 2;
            if (invSrv.modifierCardsContains("ig88"))
                totalCritPwr = totalCritPwr + 10;
            return totalCritPwr;
        }

        public int dodgeChance()
        {
            int totalDodge = 5;
            if (invSrv.modifierCardsContains("ataru"))
                totalDodge = totalDodge + 5;
            return totalDodge;
        }

        public int actionRegenBonus()
        {
            int totalActionReg = 1;
            if (invSrv.modifierCardsContains("ataru"))
                totalActionReg = totalActionReg + 25;
            return totalActionReg;
        }

        public int getTotalActionOrHealth(string actionOrHealth)
        {
            if (actionOrHealth.Equals("action"))
                return int.Parse(database.DatabaseService.GetValue("users", "level", "username", AccountService.GetUsername())) * 125;
            else
            {
                int totalHealth = int.Parse(database.DatabaseService.GetValue("users", "level", "username", AccountService.GetUsername())) * 150;
                if (invSrv.modifierCardsContains("ataru"))
                    totalHealth = totalHealth + (totalHealth / 10);
                return totalHealth;
            }
        }
    }
}
