﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TradingCardGame.services.account;
using TradingCardGame.services.login;

namespace TradingCardGame.services.reward
{
    class RewardService
    {
        public int rewardCredits(bool win)
        {
            int creditRate = int.Parse(LoginService.getServerConfigValue("creditRate"));

            if (win)
                return 10 * int.Parse(database.DatabaseService.GetValue("users", "level", "username", AccountService.GetUsername())) * creditRate;
            else
                return 5 * int.Parse(database.DatabaseService.GetValue("users", "level", "username", AccountService.GetUsername())) * creditRate;
        }

        public void getRewards(bool win)
        {

        }
    }
}
