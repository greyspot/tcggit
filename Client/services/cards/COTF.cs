﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace TradingCardGame.services.cards
{
    class COTF
    {
        public static string[] lootCards = {
            "black_corset_dress",
            "bodyguard_jacket",
            "count_dooku_bust",
            "glass_display_case",
            "gorax_ear",
            "great_hyperspace_war_bas_relief",
            "guise_apprentice",
            "guise_sith",
            "gungan_vendor",
            "housecleaning_kit",
            "imperial_life_support_flight_suit",
            "jawa_vendor",
            "large_indoor_fountain",
            "meatlump_greeter",
            "mechno_chair",
            "muur_talisman",
            "nuna_ball_advertisement",
            "ord_pedrovia_podracer",
            "painting_jedi_crest",
            "painting_trooper",
            "radtrooper_insignia",
            "rebel_life_support_flight_suit",
            "sith_speeder",
            "smallindoorrockgarden",
            "xj_2_airspeeder"
        };
    }
}
