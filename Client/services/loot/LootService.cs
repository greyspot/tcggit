﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using TradingCardGame.services.inventory;

namespace TradingCardGame.services.loot
{
    class LootService
    {
        InventoryService invSrv = new InventoryService();
        public int lootType()
        {
            int roll = new Random().Next(1, 10000);
            if (roll == 9999)
                return 0; //legendary
            else if (roll >= 9000)
                return 1; // unique
            else if (roll >= 8000)
                return 2; // exeptional
            else if (roll >= 7000)
                return 3; // rare
            else if (roll >= 5000)
                return 4; // uncommon
            else
                return 5; // regular
        }

        public int selectedCard()
        {
            Random random = new Random();
            return random.Next(1, 11);
        }

        public int lootAmount()
        {
            Random random = new Random();
            return random.Next(1, 4);
        }

        public void givePlayerCard(string cardName)
        {
            invSrv.addCardToInventory(cardName);
        }
    }
}
