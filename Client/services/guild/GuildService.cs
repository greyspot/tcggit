﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using MySql.Data.MySqlClient;
using System;
using TradingCardGame.services.account;
using TradingCardGame.services.database;

namespace TradingCardGame.services.guild
{
    class GuildService
    {
        public static string getUsersGuild(string user)
        {
            return DatabaseService.GetValue("users", "guild", "username", user);
        }

        public static string[] getGuildMembers(string guildAbbrev)
        {
            string members = getValue(guildAbbrev, "members");

            if (string.IsNullOrEmpty(members))
                return new string[0];

            return members.Substring(0, members.Length - 1).Split(',');
        }

        public static string[] getGuildOfficers(string guildAbbrev)
        {
            string officers = getValue(guildAbbrev, "officers");
            return officers.Substring(0, officers.Length - 1).Split(',');
        }
        
        public static string[] getTotalMemberNames(string guildAbbrev)
        {
            string[] names = new string[getTotalGuildMembers(guildAbbrev)];

            int numberOfGuildMembers = getNumberOfGuildMembers(guildAbbrev);
            int numberOfGuildOfficers = getNumberOfGuildOfficers(guildAbbrev);

            for (byte i = 0; i < numberOfGuildMembers; i++)
                names[i] = getGuildMembers(guildAbbrev)[i];
            for (byte i = 0; i < numberOfGuildMembers; i++)
                names[i + numberOfGuildMembers] = getGuildOfficers(guildAbbrev)[i];
            names[numberOfGuildMembers + numberOfGuildOfficers] = getGuildLeader(guildAbbrev);

            return names;
        }

        public static void removeUserFromGuild(string user)
        {
            string guildAbbrev = getUsersGuild(AccountService.GetUsername());

            string guildMembers = getValue(guildAbbrev, "members");
            string guildOfficers = getValue(guildAbbrev, "officers");

            changeValue(guildAbbrev, "members", guildMembers.Replace(user + ",", string.Empty));
            changeValue(guildAbbrev, "officers", guildOfficers.Replace(user + ",", string.Empty));
            DatabaseService.ChangeValue("users", "guild", "", user);
        }

        public static void createGuild(string guildName, string guildAbbrev)
        {
            string commandText = "INSERT INTO guilds(guildname, guildabbrev, guildleader) VALUES('" + guildName + "', '" + guildAbbrev + "', '" + AccountService.GetUsername() + "')";
            DatabaseService.ExecuteCommand(commandText);
            DatabaseService.ChangeValue("users", "guild", guildAbbrev, AccountService.GetUsername());
        }

        public static int getNumberOfGuildMembers(string guildAbbrev)
        {
            return getGuildMembers(guildAbbrev).Length;
        }

        public static int getNumberOfGuildOfficers(string guildAbbrev)
        {
            return getGuildOfficers(guildAbbrev).Length;
        }

        public static int getTotalGuildMembers(string guildAbbrev)
        {
            return getNumberOfGuildOfficers(guildAbbrev) + getNumberOfGuildMembers(guildAbbrev) + 1;
        }

        public static int getNumberOfGuilds()
        {
            using (MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) FROM guilds", DatabaseService.GetMySqlConnection(1)))
            {
                DatabaseService.OpenConnectionIfNotOpen(1);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
        }

        public static string getValue(string guildAbbrev, string valueNeeded)
        {
            return DatabaseService.GetValue("guilds", valueNeeded, "guildabbrev", guildAbbrev);
        }

        public void addNewGuildMember(string guildAbbrev, string newGuildMemberUser)
        {
            DatabaseService.ChangeValue("users", "guild", guildAbbrev, newGuildMemberUser);
            string guildMembers = getValue(guildAbbrev, "members");
            changeValue(guildAbbrev, "members", guildMembers + newGuildMemberUser + ",");
        }

        public void sendGuildInvite(string guildAbbrev, string playerToSendInviteTo)
        {
            // wait to implement (CommandService houses the guildinvite info)
        }

        public static void changeValue(string guildAbbrev, string oldValueColumn, string newValue)
        {
            string commandText = "UPDATE guilds SET " + oldValueColumn + " = '" + newValue + "' WHERE guildabbrev = '" + guildAbbrev + "'";
            DatabaseService.ExecuteCommand(commandText);
        }

        public bool guildCredentialsAlreadyExists(string guildName, string guildAbbrev)
        {
            using (MySqlCommand com = new MySqlCommand("SELECT COUNT(*) FROM guilds WHERE guildname = '" + guildName + "' OR guildabbrev = '" + guildAbbrev + "'", DatabaseService.GetMySqlConnection(1)))
            {
                DatabaseService.OpenConnectionIfNotOpen(1);
                int userExists = int.Parse(com.ExecuteScalar().ToString());

                if (userExists > 0)
                {
                    DatabaseService.GetMySqlConnection(1).Close();
                    return true;
                }
                DatabaseService.GetMySqlConnection(1).Close();
                return false;
            }
        }

        public static bool userIsInAGuild(string username)
        {
            return !string.IsNullOrEmpty(getUsersGuild(username));
        }

        public static string getGuildLeader(string guildAbbrev)
        {
            return getValue(guildAbbrev, "guildleader");
        }

        public static bool userIsGuildLeader(string username)
        {
            return getGuildLeader(getUsersGuild(AccountService.GetUsername())).Equals(AccountService.GetUsername());
        }

        public static void disbandGuild(string guildAbbrev)
        {
            for (byte i = 0; i < getTotalMemberNames(guildAbbrev).Length; i++)
                DatabaseService.ChangeValue("users", "guild", "", getTotalMemberNames(guildAbbrev)[i]);

            DatabaseService.ExecuteCommand("DELETE FROM guilds WHERE guildabbrev='" + guildAbbrev + "'");
        }
    }
}
