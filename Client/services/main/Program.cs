﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Diagnostics;
using System.Windows.Forms;
using TradingCardGame.services.updater;

namespace TradingCardGame
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            UpdaterService updateSrv = new UpdaterService();

            if (ProgramRunning())
                MessageBox.Show("Sorry, but an instance of Trading Card Game is already running.", "Please close all running Trading Card Games");
            else if (!updateSrv.getNewestGameVersion().Equals(Application.ProductVersion))
            {
                DialogResult dr = MessageBox.Show("Client version v" + updateSrv.getNewestGameVersion() + " is available.\n\nWould you like to download it now?", "Update Available", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                    Process.Start("http://www.swghuttcartel.com/forums/misc.php?action=help&hid=14");
                Application.Exit();
            }
            else if (!updateSrv.getMissingFiles().Equals("-1"))
                Application.Exit();
            else
                Application.Run(new forms.screens.Game());
        }

        public static bool ProgramRunning()
        {
            if (Environment.UserName.Equals("press"))
                return false;

            return Process.GetProcessesByName("Star Wars Galaxies TCG").Length > 1;
        }
    }
}
