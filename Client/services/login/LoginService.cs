﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;
using TradingCardGame.services.account;
using TradingCardGame.services.database;
using TradingCardGame.services.encryption;

namespace TradingCardGame.services.login
{
    class LoginService
    {
        public static bool LoginInfoIsValidAndCorrect(string userName, string enteredPassword)
        {
            try
            {
                // check username in forum DB
                using (MySqlCommand com = new MySqlCommand("SELECT * FROM mybb_users WHERE username = '" + userName + "'", DatabaseService.GetMySqlConnection(2)))
                {
                    DatabaseService.OpenConnectionIfNotOpen(2);

                    MySqlDataReader reader = com.ExecuteReader();

                    if (!reader.HasRows)
                        return false;
                    reader.Dispose();
                }
                AccountService.SetUsername(userName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #558: \n" + ex.Message);
                return false;
            }

            try
            {
                // check password in forum DB
                if (EncryptionService.getFullEncodedPassword(enteredPassword).Equals(AccountService.GetRealForumPassword()))
                {
                    makeUserTableIfDoesntExist(userName, enteredPassword);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #559: \n" + ex.Message);
                return false;
            }
        }

        private static void makeUserTableIfDoesntExist(string userName, string enteredPassword)
        {
            using (MySqlCommand com = new MySqlCommand("SELECT * FROM users WHERE username = '" + userName + "'", DatabaseService.GetMySqlConnection(1)))
            {
                DatabaseService.OpenConnectionIfNotOpen(1);

                if (!com.ExecuteReader().HasRows)
                    AccountService.CreateAccountFiles();
            }
        }

        public static string getServerConfigValue(string valueNeeded)
        {
            return DatabaseService.GetValue("config", valueNeeded, "server", "tcg");
        }
    }
}
