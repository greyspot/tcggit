﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using TradingCardGame.services.modifers;

namespace TradingCardGame.services.combat
{
    class CombatService
    {
        ModifierService modSrv = new ModifierService();

        Random random = new Random();

        private static string enemy;

        public static void setEnemyName(string name)
        {
            enemy = name;
        }

        public static string getEnemyName()
        {
            return enemy;
        }

        public int actionRegen()
        {
            return (modSrv.getTotalActionOrHealth("action") / 25) * modSrv.actionRegenBonus();
        }

        public int getNumberOfCardsInCardSet()
        {
            return starterDeck.Length;
        }

        string[] starterDeck = { "Imperial Cadet", "Imperial Security", "Imperial Stormtrooper" };

        public string drawCard(string cardSet)
        {
            int randomNumber = random.Next(0, getNumberOfCardsInCardSet());

            if (cardSet.Equals(Properties.Settings.Default.cardSetSelected))
                return starterDeck[randomNumber];
            return null;
        }

        public string[] cardsDrawn()
        {
            string cardSet = Properties.Settings.Default.cardSetSelected;
            string[] cards = { "* You draw:", drawCard(cardSet), drawCard(cardSet), drawCard(cardSet), drawCard(cardSet), drawCard(cardSet), "and " + drawCard(cardSet) };
            return cards;
        }
    }
}
