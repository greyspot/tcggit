﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows.Forms;
using TradingCardGame.services.account;
using TradingCardGame.services.cards;

namespace TradingCardGame.services.inventory
{
    class InventoryService
    {
        public string getInventorySlotItem(int inventorySlot)
        {/*
            try
            {
                using (MySqlConnection cn = new MySqlConnection(info))
                {
                    string sqlCommand = "SELECT slot" + inventorySlot + " FROM inventory WHERE username = '" + AccountService.getUsername() + "'";
                    MySqlCommand cmd = new MySqlCommand(sqlCommand, cn);
                    cn.Open();
                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        return reader.GetString("slot" + inventorySlot);
                    }
                    cmd.Dispose();
                }
            }
            catch
            {}
            */
            return null;
        }

        public int getQuantityOfSpecificCard(string cardName)
        {
            string cardAmount = database.DatabaseService.GetValue("inventory", cardName, "username", AccountService.GetUsername());

            if (string.IsNullOrEmpty(cardAmount))
                cardAmount = "0";

            return int.Parse(cardAmount);
        }

        public bool modifierCardsContains(string cardName)
        {
            return false;
        }

        public void moveInventoryCard(int slotNumberRemove, int slotNumberAdd)
        {

        }

        public void removeInventoryCard(int slotNumberRemove)
        {

        }

        public int getAvailableInventorySlot()
        {
            for (byte i = 1; i <= 60; i++)
            {
                if (string.IsNullOrEmpty(getInventorySlotItem(i)))
                    return i;
            }
            MessageBox.Show("UNABLE TO RETRIEVE CARD: INVENTORY FULL");
            return 0;
        }

        public void addCardToInventory(string newItem)
        {

        }

        public int getNumberOfCardsInInventory()
        {
            int total = 0;

            for (byte i = 0; i < COTF.lootCards.Length - 1; i++)
                total += int.Parse(database.DatabaseService.GetValue("inventory", COTF.lootCards[i], "username", AccountService.GetUsername()));

            return total;
        }
    }
}
