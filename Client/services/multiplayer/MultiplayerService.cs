﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using TradingCardGame.services.account;
using TradingCardGame.services.database;

namespace TradingCardGame.services.multiplayer
{
    class MultiplayerService
    {

        private static readonly int CASUAL_CAPACITY_MOS_EISLEY = 6;

        public static string getLobbyPositionValue(int position)
        {
            return "0";
        }

        public static string[] getAllPlayersNamesInLobby()
        {
            List<string> players = new List<string>();

            for (byte i = 0; i < CASUAL_CAPACITY_MOS_EISLEY; i++)
            {
                if (!string.IsNullOrEmpty(getLobbyPositionValue(i)))
                    players.Add(getLobbyPositionValue(i));
            }
            return players.ToArray();
        }

        public static int getNumberOfMatches()
        {
            return DatabaseService.GetNumberOfTableRows("matches");
        }

        public static int findEmptyLobbySpot()
        {
            for (byte i = 0; i < CASUAL_CAPACITY_MOS_EISLEY; i++)
            {
                if (string.IsNullOrEmpty(getLobbyPositionValue(i)))
                    return i;
            }
            return -1;
        }

        public static int getCurrentLobbyPosition()
        {
            for (byte i = 0; i < CASUAL_CAPACITY_MOS_EISLEY; i++)
            {
                if (getLobbyPositionValue(i).Equals(AccountService.GetUsername()))
                    return i;
            }
            return -1;
        }

        public static bool casualLobbyIsOnline()
        {
            if (DatabaseService.GetValue("config", "casuallobbystatus", "server", "tcg").Equals("1"))
                return true;
            return false;
        }

        public static void createMatch(string lobby, string title, string creator, string matchType, string numberOfPlayers, string playFormat, string matchStructure, string timeLimit, string password, string guildOnly, string allowObservers, string friendsOnly, string lightVsDark)
        {
            Random matchId = new Random();
            string commandText = "INSERT INTO matches(title,id,lobby,creator,matchtype,numberofplayers,playformat,matchstructure,timelimit,password,guildonly,allowobservers,friendsonly,lightvsdark) VALUES('" + title + "', '" + matchId.Next(1000, 10000) + "', '" + lobby + "', '"  + creator +"', '"+ matchType + "', '" + numberOfPlayers + "', '" + playFormat + "', '" + matchStructure + "', '" + timeLimit + "', '" + password + "', '" + guildOnly + "', '" + allowObservers + "', '" + friendsOnly + "', '" + lightVsDark + "')";
            DatabaseService.ExecuteCommand(commandText);
        }

        public static string[] getAllMatchTitles()
        {
            int numberOfMatches = getNumberOfMatches();
            string[] allGameTitles = new string[numberOfMatches];

            string query = "SELECT * FROM matches";
            MySqlCommand cmd = new MySqlCommand(query, DatabaseService.GetMySqlConnection(1));

            if (DatabaseService.GetMySqlConnection(1).State == ConnectionState.Closed)
                DatabaseService.OpenConnectionIfNotOpen(1);

            MySqlDataReader dataReader = cmd.ExecuteReader();
            
            for (byte i = 0; i < numberOfMatches; i++)
            {
                while (dataReader.Read())
                    allGameTitles[i] = dataReader["title"].ToString();
            }

            dataReader.Close();
            cmd.Dispose();
            return allGameTitles;
        }

        public void joinGame()
        {

        }

        public int getCurrentGames()
        {
            return 0;
        }

        public int getOnlinePlayers()
        {
            return 1;
        }
    }
}
