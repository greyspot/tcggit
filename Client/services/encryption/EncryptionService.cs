﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Security.Cryptography;
using System.Text;
using TradingCardGame.services.account;

namespace TradingCardGame.services.encryption
{
    class EncryptionService
    {
        public static string getFullEncodedPassword(string password)
        {
            return MD5(getEncodedSalt() + getEncodedPassword(password));
        }

        private static string MD5(string input)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] data = md5.ComputeHash(Encoding.ASCII.GetBytes(input));
            StringBuilder str = new StringBuilder();

            for (byte i = 0; i < data.Length; i++)
                str.Append(data[i].ToString("x2"));

            return str.ToString();
        }

        private static string getForumAccountSalt()
        {
            return database.DatabaseService.GetValue("mybb_users", "salt", "username", AccountService.GetUsername());
        }

        private static string getEncodedSalt()
        {
            return MD5(getForumAccountSalt());
        }

        private static string getEncodedPassword(string password)
        {
            return MD5(password);
        }
    }
}
