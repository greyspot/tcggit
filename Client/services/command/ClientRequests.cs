﻿namespace TradingCardGame.services.command
{
    class ClientRequests
    {
        private static bool loginIsCorrect;
        private static bool userIsBanned;
        private static bool userIsRegistered;

        public static void SetLoginIsCorrect(bool loginStatus)
        {
            loginIsCorrect = loginStatus;
        }

        public static bool GetLogInIsCorrect()
        {
            return loginIsCorrect;
        }

        public static void SetUserIsBanned(bool bannedStatus)
        {
            userIsBanned = bannedStatus;
        }

        public static bool GetUserIsBanned()
        {
            return userIsBanned;
        }

        public static void SetUserIsRegistered(bool registeredStatus)
        {
            userIsRegistered = registeredStatus;
        }

        public static bool GetUserIsRegistered()
        {
            return userIsRegistered;
        }
    }
}
