﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;
using TradingCardGame.services.account;
using TradingCardGame.services.friends;
using TradingCardGame.services.guild;
using TradingCardGame.services.packet;

namespace TradingCardGame.services.command
{
    class CommandService
    {
        private static readonly string[] COMMANDS_PLAYER = { "trade", "time", "me", "ignore", "unignore", "sayto",
            "whisper", "addfriend", "removefriend", "ping", "joingame", "observegame", "resume",
        "help", "clearchat" };
        private static readonly string[] COMMANDS_GUILD = { "guildinvite", "leaveguild", "guildkick" };
        private static readonly string[] COMMANDS_STAFF = { "broadcast", "kickplayer" };
        private static readonly string[] COMMANDS_ADMIN = { "banplayer", "shutdownserver" };
        public static readonly string[] COMMANDS_SYSTEM = { "GetValue", "SetValue" };

        // player command bytes
        public readonly byte COMMAND_TRADE_PLAYER = 100;
        public static readonly byte COMMAND_TIME = 101;
        public static readonly byte COMMAND_ME = 102;
        public readonly byte COMMAND_IGNORE_PLAYER = 103;
        public readonly byte COMMAND_UNIGNORE_PLAYER = 104;
        public readonly byte COMMAND_SAYTO = 105;
        public static readonly byte COMMAND_WHISPER = 106;
        public readonly byte COMMAND_ADD_FRIEND = 107;
        public readonly byte COMMAND_REMOVE_FRIEND = 108;
        public readonly byte COMMAND_PING_PLAYER = 109;
        public readonly byte COMMAND_JOIN_GAME = 110;
        public readonly byte COMMAND_OBSERVE_GAME = 111;
        public readonly byte COMMAND_RESUME = 112;
        public readonly byte COMMAND_HELP = 113;
        public readonly byte COMMAND_CLEAR_CHAT = 114;

        // guild command bytes
        public static readonly byte COMMAND_GUILD_INVITE = 120;
        public readonly byte COMMAND_LEAVE_GUILD = 121;
        public readonly byte COMMAND_GUILD_KICK = 122;

        // staff command bytes
        public static readonly byte COMMAND_BROADCAST_MESSAGE = 130;

        // admin command bytes
        public readonly byte COMMAND_BAN_PLAYER = 150;
        public static readonly byte COMMAND_KICK_PLAYER = 151;
        public readonly byte COMMAND_SHUTDOWN_SERVER = 152;

        // internal command bytes
        public static readonly byte COMMAND_UPDATE_AVAILABLE_MATCHES = 170;
        public static readonly byte COMMAND_SEND_CHAT_PRIVATE = 171;

        private static ListBox chatBox;

        public static void command(string command, ListBox listbox)
        {
            chatBox = listbox;

            if (!commandExists(command))
                return;

            //player commands

            if (command.Equals(COMMANDS_PLAYER[0]))
            {
                tradeCommand();
                return;
            }
            else if (command.Equals(COMMANDS_PLAYER[1]))
            {
                timeCommand();
                return;
            }
            else if (command.Equals("clearchat"))
            {
                clearChatcommand();
                return;
            }
            else if (command.StartsWith("addfriend"))
            {
                addFriendCommand(command);
                return;
            }
            else if (command.StartsWith("removefriend"))
            {
                removeFriendCommand(command);
                return;
            }
            else if (command.StartsWith("whisper"))
            {
                whisperCommand(command);
                return;
            }
            else if (command.StartsWith("me"))
            {
                if (command.Length < 3)
                    chatBox.Items.Add("Please type arguments after '/me'.");
                else
                    meCommand(chatBox, command.Substring(3));
                return;
            }
            else if (command.StartsWith(COMMANDS_GUILD[0]))
            {
                sendGuildInviteCommand(command);
                return;
            }
            else if (command.StartsWith(COMMANDS_GUILD[1]))
            {
                leaveGuildCommand(chatBox, command);
                return;
            }
            else if (command.StartsWith(COMMANDS_GUILD[2]))
            {
                guildKickCommand(chatBox, command);
                return;
            }

            // staff commands

            if (command.StartsWith(COMMANDS_STAFF[0]))
            {
                broadcastCommand(command.Substring(9));
                return;
            }
            else if (command.StartsWith(COMMANDS_STAFF[1]))
            {
                kickPlayerCommand(command.Substring(10));
                return;
            }

            // admin commands

            if (command.StartsWith(COMMANDS_ADMIN[0]))
            {
                // ban player command
            }
            else if (command.StartsWith(COMMANDS_ADMIN[1]))
            {
                // shutdown server command
            }
        }

        private static bool commandExists(string command)
        {
            for (byte i = 0; i < COMMANDS_PLAYER.Length; i++)
            {
                if (command.StartsWith(COMMANDS_PLAYER[i]))
                    return true;
            }

            for (byte i = 0; i < COMMANDS_GUILD.Length; i++)
            {
                if (command.StartsWith(COMMANDS_GUILD[i]))
                    return true;
            }

            string accountValue = database.DatabaseService.GetValue("mybb_users", "usergroup", "username", AccountService.GetUsername());

            if (AccountService.GetUsername().Equals(AccountService.GetUsernameWithTitle(AccountService.GetUsername())))
            {
                for (byte i = 0; i < COMMANDS_STAFF.Length; i++)
                {
                    if (command.StartsWith(COMMANDS_STAFF[i]))
                    {
                        chatBox.Items.Add("Sorry, but you do not have the permissions to use the command '" + command.Split(' ')[0] + "'.");
                        return false;
                    }
                }
            }

            // regular staff commands
            for (byte i = 0; i < COMMANDS_STAFF.Length; i++)
            {
                if (command.StartsWith(COMMANDS_STAFF[i]))
                    return true;
            }

            if (!accountValue.Equals("4"))
            {
                for (byte i = 0; i < COMMANDS_ADMIN.Length; i++)
                {
                    if (command.StartsWith(COMMANDS_ADMIN[i]))
                    {
                        chatBox.Items.Add("Sorry, but you do not have the permissions to use the command '" + command.Split(' ')[0] + "'.");
                        return false;
                    }
                }
            }

            // admin commands
            for (byte i = 0; i < COMMANDS_ADMIN.Length; i++)
            {
                if (command.StartsWith(COMMANDS_ADMIN[i]))
                    return true;
            }

            chatBox.Items.Add("The command '" + command + "' does not exist.");
            return false;
        }

        private static void guildKickCommand(ListBox chatBox, string command)
        {
            if (!GuildService.userIsInAGuild(AccountService.GetUsername()))
            {
                chatBox.Items.Add("You are currently not in a guild.");
                return;
            }

            string user = command.Substring(10);

            if (AccountService.GetUsername().Equals(user))
            {
                chatBox.Items.Add("You cannot kick yourself.");
                return;
            }

            if (!AccountService.UsernameExists(user))
            {
                chatBox.Items.Add("The user " + user + " doesn't exist.");
                return;
            }

            if (!GuildService.getUsersGuild(user).Equals(GuildService.getUsersGuild(AccountService.GetUsername())))
            {
                chatBox.Items.Add("The user " + user + " isn't in the guild.");
                return;
            }

            // TODO: Implement kick permissions

            GuildService.removeUserFromGuild(user);
            chatBox.Items.Add("The user " + user + " has been kicked from the guild.");
        }

        private static void clearChatcommand()
        {
            chatBox.Items.Clear();
            chatBox.Items.Add("Chat cleared.");
        }

        private static void meCommand(ListBox chatBox, string emote)
        {
            PacketService.SendPacket(ServerData.PacketType.ClientCommand, COMMAND_ME + AccountService.GetUsernameWithTitle(AccountService.GetUsername()) + " " + emote);
        }

        private static void broadcastCommand(string message)
        {
            PacketService.SendPacket(ServerData.PacketType.ClientCommand, COMMAND_BROADCAST_MESSAGE + message);
        }

        private static void kickPlayerCommand(string playerToKick)
        {
            if (!string.IsNullOrEmpty(playerToKick))
                PacketService.SendPacket(ServerData.PacketType.ClientCommand, COMMAND_KICK_PLAYER + playerToKick);
            else
                MessageBox.Show("Please enter a valid player name.");
        }

        private static void tradeCommand()
        {
            
        }

        private static void timeCommand()
        {
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, easternZone);
            chatBox.Items.Add("Server Time: " + easternTime);
        }

        private static void sendGuildInviteCommand(string command)
        {
            PacketService.SendPacket(ServerData.PacketType.ClientCommand, COMMAND_GUILD_INVITE + command.Substring(12) + GuildService.getUsersGuild(AccountService.GetUsername()));
        }

        private static void leaveGuildCommand(ListBox chatBox, string command)
        {
            if (!GuildService.userIsInAGuild(AccountService.GetUsername()))
            {
                chatBox.Items.Add("You currently are not in a guild to leave.");
                return;
            }

            if (GuildService.userIsGuildLeader(AccountService.GetUsername()))
            {
                DialogResult dr = MessageBox.Show("You are the guild leader of the guild, if you leave it will disband." +
                    " Are you sure you want to leave?", "Disband Confirmation", MessageBoxButtons.YesNo);

                if (dr == DialogResult.No)
                    return;
                else if (dr == DialogResult.Yes)
                {
                    GuildService.disbandGuild(GuildService.getUsersGuild(AccountService.GetUsername()));
                    chatBox.Items.Add("The guild has been disbanded.");
                    return;
                }
            }

            GuildService.removeUserFromGuild(AccountService.GetUsername());
            chatBox.Items.Add("You have left the guild.");
        }

        private static void whisperCommand(string command)
        {
            string message = command.Replace("whisper", "");
            string reciever = null;

            if (string.IsNullOrEmpty(message))
                chatBox.Items.Add("Please enter a valid message.");
            else
                reciever = command.Split(' ')[1];

            message = message.Replace(reciever, "").Substring(2);

            chatBox.Items.Add("You whisper to " + reciever + ", \"" + message + "\"");
            PacketService.SendPacket(ServerData.PacketType.ClientCommand, COMMAND_WHISPER + reciever + " " + message);
        }

        private static void addFriendCommand(string command)
        {
            string friend = command.Substring(10);

            if (friend.Equals(AccountService.GetUsername()))
            {
                chatBox.Items.Add("You cannot add yourself as a friend.");
                return;
            }

            if (!FriendsListService.userExists(friend))
            {
                chatBox.Items.Add(friend + " is not a valid username");
                return;
            }

            if (FriendsListService.userIsInFriendsList(friend))
            {
                chatBox.Items.Add(friend + " is already a friend.");
                return;
            }

            friend = database.DatabaseService.GetValue("users", "username", "username", friend);

            database.DatabaseService.ChangeValue("users", "friendslist", FriendsListService.getFriendsListValue() + friend + ",", AccountService.GetUsername());
            chatBox.Items.Add(friend + " has been added to your friends list.");
        }

        private static void removeFriendCommand(string command)
        {
            string friend = command.Substring(13);

            if (!FriendsListService.userIsInFriendsList(friend))
            {
                chatBox.Items.Add(friend + " is not on your friends list.");
                return;
            }

            string newFriendsList = FriendsListService.getFriendsListValue().Replace(friend + ",", "");
            string sqlCommand = "UPDATE users SET friendslist ='" + newFriendsList + "' WHERE username ='" + AccountService.GetUsername() + "'";
            database.DatabaseService.ExecuteCommand(sqlCommand);
            chatBox.Items.Add(friend + " has been removed from your friends list.");
        }

        public static void exitGame()
        {
            DialogResult dr = MessageBox.Show("Are you sure that you want to exit?", "Exit Confirmation", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
                Application.Exit();
        }
    }
}
