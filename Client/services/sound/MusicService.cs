﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace TradingCardGame.services.sound
{
    class MusicService
    {
        private static int index;

        public static void playMusic()
        {
            NAudio.Wave.WaveFileReader wave = null;

            if (index == 0)
            {
                index = 1;
                wave = new NAudio.Wave.WaveFileReader(Sounds.music.music_lobby_01);
            }
            else
            {
                index = 0;
                wave = new NAudio.Wave.WaveFileReader(Sounds.music.music_lobby_02);
            }
            
            NAudio.Wave.DirectSoundOut output = new NAudio.Wave.DirectSoundOut();
            output.PlaybackStopped += Output_PlaybackStopped;
            output.Init(new NAudio.Wave.WaveChannel32(wave));
            output.Play();
        }

        private static void Output_PlaybackStopped(object sender, NAudio.Wave.StoppedEventArgs e)
        {
            playMusic();
        }
    }
}
