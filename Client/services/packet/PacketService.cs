﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ServerData;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using TradingCardGame.forms;
using TradingCardGame.services.account;
using TradingCardGame.services.command;
using TradingCardGame.services.data;
using TradingCardGame.services.guild;

namespace TradingCardGame.services.packet
{
    class PacketService
    {
        private static Socket socket;
        private static IPAddress ipAddress;
        private static string ID;
        private static string login;
        private static Thread thread;
        private static bool isConnected;
        private static readonly string serverIP = "45.37.80.181";
        private static readonly int serverPort = 4242;

        private static Label usersLabel;
        private static ListBox chatBox;
        private static ListView playersNameListBox;

        public static void setComponents(Label label, ListBox listbox, ListView listview)
        {
            usersLabel = label;
            chatBox = listbox;
            playersNameListBox = listview;
        }

        public static bool GetIsConnected()
        {
            return isConnected;
        }

        public static void disconnectFromServer()
        {
            if (isConnected)
            {
                Packet p = new Packet(PacketType.ClientDisconnect, ID);
                p.data.Add(login);
                socket.Send(p.ToBytes());
                socket.Close();
                isConnected = false;
                thread.Abort();
            }
        }

        public static void connectToServer()
        {
            try
            {
                if (!IPAddress.TryParse(serverIP, out ipAddress))
                {
                    MessageBox.Show("IP Address is not valid!");
                    Application.Exit();
                }

                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, serverPort);

                socket.Connect(ipEndPoint);
                login = AccountService.GetUsernameWithTitle(AccountService.GetUsername());

                isConnected = true;

                thread = new Thread(Data_IN);
                thread.Start();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("Value cannot be null.\r\nParameter name: address"))
                {
                    MessageBox.Show("The game server is currently offline, please try again later.", "Server Offline");
                    Application.Exit();
                }
                MessageBox.Show(ex.Message);
            }
        }

        private static void Data_IN()
        {
            byte[] buffer;
            int readBytes;

            while (true)
            {
                try
                {
                    buffer = new byte[socket.SendBufferSize];
                    readBytes = socket.Receive(buffer);

                    if (readBytes > 0)
                        DataManager(new Packet(buffer));
                }
                catch (SocketException)
                {
                    //MessageBox.Show("Connection to the server has been lost!");
                }
            }
        }

        private static void DataManager(Packet p)
        {
            switch (p.packetType)
            {
                case PacketType.ClientChat:
                    DealWithChatPacket(p.data[1], p.data[0]);
                    break;

                case PacketType.ClientCommand:
                    DealWithCommandPacket(p.data[1], p.data[0]);
                    break;

                case PacketType.ClientConnect:
                    ID = p.data[0];
                    Packet packet = new Packet(PacketType.ClientConnect, ID);
                    packet.data.Add(login);
                    socket.Send(packet.ToBytes());
                    break;
                case PacketType.IncomingInfo:
                    DealWithIncomingInfoPacket(p.data[0], p.data[1]);
                    break;

            }
        }

        private delegate void DealWithPacketDelegate(string item1, string item2);

        private static void DealWithIncomingInfoPacket(string msg, string args)
        {
            if (usersLabel == null || playersNameListBox == null)
                return;

            if (msg.Equals("UPDATE_LOBBY"))
            {
                TemporaryData.SetPlayersInLobby(args.Split(',').ToList());

                // Update usersLabel text
                if (usersLabel.InvokeRequired)
                    usersLabel.Invoke(new DealWithPacketDelegate(DealWithIncomingInfoPacket), msg, args);
                else
                    usersLabel.Text = "Users: " + TemporaryData.GetPlayersInLobby().Count + " / 6";

                // Update playersNameListBox
                if (playersNameListBox.InvokeRequired)
                    playersNameListBox.Invoke(new DealWithPacketDelegate(DealWithIncomingInfoPacket), msg, args);
                else
                {
                    if (playersNameListBox.InvokeRequired)
                        playersNameListBox.Invoke(new DealWithPacketDelegate(DealWithIncomingInfoPacket), msg, args);
                    else
                    {
                        playersNameListBox.Items.Clear();

                        for (byte i = 0; i < TemporaryData.GetPlayersInLobby().Count; i++)
                            playersNameListBox.Items.Add(TemporaryData.GetPlayersInLobby()[i], "bronze");
                    }
                }
            }
        }

        private static void DealWithChatPacket(string msg, string user)
        {
            if (chatBox == null)
                return;

            if (!msg.Equals(" joined the lobby.") && !msg.Equals( " left the lobby."))
            {
                if (chatBox.InvokeRequired)
                    chatBox.Invoke(new DealWithPacketDelegate(DealWithChatPacket), msg, user);
                else
                {
                    chatBox.Items.Add(user + ": " + msg);
                    chatBox.TopIndex = chatBox.Items.Count - 1;
                }
            }
            else if (!chatBox.Items.Contains(user + " joined the lobby.") || !user.Equals(login))
            {
                if (chatBox.InvokeRequired)
                    chatBox.Invoke(new DealWithPacketDelegate(DealWithChatPacket), msg, user);
                else
                {
                    chatBox.Items.Add(user + msg);
                    chatBox.TopIndex = chatBox.Items.Count - 1;
                }   
            }
        }
        private static void DealWithCommandPacket(string msg, string user)
        {
            if (msg.Contains(CommandService.COMMAND_SEND_CHAT_PRIVATE.ToString()))
                return;

            string stringTogether = user + msg;

            if (msg.Equals(CommandService.COMMAND_UPDATE_AVAILABLE_MATCHES.ToString()))
            {
                Lobby.LoadAvailableGames();
                return;
            }

            if (msg.StartsWith(CommandService.COMMAND_BROADCAST_MESSAGE.ToString()))
            {
                MessageBox.Show(msg.Substring(3), "SERVER BROADCAST");
                return;
            }
            else if (msg.StartsWith(CommandService.COMMAND_KICK_PLAYER.ToString()))
            {
                if (msg.Substring(4).ToLower().Equals(AccountService.GetUsername().ToLower()))
                {
                    if (chatBox.InvokeRequired)
                        chatBox.Invoke(new DealWithPacketDelegate(DealWithCommandPacket), msg, user);
                    else
                        chatBox.Items.Add("You have been kicked from the server by an administrator.");
                    MessageBox.Show("You have been kicked from the server by an administrator.");
                    Application.Exit();
                }
                return;
            }
            else if (msg.StartsWith(CommandService.COMMAND_WHISPER.ToString()))
            {
                if (chatBox.InvokeRequired)
                    chatBox.Invoke(new DealWithPacketDelegate(DealWithCommandPacket), msg, user);
                else
                {
                    msg = msg.Substring(3);

                    string reciever = msg.Split(' ')[0];

                    if (reciever.Equals(AccountService.GetUsername()))
                        chatBox.Items.Add(user + " whispers to you, \"" + msg.Replace(reciever, "") + "\"");
                }
                return;
            }
            else if (msg.StartsWith(CommandService.COMMAND_GUILD_INVITE.ToString()))
            {
                if (msg.Contains(AccountService.GetUsername()))
                {
                    string guildAbbrev = msg.Replace(CommandService.COMMAND_GUILD_INVITE.ToString(), "").Replace(AccountService.GetUsername(), "");
                    DialogResult dr = MessageBox.Show("You have been invited to the guild <" + guildAbbrev + ">, would you like to join?", "Guild Invitation", MessageBoxButtons.YesNo);

                    if (dr == DialogResult.Yes)
                    {
                        GuildService guildSrv = new GuildService();
                        guildSrv.addNewGuildMember(guildAbbrev, AccountService.GetUsername());
                        MessageBox.Show("You have accepted the guild invite to <" + guildAbbrev + ">.");
                    }
                    else
                        MessageBox.Show("You have declined the guild invite to <" + guildAbbrev + ">.");
                }
                return;
            }

            if (chatBox.InvokeRequired)
                chatBox.Invoke(new DealWithPacketDelegate(DealWithCommandPacket), msg, user);
            else if (!chatBox.Items.Contains(user + " joined the lobby.") || !stringTogether.Equals(user + " joined the lobby."))
            {
                if (msg.StartsWith(CommandService.COMMAND_ME.ToString()))
                {
                    chatBox.Items.Add(msg.Substring(3));
                    return;
                }
                chatBox.Items.Add(stringTogether);
                chatBox.TopIndex = chatBox.Items.Count - 1;
            }
        }

        public static void SendPacket(PacketType packetType, string packetToSend)
        {
            Packet p = new Packet(packetType, ID);
            p.data.Add(login);
            p.data.Add(packetToSend);
            socket.Send(p.ToBytes());
        }
    }
}
