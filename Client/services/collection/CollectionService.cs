﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TradingCardGame.services.cards;

namespace TradingCardGame.services.collection
{
    class CollectionService
    {
        public static string[] getLootCardsForPage(int page)
        {
            string[] pageCards = new string[10];

            int startingValue = 0;
            int endingValue = 0;

            if (page == 1)
            {
                startingValue = 0;
                endingValue = 10;
            }
            else if (page == 2)
            {
                startingValue = 10;
                endingValue = 10;
            }
            else if (page == 3)
            {
                startingValue = 20;
                endingValue = 5;
            }

            for (byte i = 0; i < endingValue; i++)
                pageCards[i] = COTF.lootCards[i + startingValue];

            return pageCards;
        }
    }
}
