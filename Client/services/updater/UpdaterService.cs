﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.IO;
using System.Windows.Forms;

namespace TradingCardGame.services.updater
{
    class UpdaterService
    {
        private readonly string[] requiredFiles = { "MySql.Data.dll", "ServerData.dll" };

        public string getMissingFiles()
        {
            for (byte i = 0; i < requiredFiles.Length; i++)
            {
                string missingfiles = null;
                if (!File.Exists(Application.StartupPath + @"\" + requiredFiles[i]))
                {
                    missingfiles += requiredFiles[i] + "\n";
                    MessageBox.Show("Missing Required File(s):\n" + missingfiles);
                    return requiredFiles[i];
                }
            }
            return "-1";
        }

        public string getNewestGameVersion()
        {
            return database.DatabaseService.GetValue("config", "version", "server", "tcg");
        }
    }
}
