﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TradingCardGame.services.account;

namespace TradingCardGame.services.friends
{
    class FriendsListService
    {
        public static bool userIsInFriendsList(string user)
        {
            if (string.IsNullOrEmpty(user))
                return false;

            for (byte i = 0; i < getFriendsListValue().Split(',').Length; i++)
            {
                if (getFriendsListValue().Split(',')[i].ToLower().Equals(user.ToLower()))
                    return true;
            }
            return false;
        }

        public static bool userExists(string user)
        {
            return !string.IsNullOrEmpty(database.DatabaseService.GetValue("users", "username", "username", user));
        }

        public static string getFriendsListValue()
        {
            return database.DatabaseService.GetValue("users", "friendslist", "username", AccountService.GetUsername());
        }
    }
}
